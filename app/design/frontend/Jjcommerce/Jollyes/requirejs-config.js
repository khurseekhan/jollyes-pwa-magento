var config = {
    map: {
        "*": {
            flexslider: 'js/jquery.flexslider.min'
        }
    },
    config: {
        mixins: {
            'Klevu_Search/js/view/personal-data': {
                'js/personal-data-mixin': true
            }
        }
    },
    deps: [
        'js/sticky-header',
        'js/custom'
    ]
};