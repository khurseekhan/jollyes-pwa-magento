/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

var config = {
    map: {
        '*': {
            'slick': 'Magento_Cms/js/resource/slick/slick',
            'jarallax': 'Magento_Cms/js/resource/jarallax/jarallax'
        }
    },
    shim: {
        'slick': {
            deps: ['jquery']
        }
    }
};
