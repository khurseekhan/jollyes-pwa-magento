require([
    "jquery",
    "jquery/validate",
    "flexslider"

],function($) {
    var JjdesignTheme = {};
    JjdesignTheme.responsive = true;
    JjdesignTheme.maxBreak = 1400;


    var islider = {
        config: {
            elements: ".itemslider-responsive",
            columnCount: 5,
            maxBreakpoint: 960,
            breakpoints: [[1680, 3], [1440, 2], [1360, 1], [1280, 1], [960, 0], [768, -1], [640, -2], [480, -3], [320, -5]]
        }, init: function (a) {
            $.extend(islider.config, a)
        }, onResize_recalculateAllSliders: function () {
            return islider.recalculateAllSliders(), !1
        }, recalculateAllSliders: function () {
            $(islider.config.elements).each(function () {
                null != $(this).data("flexslider") && islider.recalcElement($(this))
            })
        }, recalcElement: function (a) {
            var b, c = a.data("breakpoints");
            if (c)
                b = islider.getMaxItems_CustomBreakpoints(c);
            else {
                var d = a.data("showItems");
                void 0 === d && (d = islider.config.columnCount), b = islider.getMaxItems(d)
            }
            a.data("flexslider").setOpts({minItems: b, maxItems: b})
        }, getMaxItems_CustomBreakpoints: function (a) {
            if (JjdesignTheme.viewportW)
                var b = JjdesignTheme.viewportW;
            else
                var b = $(window).width();
            var c = islider.config.maxBreakpoint;
            "undefined" != typeof JjdesignTheme && JjdesignTheme.maxBreak && (c = JjdesignTheme.maxBreak);
            for (var d, e = 0; a.length > e; e++) {
                var f = parseInt(a[e][0], 10), g = parseInt(a[e][1], 10);
                if (d = g, c >= f && b >= f)
                    return d
            }
            return d
        }, getMaxItems: function (a) {
            var b = islider.config.breakpoints;
            if (JjdesignTheme.viewportW)
                var c = JjdesignTheme.viewportW;
            else
                var c = $(window).width();
            var d = islider.config.maxBreakpoint;
            "undefined" != typeof JjdesignTheme && JjdesignTheme.maxBreak && (d = JjdesignTheme.maxBreak);
            for (var e, f = 0; b.length > f; f++) {
                var g = parseInt(b[f][0], 10), h = parseInt(b[f][1], 10);
                if (e = a + h, 0 >= e && (e = 1), d >= g && c >= g)
                    return e
            }
            return e
        }
    };

    if (typeof JjdesignTheme !== 'undefined' && JjdesignTheme.responsive) {
        islider.init({elements: '.itemslider-responsive'});
        islider.recalculateAllSliders();
        $(document).on("themeResize", islider.onResize_recalculateAllSliders);
    }


    var windowResize_t;
    $(window).resize(function () {
        clearTimeout(windowResize_t);
        windowResize_t = setTimeout(function () {
            $(document).trigger("themeResize");
        }, 100);
    });

    $(document).ready(function () {

        // Lazy load for mega menu 

        var imgValue = $('.sections.nav-sections li .product-image-photo');
            $.each(imgValue, function(value, key) {
            var imgSrcset = $(this).attr('data-src');
            $(this).attr('src',imgSrcset);
        });

        /***   for footer collapsible   ***/
        $('.collapsible').each(function (index) {
            if ($(this).hasClass('active')) {
                $(this).children('.col-block').css('display', 'block');
            } else {
                $(this).children('.col-block').css('display', 'none');
            }
        });

        $('.collapsible .col-title').click(function () {
            //if($(window).width()<959) {
            var parent = $(this).parent();
            if (parent.hasClass('active')) {
                $(this).siblings('.col-block').stop(true).slideUp(500, "easeInOutSine");
                parent.removeClass('active');
            } else {
                $('.collapsible.mobile-collapsible').removeClass('active');
                $('.collapsible.mobile-collapsible').find('.col-block').stop(true).slideUp(500, "easeInOutSine");
                $(this).siblings('.col-block').stop(true).slideDown(500, "easeInOutSine");
                parent.addClass('active');
            }
            //}
        });

        $('#spnTop').on("click", function () {
            $('html,body').animate({scrollTop: 0}, 'slow');
        });
        if ($("div.category-cms").children().hasClass("banner-block-image")) {
            $("div.banner-block").addClass("category-banner-image");
        }
        if ($("div.category-cms").children().hasClass("category")) {
            $("div.banner-block").addClass("category-banner-none");
            $("body").addClass("category-banner-show");
        }
        if ($(".columns").find(".toolbar").length > 0) {
            $("body").addClass("scroll-button-show");
        }
        mediaCheck({
            media: '(max-width: 767px)',
            entry: function () {
                $("#newsletter").attr("placeholder", "Sign up to our newsletter");
            },
            exit: function () {
                $("#newsletter").attr("placeholder", "Enter your email address to sign up to our newsletter");
            }
        });

        var quantityIncrease = function (e) {
            e.preventDefault();
            element = $(this);
            val = element.parent().children('input.qty').val();
            if (parseInt(val) < 999) {
                val = parseInt(val) + 1;
                element.parent().children('input.qty').val(val);
            }
        };

        var quantityDecrease = function (e) {
            e.preventDefault();
            element = $(this);
            val = element.parent().children('input.qty').val();
            if (parseInt(val) > 1) {
                val = parseInt(val) - 1;
                element.parent().children('input.qty').val(val);
            }
        };

        $(".product-item a.product-list-qty-plus").on("click", quantityIncrease);
        $(".product-item a.product-list-qty-minus").on("click", quantityDecrease);
        $(".product-add-form .qty").on("keyup", function (e) {
            var key = e.charCode ? e.charCode : e.keyCode;
            if (e.keyCode === 190 || e.keyCode == 110) // dot
            {
                $(this).val(1);

            } else if ((e.keyCode === 189 || e.keyCode === 109) || (e.keyCode === 187 || e.keyCode === 107)) { // minus and plus
                $(this).val(1);
            }
            var decimal = /^[-+]?[0-9]+\.[0-9]$/;
            if ($(this).val() < 0) {
                $(this).val(1);
            }
            else if ($(this).val() > 0 && $(this).val().match(decimal)) {
                $(this).val(1);
            }
        });

        $(document).on('blur','.qty-container input.qty',function(){
                var val = parseInt($(this).val());
                if(val <= 0){
                    val = 1;
                    $(this).val(val);
                }else if(val > 999){
                    val = 999;
                    $(this).val(val);
                }
        });

        $(document).on("click", "ul.tabs li", function () {
            var tab_id = $(this).attr('data-tab');
            $('ul.tabs li').removeClass('current');
            $('.tab-content').removeClass('current');
            $(this).addClass('current');
            $("#" + tab_id).addClass('current');
            setTimeout(function () {
                tabmainSlider();
            }, 500);
        });

        $(document).on("click", ".product-options-bottom .store-stock.noneditable", function () {
            alert("Please select product options before checking stock levels at a store");
        });


        $(".page-wrapper").on("click", "> .overlay-background", function () {
            $(".action.showcart").trigger("click");
        });

        $(document).on("click", ".customer-not-loggedin .customer-name", function () {
            if ($(".customer-not-loggedin").hasClass("active")) {
                $(".customer-not-loggedin").removeClass("active");
                $(".customer-not-loggedin .customer-name").removeClass("active");
            } else {
                $(".customer-not-loggedin").addClass("active");
                $(".customer-not-loggedin .customer-name").addClass("active");
            }
        });

        $('body').on('add_to_cart_success', function () {
            if ($(window).width() > 767) {
                $(".action.showcart").trigger("click");
                $("#btn-minicart-close").attr("disabled", "disabled").addClass("off-click");
                setTimeout(function () {
                    $(".action.showcart").trigger("click");
                    $("#btn-minicart-close").removeAttr("disabled").removeClass("off-click");
                }, 3000);
            }
        });

        $(document).on("click", "#minicart-content-wrapper .continue-shopping", function () {
            $(".action.showcart").trigger("click");
        });

        $(".reviews-actions-new a").on("click", function (e) {
            if(!$(body).hasClass("account")){
                e.preventDefault();
                if ($(window).width() > 1023) {
                    $('html, body').animate({
                        scrollTop: $('#reviews-link').offset().top - $(".page-header").outerHeight(true) - 50
                    }, 300);
                } else {
                    $('html, body').animate({
                        scrollTop: $('#reviews-link').offset().top - $(".page-header").outerHeight(true)
                    }, 300);
                }
            }
        });

        $(document).on('click','.payment-method.payment-method-braintree .payment-method-title',function(){
            if($("#braintree_enable_vault").is(":checked")){
                $("#braintree_enable_vault").click();
            }
        });

        tabmainSlider();
        locatorContent();
        accountSubscriptionContent();
    });

    $(window).scroll(function () {
        if ($(window).scrollTop() > 1) {
            $("#spnTop").addClass("spnTop");
        } else {
            $("#spnTop").removeClass("spnTop");
        }
    });

    $(window).load(function () {
        $(".itemslider-responsive .control-nav li").eq(0).find("> a").click();
        if($("body").hasClass("catalog-product-view")){
            reviewsUrl = $(location).attr('href');
            getId = reviewsUrl.split('#');
            if(getId[1]=='reviews'){
                if ($(window).width() > 1023) {
                    $('html, body').animate({
                        scrollTop: $('#reviews-link').offset().top - $(".page-header").outerHeight(true) - 50
                    }, 300);
                } else {
                    $('html, body').animate({
                        scrollTop: $('#reviews-link').offset().top - $(".page-header").outerHeight(true)
                    }, 300);
                }
            }
        }

        setTimeout(function(){
            if($("body").hasClass("catalog-product-view")){
                if($("#product_addtocart_form .subscription-content .subscription-data > div").hasClass("subscription-item")){
                    $(".subscription-content").addClass("active");
                }else{
                    $(".subscription-content").removeClass("active");
                }
            }
        },2000);

        tabmainSlider();
        locatorContent();
        accountSubscriptionContent();
    });

    $(window).resize(function(){
        tabmainSlider();
        locatorContent();
    });

    function locatorContent(){
        if($('body').hasClass('page-layout-1column')){
            var maxLocatorHeight = 0;
            var locatorHeight = 0;
            $('.all-details .section.group > li .store-loc-address').css({"height":'auto'});
            $('.all-details .section.group > li').each(function(){
                locatorHeight = $(this).find('.store-loc-address').height();
                if(locatorHeight >=  maxLocatorHeight){
                    maxLocatorHeight = locatorHeight;
                }
            });
            $('.all-details .section.group > li .store-loc-address').css({"height":maxLocatorHeight+30});
        }
    }

    function accountSubscriptionContent(){
        if($('body').hasClass('subscribenow-subscription-view') || $('body').hasClass('subscribenow-subscription-order') || $('body').hasClass('subscribenow-subscription-history')){
            $(".page-main .columns .column > div.page-title-wrapper.second").remove();
            getSubContent = $(".page-main .columns .column > div.page-title-wrapper").eq(0).find("div.block.block-addresses-default").html();
            $(".page-main .columns .column > div.back-subscription").after("<div class='page-title-wrapper second'><div class='block block-addresses-default'>"+getSubContent+"</div></div>");
        }
    }

    function tabmainSlider() {
        $(".best-seller-tab .tab-content").each(function () {
            $(this).find('div.itemslider-responsive').flexslider({
                namespace: "",
                animation: "slide",
                easing: "easeInQuart",
                slideshow: false,
                animationLoop: true,
                animationSpeed: 400,
                controlNav: true,
                pauseOnHover: false,
                itemWidth: 228,
                minItems: 2,
                maxItems: 5,
                move: 0
            }).data("breakpoints", [[1400, 5], [1200, 4], [1024, 4], [965, 4], [768, 3], [600, 3], [500, 2], [480, 2], [320, 2]]);
        });

        $(".for-featured-products").find('div.itemslider-responsive').flexslider({
            namespace: "",
            animation: "slide",
            easing: "easeInQuart",
            slideshow: false,
            animationLoop: true,
            animationSpeed: 400,
            controlNav: true,
            pauseOnHover: false,
            itemWidth: 310,
            minItems: 2,
            maxItems: 4,
            move: 0
        }).data("breakpoints", [[1400, 4], [1200, 3], [1024, 3], [965, 3], [768, 3], [600, 3], [500, 2], [480, 2], [320, 2]]);

        $(".amslider-container").find('div.itemslider-responsive').flexslider({
            namespace: "",
            animation: "slide",
            easing: "easeInQuart",
            slideshow: false,
            animationLoop: true,
            animationSpeed: 400,
            controlNav: true,
            pauseOnHover: false,
            itemWidth: 135,
            minItems: 3,
            maxItems: 6,
            move: 0
        }).data("breakpoints", [[1400, 6], [1200, 6], [1024, 6], [965, 6], [768, 6], [600, 4], [500, 4], [480, 3], [320, 3]]);

        $(".products-upsell").flexslider({
            namespace: "",
            animation: "slide",
            easing: "easeInQuart",
            slideshow: false,
            animationLoop: true,
            animationSpeed: 400,
            controlNav: true,
            pauseOnHover: false,
            itemWidth: 220,
            minItems: 2,
            maxItems: 3,
            move: 0
        }).data("breakpoints", [[1400, 3], [1200, 2], [1024, 2], [965, 3], [768, 3], [600, 3], [500, 2], [480, 2], [320, 2]]);

        $(".products-related").flexslider({
            namespace: "",
            animation: "slide",
            easing: "easeInQuart",
            slideshow: false,
            animationLoop: true,
            animationSpeed: 400,
            controlNav: true,
            pauseOnHover: false,
            itemWidth: 220,
            minItems: 2,
            maxItems: 3,
            move: 0
        }).data("breakpoints", [[1400, 3], [1200, 2], [1024, 2], [965, 3], [768, 3], [600, 3], [500, 2], [480, 2], [320, 2]]);

        $("#recently-viewed").flexslider({
            namespace: "",
            animation: "slide",
            easing: "easeInQuart",
            slideshow: false,
            animationLoop: true,
            animationSpeed: 400,
            controlNav: true,
            pauseOnHover: false,
            itemWidth: 220,
            minItems: 2,
            maxItems: 6,
            move: 0
        }).data("breakpoints", [[1400, 6], [1200, 4], [1024, 4], [965, 3], [768, 3], [600, 3], [500, 2], [480, 2], [320, 2]]);

        $("#products-crosssell").flexslider({
            namespace: "",
            animation: "slide",
            easing: "easeInQuart",
            slideshow: false,
            animationLoop: true,
            animationSpeed: 400,
            controlNav: true,
            pauseOnHover: false,
            itemWidth: 220,
            minItems: 2,
            maxItems: 5,
            move: 0
        }).data("breakpoints", [[1400, 5], [1200, 4], [1024, 4], [965, 3], [768, 3], [600, 3], [500, 2], [480, 2], [320, 2]]);

        $(document).trigger("themeResize");
    }
    $(window).resize(function(){
        categoryBlock();
        staticContent();
    });

    $(document).ready(function(){
        categoryBlock();
        staticContent();
    });

    function categoryBlock(){
        if($('body').hasClass('catalog-category-view')){
            var imgHeight = 0;
            var boxHeight = 0;
            $(".banner-img-text span > span").removeClass('img-height');
            $('.banner-img-text').each(function(){
                imgHeight = $(this).find('img').height();
                boxHeight = $(this).height();
                if(imgHeight > boxHeight){
                    $(this).find('span > span').addClass('img-height');
                }
            });
        }
    }
    function staticContent(){
        if($('body').hasClass('catalog-category-view')){
            var maxHeight = 0;
            var blockHeight = 0;
            $(".subcat img").removeClass('image-height');
            $('.subcat').each(function(){
                maxHeight = $(this).find('img').height();
                blockHeight = $(this).height();
                if(blockHeight <= maxHeight){
                    $(this).find('img').addClass('image-height');
                }
            });
        }
    }
    //voucher add
    $('.mainDiscountVoucher').each(function() {
        var $wrapper = $('#block-discount', this);
        $(".linkDiscount", $(this)).click(function(e) {
            $('.vocher-coupon:first-child', $wrapper).clone(true).appendTo($wrapper).find('.vocher-coupon').val('').focus();
            if  ($('.fieldset.coupon').length == 6){
                $(this).addClass('disable');
            }
        });
    });
    //loyalty-faq 
    $(document).ready(function() {
        //toggle the component with class accordion_body
        $(".accordion_head").click(function() {
            if ($('.accordion_body').is(':visible')) {
                $(".accordion_body").slideUp(300);
                $(".plusminus").text('+');
            }
            if ($(this).next(".accordion_body").is(':visible')) {
                $(this).next(".accordion_body").slideUp(300);
                $(this).children(".plusminus").text('+');
            } else {
                $(this).next(".accordion_body").slideDown(300);
                $(this).children(".plusminus").text('-');
            }
        });
    });

    /********Start of loyalty customer validation***********/
    /*require([
            'jquery'
        ],
        function ($) {
            $(window).on("load", function () {
                require([
                    'Magento_Customer/js/model/customer'
                ], function (customer) {
                    $(document).ready(function () {
                        if (!customer.isLoggedIn()) {
                            if(document.location.href.indexOf('/checkout/#shipping') != -1) {
                                $(document).on('change', "[name='telephone']", function () {
                                    var currentElement = $(this);
                                    var telephone = currentElement.val();
                                    var loyaltyCheckUrl = document.location.origin + "/loyalty/customer/validate";
                                    jQuery.ajax({
                                        type: "POST",
                                        url: loyaltyCheckUrl,
                                        dataType: 'json',
                                        data: {
                                            telephone: telephone
                                        },
                                        complete: function (response) {
                                            //console.log(response.responseJSON);
                                            if (response.responseJSON.errors) {
                                                if (response.responseJSON.errors == true) {
                                                    currentElement.val('');
                                                    currentElement.keyup();
                                                    currentElement.siblings('.field-error').children().text('Phone number already exists. Try another.');
                                                }
                                            }
                                        },
                                        error: function (xhr, status, errorThrown) {
                                            console.log('Loyalty customer validate failed. Try again.');
                                        }
                                    });
                                });
                            }
                        }
                    });

                });
            });
        });*/
    /*********End of loyalty customer validation**********/
});