define([
    'jquery'
], function ($) {
    'use strict';
    return function (pdata) {
        return pdata.extend({
            initialize: function () {
                this._super();
                $('.klevu-fluid').insertAfter('header .block-search .form.minisearch');
            }
        })
    }
});