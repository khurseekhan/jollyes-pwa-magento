define([
    'jquery',
    'matchMedia',
    'domReady!'
], function ($, mediaCheck) {
    'use strict';

    // sticky header class toggle
    // __________________________

    var stickyDummyHeight;

    var deviceCheck = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);

    $(window).scroll(function(){
        if ($(window).width() >= 1024 && !deviceCheck) {
            var scrollPosition = $(window).scrollTop();
            var sticky = $('.header-navigation');

            if(scrollPosition >= '222') {
                sticky.addClass('sticky-header');
                $('body').addClass('sticky-header');
                $('body').css('padding-top','150px');
            } else {
                sticky.removeClass('sticky-header');
                $('body').removeClass('sticky-header');
                $('body').css('padding-top','0');
            }
        }
    });

    mediaCheck({
        media: '(min-width: 1024px)',
        entry: function () {
            var scrollPosition = $(window).scrollTop();
            var sticky = $('.header-navigation');

            if(!deviceCheck){
                if(scrollPosition > $('.header-navigation').outerHeight(true)) {
                    sticky.addClass('sticky-header');
                    $('body').addClass('sticky-header');
                }
            }

        },
        exit: function () {
            var sticky = $('.header-navigation');
            sticky.removeClass('sticky-header');
            $('body').removeClass('sticky-header');
        }
    });
});
