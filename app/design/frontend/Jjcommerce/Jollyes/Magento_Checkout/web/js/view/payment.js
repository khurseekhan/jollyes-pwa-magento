/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
/*jshint browser:true jquery:true*/
/*global alert*/
define(
    [
        'jquery',
        "underscore",
        'uiComponent',
        'ko',
        'Magento_Checkout/js/model/quote',
        'Magento_Checkout/js/model/step-navigator',
        'Magento_Checkout/js/model/payment-service',
        'Magento_Checkout/js/model/payment/method-converter',
        'Magento_Checkout/js/action/get-payment-information',
        'Magento_Checkout/js/model/checkout-data-resolver',
        'mage/translate'
    ],
    function (
        $,
        _,
        Component,
        ko,
        quote,
        stepNavigator,
        paymentService,
        methodConverter,
        getPaymentInformation,
        checkoutDataResolver,
        $t
    ) {
        'use strict';

        /** Set payment methods to collection */
        paymentService.setPaymentMethods(methodConverter(window.checkoutConfig.paymentMethods));

        return Component.extend({
            defaults: {
                template: 'Magento_Checkout/payment',
                activeMethod: ''
            },
            isVisible: ko.observable(quote.isVirtual()),
            quoteIsVirtual: quote.isVirtual(),
            isPaymentMethodsAvailable: ko.computed(function () {
                return paymentService.getAvailablePaymentMethods().length > 0;
            }),

            initialize: function () {
                this._super();
                checkoutDataResolver.resolvePaymentMethod();
                stepNavigator.registerStep(
                    'payment',
                    null,
                    $t('Review & Payments'),
                    this.isVisible,
                    _.bind(this.navigate, this),
                    20
                );
                return this;
            },

            navigate: function () {
                var self = this;
                
                var shipping = window.checkoutConfig.selectedShippingMethod;
                var quoteData = window.checkoutConfig.quoteData;

                if(shipping.method_code == 'clickandcollect' && quoteData.storelocator_id <= 0){
                    this.isVisible(false);
                    stepNavigator.setHash('delivery');
                    window.location.reload(); 
                }else{
                    getPaymentInformation().done(function () {
                        self.isVisible(true);
                    });
                }
            },

            getFormKey: function() {
                return window.checkoutConfig.formKey;
            },

            updateRegion: function (items, name) {
                if (name == 'afterMethods') {
                    items.forEach(function(item){
                        item.removeChild('discount');  // we want to move coupon code html
                    });
                }
                this.getRegion(name)(items);
                return this;
            }
        });
    }
);
