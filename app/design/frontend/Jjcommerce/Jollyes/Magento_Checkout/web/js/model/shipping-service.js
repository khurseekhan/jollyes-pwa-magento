/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
/*global define*/
define(
    [
        'jquery',
        'ko',
        'Magento_Checkout/js/model/checkout-data-resolver'
    ],
    function ($, ko, checkoutDataResolver) {
        "use strict";
        var shippingRates = ko.observableArray([]);
        return {
            isLoading: ko.observable(false),
            /**
             * Set shipping rates
             *
             * @param ratesData
             */
            setShippingRates: function(ratesData) {
                $("#checkout-shipping-method-load .clickShipping").hide();
                $("#checkout-shipping-method-load .homeShipping").hide();
                shippingRates(ratesData);
                shippingRates.valueHasMutated();
                checkoutDataResolver.resolveShippingRates(ratesData);
                //console.log('test ', ratesData);
                if(window.currentDelivery=="home_delivery") {
                    $("#checkout-shipping-method-load .clickShipping").hide();
                    $("#checkout-shipping-method-load .homeShipping").show();
                }else{
                    $("#checkout-shipping-method-load .homeShipping").hide();
                    $("#checkout-shipping-method-load .clickShipping").show();
                }
            },

            /**
             * Get shipping rates
             *
             * @returns {*}
             */
            getShippingRates: function() {
                return shippingRates;
            }
        };
    }
);
