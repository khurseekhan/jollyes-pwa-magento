<?php

namespace Meetanshi\ReviewImportExport\Block\Adminhtml;

use Magento\Backend\Block\Template;

class Review extends Template
{
    public function __construct(
        Template\Context $context,
        array $data = [])
    {
        parent::__construct($context, $data);
    }

    public function getUploadUrl()
    {
        return $this->getUrl('reviewimportexport/review/upload');
    }

    public function getDownloadUrl()
    {
        return $this->getUrl('reviewimportexport/review/download');
    }

    public function getDownloadSampleFileHtml()
    {
        $path = $this->getViewFileUrl('Meetanshi_ReviewImportExport::files/Reviews.csv');
        $html = '<span id="sample-file-span"><a id="sample-file-link" href="'.$path.'" target="_blank" download="Reviews.csv">'
            . __('Download Sample File')
            . '</a></span>';
        return $html;
    }
}
