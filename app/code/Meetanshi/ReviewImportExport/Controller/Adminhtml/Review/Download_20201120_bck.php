<?php

namespace Meetanshi\ReviewImportExport\Controller\Adminhtml\Review;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Review\Model\ResourceModel\Review\Collection as CollectionFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Review\Model\ResourceModel\Rating\Option\Vote\Collection as RatingCollection;

class Download extends Action
{
    private $reviewFactory;
    private $storeManager;
    private $ratingCollection;

    public function __construct(
        Context $context,
        CollectionFactory $reviewFactory,
        RatingCollection $ratingCollection,
        StoreManagerInterface $storeManager
    )
    {
        $this->reviewFactory = $reviewFactory;
        $this->storeManager = $storeManager;
        $this->ratingCollection = $ratingCollection;
        parent::__construct($context);
    }

    public function execute()
    {
        try {
            $params = $this->getRequest()->getParams();
            $this->reviewFactory
                ->addFilterToMap('created_at', 'main_table.created_at')
                ->addFilterToMap('detail_id', 'main_table.detail_id')
                ->getSelect()->join(
                    ['review_status' => $this->reviewFactory->getTable('review_status')],
                    'review_status.status_id = main_table.status_id',
                    ['status_code']);
            $this->reviewFactory
                ->getSelect()->join(
                    ['review_detail' => $this->reviewFactory->getTable('review_detail')],
                    'review_detail.review_id = main_table.review_id',
                    ['store_id']);

            $this->reviewFactory->getSelect()->joinLeft(
                ['product_table' => $this->reviewFactory->getTable('catalog_product_entity')],
                'product_table.entity_id = entity_pk_value',
                ['sku']);

            $this->reviewFactory->getSelect()->joinLeft(
                ['rating_option_vote' => $this->reviewFactory->getTable('rating_option_vote')],
                'rating_option_vote.review_id = main_table.review_id',
                ['value']);

            $this->reviewFactory->getSelect()->group('review_id');
            if(!empty($params['review_export_from']) &&  !empty($params['review_export_to'])){
                $startDate= date_create($params['review_export_from']);
                $endDate = date_create($params['review_export_to']);
                $diff=date_diff($startDate,$endDate);
                if($diff->format("%R") == "+")
                {
                    $this->reviewFactory
                        ->addFieldToFilter(
                            'created_at',
                            array
                            (
                                'from'=>$params['review_export_from'].' 00:00:00',
                                'to'=>$params['review_export_to'].' 23:59:59'
                            )
                        );
                }else{
                    $this->messageManager->addErrorMessage(__('"From Date" must be less then "To Date"'));
                    $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
                    $resultRedirect->setUrl($this->_redirect->getRefererUrl());
                    return $resultRedirect;
                }
            }
            else{
                if (!empty($params['review_export_from'])) {
                    $this->reviewFactory
                        ->addFieldToFilter(
                            'created_at', ['gteq' => $params['review_export_from']]
                        );
                }
                if (!empty($params['review_export_to'])) {
                    $this->reviewFactory
                        ->addFieldToFilter(
                            'created_at', ['lteq' => $params['review_export_to'].' 23:59:59']
                        );
                }
            }

            $heading = [
                __('Store View'),
                __('Nick Name'),
                __('Date'),
                __('Review Summary'),
                __('Review Detail'),
                __('Ratings'),
                __('Product SKU'),
                __('Approval Status')
            ];
            if ($params['enable_customer']) {
                array_push($heading, "Customer ID", "Customer Type");
            }
            $outputFile = "review_" . date('Ymd_His') . ".csv";
            $handle = fopen($outputFile, 'w');
            fputcsv($handle, $heading);
            $ratingCollection = $this->ratingCollection->getData();
            foreach ($this->reviewFactory->getData() as $review) {
                $stores = $this->storeManager->getStores(true, false);
                foreach ($stores as $store) {
                    if ($store->getId() == $review['store_id']) {
                        $storeCode = $store->getCode();
                    }
                }
                $storeCode = 'default';
                $ratingCollection = $this->_objectManager
                    ->create(RatingCollection::class)
                    ->addRatingInfo()
                    ->addOptionInfo()
                    ->addRatingOptions()
                    ->addFieldToFilter('review_id',$review['review_id']);

                $ratingData = $ratingCollection->getData();
                $ratingArray = array();
                foreach ($ratingData as $rating){
                    $ratingArray[] = $rating['rating_id'].':'.$rating['value'];
                }
                $rating = implode(";",$ratingArray);
                
                $row = [
                    $storeCode,
                    $review['nickname'],
                    date('Y-m-d H:i:s', strtotime($review['created_at'])),
                    $review['title'],
                    $review['detail'],
                    (string)$rating,
                    $review['sku'],
                    $review['status_code']
                ];
                if ($params['enable_customer']) {
                    if ($review['customer_id'] == null) {
                        $cus = 'guest';
                    } else {
                        $cus = 'customer';
                    }
                    array_push($row, $review['customer_id'], $cus);
                }
                fputcsv($handle, $row);
            }
            $this->downloadCsv($outputFile);
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        }

        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setUrl($this->_redirect->getRefererUrl());
        return $resultRedirect;
    }

    public function downloadCsv($file)
    {
        if (file_exists($file)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/csv');
            header('Content-Disposition: attachment; filename=' . basename($file));
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            flush();
            readfile($file);
        }
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Meetanshi_ReviewImportExport::review');
    }
}
