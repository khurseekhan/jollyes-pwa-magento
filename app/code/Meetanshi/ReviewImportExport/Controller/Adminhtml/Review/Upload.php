<?php

namespace Meetanshi\ReviewImportExport\Controller\Adminhtml\Review;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Catalog\Model\Product;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\File\Csv;
use Magento\Framework\Filesystem;
use Magento\MediaStorage\Model\File\UploaderFactory;
use Magento\Review\Model\RatingFactory;
use Magento\Review\Model\Review;
use Magento\Review\Model\ReviewFactory;
use Magento\Store\Model\StoreManagerInterface;

class Upload extends Action
{
    private $csv;
    private $mediaDirectory;
    private $fileUploaderFactory;
    private $product;
    private $reviewFactory;
    private $ratingFactory;
    private $storeManager;

    public function __construct(
        Context $context,
        Filesystem $filesystem,
        UploaderFactory $fileUploaderFactory,
        Csv $csv,
        Product $product,
        ReviewFactory $reviewFactory,
        RatingFactory $ratingFactory,
        StoreManagerInterface $storeManager
    )
    {
        $this->csv = $csv;
        $this->product = $product;
        $this->reviewFactory = $reviewFactory;
        $this->ratingFactory = $ratingFactory;
        $this->storeManager = $storeManager;
        $this->mediaDirectory = $filesystem->getDirectoryWrite(DirectoryList::MEDIA);
        $this->fileUploaderFactory = $fileUploaderFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $successCounter = 0;
        $lineCounter = 1;
        try {
            $target = $this->mediaDirectory->getAbsolutePath('reviewimport/');
            $uploader = $this->fileUploaderFactory->create(['fileId' => 'file']);
            $uploader->setAllowedExtensions(['csv']);
            $uploader->setAllowRenameFiles(true);
            $result = $uploader->save($target);
            $uploadedPath = $result['path'] . $result['file'];
            $importReviewData = $this->csv->getData($uploadedPath);

            foreach ($importReviewData as $rowIndex => $reviewData) {
                if ($rowIndex == 0) {
                    if (!in_array('Store View', $reviewData)) {
                        throw new LocalizedException(__('Missing Column "Store View" in line ' . $lineCounter));
                    }
                    if (!in_array('Nick Name', $reviewData)) {
                        throw new LocalizedException(__('Missing Column "Nick Name" in line ' . $lineCounter));
                    }
                    if (!in_array('Date', $reviewData)) {
                        throw new LocalizedException(__('Missing Column "Date" in line ' . $lineCounter));
                    }
                    if (!in_array('Review Summary', $reviewData)) {
                        throw new LocalizedException(__('Missing Column "Review Summary" in line ' . $lineCounter));
                    }
                    if (!in_array('Review Detail', $reviewData)) {
                        throw new LocalizedException(__('Missing Column "Review Detail" in line ' . $lineCounter));
                    }
                    if (!in_array('Ratings', $reviewData)) {
                        throw new LocalizedException(__('Missing Column "Ratings" in line ' . $lineCounter));
                    }
                    if (!in_array('Product SKU', $reviewData)) {
                        throw new LocalizedException(__('Missing Column "Product SKU" in line ' . $lineCounter));
                    }
                    if (!in_array('Approval Status', $reviewData)) {
                        throw new LocalizedException(__('Missing Column "Approval Status" in line ' . $lineCounter));
                    }
                    if (!in_array('Customer ID', $reviewData)) {
                        throw new LocalizedException(__('Missing Column "Customer ID" in line ' . $lineCounter));
                    }

                } elseif ($rowIndex > 0) {
                    $review = $this->reviewFactory->create();
                    $review->unsetData('review_id');
                    $productId = $this->product->getIdBySku($reviewData['6']);

                    if (is_numeric($productId)) {
                        $review->setEntityPkValue($productId);
                    } else {
                        throw new LocalizedException(__('Invalid ' . $reviewData['6'] . ' SKU value in line ' . $lineCounter));
                    }


                    if ($reviewData['7'] == 'Pending') {
                        $review->setStatusId(Review::STATUS_PENDING);
                    } elseif ($reviewData['7'] == 'Approved') {
                        $review->setStatusId(Review::STATUS_APPROVED);
                    } elseif ($reviewData['7'] == 'Not Approved') {
                        $review->setStatusId(Review::STATUS_NOT_APPROVED);
                    } else {
                        throw new LocalizedException(__('Invalid Status ' . $reviewData['7'] . ' in line ' . $lineCounter));
                    }


                    if (!empty($reviewData['3'])) {
                        $review->setTitle($reviewData['3']);
                    } else {
                        throw new LocalizedException(__('Invalid Title ' . $reviewData['3'] . ' in line ' . $lineCounter));
                    }


                    if (!empty($reviewData['4'])) {
                        $review->setDetail($reviewData['4']);
                    } else {
                        throw new LocalizedException(__('Invalid Detail ' . $reviewData['4'] . ' in line ' . $lineCounter));
                    }


                    if (!empty($reviewData['0'])) {
                        $stores = $this->storeManager->getStores(true, false);
                        $storeCode = false;
                        foreach ($stores as $store) {
                            if ($store->getCode() == $reviewData['0']) {
                                $storeCode = $store->getId();
                                break;
                            }
                        }
                        if (!$storeCode) {
                            throw new LocalizedException(__('Invalid Store View ' . $reviewData['0'] . ' in line ' . $lineCounter));
                        } else {
                            $review->setStoreId($storeCode);
                            $review->setStores(array($storeCode));
                        }

                    } else {
                        throw new LocalizedException(__('Invalid Store View ' . $reviewData['0'] . ' in line ' . $lineCounter));
                    }


                    $review->setEntityId($review->getEntityIdByCode(Review::ENTITY_PRODUCT_CODE));


                    if (is_numeric($reviewData['8'])) {
                        $review->setCustomerId($reviewData['8']);
                    } elseif (empty($reviewData['8'])) {
                        $review->setCustomerId();
                    } else {
                        throw new LocalizedException(__('Invalid Customer ID in line ' . $lineCounter));
                    }

                    if (!empty($reviewData['1'])) {
                        $review->setNickname($reviewData['1']);
                    } else {
                        throw new LocalizedException(__('Invalid nick name' . $reviewData['1'] . ' in line ' . $lineCounter));
                    }


                    $review->save();

                    if (!empty($reviewData['2'])) {
                        $review->setCreatedAt($reviewData['2']);
                        $review->save();
                    }

                    if (!empty($reviewData['5'])) {
                        $string = $reviewData['5'];
                        $ratingOptions = [];
                        $asArr = explode(';', $string);
                        $add = 0;
                        foreach ($asArr as $val) {
                            $tmp = explode(':', $val);
                            $ratingOptions[$tmp[0]] = $tmp[1] + $add;
                            $add = $add + 5;
                        }
                    } else {
                        throw new LocalizedException(__('Invalid rating ' . $reviewData['5'] . ' in line ' . $lineCounter));
                    }

                    foreach ($ratingOptions as $ratingId => $optionId) {
                        $this->ratingFactory->create()
                            ->setRatingId($ratingId)
                            ->setReviewId($review->getId())
                            ->addOptionVote($optionId, $productId);
                    }
                    $review->aggregate();
                    $successCounter++;
                    $lineCounter++;
                }

            }
        } catch (\Exception $e) {
            if ($successCounter > 0) {
                $this->messageManager->addSuccessMessage(__($successCounter . ' has been saved.'));
            }
            $this->messageManager->addErrorMessage(__($e->getMessage()));
        }
        if ($successCounter > 0) {
            $this->messageManager->addSuccessMessage(__($successCounter . ' has been saved.'));
        }
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setUrl($this->_redirect->getRefererUrl());
        return $resultRedirect;
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Meetanshi_ReviewImportExport::review');
    }
}
