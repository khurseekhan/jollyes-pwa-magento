<?php
/**
 * Jjcommerce_UpgradeProcessFixes
 *
 * @category  TwoJay
 * @package   Jjcommerce_UpgradeProcessFixes
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Jjcommerce_UpgradeProcessFixes',
    __DIR__
);
