<?php
/**
 * Jjcommerce_UpgradeProcessFixes
 *
 * @category  Jjcommerce
 * @package   Jjcommerce_UpgradeProcessFixes
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */
namespace Jjcommerce\UpgradeProcessFixes\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use Magento\Framework\App\Filesystem\DirectoryList;

class RunUpgradeScript extends Command
{

    const ATTRIBUTE_CODE = 'loyalty_number'; // 474
    const FILE_PATH = 'loyalty_number/customer.csv';
    /**
     * @var \Magento\Framework\App\State
     */
    protected $_appState;

    protected $varDirectoryToRead;

    protected $csvProcessor;
    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $_connection;

    /**
     * RunUpgradeScript constructor.
     * @param \Magento\Framework\App\State $appState
     * @param \Magento\Framework\Filesystem $filesystem
     * @param DirectoryList $directoryList
     * @param \Magento\Framework\File\Csv $csvProcessor
     * @param \Magento\Framework\App\ResourceConnection $resourceConnection
     * @param null $name
     */
    public function __construct(
        \Magento\Framework\App\State $appState,
        \Magento\Framework\Filesystem $filesystem,
         DirectoryList $directoryList,
        \Magento\Framework\File\Csv $csvProcessor,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        $name = null
    ) {
        $this->_appState = $appState;

        $this->varDirectoryToRead = $filesystem->getDirectoryRead(DirectoryList::MEDIA);
        $this->csvProcessor = $csvProcessor;
        $this->_connection = $resourceConnection;

        parent::__construct($name);
    }

    protected function configure()
    {
        $this->setName('twojay:upgrade:fixes')
            ->setDescription('This is used to drop foreign key during magento upgrade (Do not execute this command)');

        parent::configure();
    }

    protected function _archiveFile()
    {
       $data =  $this->csvProcessor->getData( $this->varDirectoryToRead->getAbsolutePath( self::FILE_PATH) );
       $realData = [];
       foreach($data as $ndx => $eachElement){
           if($ndx==0) continue;
           $realData[ $eachElement[0] ] = $eachElement[1];
       }
       return $realData;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->_appState->setAreaCode('adminhtml');

        try {
            $output->writeln('Started');
            $data =  $this->_archiveFile();
            $rows = $this->_connection->getConnection()->fetchRow(
                "select attribute_id,backend_type from eav_attribute where attribute_code='".self::ATTRIBUTE_CODE."'"
            );


            foreach($data as $email => $loyaltynumber) {

                $query = "SELECT c.entity_id,cv.value_id from customer_entity  as c 
             left join customer_entity_varchar as cv on c.entity_id=cv.entity_id and cv.attribute_id=".$rows['attribute_id']." 
             where c.email='".addslashes($email)."'";

                $crows = $this->_connection->getConnection()->fetchRow($query);

                if(!empty($crows)){
                if($crows['value_id'] ){
                    $updQuery = "update customer_entity_varchar set value='".$loyaltynumber."' where value_id=".$crows['value_id'] ;
                    $this->_connection->getConnection()->query($updQuery );
                }
                else{
                    $insQuery = "Insert into customer_entity_varchar ( attribute_id,entity_id,value ) values ( ".$rows['attribute_id'].','. $crows['entity_id'] .','.$loyaltynumber.")";
                    $this->_connection->getConnection()->query($insQuery );
                }
                }
            }

            $output->writeln('Done');
        } catch (\Exception $e) {
            $output->writeln('<error>Error during performing operation</error>');
            $output->writeln($e->getMessage());
            $output->writeln($e->getTraceAsString());
            exit;
        }
    }

}
