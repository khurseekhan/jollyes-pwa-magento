<?php
/**
 * Jjcommerce_AffiliatePartnerTags
 *
 * PHP version ~5.5.0|~5.6.0|~7.0.0
 *
 * @category  PHP
 * @package   Jjcommerce\AffiliatePartnerTags
 * @author    Swapnil Tatkondawar <swapnilt@2jcommerce.in>
 * @copyright 2017 Copyright 2J Commerce, Inc. http://www.2jcommerce.in/
 * @license   http://www.2jcommerce.in/ Private
 * @link      http://www.jollyes.co.uk/
 */

namespace Jjcommerce\AffiliatePartnerTags\Block;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Checkout\Model\Session;
use Magento\Sales\Model\Order;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;

class AffiliateTagScript extends Template
{
    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;

    /**
     * @var
     */
    protected $_salesFactory;
    /**
     * @var StoreManagerInterface
     */
    protected $_storeManager;
    /**
     * @var ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * Merchant Id XML Path
     */
    const XML_PATH_MERCHANT_ID = 'affiliate_tag/general/merchant_id';

    /**
     * AffiliateTagScript constructor.
     * @param Context $context
     * @param Session $checkoutSession
     * @param Order $salesOrderFactory
     * @param StoreManagerInterface $storeManager
     * @param array $data
     */
    public function __construct(
        Context $context,
        Session $checkoutSession,
        Order $salesOrderFactory,
        StoreManagerInterface $storeManager,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_checkoutSession = $checkoutSession;
        $this->_salesFactory = $salesOrderFactory;
        $this->_storeManager = $storeManager;
        $this->_scopeConfig  = $context->getScopeConfig();
    }

    /**
     * @return $this
     */
    public function getOrder(){

        $orderId = $this->_checkoutSession->getLastOrderId();
        $order   = $this->_salesFactory->load($orderId);

        return $order;
    }

    /**
     * @return mixed
     */
    public function getCurrency(){

        return $this->_storeManager->getStore()->getCurrentCurrency()->getCode();
    }

    /**
     * @return string
     */
    public function getMerchantId(){
        return $this->_scopeConfig->getValue(
            self::XML_PATH_MERCHANT_ID,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

}