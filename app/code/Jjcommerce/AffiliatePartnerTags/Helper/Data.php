<?php
/**
 * Jjcommerce_AffiliatePartnerTags
 *
 * PHP version ~5.5.0|~5.6.0|~7.0.0
 *
 * @category  PHP
 * @package   Jjcommerce\AffiliatePartnerTags
 * @author    Swapnil Tatkondawar <swapnilt@2jcommerce.in>
 * @copyright 2017 Copyright 2J Commerce, Inc. http://www.2jcommerce.in/
 * @license   http://www.2jcommerce.in/ Private
 * @link      http://www.jollyes.co.uk/
 */

namespace Jjcommerce\AffiliatePartnerTags\Helper;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;

class Data extends AbstractHelper
{
    /**
     * @var ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * XML Path
     */
    const PATH_GENERAL_ACTIVE = 'affiliate_tag/general/enabled';
    /**
     * Data constructor.
     * @param Context $context
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        Context $context,
        StoreManagerInterface $storeManager
    )
    {
        $this->_scopeConfig  = $context->getScopeConfig();
        $this->_storeManager = $storeManager;
        parent::__construct($context);
    }

    /**
     * Check whether module is enabled or not
     * @return bool
     */
    public function IsEnabled()
    {
        return (bool)$this->_scopeConfig->getValue(
            self::PATH_GENERAL_ACTIVE,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
}
