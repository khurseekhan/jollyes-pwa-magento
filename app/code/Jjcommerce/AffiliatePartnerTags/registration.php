<?php
/**
 * Jjcommerce_AffiliatePartnerTags
 *
 * PHP version ~5.5.0|~5.6.0|~7.0.0
 *
 * @category  PHP
 * @package   Jjcommerce\AffiliatePartnerTags
 * @author    Swapnil Tatkondawar <swapnilt@2jcommerce.in>
 * @copyright 2017 Copyright 2J Commerce, Inc. http://www.2jcommerce.in/
 * @license   http://www.2jcommerce.in/ Private
 * @link      http://www.jollyes.co.uk/
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Jjcommerce_AffiliatePartnerTags',
    __DIR__
);
