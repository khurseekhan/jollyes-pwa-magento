<?php
/* *
 * Copyright © 2016 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace Jjcommerce\ProductUrlRegenerator\Console\Command;

use Magento\Framework\App\Area;
use Magento\Framework\App\Cache;
use Magento\Framework\App\DeploymentConfig;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\App\State;
use Magento\CatalogUrlRewrite\Model\ProductUrlRewriteGenerator;
use Magento\Catalog\Model\ResourceModel\Product\Collection;
use Magento\Store\Model\Store;
use Magento\UrlRewrite\Service\V1\Data\UrlRewrite;
use Magento\UrlRewrite\Model\UrlPersistInterface;



class ProductUrl extends Command
{
    protected $_state = null;

    /**
     * @var ProductUrlRewriteGenerator
     */
    protected $productUrlRewriteGenerator;

    /**
     * @var UrlPersistInterface
     */
    protected $urlPersist;

    /**
     * @var ProductRepositoryInterface
     */
    protected $collection;

    public function __construct(
        \Magento\Framework\App\State $state,
        Collection $collection,
        ProductUrlRewriteGenerator $productUrlRewriteGenerator,
        UrlPersistInterface $urlPersist
    ) {
        $this->_state = $state;
        $this->collection = $collection;
        $this->productUrlRewriteGenerator = $productUrlRewriteGenerator;
        $this->urlPersist = $urlPersist;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('jjcommerce:product_url_regenerate')
            ->setDescription('product url regenerate.')
            ->addArgument(
            'pids',
            InputArgument::OPTIONAL | InputArgument::IS_ARRAY,
            'Products to regenerate'
        )
        ->addOption(
            'store', 's',
            InputOption::VALUE_REQUIRED,
            'Use the specific Store View',
            Store::DEFAULT_STORE_ID
        );
        parent::configure();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

            try {
                $this->_state->setAreaCode('adminhtml');
                $store_id = $input->getOption('store');
                $this->collection->addStoreFilter($store_id)->setStoreId($store_id);

                $pids = $input->getArgument('pids');
                if(!empty($pids)) {
                    $this->collection->addIdFilter($pids);
                }

                $this->collection->addAttributeToSelect(['url_path', 'url_key']);
                $list = $this->collection->load();

                foreach($list as $product) {
                    $product->setStoreId($store_id);
                    $this->urlPersist->deleteByData([
                        UrlRewrite::ENTITY_ID => $product->getId(),
                        UrlRewrite::ENTITY_TYPE => ProductUrlRewriteGenerator::ENTITY_TYPE,
                        UrlRewrite::REDIRECT_TYPE => 0,
                        UrlRewrite::STORE_ID => $store_id
                    ]);
                    try {
                        $this->urlPersist->replace(
                            $this->productUrlRewriteGenerator->generate($product)
                        );
                    } catch (\Magento\Framework\Exception\LocalizedException $e) {
                        $output->writeln('<error>Duplicated url for '. $product->getId() .'</error>');
                    }
                }

                $output->writeln('<info>Product URL has been regenerated!</info>');

            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $output->writeln('<error>' . $e->getMessage() . '</error>');
                $returnValue = \Magento\Framework\Console\Cli::RETURN_FAILURE;
            }

            $returnValue = \Magento\Framework\Console\Cli::RETURN_FAILURE;
            return $returnValue ;
        }

}
