<?php
/**
 * Jjcommerce_Base
 *
 * PHP version 5.x
 *
 * @category  PHP
 * @package   Jjcommerce\Base
 * @author    Swapnil Tatkondawar <swapnilt@2jcommerce.in>
 * @copyright 2017 Copyright 2J Commerce, Inc. http://www.2jcommerce.in/
 * @license   http://www.2jcommerce.in/ Private
 * @link      http://www.jollyes.co.uk
 */
namespace Jjcommerce\Base\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\CatalogInventory\Api\StockRegistryInterfaceFactory;
use Magento\Framework\App\Helper\Context;
use Magento\Catalog\Model\ProductFactory;
use Magento\Catalog\Helper\Image;

class Data extends AbstractHelper
{

    /**
     * Stock Registry InterfaceFactory
     *
     * @var StockRegistryInterfaceFactory
     */
    protected $_stockRegistryInterfaceFactory;

    /**
     * @var \Magento\Catalog\Helper\Image
     */
    protected $_imageHelper;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $_productFactory;

    /**
     * Data constructor.
     * @param Context $context
     * @param StockRegistryInterfaceFactory $stockRegistryInterfaceFactory
     * @param ProductFactory $productFactory
     * @param Image $imageHelper
     */
    public function __construct(
        Context $context,
        StockRegistryInterfaceFactory $stockRegistryInterfaceFactory,
        ProductFactory $productFactory,
        Image $imageHelper
    ) {
        parent::__construct($context);
        $this->_stockRegistryInterfaceFactory = $stockRegistryInterfaceFactory;
        $this->_productFactory = $productFactory;
        $this->_imageHelper = $imageHelper;
    }

    /**
     * get Stock qty
     * @param $product
     * @return int
     */
    public function getStockQty($product)
    {
        $stockRegistry = $this->_stockRegistryInterfaceFactory->create();

        $quantity = (int)$stockRegistry->getStockItem($product->getId())->getData('qty');

        return $quantity;
    }

    /**
     * Get Url of Thumbnail Image
     * @param $productId
     * @return mixed
     */
    public function getThumbnailImageUrl($productId)
    {
        $product = $this->_productFactory->create();
        $product->load($productId);

        $image_url = $this->_imageHelper->init($product, 'product_page_image_small')
            ->setImageFile($product->getFile())
            ->getUrl();

        return $image_url;

    }

    /**
     * To show Saving column if special price is applied
     * @param $associatedProducts
     * @return bool
     */
    public function showSavingColumn($associatedProducts)
    {
        $itemSpecialPrices = [];

        foreach ($associatedProducts as $item) {

            $specialPrice  = $item->getSpecialPrice();

            if($specialPrice){
                $itemSpecialPrices[$item->getId()]= 'true';
            }
        }

        if (in_array("true", $itemSpecialPrices)){
            return true;
        }

        return false;

    }

}
