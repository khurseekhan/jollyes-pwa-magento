<?php
/**
 * Jjcommerce_Base
 *
 * PHP version 5.x
 *
 * @category  PHP
 * @package   Jjcommerce\Base
 * @author    Swapnil Tatkondawar <swapnilt@2jcommerce.in>
 * @copyright 2017 Copyright 2J Commerce, Inc. http://www.2jcommerce.in/
 * @license   http://www.2jcommerce.in/ Private
 * @link      http://www.jollyes.co.uk/
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Jjcommerce_Base',
    __DIR__
);
