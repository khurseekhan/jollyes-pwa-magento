<?php
/**
 * Jjcommerce_Base
 *
 * PHP version 5.x
 *
 * @category  PHP
 * @package   Jjcommerce\Base
 * @author    Swapnil Tatkondawar <swapnilt@2jcommerce.in>
 * @copyright 2017 Copyright 2J Commerce, Inc. http://www.2jcommerce.in/
 * @license   http://www.2jcommerce.in/ Private
 * @link      http://www.jollyes.co.uk
 */
namespace Jjcommerce\Base\Plugin\Catalog\Block\Product;

class View
{

    /**
     * @param \Magento\Catalog\Block\Product\View $subject
     * @param $result
     * @return mixed
     */
    public function afterSetLayout(\Magento\Catalog\Block\Product\View $subject, $result) {
        $subject->getLayout()->createBlock(\Magento\Catalog\Block\Breadcrumbs::class);

        return $result;
    }
}