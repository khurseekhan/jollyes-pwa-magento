<?php
/**
 * Jjcommerce_Base
 *
 * PHP version 5.x
 *
 * @category  PHP
 * @package   Jjcommerce\Base
 * @author    Swapnil Tatkondawar <swapnilt@2jcommerce.in>
 * @copyright 2017 Copyright 2J Commerce, Inc. http://www.2jcommerce.in/
 * @license   http://www.2jcommerce.in/ Private
 * @link      http://www.jollyes.co.uk
 */
namespace Jjcommerce\Base\Plugin\Block\Html;

class Pager extends \Magento\Theme\Block\Html\Pager
{

    /**
     * The list of available pager limits
     *
     * @var array
     */
    protected $_availableLimit = [6 => 6, 10 => 10, 15 => 15];

    /**
     * @param \Magento\Theme\Block\Html\Pager $subject
     * @param $result
     * @return array
     */
    public function afterGetAvailableLimit(\Magento\Theme\Block\Html\Pager $subject, $result)
    {
        return $this->_availableLimit;
    }

}
