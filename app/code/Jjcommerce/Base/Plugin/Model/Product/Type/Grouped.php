<?php
/**
 * Jjcommerce_Base
 *
 * PHP version 5.x
 *
 * @category  PHP
 * @package   Jjcommerce\Base
 * @author    Swapnil Tatkondawar <swapnilt@2jcommerce.in>
 * @copyright 2017 Copyright 2J Commerce, Inc. http://www.2jcommerce.in/
 * @license   http://www.2jcommerce.in/ Private
 * @link      http://www.jollyes.co.uk
 */
namespace Jjcommerce\Base\Plugin\Model\Product\Type;

use \Magento\Catalog\Model\ResourceModel\Product\Link\Product\Collection;
use \Magento\GroupedProduct\Model\Product\Type\Grouped as TypeGrouped;

class Grouped
{
    /**
     * @param TypeGrouped $subject
     * @param Collection $result
     * @return Collection
     */
    public function afterGetAssociatedProductCollection(TypeGrouped $subject, Collection $result)
    {

        $result->addAttributeToSelect('small_image');
        $result->addAttributeToSelect('thumbnail');

        return $result;

    }
}

