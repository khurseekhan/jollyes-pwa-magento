/* ==========================================================================
 Scripts voor de frontend
 ========================================================================== */
require(['jquery','matchMedia'], function ($,mediaCheck) {
    $(function () {
        var leftVar = 0;
        var timeout;

        var showHideContainer = function(curEle){
            if(curEle.find("span.cat-main-title").hasClass("active")){
                curEle.find("span.cat-main-title").removeClass("active");
                curEle.parent().children("div.outer-container").hide();
                curEle.closest("div.filter-content").removeClass("hide-cat");
            }else{
                curEle.find("span.cat-main-title").addClass("active");
                curEle.parent().children("div.outer-container").show();
                curEle.closest("div.filter-content").addClass("hide-cat");
            }
        };

        $('.sidebar').on({
                mouseenter: function (e) {
                    var element = $(this);
                    if($(window).width() > 1023){
                        if (!this.entered) {
                            this.timeoutId && clearTimeout(this.timeoutId);
                            this.timeoutId = setTimeout(function(){
                                if(!element.find(">div").hasClass("inner-container")){
                                    return false;
                                }
                                element.find("div.inner-container:first").show();
                                element.siblings('li').children('div.inner-container').hide();

                                element.addClass('active');
                                element.siblings('li').removeClass('active');
                                element.siblings('li').find('li').removeClass('active');
                                element.siblings('li').find('div.inner-container').hide();
                            },300);
                            this.entered = true;
                        }

                    }
                },
                mouseleave: function (e) {
                    if($(window).width() > 1023){
                        this.entered = null;
                        this.timeoutId && clearTimeout(this.timeoutId);
                        var element = $(this);
                        this.timeoutId = setTimeout(function(){
                            element.find('div.inner-container').hide();
                            element.removeClass('active');
                            element.find('li').removeClass('active');
                        },500);
                    }
                }
            },".o-list li"
        );

        $(".sidebar").on({
            click: function(e) {
                if ($(window).width() < 1024) {
                    e.preventDefault();
                    var element = $(this);
                    if(!element.parent().find(">div").hasClass("inner-container")){
                        return false;
                    }
                    leftVar = leftVar - 100;
                    if($("body").hasClass("page-with-filter")){
                        element.closest('.c-sidebar--categories').addClass("hide-main");
                    }
                    element.closest('.outer-container').css({left: leftVar + "%"});

                    element.closest('ul').find('> li').hide();
                    element.parent().addClass("open").show();
                    element.closest('ul').css({'min-height':element.parent().find("div.inner-container:first").height()+'px'});

                    element.parent().find("div.inner-container:first").show();
                    element.parent().find("div.inner-container:first").children('ul').addClass("open");
                    element.parent().find("div.inner-container:first").children('ul').children(".cat-title").children("a").text((element.parent().children("a").text()));
                   // $(".outer-container").css({"height": element.parent().find("div.inner-container:first").children('ul.open').outerHeight(true)});
                }
            }

        },".o-list li.parent > span.expand" );

        $(document).on('click','.cat-title > a',function(e){
            if($(window).width() < 1024) {
                e.preventDefault();
                leftVar = leftVar + 100;
                element = $(this);
                element.parent().parent().removeClass("open");
                element.parent().parent().parent('.inner-container').hide();

                element.parent().parent().parent('.inner-container').closest('ul').find('>li').show();
                element.parent().parent().parent('.inner-container').closest('ul').css({'min-height':'auto'});

                //$(".outer-container").css({"height": element.closest("li.open").parent().outerHeight(true)});
                element.closest('.outer-container').css({left: leftVar + "%"});
                if(leftVar == 0 && $("body").hasClass("page-with-filter")){
                    element.closest('.c-sidebar--categories').removeClass("hide-main");
                }
            }
        });

        $(document).on('click','.c-sidebar--categories > h3',function(){
            if($(window).width() < 1024){
                showHideContainer($(this));
            }

            if($(window).width() >= 1024 && $("body").hasClass("page-with-filter")){
                showHideContainer($(this));
            }
        });

        function checkMenuContent(){
            setTimeout(function(){
                $(".sidebar.sidebar-main .c-sidebar .outer-container li.parent").each(function(){
                    if(!$(this).find("> div").hasClass("inner-container ")){
                        $(this).removeClass("parent");
                        $(this).find("span.expand").remove();
                    }
                });

                $("ul.o-list.main-list, ul.o-list.main-list ul").removeAttr("style").removeClass("open");
                $("ul.o-list.main-list li").removeClass("open").removeAttr("style");

            },1000);
        }

        mediaCheck({
            media: '(max-width: 1023px)',
            entry: function(){
                checkMenuContent();
                $('.c-sidebar h3:first-child .cat-main-title').addClass('active');
            },
            exit: function(){
                $(".outer-container").removeAttr("style");
                checkMenuContent();
                $('.c-sidebar h3:first-child .cat-main-title').removeClass('active');
            }
        });
    });
});
