<?php

/**
 * @category    Jjcommerce
 * @package     Jjcommerce_StoreLocator
 * @author	Jjcommerce Team<support@2jdesign.co.uk>
 */
namespace Jjcommerce\StoreLocator\Ui\DataProvider\Product\Form\Modifier;

use Magento\Catalog\Model\Locator\LocatorInterface;
use Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\AbstractModifier;
use Magento\Framework\Stdlib\ArrayManager;
use Magento\Ui\Component\Form\Field;
use Magento\Ui\Component\Form\Element\Input;

class Storelocator extends AbstractModifier
{

    protected $storeLocatorData;
    protected $_stores = null;

    const STORELOCATOR_AVAILABLE = 'storelocator';

    /**
     * @var LocatorInterface
     */
    protected $locator;

    /**
     * @var ArrayManager
     */
    protected $arrayManager;

    /**
     * @param LocatorInterface $locator
     * @param Data $data
     * @param ArrayManager $arrayManager
     */
    public function __construct(
        LocatorInterface $locator,
        \Jjcommerce\StoreLocator\Model\Storelocator $storelocator,
        \Jjcommerce\StoreLocator\Model\CataloginventoryStoreStock $cataloginventoryStoreStock,
        ArrayManager $arrayManager
    ) {
        $this->locator = $locator;
        $this->storeLocatorData = $cataloginventoryStoreStock;

        $collection = $storelocator->getCollection()
            ->addFieldToSelect( array('shop_id','name') )
            ->addFieldToFilter( 'status',1);
        foreach ($collection as $item) {
            $this->_stores[ $item->getShopId()] =   $item->getName();
        }

        $this->arrayManager = $arrayManager;
    }


    public function modifyMeta(array $meta)
    {
        $fields = array();

        foreach ($this->_stores as $shop_id => $name ){
            $storeName = str_replace( ' ','_',   strtolower($name)  );

            $fields[ $storeName ]  = array(
                'arguments' => array
                (
                    'data' => array
                    (
                        'config' => array
                        (
                            'dataType' => 'text',
                            'formElement' => Input::NAME,
                            'visible' => 1,
                            'disabled' => 1,
                            'required' => 0,
                            'notice' => '',
                            'default' => '',
                            'label' => __($name),
                            'code' => $storeName,
                            'source' => static::STORELOCATOR_AVAILABLE,
                            'scopeLabel' => 'GLOBAL',
                            'globalScope' => 1,
                          //  'validation' => array( 'validate-zero-or-greater' => 1 ),
                            'validation' => array( 'validate-number' => 1 ),
                            'sortOrder' => $shop_id,
                            'componentType' => Field::NAME,
                            'imports' => [
                                'handle'.$storeName.'Changes' => '${$.provider}:data.product.'.static::STORELOCATOR_AVAILABLE.'.'.$storeName
                            ],
                            'exports' => [
                                'productId' => '${ $.externalProvider }:params.current_product_id'
                            ],
                        )
                    )
                )
            );

        }

        $meta = $this->arrayManager->set(
            static::STORELOCATOR_AVAILABLE . '/arguments/data/config' ,
            $meta,
            [
                'formElement' => 'container',
                'componentType' => 'container',
                'breakLine' => '',
                'label' => __('Store Location Stock'),
                'required' => 0,
                'sortOrder' => 50,
                'scopeLabel' => 'WEBSITE',
                'dataScope' => 'data.product'

            ]);

        $meta = $this->arrayManager->set(
            static::STORELOCATOR_AVAILABLE . '/children/'.static::CONTAINER_PREFIX. static::STORELOCATOR_AVAILABLE ,
            $meta,
            [
                'children'  => array(
                    static::CONTAINER_PREFIX.static::STORELOCATOR_AVAILABLE => array(

                         'arguments' => array(
                             'data' => array
                             (
                                 'config' => array
                                 (
                                     'formElement' => 'container',
                                     'componentType' => 'container',
                                     'breakLine' => '',
                                     'label' => __('Store Location Stock'),
                                     'required' => 0,
                                     'sortOrder' => 0,
                                     'scopeLabel' => 'WEBSITE'
                                 )
                             )
                         ),
                         'children' => $fields
                     )
                ),
                'arguments' => array(

                    'data' => array
                    (
                        'config' => array
                        (
                            'componentType' => 'fieldset',
                            'label' => __(static::STORELOCATOR_AVAILABLE, $meta),
                            'collapsible' => 1,
                            'dataScope' => static::STORELOCATOR_AVAILABLE,
                            'sortOrder' => 80
                        )
                    )
                )
            ]
        );


        return $meta;
    }

    /**
     * {@inheritdoc}
     */
    public function modifyData(array $data)
    {
        $modelId = $this->locator->getProduct()->getId();
        $storeData = array();

        foreach($this->_stores as $shop_id => $name ){
            $storeName = str_replace( ' ','_',   strtolower($name )  );
            $storeData[$storeName] = 0.00;
        }

        if($modelId>0) {
            $collection = $this->storeLocatorData->getCollection()
                ->addFieldToSelect(array('store_id', 'stock'))
                ->addFieldToFilter('product_id', $modelId);

            if ($collection->count() ) {
                foreach ($collection as $stock) {
                    if(isset($this->_stores[ $stock->getStoreId() ])){
                        $storeName = str_replace( ' ','_',   strtolower($this->_stores[ $stock->getStoreId() ] )  );
                        $storeData[$storeName] = $stock->getStock();
                    }
                }
            }
        }

        $data[$modelId]['product'][static::STORELOCATOR_AVAILABLE] = $storeData;

        return $data;
    }

}