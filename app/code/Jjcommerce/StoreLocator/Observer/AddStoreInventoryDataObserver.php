<?php
/**
 * @category    Jjcommerce
 * @package     Jjcommerce_StoreLocator
 * @author	Jjcommerce Team<support@2jdesign.co.uk>
 */

namespace Jjcommerce\StoreLocator\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer as EventObserver;

class AddStoreInventoryDataObserver extends StockObserver implements ObserverInterface
{

    /**
     * Add store stock information to product
     *
     * @param EventObserver $observer
     * @return void
     */
    public function execute(EventObserver $observer)
    {
        $product = $observer->getEvent()->getProduct();

        //if ($product instanceof \Magento\Catalog\Model\Product &&  !$product->getIsStorelocator() ) {
        if ($product->getId()>0 &&  !$product->getIsStorelocator() ) {
            $collection = $this->storeLocatorData->getCollection()
                ->addFieldToSelect(array('store_id', 'stock'))
                ->addFieldToFilter('product_id', $product->getId());

            $data = array();

            if ($collection->count()) {
                foreach ($collection as $stock) {
                    if(isset($this->_stores[ $stock->getStoreId() ])){
                        $data[$this->_stores[$stock->getStoreId()]] = $stock->getStock();
                    }
                }
                $product->setStorelocator($data);
            }
            $product->setIsStorelocatorSet(true);
        }
        return $this;

    }
}
