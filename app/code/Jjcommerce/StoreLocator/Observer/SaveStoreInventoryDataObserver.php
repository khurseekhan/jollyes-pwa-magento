<?php
/**
 * @category    Jjcommerce
 * @package     Jjcommerce_StoreLocator
 * @author	Jjcommerce Team<support@2jdesign.co.uk>
 */

namespace Jjcommerce\StoreLocator\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer as EventObserver;

class SaveStoreInventoryDataObserver extends StockObserver implements ObserverInterface
{
    protected $_request = null;

    /**
     * @param \Magento\CatalogInventory\Helper\Stock $stockHelper
     */
    public function __construct(
        \Jjcommerce\StoreLocator\Model\Storelocator $storelocator,
        \Jjcommerce\StoreLocator\Model\CataloginventoryStoreStock $cataloginventoryStoreStock,
         \Magento\Framework\App\RequestInterface $request )
    {
        $this->_request = $request;
        parent::__construct($storelocator,$cataloginventoryStoreStock);
    }

    /**
     * Saving product inventory data. Product qty calculated dynamically.
     *
     * @param EventObserver $observer
     * @return $this
     */
    public function execute(EventObserver $observer)
    {
        $product = $observer->getEvent()->getProduct();

        if ($product->getId() === null) {
            return $this;
        }

        $this->saveStoreStockData($product);
        return $this;
    }

    protected function resetStores()
    {
        $data = array();
        foreach ($this->_stores as $storeId => $storename ) {
          $data[ $storename ] = $storeId;
        }

        return $data;
    }

    /**
     * Prepare stock item data for save
     *
     * @param \Magento\Catalog\Model\Product $product
     * @return $this
     */
    protected function saveStoreStockData($product)
    {
       $productId = $product->getId();
       $fdata = array();

       $stores = $this->resetStores();
       $sdata =  $this->_request->getPostValue();

        if(!isset($sdata['product']['storelocator'])){
            return $this;
        }

       $data = $sdata['product']['storelocator'];

       foreach ($data as $store => $stock) {
           if(isset($stores[$store])) {
               $fdata[$stores[$store]] = $stock;
           }
       }

        $this->storeLocatorData->saveStoreStock($product,$fdata);
        return $this;
    }

}
