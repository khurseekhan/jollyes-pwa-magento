<?php
/**
 * @category    Jjcommerce
 * @package     Jjcommerce_StoreLocator
 * @author	Jjcommerce Team<support@2jdesign.co.uk>
 */

namespace Jjcommerce\StoreLocator\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer as EventObserver;

class StockObserver
{
    /**
     * @var \Magento\CatalogInventory\Helper\Stock
     */
    protected $stockHelper;

    protected $storeLocatorData;
    protected $_stores = null;

    /**
     * @param \Magento\CatalogInventory\Helper\Stock $stockHelper
     */
    public function __construct(
        \Jjcommerce\StoreLocator\Model\Storelocator $storelocator,
        \Jjcommerce\StoreLocator\Model\CataloginventoryStoreStock $cataloginventoryStoreStock )
    {
        $this->storeLocatorData = $cataloginventoryStoreStock;

        $collection = $storelocator->getCollection()
            ->addFieldToSelect( array('shop_id','name') )
            ->addFieldToFilter( 'status',1);
        foreach ($collection as $item) {
            $storeName = str_replace( ' ','_',   strtolower($item->getName() ) );
            $this->_stores[ $item->getShopId()] =   $storeName;
        }
    }


}
