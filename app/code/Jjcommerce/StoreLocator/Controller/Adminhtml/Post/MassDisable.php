<?php
/**
 * @category    Jjcommerce
 * @package     Jjcommerce_StoreLocator
 * @author	Jjcommerce Team<support@2jdesign.co.uk>
 */
namespace Jjcommerce\StoreLocator\Controller\Adminhtml\Post;

use Jjcommerce\StoreLocator\Controller\Adminhtml\AbstractMassStatus;

/**
 * Class MassDisable
 */
class MassDisable extends AbstractMassStatus
{
    /**
     * Field id
     */
    const ID_FIELD = 'id';

    /**
     * Resource collection
     *
     * @var string
     */
    protected $collection = 'Jjcommerce\StoreLocator\Model\ResourceModel\Storelocator\Collection';

    /**
     * Page model
     *
     * @var string
     */
    protected $model = 'Jjcommerce\StoreLocator\Model\StoreLocator';

    /**
     * Page disable status
     *
     * @var boolean
     */
    protected $status = false;
}
