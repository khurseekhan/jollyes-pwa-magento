<?php
/**
 * @category    Jjcommerce
 * @package     Jjcommerce_StoreLocator
 * @author	Jjcommerce Team<support@2jdesign.co.uk>
 */
namespace Jjcommerce\StoreLocator\Controller\Adminhtml\Post;

use Magento\Backend\App\Action;

class Edit extends \Magento\Backend\App\Action
{
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    protected $_session = null;

    protected $_storelocator = null;

    /**
     * @param Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Framework\Registry $registry
     */
    public function __construct(
        Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Registry $registry,
        \Jjcommerce\StoreLocator\Model\Storelocator $storelocator
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->_coreRegistry = $registry;
        $this->_session  = $context->getSession();
        $this->_storelocator = $storelocator;

        parent::__construct($context);
    }


    /**
     * Init actions
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    protected function _initAction()
    {
        // load layout, set active menu and breadcrumbs
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Jjcommerce_StoreLocator::post')
            ->addBreadcrumb(__('Storelocator'), __('Storelocator'))
            ->addBreadcrumb(__('Manage Storelocator'), __('Manage Storelocator'));
        return $resultPage;
    }

    /**
     * Edit Storelocator post
     *
     * @return \Magento\Backend\Model\View\Result\Page|\Magento\Backend\Model\View\Result\Redirect
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');

        if ($id) {
            $this->_storelocator->load($id);
            if (!$this->_storelocator->getId()) {
                $this->messageManager->addErrorMessage(__('This Store no longer exists.'));
                /** \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();

                return $resultRedirect->setPath('*/*/');
            }
        }

        $data = $this->_session->getFormData(true);
        if (!empty($data)) {
            $this->_storelocator->setData($data);
        }

        $this->_coreRegistry->register('store_info', $this->_storelocator);

        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->_initAction();
        $resultPage->addBreadcrumb(
            $id ? __('Edit Storelocator') : __('New Storelocator'),
            $id ? __('Edit Storelocator') : __('New Storelocator')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('StoreLocator'));
        $resultPage->getConfig()->getTitle()
            ->prepend($this->_storelocator->getId() ? $this->_storelocator->getTitle() : __('StoreLocator'));
        return $resultPage;
    }
    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Jjcommerce_StoreLocator::save');
    }

}

