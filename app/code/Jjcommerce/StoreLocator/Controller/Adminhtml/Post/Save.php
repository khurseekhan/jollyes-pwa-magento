<?php
/**
 * @category    Jjcommerce
 * @package     Jjcommerce_StoreLocator
 * @author	Jjcommerce Team<support@2jdesign.co.uk>
 */
namespace Jjcommerce\StoreLocator\Controller\Adminhtml\Post;

use Magento\Backend\App\Action;
use Magento\TestFramework\ErrorLog\Logger;
use Magento\Framework\Filesystem;
use Magento\Framework\App\Filesystem\DirectoryList;

class Save extends \Magento\Backend\App\Action
{
    protected $_fileUploaderFactory;

    protected $_mediaDirectory;

    protected $request;

    /**
     * @param Action\Context $context
     */
    public function __construct(Action\Context $context,
                                \Jjcommerce\StoreLocator\Model\Storelocator $storelocator,
                                Filesystem $filesystem,
                                \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory,
                                \Magento\Framework\App\Request\Http $request
    )
    {
        $this->request = $request;

        $this->_mediaDirectory = $filesystem->getDirectoryWrite(DirectoryList::MEDIA);
        $this->_fileUploaderFactory = $fileUploaderFactory;
        $this->_filesystem = $filesystem;

        $this->storeLocator = $storelocator;
        parent::__construct($context);
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Jjcommerce_StoreLocator::save');
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {

        $data = $this->getRequest()->getParams();

       // $id =  $data['shop_id'];

        $resultRedirect = $this->resultRedirectFactory->create();
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */

        if ($data) {

            /** @var \Jjcommerce\StoreLocator\Model\StoreLocator $model */


           // $id = $this->getRequest()->getParam('id');

            if (isset($data['shop_id'])) {
                $this->storeLocator->load($data['shop_id']);

            }
            $this->storeLocator->setData($data);

            try {

                $this->getImage('image','storelocator/image');
                $this->getImage('hover_image','storelocator/hoverimage');

                $this->_eventManager->dispatch(
                    'storelocator_post_prepare_save',
                    ['post' =>  $this->storeLocator, 'request' => $this->getRequest()]
                );
                $this->storeLocator->save();
                $this->messageManager->addSuccess(__('You saved this Store.'));
                $this->_objectManager->get('Magento\Backend\Model\Session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['id' =>  $this->storeLocator->getId(), '_current' => true]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the Store.'));
            }

            $this->_getSession()->setFormData($data);
            return $resultRedirect->setPath('*/*/edit', ['id' => $this->getRequest()->getParam('id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }

    public function getImage($name,$path){

        $file = $this->getRequest()->getFiles($name);
        $target = $this->_filesystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath($path);
        $data = $this->getRequest()->getParams();

        if($name=='image') {
            if (!array_key_exists('image',$data) && $file['name']==''){
                throw new \Magento\Framework\Exception\LocalizedException(
                    __('Please upload store image')
                );
                return $resultRedirect->setPath('*/*/edit', ['id' => $this->getRequest()->getParam('id')]);
            }
        }

        if (isset($data[$name]['delete'])) {
            $this->storeLocator->setData($name,'');
        } else if($file['name']!=null) {
            $fileName = $file['name'];
            $image = $path.$fileName;
            /** @var $uploader \Magento\MediaStorage\Model\File\Uploader */
            $uploader = $this->_fileUploaderFactory->create(
                ['fileId' => $name]
            );
            $uploader->setAllowedExtensions(
                ['jpg', 'jpeg', 'gif', 'png']
            );
            $uploader->setAllowRenameFiles(true);
            $uploader->setFilesDispersion(true);
            $uploader->save($target);

            $this->storeLocator->setData($name,$path.$uploader->getUploadedFileName());
        }else if($this->getRequest()->getParam($name)['value']){
            
            $this->storeLocator->setData($name,$this->getRequest()->getParam($name)['value']);
        }
    }

}
