<?php
/**
 * @category    Jjcommerce
 * @package     Jjcommerce_StoreLocator
 * @author	Jjcommerce Team<support@2jdesign.co.uk>
 */

namespace Jjcommerce\StoreLocator\Controller\Product;

use Magento\Framework\Exception\NotFoundException;
use Magento\Framework\App\RequestInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\Registry;


class StoreStockInfo extends \Magento\Framework\App\Action\Action
{
    /**
     * @var Registry
     */
    protected $registry;


    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $collectionFactory,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Framework\Controller\Result\JsonFactory $jsonResultFactory,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Catalog\Model\ProductRepository $productRepository
    )
    {
        $this->productFactory = $productFactory;
        $this->_productRepository = $productRepository;
        $this->collectionFactory = $collectionFactory;
        $this->jsonResultFactory = $jsonResultFactory;
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }


    /**
     * Show store Information
     *
     */
    public function execute()
    {
        $productId = $this->getRequest()->getParam('simple_id');
        $product = $this->_productRepository->getById($productId);

        $databoxes = $this->_view->getLayout()
            ->createBlock('Magento\Framework\View\Element\Template')
            ->setProduct($product)
            ->setTemplate('Jjcommerce_StoreLocator::product/view/store_stock.phtml')
            ->toHtml();
        $result = $this->jsonResultFactory->create();
        $result->setData($databoxes);
        $result->setHeader('Content-type', 'application/json', true);
        return $result;

    }

}
