<?php
/**
 * @category    Jjcommerce
 * @package     Jjcommerce_StoreLocator
 * @author	Jjcommerce Team<support@2jdesign.co.uk>
 */

namespace Jjcommerce\StoreLocator\Controller\Product;

use Magento\Framework\Exception\NotFoundException;
use Magento\Framework\App\RequestInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\Registry;


class GroupStoreStock extends \Magento\Framework\App\Action\Action
{
    /**
     * @var Registry
     */
    protected $registry;


    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $jsonResultFactory,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Jjcommerce\StoreLocator\Helper\Data $storeHelper
    )
    {
        $this->_productRepository = $productRepository;
        $this->jsonResultFactory = $jsonResultFactory;
        $this->resultPageFactory = $resultPageFactory;
        $this->helper = $storeHelper;
        parent::__construct($context);
    }


    /**
     * Show store Information
     *
     */
    public function execute()
    {
        $message = array();
        $productIds = $this->getRequest()->getParam('product');
        $store = strtolower($this->getRequest()->getParam('store'));
        $lowStock = $this->helper->lowStockMessage();

        foreach($productIds as $id){
            $product = $this->_productRepository->getById($id);
            $storeInfo = $product->getStorelocator();
            if(isset($storeInfo[$store])){
            $stock = $storeInfo[$store];
            if($stock > 0){
                if($stock <= $lowStock){
                    $message[$id] = 'Low Stock';
                }else{
                    $message[$id] = 'In Stock';
                }
            }else{
                $message[$id] = 'Out Of Stock';
            }
            }else{
                $message[$id] = '';
            }

        }

        $result = $this->jsonResultFactory->create();
        $result->setData($message);
        $result->setHeader('Content-type', 'application/json', true);
        return $result;

    }

}
