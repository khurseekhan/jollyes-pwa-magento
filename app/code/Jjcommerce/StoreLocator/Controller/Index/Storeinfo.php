<?php
/**
 * @category    Jjcommerce
 * @package     Jjcommerce_StoreLocator
 * @author	Jjcommerce Team<support@2jdesign.co.uk>
 */

namespace Jjcommerce\StoreLocator\Controller\Index;

use Magento\Framework\Exception\NotFoundException;
use Magento\Framework\App\RequestInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\Registry;


class Storeinfo extends \Magento\Framework\App\Action\Action
{
    /**
     * @var Registry
     */
    protected $registry;


    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        Registry $registry
    )
    {
        $this->registry = $registry;
        parent::__construct($context);

    }


    /**
     * Show store Information
     *
     * @return void
     */
    public function execute()
    {

        $id = $this->_request->getParam('id',0 );
        if($id>0){

            $this->registry->register('LOCATOR', $id);
            $this->_view->loadLayout();
            $this->_view->renderLayout();
            return;
        }

        throw new NotFoundException(__('Page not found.'));
    }
}
