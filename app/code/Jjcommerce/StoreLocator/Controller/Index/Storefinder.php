<?php
/**
 * @category    Jjcommerce
 * @package     Jjcommerce_StoreLocator
 * @author	Jjcommerce Team<support@2jdesign.co.uk>
 */

namespace Jjcommerce\StoreLocator\Controller\Index;

use Magento\Framework\Exception\NotFoundException;
use Magento\Framework\App\RequestInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\Registry;


class Storefinder extends \Magento\Framework\App\Action\Action
{
    /**
     * @var Registry
     */
    protected $registry;

    protected $helper;
    protected $storeLocator;

    /**
     * @var \Magento\Framework\Session\SessionManagerInterface
     */
    protected $session;

    /**
     * @var \Magento\Directory\Model\CountryFactory
     */
    protected $_countryFactory;


    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Jjcommerce\StoreLocator\Helper\Data $helper,
        Registry $registry,
        \Jjcommerce\StoreLocator\Model\Storelocator $storeLocator,
        \Magento\Framework\Session\SessionManagerInterface $session,
        \Magento\Directory\Model\CountryFactory $countryFactory
    )
    {
        $this->registry = $registry;
        $this->helper = $helper;
        $this->storeLocator = $storeLocator;
        $this->session = $session;
        $this->_countryFactory = $countryFactory;
        parent::__construct($context);

    }

    /**
     * Show store Information
     *
     * @return void
     */
    public function execute()
    {
        $postData = $this->_request->getPost();
        if (isset($postData) && !empty($postData)) {
            if (isset($postData['address']) && !empty($postData['address']) && isset($postData['radius']) && !empty($postData['radius'])) {
                $defaultCountryCode = $this->helper->getDefaultCountry();
                $address = explode (',',$postData['address']);
                $defaultCountryName = $this->_countryFactory->create()->loadByCode($defaultCountryCode)->getName();

                if(is_array($address) && count($address)== 1) {
                    $address[1] = ' ' . $defaultCountryName;
                    $postData['address'] = implode(',', $address);

                }

                $userLatitude = (double)$this->helper->latitude($postData['address']);
                $userLongitude = (double)$this->helper->longitude($postData['address']);

                $radius = (int)$postData['radius'];

                $userSearchArray = array();
                array_push($userSearchArray, $userLatitude, $userLongitude, $radius);

                //$this->log(__FILE__);
                //$this->log(__LINE__);
                //$this->log($userSearchArray);
                //$this->log("userLatitude".$userLatitude);
                $isLatitude = (int)trim($this->helper->latitude($postData['address']));
                //$this->log("isLatitude".$isLatitude);
                if ($isLatitude == 0) {
                  //  $userLatitude = (double)$this->helper->latitude($postData['address']);
                  //  $userLongitude = (double)$this->helper->longitude($postData['address']);
                  //  $postData['radius'] = 100;
                    $this->messageManager->addError(__('Sorry, no store found in specified location.'));
                    $this->_redirect('locator');
                    return ;
                }

                //$this->log("radius".$postData['radius']);
                //$this->log(__LINE__);
                $this->session->start();
                $this->session->setSearchData($userSearchArray);
                //$this->log(__LINE__);
                $this->_redirect('locator');
                //$this->log(__LINE__);
                return;
            } else {
                $this->messageManager->addError(__('Please enter valid data .'));
                $this->_redirect('locator');
            }
        } else {
            $this->messageManager->addError($this->__('Sorry, no store data found in specified location.'));
            $this->_redirect('locator');
        }
        $this->_redirect('locator');
        return;
    }



    public function log($message){
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/storefinder.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info($message);
    }
}
