<?php
/*
* @author      Jjcommerce Team (support@jjcommerce.com)
* @copyright   Copyright (c) 2016 Jjcommerce (http://www.jjcommerce.com)

* @category    Jjcommerce
* @package     Jjcommerce_StoreLocator
*/
namespace Jjcommerce\StoreLocator\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Upgrade the CatalogRule module DB scheme
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * {@inheritdoc}
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '0.0.2', '<')) {
            $this->addFlatData($setup);
        }

        if (version_compare($context->getVersion(), '2.0.2', '<')) {
            $this->storeFields($setup);
        }

        if (version_compare($context->getVersion(), '2.0.3', '<')) {
            $this->storeNewFields($setup);
        }

        if (version_compare($context->getVersion(), '2.0.4', '<')) {
            $this->storeFieldsUpdate($setup);
        }

        if (version_compare($context->getVersion(), '2.0.5', '<')) {
            $this->newStoreFieldsUpdate($setup);
        }

        $setup->endSetup();
    }

    public function addFlatData($setup)
    {
        if (!$setup->tableExists('jj_store_stock')) {
            $table  = $setup->getConnection()
                ->newTable($setup->getTable('jj_store_stock'))
                ->addColumn(
                    'id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                    'Id'
                )->addColumn(
                    'product_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    12,
                    [],
                    'Product id'
                )->addColumn(
                    'store_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    12,
                    [],
                    'Store id'
                )->addColumn(
                    'stock',
                    \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                    "12,4",
                    ['default' => 0.00],
                    'stock'
                );
            $setup->getConnection()->createTable($table);

        }

    }

    public function storeFields($setup){
        //if ($setup->tableExists('jj_storelocator')) {

        $installer = $setup;
        $installer->startSetup();
        $tableAdmins = $installer->getTable('jj_storelocator');

        $installer->getConnection()->addColumn(
            $tableAdmins,
            'service_flag',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                'nullable' => true,
                'default' => 0,
                'comment' => 'Service Flag'
            ]
        );
        $installer->getConnection()->addColumn(
            $tableAdmins,
            'hover_image',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'comment' => 'Store Image (hover)'
            ]
        );
    }

    public function storeNewFields($setup){
        //if ($setup->tableExists('jj_storelocator')) {

        $installer = $setup;
        $installer->startSetup();
        $tableAdmins = $installer->getTable('jj_storelocator');

        $installer->getConnection()->addColumn(
            $tableAdmins,
            'direction_link',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'nullable' => true,
                'comment' => 'Direction Link'
            ]
        );
    }

    public function storeFieldsUpdate($setup){
        //if ($setup->tableExists('jj_storelocator')) {

        $installer = $setup;
        $installer->startSetup();
        $tableAdmins = $installer->getTable('jj_storelocator');

        $installer->getConnection()->modifyColumn(
            $tableAdmins,
            'phone',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'nullable' => false,
                'length' => '255',
                'comment' => 'Phone'

            ]
        );

        $installer->getConnection()->modifyColumn(
            $tableAdmins,
            'image',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'nullable' => false,
                'comment' => 'Store Image',
            ]
        );
    }

    /**
     * Add new column
     *
     * @param $setup
     */
    public function newStoreFieldsUpdate($setup){

        $installer = $setup;
        $installer->startSetup();
        $tableAdmins = $installer->getTable('jj_storelocator');

        $installer->getConnection()->addColumn(
            $tableAdmins,
            'store_code',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'nullable' => false,
                'length' => '255',
                'comment' => 'Store Code'

            ]
        );
    }


}
