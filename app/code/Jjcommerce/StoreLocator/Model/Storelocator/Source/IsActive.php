<?php
/**
 * @category    Jjcommerce
 * @package     Jjcommerce_StoreLocator
 * @author	Jjcommerce Team<support@2jdesign.co.uk>
 */
namespace Jjcommerce\StoreLocator\Model\Storelocator\Source;

class IsActive implements \Magento\Framework\Data\OptionSourceInterface
{
    /**
     * @var \Oscprofessionals\StoreLocator\Model\Storelocator
     */
    protected $storelocator;

    /**
     * Constructor
     *
     * @param \Oscprofessionals\StoreLocator\Model\StoreLocator $storelocator
     */
    public function __construct(\Jjcommerce\StoreLocator\Model\Storelocator $storelocator)
    {
        $this->storelocator = $storelocator;
    }

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options[] = ['label' => '', 'value' => ''];
        $availableOptions =  array('1'=> 'Enable', '0'=> 'Disable' );   //$this->storelocator->getAvailableStatuses();
        foreach ($availableOptions as $key => $value) {
            $options[] = [
                'label' => $value,
                'value' => $key,
            ];
        }
        return $options;
    }
}
