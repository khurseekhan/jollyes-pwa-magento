<?php
/**
 * Copyright © 2015 Jjcommerce. All rights reserved.
 */

namespace Jjcommerce\StoreLocator\Model;


class Storelocator extends \Magento\Framework\Model\AbstractModel
{

    
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []    
    ){
       parent::__construct($context,$registry,$resource,$resourceCollection,$data);
    }
     
     /**
     * Constructor
     *
     * @return void
     */
    
    protected function _construct()
    {
        parent::_construct();
        $this->_init('Jjcommerce\StoreLocator\Model\ResourceModel\Storelocator');
    }

    public function getStockByProductId($product){
        return   $this->getResource()->getStockByProductId($product);
    }

    public function countProductWithStorePickupOnly($productIds,$storeId){

        if(empty($productIds ))return 0;

        return   $this->getResource()->countProductWithStorePickupOnly($productIds,$storeId);

    }

}
