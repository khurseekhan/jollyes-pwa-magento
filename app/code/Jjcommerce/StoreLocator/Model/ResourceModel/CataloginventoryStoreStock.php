<?php
/**
 * Copyright © 2015 Jjcommerce. All rights reserved.
 */

namespace Jjcommerce\StoreLocator\Model\ResourceModel;


class CataloginventoryStoreStock extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    protected $productId = null;
    /**
     * Initialize resources
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('jj_store_stock', 'id');
    }


    public function saveStoreStock($product,$data)
    {
       if(!$product->getId() || !is_array($data) || empty($data) ){
           return;
       }

        $this->productId = $product->getId();
        $totalStock = 0.00;
       foreach($data as $shop_id => $stock ) {

           // =======================================================
           $select = $this->getConnection()->select()->from(
               $this->getMainTable(),
               ['id']
           )->where('product_id=' . $product->getId())
               ->where('store_id=' . $shop_id);

           $item_id = $this->getConnection()->fetchOne($select);

           unset($select);

           if ((int)$item_id > 0) {

               $this->getConnection()->update($this->getMainTable(),
                   array('stock' => $stock),
                   'product_id=' . $product->getId() . ' and store_id =' . $shop_id);
           } else {

               $dataInsert = array(
                   'stock' => $stock,
                   'product_id' => $product->getId(),
                   'store_id' => $shop_id
               );
               $this->getConnection()->insert($this->getMainTable(), $dataInsert);
           }
           // =======================================================

           $totalStock += $stock;
       }

      // if($totalStock>0.00){
       //    $this->updateMainInventory($totalStock);
     //  }

    }

    protected function updateMainInventory($stock){

        $inventoryStockItemtable = $this->getTable('cataloginventory_stock_item');

        if($stock>0)$stockStatus = 1;
        else $stockStatus = 0;

        $select = $this->getConnection()->select()->from(
            $inventoryStockItemtable,
            ['item_id']
        )->where('product_id='.$this->productId);

        $item_id = $this->getConnection()->fetchOne($select);
        if((int)$item_id>0) {
            $this->getConnection()->update($inventoryStockItemtable , array('qty'=> $stock,'is_in_stock' => $stockStatus), 'product_id='.$this->productId );
        }
        else{
            $dataInsert = array(
                'qty' => $stock,
                'product_id' => $this->productId,
                'stock_id' => 1,
                'is_in_stock' => $stockStatus
            );

            $this->getConnection()->insert($inventoryStockItemtable, $dataInsert );
        }

        // ========================================= //

        $inventoryStockStatustable = $this->getTable('cataloginventory_stock_status');
        $select = $this->getConnection()->select()->from(
            $inventoryStockStatustable,
            ['product_id']
        )->where('product_id='.$this->productId);

        $product_id = $this->getConnection()->fetchOne($select);

        if((int)$product_id>0) {
            $this->getConnection()->update($inventoryStockStatustable , array('qty'=> $stock,'stock_status'=> $stockStatus), 'product_id='.$this->productId );
        }
        else{
            $dataInsert = array(
                'qty' => $stock,
                'product_id' => $this->productId,
                'stock_id' => 1,
                'website_id' => 0,
                'stock_status' => $stockStatus
            );

            $this->getConnection()->insert($inventoryStockStatustable, $dataInsert );
        }
    }

    public function log($message) {
        //\Zend\Log\Logger::INFO

        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/cataloginventoryStoreStock.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info($message);
    }

}
