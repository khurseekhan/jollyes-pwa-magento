<?php
/**
 * Copyright © 2015 Jjcommerce. All rights reserved.
 */

namespace Jjcommerce\StoreLocator\Model\ResourceModel;
use \Jjcommerce\ClickAndCollect\Setup\UpgradeData;
use Jjcommerce\ClickAndCollect\Helper\Data;

class Storelocator extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    private $_BackendType = null;

    /**
     * @var Data
     */
    protected $dataHelper;

    /**
     * Storelocator constructor.
     * @param \Magento\Framework\Model\ResourceModel\Db\Context $context
     * @param null $connectionName
     * @param Data $dataHelper
     */
    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context,
        $connectionName = null,
        Data $dataHelper
    ) {
       $this->dataHelper =  $dataHelper;
        parent::__construct($context, $connectionName);
    }

    /**
     * Initialize resources
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('jj_storelocator', 'shop_id');
    }

    public function getStockByProductId($product){

        $stocktable =  $this->getTable('jj_store_stock');
        $standardDeliveryEnable = $this->dataHelper->isStandardCollectOptionEnabled();

        $standardCond =  (!$standardDeliveryEnable)? 'store.status=1 AND stock.stock > 0' :'store.status=1';
        $query = $this->getConnection()->select()->from(
            ['store' => $this->getMainTable()],
            ['shop_id', 'name']
        )->joinLeft(
            ['stock' => $stocktable],
            'stock.store_id=store.shop_id  and stock.product_id='.$product->getId(),
            ['stock']
        )->where(
            $standardCond
        );

        return $this->getConnection()->fetchAll($query);
    }

    public function countProductWithStorePickupOnly($productIds,$storeId=0){

     /*
      * SELECT `main_table`.*, `tdv`.`value` AS `default_value`, `tsv`.`value` AS `store_default_value`,
      * IF(tsv.value_id > 0, tsv.value, tdv.value) AS `value`

FROM `eav_attribute_option` AS `main_table`
 INNER JOIN `eav_attribute_option_value` AS `tdv` ON tdv.option_id = main_table.option_id
  LEFT JOIN `eav_attribute_option_value` AS `tsv` ON tsv.option_id = main_table.option_id AND tsv.store_id = '0'

WHERE (`attribute_id` = '291') AND (tdv.store_id = 0) ORDER BY main_table.sort_order ASC, value ASC
      */

       $deliveryTypeId = $this->getAttributeDeliveryType();

        $option =  $this->getTable('eav_attribute_option');
        $optionValue =  $this->getTable('eav_attribute_option_value');

        $query = $this->getConnection()->select()->from(
            ['main_table' => $option]
        )->joinInner(
            ['tdv' => $optionValue],
            'tdv.option_id = main_table.option_id',
            [ 'default_value' => 'value', 'default_value_id' => 'value_id' ]
        )->joinLeft(
            ['tsv' => $optionValue],
            'tsv.option_id = main_table.option_id AND tsv.store_id = '.$storeId,
            [ 'store_default_value' => 'value', 'store_default_value_id' => 'value_id' ]
        )->where( 'attribute_id= '.$deliveryTypeId  )
            ->where( 'tdv.store_id = 0 '  )->order( ['main_table.sort_order','tdv.value'  ] );

        //$this->log( __line__.'===='.(string)$query );
        $optionsData = $this->getConnection()->fetchAll($query);

        //$this->log( __line__.'===='.print_r($optionsData,1) );

        $optionsData = $this->checkLabelForStoreOnly($optionsData);

        //$this->log( __line__.'===='.print_r($optionsData,1) );

        /*
         * SELECT e.*, delivery_type.value
         * FROM `catalog_product_entity` as e
           Inner join `catalog_product_entity_varchar` as delivery_type on ( e.row_id=delivery_type.row_id and delivery_type.attribute_id=291 )
           where e.entity_id in( 2049,1)
         */

        $entity =  $this->getTable('catalog_product_entity');
        $delivery_type =  $this->getTable('catalog_product_entity_'.$this->_BackendType);

        $query = $this->getConnection()->select()->from(
            ['main_table' => $entity]
        )->joinInner(
            ['tdv' => $delivery_type],
            'tdv.value_id = main_table.entity_id  AND tdv.attribute_id='.$deliveryTypeId,
            [ 'delivery_type' => 'value']
        )->where( 'main_table.entity_id IN('.implode(',',$productIds).'  )'  );

        //$this->log( __line__.'===='.(string)$query );
        $productData = $this->getConnection()->fetchAll($query);
        //$this->log( __line__.'===='.print_r($productData,1) );
        $countStoreOnlyProduct = 0;
        foreach( $productData as $prow ){
            if(!empty($optionsData) ) {
                foreach ($optionsData as $orow) {
                    if ($orow['option_id'] == $prow['delivery_type'] &&
                        $this->processLabelStorepickupOnly( $orow['default_value'] ) == $this->processLabelStorepickupOnly( UpgradeData::STOREPICKUPONLY) ){
                        $countStoreOnlyProduct++;
                    }
                }
            }
        }
        //$this->log( __line__.'===='.$countStoreOnlyProduct );
         return $countStoreOnlyProduct;

    }

    private function checkLabelForStoreOnly($optionsData)
    {
        $optionValue =  $this->getTable('eav_attribute_option_value');

        foreach ($optionsData as $index => $orow) {
            if ( $this->processLabelStorepickupOnly( $orow['default_value'] ) == $this->processLabelStorepickupOnly( 'In Store only' )  ){

                $valueIds = '';

                if( (int) $orow['store_default_value_id']>0 ) $valueIds .= $orow['store_default_value_id'];
                if( (int) $orow['default_value_id']>0 ) $valueIds .=  ','.$orow['default_value_id'];

                //$this->log( __line__.'==='.$valueIds );

                $this->getConnection()->update($optionValue ,
                    array('value'=> UpgradeData::STOREPICKUPONLY ),
                    'value_id IN ('. trim($valueIds,',').' )' );
                $optionsData[$index]['default_value'] = UpgradeData::STOREPICKUPONLY;
            }
        }

       return  $optionsData;
    }

    protected function processLabelStorepickupOnly($string){
        return str_replace( ' ','', strtolower($string) );

    }
    protected function getAttributeDeliveryType(){

        $entityTypetable = $this->getTable('eav_entity_type');
        $select = $this->getConnection()->select()->from(
            $entityTypetable,
            ['entity_type_id']
        )->where('entity_type_code="catalog_product"');

        $entityTypeId =  $this->getConnection()->fetchOne($select);

        // ============== get brand attribute id ==========================

        $attributetable = $this->getTable('eav_attribute');
        $select = $this->getConnection()->select()->from(
            $attributetable,
            ['attribute_id','backend_type']
        )->where('entity_type_id='.$entityTypeId.' and attribute_code="'. UpgradeData::SCHEMA_ATTRIBUTE.'" ' );

        $data = $this->getConnection()->fetchAll($select);

        if(empty( $data ))return null;

        $this->_BackendType = $data[0]['backend_type'];

        return   $data[0]['attribute_id']; // $this->getConnection()->fetchAll($select); // 291

    }

    public function log($message) {
        //\Zend\Log\Logger::INFO

        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/countProductWithStorePickupOnly.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info($message);
    }

}
