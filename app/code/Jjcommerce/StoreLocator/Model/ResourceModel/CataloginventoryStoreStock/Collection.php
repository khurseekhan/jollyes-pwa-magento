<?php
/**
 * Copyright © 2015 Jjcommerce. All rights reserved.
 */

namespace Jjcommerce\StoreLocator\Model\ResourceModel\CataloginventoryStoreStock;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Jjcommerce\StoreLocator\Model\CataloginventoryStoreStock',
                     'Jjcommerce\StoreLocator\Model\ResourceModel\CataloginventoryStoreStock');
    }
}
