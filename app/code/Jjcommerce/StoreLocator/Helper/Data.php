<?php

/**
 * @category    Jjcommerce
 * @package     Jjcommerce_StoreLocator
 * @author	Jjcommerce Team<support@2jdesign.co.uk>
 */

namespace Jjcommerce\StoreLocator\Helper;

use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\StoreManagerInterface;


class Data extends \Magento\Framework\App\Helper\AbstractHelper {

    /*
     * Initializing Constants
     */
    const XML_PATH_TIMING_MON_THU = 'storelocator/opening_hrs/mon_thu';
    const XML_PATH_TIMING_FRI_SAT = 'storelocator/opening_hrs/fri_sat';
    const XML_PATH_TIMING_SUNDAY = 'storelocator/opening_hrs/sun';

    /**
     * Special Price SAVE tag Image
     */
    const XML_PATH_PET_CLINIC = 'storelocator/general/petclinic';
    const XML_PATH_PET_ZONE =   'storelocator/general/petzone';
    const XML_PATH_API_SENSOR = 'storelocator/general/apisensor';
    const XML_PATH_DIRECTION = 'storelocator/general/direction';
    const XML_PATH_APIURL = 'storelocator/general/apiurl';
    const XML_PATH_APIKEY = 'storelocator/general/apikey';

    const IMAGES_DIR = '/storelocator/service_flag/';

    /**
     * Notify stock config path
     */
    const XML_PATH_NOTIFY_STOCK_QTY = 'cataloginventory/item_options/notify_stock_qty';

    const XML_PATH_DEFAULT_COUNTRY = 'general/country/default';

    /**
     * __construct
     * @param Context $context
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        Context $context,
        StoreManagerInterface $storeManager
    )
    {
        $this->_scopeConfig  = $context->getScopeConfig();
        $this->_storeManager = $storeManager;

        parent::__construct($context);
    }


    /*
     * @return Latitude
     */
    public function latitude($address) {
       // $request_url = "http://maps.googleapis.com/maps/api/geocode/xml?key=AIzaSyDp0fh9i9x_rBbjosDyJrvtS2dk0a6yoaE&address=" . $address . "&sensor=true";
      //  $xml = simplexml_load_file($request_url) or die("url not loading");
        $geo = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?key='.$this->getApiKey().'&address=' . urlencode($address) . '&sensor=true');
        $geo = json_decode($geo, true);
        if ($geo['status'] = 'OK') {
            if (is_array($geo['results']) && !empty($geo['results'])) {
                $latitude = $geo['results'][0]['geometry']['location']['lat'];
                if (!empty($latitude)) {
                    return $latitude;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }


       /* if ($xml) {
            $status = $xml->status;
            if ($status) {
                $lat = $xml->result->geometry->location->lat;
                return $lat;
            }
        }*/
    }

    /*
     * @return Longitude
     */
    public function longitude($address) {
        //$request_url = "http://maps.googleapis.com/maps/api/geocode/xml?key=AIzaSyDp0fh9i9x_rBbjosDyJrvtS2dk0a6yoaE&address=" . $address . "&sensor=true";
       // $xml = simplexml_load_file($request_url) or die("url not loading");
        $geo = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?key='.$this->getApiKey().'&address=' . urlencode($address) . '&sensor=true');
        $geo = json_decode($geo, true);
        if ($geo['status'] = 'OK') {
            if (is_array($geo['results']) && isset($geo['results']) && !empty($geo['results'])) {
                $longitude = $geo['results'][0]['geometry']['location']['lng'];
                if (!empty($longitude)) {
                    return $longitude;
                } else {
                    return false;
                }
            }else{
                return false;
            }
        }

      /*  if ($xml) {
            $status = $xml->status;
            if ($status) {
                $lng = $xml->result->geometry->location->lng;
                return $lng;
            }
        }*/
    }


    public function monToThurTiming()
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_TIMING_MON_THU,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }


    public function lowStockMessage(){
        return $this->scopeConfig->getValue(
            self::XML_PATH_NOTIFY_STOCK_QTY,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getPetClinicImage($store = null){
        return self::IMAGES_DIR . $this->_scopeConfig->getValue(
            self::XML_PATH_PET_CLINIC,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    public function getPetZoneImage($store = null){
        return self::IMAGES_DIR . $this->_scopeConfig->getValue(
            self::XML_PATH_PET_ZONE,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    public function getApiSensor(){
        return $this->scopeConfig->getValue(
            self::XML_PATH_API_SENSOR,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getDirection(){
        return $this->scopeConfig->getValue(
            self::XML_PATH_DIRECTION,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getApiUrl(){
        return $this->scopeConfig->getValue(
            self::XML_PATH_APIURL,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getApiKey(){
        return $this->scopeConfig->getValue(
            self::XML_PATH_APIKEY,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getDefaultCountry(){
        return $this->scopeConfig->getValue(
            self::XML_PATH_DEFAULT_COUNTRY,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }


    /*
     * Returning Google Api Url from store configuration
    */
    public function getGoogleApiUrl()
    {
        $apiUrl = $this->getApiUrl();
        if (is_null($apiUrl)) {
            $apiUrl = "";
        }
        $apiKey = "&key=" . $this->getApiKey();
        $apiSensor = $this->getApiSensor();
        $sensor = ($apiSensor == 0) ? 'false' : 'true';
        $urlGoogleApi = $apiUrl . "&sensor=" . $sensor . $apiKey . "&callback=initialize&libraries=places";

        return $urlGoogleApi;
    }


}