<?php
/**
 * @category    Jjcommerce
 * @package     Jjcommerce_StoreLocator
 * @author	Jjcommerce Team<support@2jdesign.co.uk>
 */

namespace Jjcommerce\StoreLocator\Api\Data;


interface PostInterface
{
    /**#@+
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const ID             = 'shop_id';
    const NAME           = 'name';
    const ADDRESS        = 'address';
    const ADDRESS1       = 'address1';
    const ADDRESS2       = 'address2';
    const ZIPCODE        = 'zipcode';
    const CITY           = 'city';
    const COUNTY         = 'county';
    const COUNTRYID      = 'country_id';
    const PHONE          = 'phone';
    const FAX            = 'fax';
    const STOREURL       = 'store_url';
    const STATUS         = 'status';
    const IMAGE          = 'image';
    const LATTITUDE      = 'lattitude';
    const LONGITUDE      = 'longitude';
    const CREATEDAT      = 'created_at';
    const UPDATEDAT      = 'update_at';


    /**#@-*/

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId();

    /**
     * Set ID
     *
     * @param int $id
     * @return \Jjcommerce\StoreLocator\Api\Data\PostInterface
     */
    public function setId($id);

    /**
     * Get Name
     *
     * @return text
     */
    public function getName();
    /**
     * Set Name
     *
     * @param int $name
     * @return \Jjcommerce\StoreLocator\Api\Data\PostInterface
     */
    public function setName($name);


    /**
     * Get Address
     *
     * @return text
     */
    public function getAddress();
    /**
     * Set Address
     *
     * @param int $address
     * @return \Jjcommerce\StoreLocator\Api\Data\PostInterface
     */
    public function setAddress($address);

    /**
     * Get Address1
     *
     * @return text
     */
    public function getAddress1();
    /**
     * Set Address1
     *
     * @param int $address1
     * @return \Jjcommerce\StoreLocator\Api\Data\PostInterface
     */
    public function setAddress1($address1);

    /**
     * Get Address2
     *
     * @return text
     */
    public function getAddress2();
    /**
     * Set Address2
     *
     * @param int $address2
     * @return \Jjcommerce\StoreLocator\Api\Data\PostInterface
     */
    public function setAddress2($address2);


    /**
     * Get Zipcode
     *
     * @return text
     */
    public function getZipcode();
    /**
     * Set Zipcode
     *
     * @param int $zipcode
     * @return \Jjcommerce\StoreLocator\Api\Data\PostInterface
     */
    public function setZipcode($zipcode);

    /**
     * Get City
     *
     * @return text
     */
    public function getCity();
    /**
     * Set City
     *
     * @param int $city
     * @return \Jjcommerce\StoreLocator\Api\Data\PostInterface
     */
    public function setCity($city);

    /**
     * Get County
     *
     * @return text
     */
    public function getCounty();
    /**
     * Set County
     *
     * @param int $county
     * @return \Jjcommerce\StoreLocator\Api\Data\PostInterface
     */
    public function setCounty($county);

    /**
     * Get CountryId
     *
     * @return text
     */
    public function getCountryId();
    /**
     * Set CountryId
     *
     * @param int $countryId
     * @return \Jjcommerce\StoreLocator\Api\Data\PostInterface
     */
    public function setCountryId($countryId);

    /**
     * Get Phone
     *
     * @return text
     */
    public function getPhone();
    /**
     * Set Phone
     *
     * @param int $phone
     * @return \Jjcommerce\StoreLocator\Api\Data\PostInterface
     */
    public function setPhone($phone);

    /**
     * Get Fax
     *
     * @return text
     */
    public function getFax();
    /**
     * Set Fax
     *
     * @param int $fax
     * @return \Jjcommerce\StoreLocator\Api\Data\PostInterface
     */
    public function setFax($fax);

    /**
     * Get StoreUrl
     *
     * @return text
     */
    public function getStoreUrl();
    /**
     * Set StoreUrl
     *
     * @param int $storeUrl
     * @return \Jjcommerce\StoreLocator\Api\Data\PostInterface
     */
    public function setStoreUrl($storeUrl);

    /**
     * Get Status
     *
     * @return text
     */
    public function getStatus();
    /**
     * Set Status
     *
     * @param int $status
     * @return \Jjcommerce\StoreLocator\Api\Data\PostInterface
     */
    public function setStatus($status);

    /**
     * Get Image
     *
     * @return text
     */
    public function getImage();
    /**
     * Set Image
     *
     * @param int $image
     * @return \Jjcommerce\StoreLocator\Api\Data\PostInterface
     */
    public function setImage($image);

    /**
     * Get Lattitude
     *
     * @return text
     */
    public function getLattitude();
    /**
     * Set Lattitude
     *
     * @param int $lattitude
     * @return \Jjcommerce\StoreLocator\Api\Data\PostInterface
     */
    public function setLattitude($lattitude);

    /**
     * Get Longitude
     *
     * @return text
     */
    public function getLongitude();
    /**
     * Set Longitude
     *
     * @param int $longitude
     * @return \Jjcommerce\StoreLocator\Api\Data\PostInterface
     */
    public function setLongitude($longitude);

    /**
     * Get Created At
     *
     * @return text
     */
    public function getCreatedAt();
    /**
     * Set Created At
     *
     * @param int $createdAt
     * @return \Jjcommerce\StoreLocator\Api\Data\PostInterface
     */
    public function setCreatedAt($createdAt);

    /**
     * Get Updated At
     *
     * @return text
     */
    public function getUpdatedAt();
    /**
     * Set Updated At
     *
     * @param int $updatedAt
     * @return \Jjcommerce\StoreLocator\Api\Data\PostInterface
     */
    public function setUpdatedAt($updatedAt);

}
