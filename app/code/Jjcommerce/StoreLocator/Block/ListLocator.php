<?php
/**
 * @category    Jjcommerce
 * @package     Jjcommerce_StoreLocator
 * @author	Jjcommerce Team<support@2jdesign.co.uk>
 */
namespace Jjcommerce\StoreLocator\Block;

use Magento\Framework\View\Element\Template;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\Messages;

/**
 * Main contact form block
 */
class ListLocator extends Template
{
    protected $_stores = null;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    protected $helper;

    /**
     * @var Registry
     */
    protected $registry;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;

    /**
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Jjcommerce\StoreLocator\Model\Storelocator $storelocator,
        \Magento\Directory\Model\CountryFactory $country,
        Template\Context $context,
        \Jjcommerce\StoreLocator\Helper\Data $helper,
        Registry $registry,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        array $data = [])
    {
        $this->_storeManager = $context->getStoreManager();
        $this->country  = $country;
        $this->_stores = $storelocator;
        $this->helper = $helper;
        $this->registry = $registry;
        $this->session = $context->getSession();
        parent::__construct($context, $data);
        $this->messageManager = $messageManager;

    }

    /**
     * Returns store locators
     *
     * @return string
     */
    public function getAllStores()
    {
        return $this->_stores;
    }
    public function getMediaUrl()
    {
        return $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA );
    }

    public function getCountry($countryCode)
    {
        $country = $this->country->create()->loadByCode($countryCode);
        return $country->getName();
    }

    public function getStoreCollection(){
       // $this->log(__FILE__);
      //  $this->log(__LINE__);
        $searchData = $this->getSearchData();

      //  $this->log(__LINE__);
        if(isset($searchData) && !empty($searchData)) {
          //  $this->log(__LINE__);
            $searchStoreCollection = $this->getSearchStore();
            if($searchStoreCollection->count() > 0){
                $storeCollection = $searchStoreCollection->toArray();
                $storeCollection['totalRecords'] = $searchStoreCollection->count();
            }else{
                $this->messageManager->addError(__('Sorry, no store found in specified location.'));
                $storeCollection = $this->_stores->getCollection()->addFieldToFilter('status', 1)->setOrder('name','ASC')->toArray();
            }


        }else {
            $storeCollection = $this->_stores->getCollection()->addFieldToFilter('status', 1)->setOrder('name','ASC')->toArray();
        }

       // $this->log(__LINE__);
        $data = array('totalRecords'=>$storeCollection['totalRecords']);

        foreach($storeCollection['items'] as $item){
            if(strpos($item['name'], "Fulfilment" ) !== false ) {
                continue;
            }
            $url = $item['store_url'];
            $storeUrl =  explode('.co.uk/',$url);
            if(is_array($storeUrl)){
                $storeUrlLink = $this->getUrl().$storeUrl[1];
            }else{
                $storeUrlLink = '#';
            }
            $data['items'][] = array('shop_id'=>$item['shop_id'],'name'=>$item['name'],'address'=>$item['address'],'address1'=>$item['address1'],'address2'=>$item['address2'],'zipcode'=>$item['zipcode'],'city'=>$item['city'],'county'=>$item['county'],'country_id'=>$item['country_id'],'phone'=>$item['phone'],'fax'=>$item['fax'],'direction_link'=>$item['direction_link'],'store_url'=>$item['store_url'],'image'=>$item['image'],
                'lat'=>$item['lattitude'], 'long'=>$item['longitude'],'service_flag'=>$item['service_flag'],'hover_image'=>$item['hover_image'],'store_country'=>$this->getCountry($item['country_id']),'store_popup_url'=>$storeUrlLink);
        }

       return $data;
    }

    protected function getSearchStore()
    {
        $userData = $this->getSearchData();

        if(empty($userData)){
            return false;
        }

        $userLat = $userData[0];
        $userLng = $userData[1];
        $radius = $userData[2];
        //Creating Collection for store locator
        $stores = $this->_stores->getCollection()
            ->addFieldToSelect('*')
            ->addExpressionFieldToSelect('distance', sprintf("(3959 * acos(cos(radians('%s')) * cos(radians(`lattitude`)) * cos(radians(`longitude`) - radians('%s')) + sin(radians('%s')) * sin( radians(`lattitude`))))", $userLat, $userLng, $userLat, $radius), array('shop_id'))
            ->setOrder('distance', \Magento\Framework\DB\Select::SQL_ASC)
           ->setOrder('name', \Magento\Framework\DB\Select::SQL_ASC);

        if ($radius !== 0) {
            $stores->getSelect()->group('shop_id');
            $stores->getSelect()->having('distance < ?', $radius);
        }

       // echo "select :".$stores->getSelect();
       // echo "count :".$stores->count();

        /* Fix Added for No Stores Availble Issue */
        if (count($stores->getData()) == 0) {
            $radiusInc = 3500; //Incresing radius to show nearest store if not available in same city
            /*$this->_stores->getCollection()
                ->addFieldToSelect('*')
                ->addExpressionFieldToSelect('distance', sprintf("(3959 * acos(cos(radians('%s')) * cos(radians(`lattitude`)) * cos(radians(`longitude`) - radians('%s')) + sin(radians('%s')) * sin( radians(`lattitude`))))", $userLat, $userLng, $userLat, $radiusInc), array('shop_id'))
                ->setOrder('distance', \Magento\Framework\DB\Select::SQL_ASC)
               ->setOrder('name', \Magento\Framework\DB\Select::SQL_ASC);*/

            $storeCollection = $this->_stores->getCollection()
                ->addFieldToSelect('*')
                ->addExpressionFieldToSelect('distance', sprintf("(3959 * acos(cos(radians('%s')) * cos(radians(`lattitude`)) * cos(radians(`longitude`) - radians('%s')) + sin(radians('%s')) * sin( radians(`lattitude`))))", $userLat, $userLng, $userLat, $radius), array('shop_id'))
                ->setOrder('distance', \Magento\Framework\DB\Select::SQL_ASC)
                ->setOrder('name', \Magento\Framework\DB\Select::SQL_ASC);


            if ($radiusInc !== 0) {
                $storeCollection->getSelect()->group('shop_id');
                $storeCollection->getSelect()->reset(\Magento\Framework\DB\Select::HAVING);
                $storeCollection->getSelect()->having('distance < ?', $radiusInc);
                $storeCollection->getSelect()->limit(5);
            }
            $storeCollection->load();

            return $storeCollection;
        }
        /* EOF */
       // echo "select1 :".$stores->getSelect();exit;
      //  $this->log("select".$stores->getSelect());
        // $stores->getSelect()->order('distance', 'ASC');
        $stores->load();

        return $stores;

    }

    /*
     * To get finder search params posted data in session
     * */

    protected function getSearchData(){
        $this->session->start();
        $userData = $this->session->getSearchData();
        return $userData;
    }


    /**
     * To get finder search params posted data in session
     */
    public function clearSessionData(){
        $this->session->start();
        $this->session->unsSearchData();
    }

    /**
     * Returns action url for store finder on store listing form
     *
     * @return string
     */
    public function getFormAction()
    {
        return $this->getUrl('locator/index/storefinder', ['_secure' => true]);
    }

    public function log($message){
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/datamigration.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info($message);
    }

}
