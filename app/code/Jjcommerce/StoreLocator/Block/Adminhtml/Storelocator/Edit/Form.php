<?php
/**
 * @category    Jjcommerce
 * @package     Jjcommerce_StoreLocator
 * @author	Jjcommerce Team<support@2jdesign.co.uk>
 */
namespace Jjcommerce\StoreLocator\Block\Adminhtml\Storelocator\Edit;

/**
 * Adminhtml Storelocator form
 */
class Form extends \Magento\Backend\Block\Widget\Form\Generic
{

    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $_systemStore;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig
     * @param \Magento\Store\Model\System\Store $systemStore
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Store\Model\System\Store $systemStore,
        \Magento\Directory\Model\Config\Source\Country $countryFactory,
        array $data = []
    ) {
        $this->_systemStore = $systemStore;
        $this->_countryFactory = $countryFactory;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Init form
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('store_info');
        $this->setTitle(__('Storelocator Information'));
    }

    /**
     * Prepare form
     *
     * @return $this
     */
    protected function _prepareForm()
    {
        /** @var \Jjcommerce\StoreLocator\Model\StoreLocator $model */
        $model = $this->_coreRegistry->registry('store_info');

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create(
            ['data' => ['id' => 'edit_form', 'action' => $this->getData('action'), 'method' => 'post' ,'enctype' => 'multipart/form-data'  ]]
        );

        $form->setHtmlIdPrefix('post_');
        $countyOptions = $this->_countryFactory->toOptionArray();
        $fieldset = $form->addFieldset(
            'base_fieldset',
            ['legend' => __('Store Information'), 'class' => 'fieldset-wide']
        );

        if ($model->getShopId()) {
            $fieldset->addField('shop_id', 'hidden', ['name' => 'shop_id']);
        }

        $fieldset->addField(
            'name',
            'text',
            [
                'name' => 'name',
                'label' => __('Store Name'),
                'title' => __('Store Name'),
                'required' => true
            ]
        );
        $fieldset->addField(
            'store_code',
            'text',
            [
                'name' => 'store_code',
                'label' => __('Warehouse ID'),
                'title' => __('Warehouse ID'),
                'required' => true
            ]
        );
        $fieldset->addField(
            'address',
            'text',
            [
                'name' => 'address',
                'label' => __('Store Address'),
                'title' => __('Store Address'),
                'required' => true
            ]
        );

        $fieldset->addField(
            'address1',
            'text',
            [
                'name' => 'address1',
                'label' => __('Store Address1'),
                'title' => __('Store Address1'),
                'required' => false
            ]
        );
        $fieldset->addField(
            'zipcode',
            'text',
            [
                'name' => 'zipcode',
                'label' => __('Postal Code'),
                'title' => __('Postal Code'),
                'required' => true
            ]
        );
        $fieldset->addField(
            'city',
            'text',
            [
                'name' => 'city',
                'label' => __('City'),
                'title' => __('City'),
                'required' => true
            ]
        );
        $fieldset->addField(
            'county',
            'text',
            [
                'name' => 'county',
                'label' => __('Region'),
                'title' => __('Region'),
                'required' => false
            ]
        );
        $fieldset->addField(
            'country_id',
            'select',
            [
                'name' => 'country_id',
                'label' => __('Country'),
                'title' => __('Country'),
                'values' => $countyOptions,
                'required' => true,
            ]
        );
        $fieldset->addField(
            'phone',
            'text',
            [
                'name' => 'phone',
                'label' => __('Phone Number'),
                'title' => __('Phone Number'),
                'required' => true
            ]
        );
        $fieldset->addField(
            'fax',
            'text',
            [
                'name' => 'fax',
                'label' => __('Fax'),
                'title' => __('Fax'),
                'required' => false
            ]
        );
        $fieldset->addField(
            'store_url',
            'text',
            [
                'name' => 'store_url',
                'label' => __('Store Link'),
                'title' => __('Store Link'),
                'required' => true
            ]
        );

        $fieldset->addField(
            'direction_link',
            'text',
            [
                'name' => 'direction_link',
                'label' => __('Direction Link'),
                'title' => __('Direction Link'),
                'required' => true
            ]
        );

        $fieldset->addField(
            'status',
            'select',
            [
                'label' => __('Status'),
                'title' => __('Status'),
                'name' => 'status',
                'required' => false,
                'options' => [
                    '1' => __('Enabled'),
                    '0' => __('Disabled')
                ]
            ]
        );

        $fieldset->addField(
            'image',
            'image',
            [
                'name' => 'image',
                'label' => __('Store Image'),
                'title' => __('Store Image'),
                'required' => true,
                'class'     => 'required-entry'
            ]
        );

        $fieldset->addField(
            'hover_image',
            'image',
            [
                'name' => 'hover_image',
                'label' => __('Store Image (hover)'),
                'title' => __('Store Image (hover)')
            ]
        );

        $fieldset->addField(
            'service_flag',
            'select',
            [
                'label' => __('Store Service Flag'),
                'title' => __('Store Service Flag'),
                'name' => 'service_flag',
                'required' => false,
                'options' => [
                    '0' => __('No Flag'),
                    '1' => __('Pet Clinic'),
                    '2' => __('Pet Zone')
                ]
            ]
        );

        $fieldset->addField(
            'lattitude',
            'text',
            [
                'name' => 'lattitude',
                'label' => __('Latitude'),
                'title' => __('Latitude'),
                'required' => true
            ]
        );

        $fieldset->addField(
            'longitude',
            'text',
            [
                'name' => 'longitude',
                'label' => __('Longitude'),
                'title' => __('Longitude'),
                'required' => true
            ]
        );

        if (!$model->getId()) {
            $model->setData('status', '1');
        }

        $form->setValues($model->getData());
        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }
}
