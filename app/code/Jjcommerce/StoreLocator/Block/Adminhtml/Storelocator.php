<?php

/**
 * @category    Jjcommerce
 * @package     Jjcommerce_StoreLocator
 * @author	Jjcommerce Team<support@2jdesign.co.uk>
 */
namespace Jjcommerce\StoreLocator\Block\Adminhtml;

class Storelocator extends \Magento\Backend\Block\Widget\Grid\Container
{
    protected function _construct()
    {
        $this->_controller = 'adminhtml_storelocator';
        $this->_blockGroup = 'Jjcommerce_StoreLocator';
        $this->_headerText = __('Manage Storelocator');

        parent::_construct();

        if ($this->_isAllowedAction('Jjcommerce_StoreLocator::save')) {
            $this->buttonList->update('add', 'label', __('Add New Store'));
        } else {
            $this->buttonList->remove('add');
        }
    }

    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }
}
