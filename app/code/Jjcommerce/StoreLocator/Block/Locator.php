<?php
/**
 * @category    Jjcommerce
 * @package     Jjcommerce_StoreLocator
 * @author	Jjcommerce Team<support@2jdesign.co.uk>
 */
namespace Jjcommerce\StoreLocator\Block;

use Magento\Framework\View\Element\Template;
use Magento\Framework\Registry;
/**
 * Main contact form block
 */
class Locator extends Template
{
    protected $_stores = null;
    protected $_locator = null;

    /**
     * @var Registry
     */
    protected $registry;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;
    protected $_storelocator;

    /**
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Jjcommerce\StoreLocator\Model\Storelocator $storelocator,
        \Magento\Directory\Model\CountryFactory $country,
        Registry $registry,
        Template\Context $context,
        array $data = [])
    {
        $this->registry = $registry;
        $this->_storeManager = $context->getStoreManager();
        $this->_storelocator = $storelocator;
        $this->country  = $country;
        parent::__construct($context, $data);
    }


    public function getMediaUrl()
    {
        return $this->_storeManager->getStore()->getBaseUrl(  \Magento\Framework\UrlInterface::URL_TYPE_MEDIA );
    }

    public function getCountry($countryCode)
    {
        $country = $this->country->create()->loadByCode($countryCode);
        return $country->getName();
    }

    public function getCurrentLocator(){

        if(!$this->_locator) {
            $id = $this->registry->registry('LOCATOR');
            $this->_locator = $this->_storelocator->load($id);
        }
        return $this->_locator;
    }

}
