<?php
/**
 * @category    Jjcommerce
 * @package     Jjcommerce_FreeDeliveryBanner
 * @author	Jjcommerce Team<support@2jdesign.co.uk>
 */
namespace Jjcommerce\PetsInformation\Helper;


use Magento\Store\Model\Store;
use Magento\Store\Model\ScopeInterface;


class Data extends \Magento\Framework\App\Helper\AbstractHelper
{

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;


    const XML_PATH_ENABLED = 'petsinformation/general/enabled';
    const XML_PATH_PET_TYPES = 'petsinformation/general/pet_type';

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @codeCoverageIgnore
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->_storeManager = $storeManager;
        parent::__construct($context);
    }


    /**
     * Check if enabled
     *
     * @return string|null
     */
    public function isEnabled()
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_ENABLED,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }


    /**
     * Get Shipping Setting Default Country
     *
     */
    public function getPetTypes()
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_PET_TYPES,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

}
