<?php
/**
 *
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Jjcommerce\PetsInformation\Controller\Pets;


use Magento\Framework\App\RequestInterface;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Framework\Stdlib\DateTime\Filter\Date;


class Save extends \Magento\Framework\App\Action\Action
{

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;


    /**
     * Initialize dependencies.
     *
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Jjcommerce\PetsInformation\Model\PetsInformation $petsInformation
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Customer\Model\Session $customerSession,
        \Jjcommerce\PetsInformation\Model\PetsInformation $petsInformation,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Magento\Framework\Stdlib\DateTime\Filter\Date $filterDate

    ) {
        $this->storeManager = $storeManager;
        $this->customerSession = $customerSession;
        $this->resultPageFactory = $resultPageFactory;
        $this->petsInformation = $petsInformation;
        $this->date = $date;
        $this->filterDate = $filterDate;
        parent::__construct($context);
    }

    /**
     * Save newsletter subscription preference action
     *
     * @return void|null
     */
    public function execute()
    {
        $customerId = $this->customerSession->getCustomerId();
        $customerStoreId = $this->customerSession->getCustomer()->getStoreId();
        $storeId = $this->storeManager->getStore($customerStoreId)->getId();

        $data = $this->getRequest()->getPost();

        if(!$data)
        {
            $this->_redirect('*/*/');
        }


        if($data && $customerId > 0) {
            try {
                $error = false;

                if (!\Zend_Validate::is(trim($data['pet_name']), 'NotEmpty')) {
                    $error = true;
                }
                if (!\Zend_Validate::is(trim($data['pet_type']), 'NotEmpty')) {
                    $error = true;
                }
                if (!\Zend_Validate::is(trim($data['breed_type']), 'NotEmpty')) {
                    $error = true;
                }
                if (!\Zend_Validate::is(trim($data['pet_bday']), 'NotEmpty')) {
                    $error = true;
                }

                if ($error) {
                    throw new \Exception();
                }

                $dataPost = array('pet_name'=>$data['pet_name'],'pet_type'=>$data['pet_type'],'breed_type'=>$data['breed_type'],'pet_bday'=>$data['pet_bday']);


              /*  if (!empty($data['pet_bday'])) {
                    $inputFilter = new \Zend_Filter_Input(['pet_bday' => $this->filterDate], [], $dataPost);
                    $dataPost = $inputFilter->getUnescaped();
                }*/
              //  $bDate = explode("-",$dataPost['pet_bday']);
              //  $increOneDay = $bDate[2]+1;
              //  $nBDate = $bDate[0]."-".$bDate[1]."-".$increOneDay;

                $this->petsInformation->setData( array(
                        'pet_name' => $dataPost['pet_name'],
                        'customer_id' =>  $customerId,
                        'store_id' => $storeId,
                        'pet_type' => $dataPost['pet_type'],
                        'breed_type'=> $dataPost['breed_type'],
                        'pet_bday' => $dataPost['pet_bday']
                    )
                );


                $this->petsInformation->save();

                $this->messageManager->addSuccess(
                    __('Your pets information has successfully saved!.')
                );
                $this->_redirect('petsinfo/pets/index/');
                return;

            } catch (\Exception $e) {
                $this->messageManager->addError(
                    __("We can't process your request right now. Sorry, that's all we know." .$e->getMessage())
                );
                $this->_redirect('petsinfo/pets/index/');
                return;
            }
        }

        return $this->resultPageFactory->create()->setPath('petinfo/pets/index');

    }

}
