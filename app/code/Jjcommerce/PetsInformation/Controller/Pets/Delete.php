<?php
/**
 *
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Jjcommerce\PetsInformation\Controller\Pets;

use Magento\Framework\App\RequestInterface;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Json\Helper\Data;

class Delete extends \Magento\Framework\App\Action\Action
{

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;


    /**
     * Initialize dependencies.
     *
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Jjcommerce\PetsInformation\Model\PetsInformation $petsInformation
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Customer\Model\Session $customerSession,
        \Jjcommerce\PetsInformation\Model\PetsInformation $petsInformation,
        \Magento\Framework\Json\Helper\Data $jsonHelper

    ) {
        $this->storeManager = $storeManager;
        $this->_customerSession = $customerSession;
        $this->resultPageFactory = $resultPageFactory;
        $this->petsInformation = $petsInformation;
        $this->jsonHelper = $jsonHelper;
        parent::__construct($context);
    }

    public function execute() {
        // $this->_view->loadLayout();
        if (!$this->_getSession()->authenticate()) {
            $this->jsonHelper->jsonEncode([
                'error' => "Session Expire!",
                'backUrl' => $this->_redirect->getRedirectUrl()
            ]);
            return;
        }

        $id = $this->getRequest()->getParam('id');
        try{
            $this->petsInformation->setId($id)->delete();

            $this->getResponse()->representJson(
                $this->jsonHelper->jsonEncode([
                    'success_msg' => 'Item has deleted successfully!',
                    'id' => $id
                ])
            );
            return;

        } catch (Exception $e) {
            $this->getResponse()->representJson(
                $this->jsonHelper->jsonEncode([
                    'error' => "We can't deleted this iteam.!" .$e->getMessage() ,
                    'id' => $id
                ])
            );
            return;

        }

        return;
    }


    /**
     * Retrieve customer session object
     *
     * @return \Magento\Customer\Model\Session
     */
    protected function _getSession()
    {
        return $this->_customerSession;
    }
}
