<?php
/**
 *
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Jjcommerce\PetsInformation\Controller\Pets;

use Magento\Framework\App\RequestInterface;
use Magento\Framework\Controller\Result\Redirect;


class Edit extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    protected $jsonHelper;


    /**
     * Initialize dependencies.
     *
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Jjcommerce\PetsInformation\Model\PetsInformation $petsInformation
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Customer\Model\Session $customerSession,
        \Jjcommerce\PetsInformation\Model\PetsInformation $petsInformation,
        \Magento\Framework\Json\Helper\Data $jsonHelper


    ) {
        $this->storeManager = $storeManager;
        $this->_customerSession = $customerSession;
        $this->resultPageFactory = $resultPageFactory;
        $this->petsInformation = $petsInformation;
        $this->jsonHelper = $jsonHelper;
        parent::__construct($context);
    }

    /**
     * Customer address edit action
     *
     * @return \Magento\Framework\Controller\Result\Forward
     */
    public function execute()
    {
        $customerId = $this->_customerSession->getCustomerId();
        $customerStoreId = $this->_customerSession->getCustomer()->getStoreId();
        $storeId = $this->storeManager->getStore($customerStoreId)->getId();

        $postData = $this->getRequest()->getParam('petinfo',false);
        $data = json_decode($postData);

        if(!$data)
        {
            $this->_redirect('*/*/');
        }

        try {

            $error = false;

            if (!\Zend_Validate::is(trim($data->pet_name), 'NotEmpty')) {
                $error = true;
                $errorMessage =  'Pet name should not be empty';
            }
            if (!\Zend_Validate::is(trim($data->pet_type), 'NotEmpty')) {
                $error = true;
                $errorMessage =  'Pet type should not be empty';
            }
            if (!\Zend_Validate::is(trim($data->breed_type), 'NotEmpty')) {
                $error = true;
                $errorMessage =  'Pet breed type should not be empty';
            }
            if (!\Zend_Validate::is(trim($data->pet_bday), 'NotEmpty')) {
                $error = true;
                $errorMessage =  'Pet birth day should not be empty';
            }
            $petBirthdate = $data->pet_bday; // /10-02-2010/
            if (!preg_match("/[0-9]{2}-[0-9]{2}-[0-9]{4}/", $petBirthdate))
            {
                $error = true;
                $errorMessage = "Pet birth day is invalid";
            }

            if ($error) {
                // throw new \Exception();
                $this->messageManager->addError(__($errorMessage));
                $this->getResponse()->representJson(
                    $this->jsonHelper->jsonEncode([
                        'error' => $errorMessage,
                        'id' => $data->id
                    ])
                );
                return;
            }


            $setData = array(
                    'pet_name' => $data->pet_name,
                    'customer_id' => $customerId,
                    'store_id' => $storeId,
                    'pet_type' => $data->pet_type,
                    'breed_type' => $data->breed_type,
                    'pet_bday' => $data->pet_bday
                );
            $this->petsInformation->load($data->id)->addData($setData);
            $this->petsInformation->setData('id',$data->id);

            $this->petsInformation->save();

            $this->getResponse()->representJson(
                $this->jsonHelper->jsonEncode([
                    'success_msg' => 'Item has updated successfully!',
                    'id' => $data->id,
                    'pet_name' => $data->pet_name,
                    'pet_type' => $data->pet_type,
                    'breed_type' => $data->breed_type,
                    'pet_bday' => $data->pet_bday
                ])
            );

            return;

        } catch (\Exception $e) {
            $this->getResponse()->representJson(
                $this->jsonHelper->jsonEncode([
                    'error' => "We can't process to update this item information. Please try again!",
                    'id' => $data->id
                ])
            );
            return;
        }

    }
}
