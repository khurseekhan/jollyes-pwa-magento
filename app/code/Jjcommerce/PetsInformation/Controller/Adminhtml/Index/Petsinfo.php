<?php
/**
 * @category    Jjcommerce
 * @package     Jjcommerce_PetsInformation
 * @author	Jjcommerce Team<support@2jdesign.co.uk>
 */
namespace Jjcommerce\PetsInformation\Controller\Adminhtml\Index;

class Petsinfo extends \Magento\Customer\Controller\Adminhtml\Index
{
    /**
     * Get customer's pet profile list
     *
     * @return \Magento\Framework\View\Result\Layout
     */
    public function execute()
    {
        $customerId = $this->initCurrentCustomer();
        $resultLayout = $this->resultLayoutFactory->create();
        $block = $resultLayout->getLayout()->getBlock('customer.petsinfo.grid');
        $block->setCustomerId($customerId)->setUseAjax(true);
        return $resultLayout;
    }


}

