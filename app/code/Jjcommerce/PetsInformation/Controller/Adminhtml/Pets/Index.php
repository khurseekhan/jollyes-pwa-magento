<?php
/**
 * @category    Jjcommerce
 * @package     Jjcommerce_PetsInformation
 * @author	Jjcommerce Team<support@2jdesign.co.uk>
 */
namespace Jjcommerce\PetsInformation\Controller\Adminhtml\Pets;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Index extends \Magento\Backend\App\Action
{
    const ADMIN_RESOURCE = 'Jjcommerce_PetsInformation::petsinformation';

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * Index action
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Jjcommerce_PetsInformation::petsinformation');
        $resultPage->addBreadcrumb(__('Pets'), __('Pets'));
        $resultPage->addBreadcrumb(__('Pets Info'), __('Pets Info'));
        $resultPage->getConfig()->getTitle()->prepend(__('Pets'));

        return $resultPage;
    }

    /**
     * Determine if authorized to perform group actions.
     *
     * @return bool
     */
    protected function _isAllowed() {
        return $this->_authorization->isAllowed('Jjcommerce_PetsInformation::petsinformation');
    }
}
