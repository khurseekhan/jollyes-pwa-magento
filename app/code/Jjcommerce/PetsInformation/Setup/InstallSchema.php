<?php
/**
 * @category    Jjcommerce
 * @package     Jjcommerce_PetsInformation
 * @author	Jjcommerce Team<support@2jdesign.co.uk>
 */
namespace Jjcommerce\PetsInformation\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use \Magento\Framework\DB\Ddl\Table;

class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        if (!$installer->tableExists('jj_pet_collection')) {
            $table  = $installer->getConnection()
                ->newTable($installer->getTable('jj_pet_collection'))
                ->addColumn(
                    'id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                    'Pets Information Id'
                )
                ->addColumn(
                    'customer_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    ['unsigned' => true, 'nullable' => false, 'default' => '0'],
                    'Customer Id'
                )
                ->addColumn(
                    'store_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                    null,
                    ['unsigned' => true, 'default' => '0'],
                    'Store Id'
                )
                ->addColumn(
                    'pet_name',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    '255',
                    [],
                    'Pet Name'
                )
                ->addColumn(
                    'pet_type',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    '64k',
                    [],
                    'Pet Type'
                )
                ->addColumn(
                    'pet_bday',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                    null,
                    ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
                    'Pet Bday Time'
                )->addIndex(
                    $installer->getIdxName('jj_pet_collection', ['customer_id']),
                    ['customer_id']
                )->addIndex(
                    $installer->getIdxName('jj_pet_collection', ['store_id']),
                    ['store_id']
                )
            ;

            $installer->getConnection()->createTable($table);
        }

        $installer->endSetup();
    }
}
