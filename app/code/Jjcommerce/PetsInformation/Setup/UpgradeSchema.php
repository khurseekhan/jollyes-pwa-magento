<?php
/**
 * @category    Jjcommerce
 * @package     Jjcommerce_PetsInformation
 * @author	Jjcommerce Team<support@2jdesign.co.uk>
 */
namespace Jjcommerce\PetsInformation\Setup;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;
/**
 * @codeCoverageIgnore
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * {@inheritdoc}
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        if (version_compare($context->getVersion(), '0.0.3', '<')) {
            $setup->getConnection()->addColumn(
                $setup->getTable('jj_pet_collection'),
                'breed_type',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, // Or any other type
                    'nullable' => true, // Or false
                    'comment' => 'Breed Type',
                    'after' => 'pet_type'
                ]
            );
        }
        $setup->endSetup();
    }
}