<?php
/**
 * @category    Jjcommerce
 * @package     Jjcommerce_PetsInformation
 * @author	Jjcommerce Team<support@2jdesign.co.uk>
 */
namespace Jjcommerce\PetsInformation\Block;

use Magento\Framework\View\Element\Template;

/**
 * Main contact form block
 */
class PetsInformation extends Template
{
    protected $_collection = null;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Jjcommerce\PetsInformation\Model\PetsInformation $petsinformation,
        \Jjcommerce\PetsInformation\Helper\Data $helperData,
        Template\Context $context,
        array $data = [])
    {
        $this->_storeManager = $context->getStoreManager();
        $this->_helperData = $helperData;
        $this->_collection = $petsinformation->getCollection();
        parent::__construct($context, $data);

    }

    /**
     * Retrieve form action url and set "secure" param to avoid confirm
     * message when we submit form from secure page to unsecure
     *
     * @return string
     */
    public function getFormActionUrl()
    {
        return $this->getUrl('petsinfo/pets/save', ['_secure' => true]);
    }


    public function getPetTypes(){
        $petTypes = $this->_helperData->getPetTypes($this->_storeManager->getStore()->getId());
        return  $petTypes;
    }

    public function getMediaUrl()
    {
        return $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
    }


}
