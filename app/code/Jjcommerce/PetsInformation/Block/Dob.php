<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Jjcommerce\PetsInformation\Block;


/**
 * Class Dob
 *
 * @SuppressWarnings(PHPMD.DepthOfInheritance)
 */
class Dob extends \Magento\Customer\Block\Widget\Dob
{

    /**
     * @return void
     */
    public function _construct()
    {
        parent::_construct();
        $this->setTemplate('pets/dob.phtml');
    }

    /**
     * Return label
     *
     * @return \Magento\Framework\Phrase
     */
    public function getLabel()
    {
        return __('Birthday');
    }

    /**
     * Create correct date field
     *
     * @return string
     */
    public function getFieldHtml()
    {
        $this->dateElement->setData([
            'extra_params' => $this->isRequired() ? 'data-validate="{required:true}"' : '',
            'name' => $this->getHtmlId(),
            'id' => $this->getHtmlId(),
            'class' => $this->getHtmlClass(),
            'value' => $this->getValue(),
            'date_format' => $this->getDateFormat(),
            'image' => $this->getViewFileUrl('Magento_Theme::calendar.png'),
          //  'years_range' => '-120y:c+nn',
           // 'max_date' => '-1d',
            'change_month' => 'true',
            'change_year' => 'true',
            'show_on' => 'both'
        ]);
        return $this->dateElement->getHtml();
    }

    /**
     * Returns format which will be applied for DOB in javascript
     *
     * @return string
     */
    public function getDateFormat()
    {
       // return 'Y-m-d';
        return 'd-m-Y';
    }


    /**
     * Return Id and Name of the element birthday date
     *
     * @return string
     */
    public function getHtmlId()
    {
        return 'pet_bday';
    }

    /**
     *
     * @return string
     */
    public function getHtmlClass()
    {
        return 'required';
    }

}
