<?php
/**
 * @category    Jjcommerce
 * @package     Jjcommerce_PetsInformation
 * @author	Jjcommerce Team<support@2jdesign.co.uk>
 */
namespace Jjcommerce\PetsInformation\Block;

use Magento\Framework\View\Element\Template;

/**
 * Main contact form block
 */
class Petslist extends Template
{
    /**
     * @var \Jjcommerce\PetsInformation\Model\PetsInformation
     */
    protected $petsinformation = null;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $customerSession;

    /**
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Jjcommerce\PetsInformation\Model\PetsInformation $petsinformation,
        \Jjcommerce\PetsInformation\Helper\Data $helperData,
        Template\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        array $data = [])
    {
        $this->_storeManager = $context->getStoreManager();
        $this->_helperData = $helperData;
        $this->petsinformation = $petsinformation;
        $this->customerSession = $customerSession;
        parent::__construct($context, $data);

    }

    /**
     * Get Collection of Pets
     * @return object
     */
    public function petCollection()
    {
        $customerId = $this->customerSession->getCustomerId();
        return $this->petsinformation->getCollection()->addFieldToFilter( 'customer_id', array( 'eq' => $customerId ) );
    }

    /**
     * Get Save Action URL
     * @return string
     */
    public function getSaveUrl()
    {
        return $this->getUrl('petsinfo/pets/save', ['_secure' => true]);
    }

    /**
     * Get Edit Action URL
     * @return string
     */
    public function getEditUrl()
    {
        return $this->getUrl('petsinfo/pets/edit', ['_secure' => true]);

    }

    /**
     * Get Remove Action URL
     * @return string
     */
    public function getRemoveUrl()
    {
       return $this->getUrl('petsinfo/pets/delete', ['_secure' => true]);

    }

    /**
     * Get delete action url
     * @return string
     */
    public function getMediaUrl()
    {
        return $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
    }


}
