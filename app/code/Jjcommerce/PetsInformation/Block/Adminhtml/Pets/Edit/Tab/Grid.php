<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Jjcommerce\PetsInformation\Block\Adminhtml\Pets\Edit\Tab;

use Magento\Customer\Controller\RegistryConstants;

/**
 * Adminhtml PetsInformation grid block
 *
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Grid extends \Magento\Backend\Block\Widget\Grid\Extended
{
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry|null
     */
    protected $_coreRegistry = null;

    /**
     * @var \Jjcommerce\PetsInformation\Model\PetsInformationFactory
     */
    protected $_collectionFactory;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \Jjcommerce\PetsInformation\Model\PetsInformationFactory $petsFactory
     * @param \Magento\Framework\Registry $coreRegistry
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Jjcommerce\PetsInformation\Model\PetsInformationFactory $petsFactory,
        \Magento\Framework\Registry $coreRegistry,
        array $data = []
    ) {
        $this->_coreRegistry = $coreRegistry;
        $this->_petsFactory = $petsFactory;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('customer_petsinfo_grid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('desc');

        $this->setUseAjax(true);

        $this->setEmptyText(__('No Pets Info Found'));
    }

    /**
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('*/*/*', ['_current' => true]);
    }

    /**
     * Retrieve row url
     *
     * @param object $item
     * @return string
     */
    public function getRowUrl($item)
    {
        return '';
    }


    /**
     * @return $this
     */
    protected function _prepareCollection()
    {

        $customerId = $this->_coreRegistry->registry(RegistryConstants::CURRENT_CUSTOMER_ID);
        $collection = $this->_petsFactory->create()->getCollection()
            ->addFieldToSelect('*')
            ->addFieldToFilter('customer_id', $customerId )
            ->setOrder('id', 'asc');

       // echo $collection->getSelect();
       // exit;
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    /**
     * @return $this
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'id',
            ['header' => __('ID'), 'index' => 'id', 'width' => 10]
        );

        $this->addColumn(
            'pet_name',
            [
                'header' => __('Pet Name'),
                'type' => 'text',
                'index' => 'pet_name',
                'default' => ' ---- '
            ]
        );

        $this->addColumn(
            'pet_type',
            [
                'header' => __('Pet Type'),
                'type' => 'text',
                'index' => 'pet_type',
                'default' => ' ---- '
            ]
        );
        $this->addColumn(
            'breed_type',
            [
                'header' => __('Breed'),
                'type' => 'text',
                'index' => 'breed_type',
                'default' => ' ---- '
            ]
        );
        $this->addColumn(
            'pet_bday',
            [
                'header' => __('Pet Birthday'),
                'type' => 'text',
                'index' => 'pet_bday',
                'default' => ' ---- '
            ]
        );

        $this->addColumn(
            'customer_id',
            [
                'header' => __('Customer ID'),
                'type' => 'text',
                'index' => 'customer_id',
                'default' => ' ---- '
            ]
        );

        $this->addColumn(
            'store_id',
            [
                'header' => __('Store ID'),
                'type' => 'text',
                'index' => 'store_id',
                'default' => ' ---- '
            ]
        );

        return parent::_prepareColumns();
    }
}
