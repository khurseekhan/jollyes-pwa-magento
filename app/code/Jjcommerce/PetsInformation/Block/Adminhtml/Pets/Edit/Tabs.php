<?php
namespace Jjcommerce\PetsInformation\Block\Adminhtml\Pets\Edit;

use Magento\Customer\Controller\RegistryConstants;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\Context;
use Magento\Ui\Component\Layout\Tabs\TabWrapper;

/**
 * Class PetsInfoTab
 *
 * @package Magento\Review\Block\Adminhtml
 */
class Tabs extends TabWrapper
{
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * @return string|null
     */
    protected $isAjaxLoaded = true;

    /**
     * Constructor
     *
     * @param Context $context
     * @param Registry $registry
     * @param array $data
     */
    public function __construct(Context $context, Registry $registry, array $data = [])
    {
        $this->coreRegistry = $registry;
        parent::__construct($context, $data);
    }
    /**
     * @inheritdoc
     */
    public function getTabLabel()
    {
        return __('Pet Information');
    }

    /**
     * @return bool
     */
    public function canShowTab()
    {
        return $this->coreRegistry->registry(RegistryConstants::CURRENT_CUSTOMER_ID);
    }

    /**
     * Return URL link to Tab content
     *
     * @return string
     */
    public function getTabUrl()
    {
        return $this->getUrl('petsinfo/*/petsinfo', ['_current' => true]);
    }
}