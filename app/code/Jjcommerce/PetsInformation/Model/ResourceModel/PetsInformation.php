<?php
/**
 * Copyright © 2015 Jjcommerce. All rights reserved.
 */

namespace Jjcommerce\PetsInformation\Model\ResourceModel;


class PetsInformation extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * Initialize resources
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('jj_pet_collection', 'id');
    }

}
