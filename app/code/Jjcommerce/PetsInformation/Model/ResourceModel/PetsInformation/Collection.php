<?php
/**
 * Copyright © 2015 Jjcommerce. All rights reserved.
 */

namespace Jjcommerce\PetsInformation\Model\ResourceModel\PetsInformation;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Jjcommerce\PetsInformation\Model\PetsInformation',
                     'Jjcommerce\PetsInformation\Model\ResourceModel\PetsInformation');
    }
}
