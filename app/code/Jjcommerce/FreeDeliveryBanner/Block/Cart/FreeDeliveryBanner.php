<?php
/**
 * @category    Jjcommerce
 * @package     Jjcommerce_FreeDeliveryBanner
 * @author	Jjcommerce Team<support@2jdesign.co.uk>
 */

namespace Jjcommerce\FreeDeliveryBanner\Block\Cart;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class FreeDeliveryBanner extends \Magento\Checkout\Block\Cart\AbstractCart
{
    /**
     * @var \Magento\Checkout\Model\CompositeConfigProvider
     */
    protected $configProvider;

    /**
     * @var array|\Magento\Checkout\Block\Checkout\LayoutProcessorInterface[]
     */
    protected $layoutProcessors;

    /**
     * @var Address\RateRequestFactory
     */
    protected $_rateRequestFactory;

    /**
     * @var \Magento\Quote\Model\Quote\Address\RateCollectorInterfaceFactory
     */
    protected $_rateCollector;

    /**
     * @var \Magento\Framework\Pricing\Helper\Data
     */
    protected $pricingHelper;

    protected  $_isFreeShipping = false ;

    protected  $_spendValue ;

    protected  $_spendValueFreeShipping ;

    protected $_countryId;

    protected $_postCode;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Checkout\Model\CompositeConfigProvider $configProvider
     * @param \Magento\Quote\Model\Quote\Address\RateRequestFactory $rateRequestFactory
     * @param \Magento\Quote\Model\Quote\Address\RateCollectorInterfaceFactory $rateCollector
     * @param \Jjcommerce\FreeDeliveryBanner\Helper\Data $dataHelper
     * @param \Magento\Quote\Model\Quote\Address\RateFactory $addressRateFactory
     * @param \Magento\Framework\Pricing\Helper\Data $pricingHelper
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param array $layoutProcessors
     * @param array $data
     * @codeCoverageIgnore
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Checkout\Model\CompositeConfigProvider $configProvider,
        \Magento\Quote\Model\Quote\Address\RateRequestFactory $rateRequestFactory,
        \Magento\Quote\Model\Quote\Address\RateCollectorInterfaceFactory $rateCollector,
        \Jjcommerce\FreeDeliveryBanner\Helper\Data $dataHelper,
        \Magento\Quote\Model\Quote\Address\RateFactory $addressRateFactory,
        \Magento\Framework\Pricing\Helper\Data $pricingHelper,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        array $layoutProcessors = [],
        array $data = []
    ) {
        $this->configProvider = $configProvider;
        $this->layoutProcessors = $layoutProcessors;
        $this->_isScopePrivate = true;
        $this->_checkoutSession = $checkoutSession;
        $this->_rateRequestFactory = $rateRequestFactory;
        $this->_customerSession = $customerSession;
        $this->_rateCollector = $rateCollector;
        $this->_dataHelper = $dataHelper;
        $this->_addressRateFactory = $addressRateFactory;
        $this->_pricingHelper = $pricingHelper;
        $this->_messageManager = $messageManager;
        parent::__construct($context, $customerSession, $checkoutSession, $data);
    }

    /**
     * Get base url for block.
     *
     * @return string
     * @codeCoverageIgnore
     */
    public function getAllShippingRates()
    {
        $quote = $this->_checkoutSession->getQuote();
        $shippingAddress = $this->_checkoutSession->getQuote()->getShippingAddress();
        $cartSubTotal = $this->_checkoutSession->getQuote()->getSubtotal();

       // $this->_checkoutSession->getQuote()->getShippingAddress()->setCollectShippingRates(true);
       // $rates = $shippingAddress->getAllShippingRates();

        /** @var $request \Magento\Quote\Model\Quote\Address\RateRequest */
        $request = $this->_rateRequestFactory->create();
        $request->setAllItems($quote->getAllItems());
        $customer = $this->_customerSession->getCustomer();

       $defaultPostCode = trim($this->_dataHelper->getDefaultShippingPostcode());
       $defaultCountry = $this->_dataHelper->getDefaultShippingCountry();

      // echo "<br>eeeee".$quote->getShippingAddress()->getCountryId();
     //  echo "<br>eeeee".$quote->getShippingAddress()->getPostcode();


        if($this->_customerSession->isLoggedIn()){
            if($quote->getShippingAddress()->getCountryId())
                $this->_countryId = $quote->getShippingAddress()->getCountryId();
                $this->_postCode = $quote->getShippingAddress()->getPostcode();
            if($customer->getDefaultBillingAddress()){
                $this->_countryId = $customer->getDefaultBillingAddress()->getCountryId();
                $this->_postCode =  $customer->getDefaultBillingAddress()->getPostcode();
            }elseif($customer->getDefaultShippingAddress()){
                $this->_countryId = $customer->getDefaultShippingAddress()->getCountryId();
                $this->_postCode = $customer->getDefaultShippingAddress()->getPostcode();
            }elseif($customer->getPrimaryBillingAddress()){
                $this->_countryId = $customer->getPrimaryBillingAddress()->getCountryId();
                $this->_postCode = $customer->getPrimaryBillingAddress()->getPostcode();
            }elseif($customer->getPrimaryShippingAddress()){
                $this->_countryId = $customer->getPrimaryShippingAddress()->getCountryId();
                $this->_postCode = $customer->getPrimaryShippingAddress()->getPostcode();
            }else{
                $this->_countryId = $defaultCountry;
                $this->_postCode = $defaultPostCode;
            }

            $request->setDestCountryId($this->_countryId);
            $request->setDestPostcode($this->_postCode);

        }else{
            if ($quote->getShippingAddress()->getCountryId()) {
                $this->_countryId = $quote->getShippingAddress()->getCountryId();
                $this->_postCode = $quote->getShippingAddress()->getPostcode();
            }else {
                $this->_countryId =  $defaultCountry;
                $this->_postCode =  $defaultPostCode;
            }

            $request->setDestCountryId($this->_countryId);
            $request->setDestPostcode($this->_postCode);
        }

        $request->setPackageValue($quote->getShippingAddress()->getBaseSubtotal());
        $packageWithDiscount = $quote->getShippingAddress()->getBaseSubtotalWithDiscount();
        $request->setPackageValueWithDiscount($packageWithDiscount);
        $request->setPackageWeight($quote->getShippingAddress()->getWeight());
        $request->setPackageQty($quote->getShippingAddress()->getItemQty());

        /**
         * Need for shipping methods that use insurance based on price of physical products
         */
        $packagePhysicalValue = $quote->getShippingAddress()->getBaseSubtotal();
        $request->setPackagePhysicalValue($packagePhysicalValue);

        /**
         * Store and website identifiers need specify from quote
         */
        $request->setStoreId($quote->getStore()->getId());
        $request->setWebsiteId($quote->getStore()->getWebsiteId());

        /**
         * Currencies need to convert in free shipping
         */
        $request->setBaseCurrency($quote->getStore()->getBaseCurrency());
        $request->setPackageCurrency($quote->getStore()->getCurrentCurrency());
        $request->setLimitCarrier($this->getLimitCarrier());

        $result = $this->_rateCollector->create()->collectRates($request)->getResult();

        if ($result) {
            $shippingRates = $result->getAllRates();
        }

        return  $shippingRates;
    }

    /*
     * Get required value/price to spend to get free shipping
     * @return (int)
     * */
    public function getFreeShippingSpendValue()
    {
        if(!$this->_dataHelper->isEnabled($this->_checkoutSession->getQuote()->getStore()->getId())){
            $this->messageManager->addError(
                __('Please Check module configuration setting.')
            );

            return;
        }

        $cartTotals = $this->_checkoutSession->getQuote()->getTotals();
        $cartSubTotal = $cartTotals['subtotal']['value_incl_tax'];

        $shippingRates = $this->getAllShippingRates();
        if($shippingRates) {
            foreach ($shippingRates as $shippingRate) {
                $rate = $this->_addressRateFactory->create()->importShippingRate($shippingRate);
                $method = $rate->getMethodTitle();
               // if ($rate->getCarrier() == 'matrixrate' && $method == 'Free Delivery') {
                if ($rate->getCarrier() == 'matrixrate') {
                    $this->_isFreeShipping = true;
                }
            }
        }

        if($this->_isFreeShipping){
            if($cartSubTotal < $this->_dataHelper->getThresholdValue()){
                $thresholdValue = $this->_dataHelper->getThresholdValue();
                $this->_spendValue =  ($thresholdValue - $cartSubTotal);
            }else{
                $this->_spendValue = 0;
            }
        }else{
            $this->_spendValue = 0;
        }

        if($this->_spendValue > 0 && $this->_spendValue){
            $this->_spendValueFreeShipping = $this->_spendValue ;
        }else{
            $this->_spendValueFreeShipping = 0;
        }

        return $this->_spendValueFreeShipping;

    }

    /*
     * Get price format
     *
     * */
    public function getSpendPrice(){

        return $this->_pricingHelper->currency($this->_spendValue, true, false);
    }


}
