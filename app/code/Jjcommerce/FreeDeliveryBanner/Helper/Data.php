<?php
/**
 * @category    Jjcommerce
 * @package     Jjcommerce_FreeDeliveryBanner
 * @author	Jjcommerce Team<support@2jdesign.co.uk>
 */
namespace Jjcommerce\FreeDeliveryBanner\Helper;


use Magento\Store\Model\Store;
use Magento\Store\Model\ScopeInterface;


class Data extends \Magento\Framework\App\Helper\AbstractHelper
{

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;

    const XML_PATH_ENABLED = 'free_delivery_banner/general/enabled';

    const XML_FREE_SHIPPING_VALUE = 'free_delivery_banner/general/threshold_value';

    const XML_PATH_DEFAULT_POST_CODE = 'free_delivery_banner/general/post_code';

    const XML_PATH_DEFAULT_COUNTRY_ID = 'free_delivery_banner/general/country_id';

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @codeCoverageIgnore
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->_storeManager = $storeManager;
        parent::__construct($context);
    }


    /**
     * Check if enabled
     *
     * @return string|null
     */
    public function isEnabled()
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_ENABLED,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }


    /**
     * Get Shipping Setting Default Country
     *
     */
    public function getDefaultShippingCountry()
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_DEFAULT_COUNTRY_ID,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }


    /**
     * Get Shipping Setting Default Zip Code
     *
     */
    public function getDefaultShippingPostcode()
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_DEFAULT_POST_CODE,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /*
     * Get threshold value/price for free shipping banner
     * @return Float
     * */
    public function getThresholdValue()
    {
        return $this->scopeConfig->getValue(
            self::XML_FREE_SHIPPING_VALUE,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

}
