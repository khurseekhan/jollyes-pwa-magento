<?php
/**
 * Jjcommerce_BrandSlider
 *
 * PHP version 5.x
 *
 * @category  PHP
 * @package   Jjcommerce\BrandSlider
 * @author    Swapnil Tatkondawar <swapnilt@2jcommerce.in>
 * @copyright 2017 Copyright 2J Commerce, Inc. http://www.2jcommerce.in/
 * @license   http://www.2jcommerce.in/ Private
 * @link      http://www.jollyes.co.uk
 */
namespace Jjcommerce\BrandSlider\Block\Widget;

ini_set("display_errors", 1);
use Amasty\ShopbyBase\Helper\OptionSetting as OptionSettingHelper;
use Magento\Framework\View\Element\Template\Context;
use Magento\Catalog\Model\Product\Attribute\Repository;
use Magento\Framework\Registry;
use Magento\Catalog\Model\Layer\Resolver;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\Catalog\Model\Product\Visibility;
use Magento\CatalogInventory\Helper\Stock;
use Magento\Catalog\Model\CategoryFactory;
use Amasty\ShopbyBase\Api\Data\OptionSettingInterface;
use Magento\Store\Model\ScopeInterface;
use Amasty\ShopbyBase\Api\UrlBuilderInterface;
use Amasty\ShopbyBrand\Model\ProductCount;
/**
 * Class BrandSlider
 * @package Amasty\ShopbyBrand\Block\Widget
 * @author Evgeni Obukhovsky
 */
class BrandSlider extends \Amasty\ShopbyBrand\Block\Widget\BrandSlider
{
    /**
     * @var CollectionFactory
     */
    protected $productCollectionFactory;

    /**
     * @var Visibility
     */
    protected $catalogProductVisibility;

    /**
     * @var \Magento\Catalog\Model\Layer
     */
    protected $catalogLayer;

    /**
     * @var CategoryFactory
     */
    protected $_categoryFactory;

    private $_registry;

    protected $_storeManager;

    protected $_scopeConfig;

    protected $repository;

    protected $optionSettingHelper;

    const BRAND_ATTRIBUTE_CODE = 'brands';

    /**
     * BrandSlider constructor.
     * @param Context $context
     * @param Repository $repository
     * @param OptionSettingHelper $optionSetting
     * @param Registry $registry
     * @param CollectionFactory $productCollectionFactory
     * @param CategoryFactory $categoryFactory
     * @param Visibility $catalogProductVisibility
     * @param Resolver $layerResolver
     * @param array $data
     */
    public function __construct(
        Repository $repository,
        Registry $registry,
        CollectionFactory $productCollectionFactory,
        CategoryFactory $categoryFactory,
        Visibility $catalogProductVisibility,
        Resolver $layerResolver,
        OptionSettingHelper $optionSetting,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        $this->productCollectionFactory = $productCollectionFactory;
        $this->catalogProductVisibility = $catalogProductVisibility;
        $this->_categoryFactory = $categoryFactory;
        $this->catalogLayer = $layerResolver->get();
        $this->optionSettingHelper = $optionSetting;
        $this->registry = $registry;
        $this->repository =  $repository;
        $this->_storeManager = $storeManager;
        $this->_scopeConfig = $scopeConfig;
    }

    public function _construct()
    {
        parent::_construct();
    }

    /**
     * Get Brands Value From Top Category Products
     * @return array|bool
     */
    public function getBrandsFromTopCategories(){

        $brands = array();

        if (!isset($category)) {
            $currentCategory = $this->getCurrentCategory();
        }

        $path = $currentCategory->getPath();

        $ids = explode('/', $path);

        if (isset($ids[2])){
            $topCategoryId = $ids[2];
            $topCategory = $this->_categoryFactory->create()->load($topCategoryId);
        } else {
            $topCategory = $currentCategory;
        }

        $categories = $topCategory->getChildrenCategories();

        if ($categories && count($categories) < 1) {
            return false;
        }

        $storeId = $this->_storeManager->getStore()->getId();

        /** @var $collection \Magento\Catalog\Model\ResourceModel\Product\Collection */
        $collection = $this->productCollectionFactory->create();
        $collection->setVisibility($this->catalogProductVisibility->getVisibleInCatalogIds());

        $collection->addAttributeToSelect(array('name','brands'))
            ->addCategoryFilter($topCategory)
            ->addStoreFilter($storeId)
            ->addAttributeToFilter('brands', array('neq' => ''))
            ->setPageSize($this->getPageSize());

        if (count($collection->getItems()) > 1) {

            foreach ($collection->getItems() as $item) {
                $brands[] = $this->getOptionLabelByValue($item, $item->getBrands());
            }
        }
        $brands = array_unique($brands);

        return $brands;
    }


    /**
     * @return array
     */
    public function getItems()
    {
        $brands = $this->getBrandsFromTopCategories();

        if ($this->items === null) {
            $this->items = [];
            $attributeCode = $this->_scopeConfig->getValue(
                self::PATH_BRAND_ATTRIBUTE_CODE,
                ScopeInterface::SCOPE_STORE
            );
            if ($attributeCode == '') {
                return $this->items;
            }

            $options = $this->repository->get($attributeCode)->getOptions();
            array_shift($options);


            foreach ($options as $option) {
                $filterCode = \Amasty\ShopbyBase\Helper\FilterSetting::ATTR_PREFIX . $attributeCode;
                $setting = $this->optionSettingHelper->getSettingByValue(
                    $option->getValue(),
                    $filterCode,
                    $this->_storeManager->getStore()->getId()
                );

                if(in_array($setting->getLabel(), $brands)) {
                    if ($setting->getIsFeatured() && $setting->getSliderImageUrl()) {
                        $this->items[] = [
                            'label' => $setting->getLabel(),
                            'url' => $setting->getUrlPath(),
                            'img' => $setting->getSliderImageUrl(),
                            'position'  => $setting->getSliderPosition()
                        ];
                    }
                }
            }
        }

        return $this->items;
    }

    /**
     * Get Label by option id
     * @param $_product
     * @param $optionId
     * @return string
     */
    public function getOptionLabelByValue($_product,$optionId)
    {
        $isAttributeExist = $_product->getResource()->getAttribute(self::BRAND_ATTRIBUTE_CODE);
        $optionText = '';
        if ($isAttributeExist && $isAttributeExist->usesSource()) {
            $optionText = $isAttributeExist->getSource()->getOptionText($optionId);
        }

        return $optionText;
    }

    /**
     * get Current Category
     * @return Category|null
     */
    public function getCurrentCategory()
    {
        /** @var Category $category */
        $category = null;
        if ($this->catalogLayer) {
            $category = $this->catalogLayer->getCurrentCategory();
        } elseif ($this->_registry->registry('current_category')) {
            $category = $this->_registry->registry('current_category');
        }
        return $category;
    }
}
