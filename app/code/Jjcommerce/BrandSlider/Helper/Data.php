<?php
/**
 * Jjcommerce_BrandSlider
 *
 * PHP version 5.x
 *
 * @category  PHP
 * @package   Jjcommerce\BrandSlider
 * @author    Swapnil Tatkondawar <swapnilt@2jcommerce.in>
 * @copyright 2017 Copyright 2J Commerce, Inc. http://www.2jcommerce.in/
 * @license   http://www.2jcommerce.in/ Private
 * @link      http://www.jollyes.co.uk
 */
namespace Jjcommerce\BrandSlider\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

class Data extends AbstractHelper
{
    const XML_PATH_AM_BRAND_SLIDER_SORTBY = 'amshopby_brand/slider/sort_by';

    /**
     * Data constructor.
     * @param \Magento\Framework\App\Helper\Context $context
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context
    ) {
        parent::__construct($context);
    }

    /**
     * Check if enabled
     *
     * @return string|null
     */
    public function brandSliderSortBy()
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_AM_BRAND_SLIDER_SORTBY,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }



}
