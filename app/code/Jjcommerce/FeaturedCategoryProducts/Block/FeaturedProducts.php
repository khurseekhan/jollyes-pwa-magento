<?php
/**
 * Jjcommerce_FeaturedCategoryProducts
 *
 * PHP version 5.x
 *
 * @category  PHP
 * @package   Jjcommerce\FeaturedCategoryProducts
 * @author    Swapnil Tatkondawar <swapnilt@2jcommerce.in>
 * @copyright 2017 Copyright 2J Commerce, Inc. http://www.2jcommerce.in/
 * @license   http://www.2jcommerce.in/ Private
 * @link      http://www.jollyes.co.uk/
 */
namespace Jjcommerce\FeaturedCategoryProducts\Block;

use Magento\Catalog\Block\Product\Context;
use Jjcommerce\FeaturedCategoryProducts\Helper\Data;
use Magento\Catalog\Model\Layer\Resolver;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\Catalog\Model\Product\Visibility;
use Magento\CatalogInventory\Helper\Stock;
use Magento\Framework\App\Http\Context as HttpContext;
use Magento\Customer\Model\Context as CustomerContext;

class FeaturedProducts extends \Magento\Catalog\Block\Product\AbstractProduct
{
    const CACHE_TAG = 'featured_category_products';

    /** @var \Jjcommerce\CategoryRelation\Helper\Data  */
    private $_dataHelper;

    /** @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory */
    private $productCollectionFactory;

    /** @var \Magento\Catalog\Model\Product\Visibility $catalogProductVisibility */
    private $catalogProductVisibility;

    /** @var \Magento\Store\Model\StoreManagerInterface $storeManager */
    private $storeManager;

    /** @var \Magento\Catalog\Model\Layer $catalogLayer */
    private $catalogLayer;

    /** @var \Magento\Framework\Registry $registry */
    private $registry;

    /** @var \Magento\CatalogInventory\Helper\Stock $stockHelper */
    private $stockHelper;

    /**
     * @var \Magento\Framework\App\Http\Context
     */
    protected $_httpContext;

    /**
     * FeaturedProducts constructor.
     * @param Context $context
     * @param Data $dataHelper
     * @param CollectionFactory $productCollectionFactory
     * @param Visibility $catalogProductVisibility
     * @param Resolver $layerResolver
     * @param Stock $stockHelper
     * @param HttpContext $httpContext
     * @param array $data
     */
    public function __construct(
        Context $context,
        Data $dataHelper,
        CollectionFactory $productCollectionFactory,
        Visibility $catalogProductVisibility,
        Resolver $layerResolver,
        Stock $stockHelper,
        HttpContext $httpContext,
        $data = []
    ) {
        $this->_httpContext = $httpContext;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->catalogProductVisibility = $catalogProductVisibility;
        $this->registry = $context->getRegistry();
        $this->storeManager = $context->getStoreManager();
        $this->catalogLayer = $layerResolver->get();
        $this->stockHelper = $stockHelper;
        $this->_dataHelper = $dataHelper;
        parent::__construct(
            $context,
            $data
        );
    }

	/**
     * @return void
    */
    protected function _construct()
    {
        $this->addData(
            ['cache_lifetime' => 86400, 'cache_tags' => [self::CACHE_TAG]]
        );
    }

	 /**
     * Get Key pieces for caching block content
     *
     * @return array
     */
    public function getCacheKeyInfo()
    {
        return [
           'FEATURED_CATEGORY_PRODUCTS_'.$this->catalogLayer->getCurrentCategory()->getId(),
           $this->storeManager->getStore()->getId(),
           $this->_design->getDesignTheme()->getId(),
           $this->_httpContext->getValue(CustomerContext::CONTEXT_GROUP),
           'template' => $this->getTemplate()
        ];

    }

    protected function _beforeToHtml()
    {
        if (!isset($category)) {
            $currentCategory = $this->getCurrentCategory();
        }

        if ($this->_dataHelper->IsModuleEnabled() && $currentCategory->getLevel()==2) {
            $productCollection = $this->createCollection();
            if ($productCollection) {
                $this->setProductCollection($productCollection);
                return $this;
            } else {
                parent::_beforeToHtml();
            }
        } else {
            parent::_beforeToHtml();
        }
    }

    /**
     * Prepare and return product collection
     *
     * @return \Magento\Catalog\Model\ResourceModel\Product\Collection
     */
    public function createCollection()
    {
        if (!isset($category)) {
            $currentCategory = $this->getCurrentCategory();
        }

        $categories = $currentCategory->getChildrenCategories();
        if ($categories && count($categories) < 1) {
            return false;
        }

        $limit = $this->_dataHelper->getFeaturedProductLimit();

        $storeId = $this->getCurrentStoreId();

        /** @var $collection \Magento\Catalog\Model\ResourceModel\Product\Collection */
        $collection = $this->productCollectionFactory->create();
        $collection->setVisibility($this->catalogProductVisibility->getVisibleInCatalogIds());

        $collection = $this->_addProductAttributesAndPrices($collection)
            ->addCategoryFilter($currentCategory)
            ->addStoreFilter($storeId)
            //->addVisibleFilter()
            ->addAttributeToFilter('inchoo_featured_product', 1)
            ->setPageSize($this->getPageSize());

        $this->stockHelper->addInStockFilterToCollection($collection);
        $collection->getSelect()->order($this->getSortBy())->limit($limit);

        return $collection;
    }

    /**
     * Retrieves a current category
     *
     * @return Category
     */
    public function getCurrentCategory()
    {
        /** @var Category $category */
        $category = null;
        if ($this->catalogLayer) {
            $category = $this->catalogLayer->getCurrentCategory();
        } elseif ($this->registry->registry('current_category')) {
            $category = $this->registry->registry('current_category');
        }
        return $category;
    }

    /**
     * Get current store id
     * @return int
     */
    private function getCurrentStoreId()
    {
        return $this->storeManager->getStore()->getId();
    }

    /**
     * Return Data Helper object
     * @return \Jjcommerce\CategoryRelation\Helper\Data
     */
    public function getDataHelper()
    {
        return $this->_dataHelper;
    }

    /**
     * Return identifiers for produced content
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG];
    }
}
