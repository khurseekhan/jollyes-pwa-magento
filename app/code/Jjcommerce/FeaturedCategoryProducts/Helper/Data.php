<?php
/**
 * Jjcommerce_FeaturedCategoryProducts
 *
 * PHP version 5.x
 *
 * @category  PHP
 * @package   Jjcommerce\FeaturedCategoryProducts
 * @author    Swapnil Tatkondawar <swapnilt@2jcommerce.in>
 * @copyright 2017 Copyright 2J Commerce, Inc. http://www.2jcommerce.in/
 * @license   http://www.2jcommerce.in/ Private
 * @link      http://www.jollyes.co.uk
 */
namespace Jjcommerce\FeaturedCategoryProducts\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Helper\Context;

class Data extends AbstractHelper
{
    const XML_PATH_FEATURED_IS_ENABLED      =   'jjcommerce/featured_products/is_enabled';
    const XML_PATH_FEATURED_DISPLAY_TITLE   =   'jjcommerce/featured_products/display_title';
    const XML_PATH_FEATURED_NO_OF_PRODUCTS  =   'jjcommerce/featured_products/no_of_products';

    /**
     * @var null
     */
    private $featuredProductVars = null;

    /**
     * @var StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * Data constructor.
     * @param Context $context
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        Context $context,
        StoreManagerInterface $storeManager
    ) {
        $this->_storeManager = $storeManager;
        parent::__construct($context);
    }


    /**
     * Get Featured display title from system configuration
     * @return string
     */
    public function getFeaturedDisplayTitle()
    {
        if (!isset($this->featuredProductVars['display_title'])) {
            $this->featuredProductVars['display_title'] = $this->scopeConfig->getValue(
                self::XML_PATH_FEATURED_DISPLAY_TITLE,
                ScopeInterface::SCOPE_STORE
            );
        }
        return $this->featuredProductVars['display_title'];
    }

    /**
     * Get Featured no. of product to display  from system configuration
     * @return string|int
     */
    public function getFeaturedProductLimit()
    {
        if (!isset($this->featuredProductVars['no_of_products'])) {
            $this->featuredProductVars['no_of_products'] = $this->scopeConfig->getValue(
                self::XML_PATH_FEATURED_NO_OF_PRODUCTS,
                ScopeInterface::SCOPE_STORE
            );
        }
        return $this->featuredProductVars['no_of_products'];
    }

    /**
     * Get config of category product tabs
     * @param $configPath
     * @param null $store
     * @return mixed
     */
    public function getTabsConfig($configPath, $store = null)
    {

        return $this->_scopeConfig->getValue(
            $configPath,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * Check weather module is enable or disable
     * @param null $store
     * @return bool
     */
    public function isEnabled($store = null)
    {
        $isModuleEnabled       = $this->isModuleEnabled();
        $isModuleOutputEnabled = $this->isModuleOutputEnabled();

        return $isModuleOutputEnabled && $isModuleEnabled && $this->getTabsConfig(self::XML_PATH_FEATURED_IS_ENABLED, $store);
    }

    /**
     * Check module is enable in module list
     * @return mixed
     */
    public function isModuleEnabled()
    {
        $moduleName = "Jjcommerce_FeaturedCategoryProducts";

        return $this->_moduleManager->isEnabled($moduleName);
    }


}
