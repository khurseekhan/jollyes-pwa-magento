<?php
/**
 * Jjcommerce_FeaturedCategoryProducts
 *
 * PHP version 5.x
 *
 * @category  XML
 * @package   Jjcommerce\FeaturedCategoryProducts
 * @author    Swapnil Tatkondawar <swapnilt@2jcommerce.in>
 * @copyright 2017 Copyright 2J Commerce, Inc. http://www.2jcommerce.in/
 * @license   http://www.2jcommerce.in/ Private
 * @link      http://www.jollyes.co.uk
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Jjcommerce_FeaturedCategoryProducts',
    __DIR__
);
