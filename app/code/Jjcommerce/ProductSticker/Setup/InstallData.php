<?php
/**
 * Jjcommerce_ProductSticker
 *
 * PHP version 5.x
 *
 * @category  XML
 * @package   Jjcommerce\ProductSticker
 * @author    Khushbu Dhoot <khushbu@2jcommerce.in>
 * @copyright 2017 Copyright 2J Commerce, Inc. http://www.2jcommerce.in/
 * @license   http://www.2jcommerce.in/ Private
 * @link      http://www.jollyes.co.uk
 */
namespace Jjcommerce\ProductSticker\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Eav\Setup\EavSetupFactory;

class InstallData implements InstallDataInterface
{
    /**
     * @var EavSetupFactory
     */
    protected $eavSetupFactory;

    /**
     * UpgradeData constructor
     *
     * @param EavSetupFactory $eavSetupFactory
     */
    public function __construct(EavSetupFactory $eavSetupFactory)
    {
        $this->eavSetupFactory = $eavSetupFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        /** @var \Magento\Eav\Setup\EavSetup $eavSetup */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

        if (version_compare($context->getVersion(), '2.0.1', '<')) {
            $this->upgradeVersionTwoZeroOne($eavSetup);
        }

        $setup->endSetup();
    }

    /**
     * Upgrade to Version 2.0.1
     * @param \Magento\Eav\Setup\EavSetup $eavSetup
     * @return void
     */
    private function upgradeVersionTwoZeroOne($eavSetup)
    {
        $attributeGroup = 'Jjcommerce Attributes';

        $attributes = array(
            'sticker_flag' => 'Product Sticker Flag',
            'sticker_ribbons' => 'Product Sticker Ribbons');

        foreach($attributes as $attributeCode=>$attributeLabel){

            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                $attributeCode,
                [
                    'type' => 'int',
                    'label' => $attributeLabel,
                    'input' => 'select',
                    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                    'visible' => true,
                    'required' => false,
                    'user_defined' => true,
                    'group' => $attributeGroup,
                    'visible_on_front' => true,
                    'filterable_in_search' => true,
                    'used_in_product_listing' => true
                ]
            );

            $entityTypeId = $eavSetup->getEntityTypeId(\Magento\Catalog\Model\Product::ENTITY);

            $attributeSetId = $eavSetup->getDefaultAttributeSetId($entityTypeId);
            $eavSetup->addAttributeToSet($entityTypeId, $attributeSetId, $attributeGroup, $attributeCode);
        }
    }

}
