<?php
/**
 * Jjcommerce_ProductSticker
 *
 * PHP version 5.x
 *
 * @category  XML
 * @package   Jjcommerce\ProductSticker
 * @author    Khushbu Dhoot <khushbu@2jcommerce.in>
 * @copyright 2017 Copyright 2J Commerce, Inc. http://www.2jcommerce.in/
 * @license   http://www.2jcommerce.in/ Private
 * @link      http://www.jollyes.co.uk
 */
namespace Jjcommerce\ProductSticker\Helper;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Jjcommerce\StoreLocator\Model\StorelocatorFactory;

class Data extends AbstractHelper
{
    protected $_coreDate = null;

    /**
     * Core store config
     *
     * @var ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * Get request
     *
     * @var request
     */
    protected $_request;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var Magento\CatalogInventory\Api\StockStateInterface
     */
    protected $stockState;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $registry;

    /**
     * @var StorelocatorFactory
     */
    protected $storelocatorFactory;

    /**
     * @var string
     */
    protected $storeCode = "33960";

    /**
     * Product Sticker is Enabled
     */
    const XML_PATH_PRODUCT_STICKER_IS_ENABLED = 'product_sticker/general/is_enabled';

    /**
     * Special Price SAVE tag Image
     */
    const XML_PATH_SPECIAL_PRICE_TAG_IMAGE = 'product_sticker/general/image';

    const IMAGES_DIR = '/amasty/shopby/option_images/';


    /**
     * __construct
     * @param Context $context
     * @param StoreManagerInterface $storeManager
     * @param \Amasty\ShopbyBase\Helper\OptionSetting $optionSettingHelper
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $coreDate
     */
    public function __construct(
        Context $context,
        StoreManagerInterface $storeManager,
        \Amasty\ShopbyBase\Helper\OptionSetting $optionSettingHelper,
        \Magento\Framework\Stdlib\DateTime\DateTime $coreDate,
        \Magento\Catalog\Model\ProductRepository $repository,
        \Magento\CatalogInventory\Api\StockStateInterface $stockState,
        \Magento\Framework\Registry $registry,
        ProductRepositoryInterface $productRepository,
        StorelocatorFactory $storelocatorFactory
    )
    {
        $this->_scopeConfig  = $context->getScopeConfig();
        $this->_storeManager = $storeManager;
        $this->optionSettingHelper = $optionSettingHelper;
        $this->_coreDate = $coreDate;
        $this->repos = $repository;
        $this->productRepository = $productRepository;
        $this->stockState = $stockState;
        $this->registry = $registry;
        $this->storelocatorFactory = $storelocatorFactory;
        $this->_request = $context->getRequest();

        parent::__construct($context);
    }

    /**
     * Check Product Sticker Is Enabled
     * @param null $store
     * @return mixed
     */
    public function getProductStickerIsEnabled($store = null)
    {
        return $this->_scopeConfig->getValue(
            self::XML_PATH_PRODUCT_STICKER_IS_ENABLED,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * Check module is enable in module list
     * @return mixed
     */
    public function isModuleEnabled()
    {
        $moduleName = "Jjcommerce_ProductSticker";

        return $this->_moduleManager->isEnabled($moduleName);
    }

    /**
     * Check Module is enable
     * @param null $store
     * @return bool
     */
    public function isEnabled($store = null)
    {
        $isModuleEnabled       = $this->isModuleEnabled();
        return $isModuleEnabled && $this->getProductStickerIsEnabled();
    }

    /**
     * Get Sticker Flag Image
     * @param $product
     * @return image url
     */
    public function getFlagImage($product){

        $stickerFlagIds = $product->getProductSticker();

        $attributeOptions = '';
        $storeId = $this->_storeManager->getStore()->getId();
        $stickerFlagId = explode(',',$stickerFlagIds);
        foreach($stickerFlagId as $stickerFlag){
            $attributeOptions = $this->optionSettingHelper->getSettingByValue($stickerFlag, 'attr_product_sticker', $storeId);
        }
        $specialPrice = $product->getSpecialPrice();

        $dateTime = date("Y-m-d H:i:s");

        if($attributeOptions->getImage()!= null){
            return $html ="<img src='". $this->getImageUrl($attributeOptions->getImage())."' class='corner-tags' alt=''>";

        }elseif($product->getTypeId() == 'configurable'){

            $_children = $product->getTypeInstance()->getUsedProducts($product);
            $finalPrice = [];
            $regularPrice = [];
            $flag = false;

            foreach ($_children as $child) {

                // $finalPrice[] = number_format($child->getPriceInfo()->getPrice('final_price')->getAmount()->getValue(),2);

                //$regularPrice[] = number_format($child->getPriceInfo()->getPrice('regular_price')->getAmount()->getValue(),2);

                $specialChildPrice = $child->getSpecialPrice();

                if(($specialChildPrice) && ($child->getSpecialFromDate() <= $dateTime)){
                    if(($child->getSpecialToDate() >= $dateTime) || ($child->getSpecialToDate()==null)){
                        $flag = true;
                        break;
                    }
                }

            }
            /*if(!empty($finalPrice) && !empty($regularPrice)){
                $minFinalPrice = min($finalPrice);
                $minRegularPrice = min($regularPrice);
                if($minFinalPrice < $minRegularPrice){
                    return $html ="<img src='". $this->getImageUrl($this->getSpecialPriceImage())."' class='corner-tags' alt=''>";
                }
            }*/
            if($flag){
                return $html ="<img src='". $this->getImageUrl($this->getSpecialPriceImage())."' class='corner-tags' alt=''>";
            }else{
                return false;
            }
        }elseif(($specialPrice) && ($product->getSpecialFromDate() <= $dateTime)){
            if(($product->getSpecialToDate() >= $dateTime) || ($product->getSpecialToDate()==null)){
                return $html ="<img src='". $this->getImageUrl($this->getSpecialPriceImage())."' class='corner-tags' alt=''>";
            }
        }

        /*
        elseif ($product->getSpecialFromDate() && $specialPrice > 0) {
            if ($product->getSpecialFromDate() <= $dateTime) {
                if($specialPrice != $product->getPrice()){
                    return $html ="<img src='". $this->getImageUrl($this->getSpecialPriceImage())."' class='corner-tags' alt=''>";
                }
            }
        }elseif ($product->getSpecialFromDate() && $product->getSpecialToDate() && $specialPrice > 0) {
            if ($product->getSpecialFromDate() <= $dateTime && ($product->getSpecialToDate() >= $dateTime || $product->getSpecialToDate()== null)) {
                if($specialPrice != $product->getPrice()){
                    return $html ="<img src='". $this->getImageUrl($this->getSpecialPriceImage())."' class='corner-tags' alt=''>";
                }
            }
        }elseif($specialPrice && $specialPrice >0){
            if($specialPrice != $product->getPrice()){
                return $html ="<img src='". $this->getImageUrl($this->getSpecialPriceImage())."' class='corner-tags' alt=''>";
            }
        }*/
        return false;
    }

    /**
     * Get Sticker Ribbon Image
     * @param $product
     * @return image url
     */
    public function getRibbonImage($product){
        $stickerRibbonId = $product->getStickerRibbons();
        $storeId = $this->_storeManager->getStore()->getId();
        $attributeOptions = $this->optionSettingHelper->getSettingByValue($stickerRibbonId, 'attr_sticker_ribbons', $storeId);
        if($attributeOptions->getImage()!= null){
            return $html ="<img src='". $this->getImageUrl($attributeOptions->getImage())."' class='corner-tags' alt=''>";

        }
        return false;
    }


    public function getImageUrl($image)
    {
        if(!isset($image)){
            return null;
        }
        $url = $this->_storeManager->getStore()->getBaseUrl(
                \Magento\Framework\UrlInterface::URL_TYPE_MEDIA
            ) . self::IMAGES_DIR . $image;

        return $url;
    }

    public function getSpecialPriceImage($store = null){
        return $this->_scopeConfig->getValue(
            self::XML_PATH_SPECIAL_PRICE_TAG_IMAGE,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    public function getCurrentProduct()
    {
        return $this->registry->registry('current_product');
    }

    /**
     * Retrieve stock qty whether product
     *
     * @param $productId
     * @return int|mixed
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getStockQty($productId)
    {
        $product = $this->productRepository->getById($productId);
        $data = $product->getStorelocator();
        $qty = 0;
        if( is_array($data) && !empty( $data ) ):
            foreach( $data as $store => $stock ):
                if($store == $this->getWarehouseName()) {
                    $qty = $stock;
                }
            endforeach;
        endif;
        return $qty;
    }

    /**
     * Get Stock Label
     *
     * @param $productId
     * @return int|mixed
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getStockLabel($productId)
    {
        $stockQty = $this->getStockQty($productId);
        $product = $this->productRepository->getById($productId);
        $data = $product->getStorelocator();
        $sum = 0;
        if( is_array($data) && !empty( $data ) ):
            foreach( $data as $store => $stock ):
                if($store != $this->getWarehouseName()) {
                    $sum = $stock + $sum;
                }
            endforeach;
        endif;

        $stockLabel = "";
        if ($stockQty > 0 && $sum > 0) {
            //$stockLabel = " <span>Available For Delivery</span><span> Available In Store</span>";
            $stockLabel = "";
        } else if($sum > 0) {
            $stockLabel = "<span>Click & Collect only</span>";
        } else if($stockQty > 0) {
            $stockLabel = "<span>Delivery only</span>";
        }

        return $stockLabel;
    }

    /**
     * Get Warehouse name
     *
     * @return mixed
     */
    public function getWarehouseName()
    {
        $obj = $this->storelocatorFactory->create();
        $results = $obj->getCollection()
            ->addFieldToFilter("store_code", $this->storeCode)
            ->getFirstItem();

        $storeName = str_replace( ' ','_',   strtolower($results->getName()));

        return $storeName;
    }

    /**
     * Get current request for lazy load page
     *
     * @return mixed
     */
    public function getRequest()
    {
        return $this->_request;
    }

}
