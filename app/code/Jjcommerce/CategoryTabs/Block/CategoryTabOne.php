<?php
/**
 * Jjcommerce_CategoryTabs
 *
 * PHP version 5.x
 *
 * @category  PHP
 * @package   Jjcommerce\CategoryTabs
 * @author    Swapnil Tatkondawar <swapnilt@2jcommerce.in>
 * @copyright 2017 Copyright 2J Commerce, Inc. http://www.2jcommerce.in/
 * @license   http://www.2jcommerce.in/ Private
 * @link      http://www.jollyes.co.uk/
 */
namespace Jjcommerce\CategoryTabs\Block;

use Magento\Catalog\Block\Product\Context;
use Jjcommerce\CategoryTabs\Helper\Data;
use Magento\Catalog\Block\Product\AbstractProduct;
use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\Catalog\Model\Product\Visibility;
use Magento\Framework\App\Http\Context as HttpContext;
use Magento\Customer\Model\Context as CustomerContext;

class CategoryTabOne extends AbstractProduct implements \Magento\Framework\DataObject\IdentityInterface
{

    const CACHE_TAG = 'category_tab_one';

    /**
     * @var Data
     */
    protected $_dataHelper;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    protected $productCollectionFactory;

    /**
     * @var \Magento\Catalog\Model\Product\Visibility
     */
    protected $catalogProductVisibility;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var CategoryRepositoryInterface
     */
    protected $categoryRepository;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var \Magento\Framework\App\Http\Context
     */
    protected $httpContext;

    /**
     * @param Context $context
     * @param Data $helper
     * @param CollectionFactory $productCollectionFactory
     * @param Visibility $catalogProductVisibility
     * @param CategoryRepositoryInterface $categoryRepository
     * @param HttpContext $httpContext
     * @param array $data
     */
    public function __construct(
        Context $context,
        Data $helper,
        CollectionFactory $productCollectionFactory,
        Visibility $catalogProductVisibility,
        CategoryRepositoryInterface $categoryRepository,
        HttpContext $httpContext,
        $data = []
    ) {
        $this->httpContext = $httpContext;
        $this->_dataHelper = $helper;
        $this->logger = $context->getLogger();
        $this->productCollectionFactory = $productCollectionFactory;
        $this->catalogProductVisibility = $catalogProductVisibility;
        $this->storeManager = $context->getStoreManager();
        $this->categoryRepository = $categoryRepository;
        parent::__construct(
            $context,
            $data
        );
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->addData(
            ['cache_lifetime' => 86400, 'cache_tags' => [self::CACHE_TAG]]
        );
    }

    /**
     * Get Key pieces for caching block content
     *
     * @return array
     */
    public function getCacheKeyInfo()
    {

        return [
           'CATEGORY_TAB_ONE_BLOCK',
           $this->storeManager->getStore()->getId(),
           $this->_design->getDesignTheme()->getId(),
           $this->httpContext->getValue(CustomerContext::CONTEXT_GROUP),
           'template' => $this->getTemplate(),
           'uid'=> $this->_dataHelper->getTabCategoryOneId(),

        ];
    }

    protected function _beforeToHtml()
    {
        if ($this->_dataHelper->getTabCategoryOneIsEnabled()) {
            $productCollection = $this->createCollection();
            if ($productCollection) {
                $this->setProductCollection($productCollection);
                return $this;
            } else {
                parent::_beforeToHtml();
            }
        } else {
            parent::_beforeToHtml();
        }
    }

    /**
     * Prepare and return product collection
     *
     * @return \Magento\Catalog\Model\ResourceModel\Product\Collection
     */
    public function createCollection()
    {
        $categoryId = $this->_dataHelper->getTabCategoryOneId();

        if(!isset($categoryId)){
            return false;
        }

        try {
            $category = $this->categoryRepository->get($categoryId, $this->getCurrentStoreId());
        } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
            return false;
        }

        /** @var $collection \Magento\Catalog\Model\ResourceModel\Product\Collection */
        $collection = $this->productCollectionFactory->create();
        $collection->setVisibility($this->catalogProductVisibility->getVisibleInCatalogIds());

        $collection = $this->_addProductAttributesAndPrices($collection)
            ->addCategoryFilter($category)
            ->addStoreFilter()
            ->setPageSize($this->_dataHelper->getNumberOfItemsCategoryTabOne());

        return $collection;
    }

    /**
     * Retrieves a current category
     *
     * @return Category
     */
    public function getCurrentCategory()
    {
        /** @var Category $category */
        $category = null;
        if ($this->catalogLayer) {
            $category = $this->catalogLayer->getCurrentCategory();
        } elseif ($this->registry->registry('current_category')) {
            $category = $this->registry->registry('current_category');
        }
        return $category;
    }

    /**
     * Get current store id
     * @return int
     */
    private function getCurrentStoreId()
    {
        return $this->storeManager->getStore()->getId();
    }

    /**
     * @return Data
     */
    public function getDataHelper()
    {
        return $this->_dataHelper;
    }

    /**
     * Return identifiers for produced content
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG];
    }
}
