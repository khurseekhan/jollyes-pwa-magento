<?php
/**
 * Jjcommerce_CategoryTabs
 *
 * PHP version 5.x
 *
 * @category  PHP
 * @package   Jjcommerce\CategoryTabs
 * @author    Swapnil Tatkondawar <swapnilt@2jcommerce.in>
 * @copyright 2017 Copyright 2J Commerce, Inc. http://www.2jcommerce.in/
 * @license   http://www.2jcommerce.in/ Private
 * @link      http://www.jollyes.co.uk/
 */
namespace Jjcommerce\CategoryTabs\Helper;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Helper\AbstractHelper;

class Data extends AbstractHelper
{
    /**
     * Core store config
     *
     * @var ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * Tab1 is Enabled or Not
     */
    const XML_PATH_CATEGORY_TAB_ONE_IS_ENABLED = 'categorytabs_setting/tab1/category_tab_one_is_enabled';

    /**
     * Tab2 is Enabled or Not
     */
    const XML_PATH_CATEGORY_TAB_TWO_IS_ENABLED = 'categorytabs_setting/tab2/category_tab_two_is_enabled';

    /**
     * Tab3 is Enabled or Not
     */
    const XML_PATH_CATEGORY_TAB_THREE_IS_ENABLED = 'categorytabs_setting/tab3/category_tab_three_is_enabled';

    /**
     * Tab4 is Enabled or Not
     */
    const XML_PATH_CATEGORY_TAB_FOUR_IS_ENABLED = 'categorytabs_setting/tab4/category_tab_four_is_enabled';


    /**
     * BestSeller Category Id
     */
    const XML_PATH_CATEGORY_TAB_ONE_ID = 'categorytabs_setting/tab1/category_tab_one_id';

    /**
     * Dog Category Id
     */
    const XML_PATH_CATEGORY_TAB_TWO_ID = 'categorytabs_setting/tab2/category_tab_two_id';

    /**
     * Cat Category Id
     */
    const XML_PATH_CATEGORY_TAB_THREE_ID = 'categorytabs_setting/tab3/category_tab_three_id';

    /**
     * Cat Category Id
     */
    const XML_PATH_CATEGORY_TAB_FOUR_ID = 'categorytabs_setting/tab4/category_tab_four_id';

    /**
     * category_tab_one_label
     */
    const XML_PATH_CATEGORY_TAB_ONE_LABEL = 'categorytabs_setting/tab1/category_tab_one_label';

    /**
     * category_tab_two_label
     */
    const XML_PATH_CATEGORY_TAB_TWO_LABEL = 'categorytabs_setting/tab2/category_tab_two_label';

    /**
     * category_tab_three_label
     */
    const XML_PATH_CATEGORY_TAB_THREE_LABEL = 'categorytabs_setting/tab3/category_tab_three_label';

    /**
     * category_tab_four_label
     */
    const XML_PATH_CATEGORY_TAB_FOUR_LABEL = 'categorytabs_setting/tab4/category_tab_four_label';

    /**
     * Number of Item
     */
    const XML_PATH_NUMBER_OF_ITEMS_CATEGORY_TAB_ONE = 'categorytabs_setting/tab1/number_of_items_category_tab_one';

    /**
     * Number of Item
     */
    const XML_PATH_NUMBER_OF_ITEMS_CATEGORY_TAB_TWO = 'categorytabs_setting/tab2/number_of_items_category_tab_two';

    /**
     * Number of Item
     */
    const XML_PATH_NUMBER_OF_ITEMS_CATEGORY_TAB_THREE = 'categorytabs_setting/tab3/number_of_items_category_tab_three';

    /**
     * Number of Item
     */
    const XML_PATH_NUMBER_OF_ITEMS_CATEGORY_TAB_FOUR = 'categorytabs_setting/tab4/number_of_items_category_tab_four';


    /**
     * "Enable Module" from system config
     */
    const GENERAL_IS_ENABLED = 'categorytabs_setting/general/is_enabled';


    /**
     * __construct
     * @param Context $context
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        Context $context,
        StoreManagerInterface $storeManager
    )
    {
        $this->_scopeConfig  = $context->getScopeConfig();
        $this->_storeManager = $storeManager;
        $this->logger        = $context->getLogger();

        parent::__construct($context);
    }

    /**
     * Get Tab Category One Is Enabled
     * @param null $store
     * @return mixed
     */
    public function getTabCategoryOneIsEnabled($store = null)
    {

        return $this->_scopeConfig->getValue(
            self::XML_PATH_CATEGORY_TAB_ONE_IS_ENABLED,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * Get Tab Category Two Is Enabled
     * @param null $store
     * @return mixed
     */
    public function getTabCategoryTwoIsEnabled($store = null)
    {

        return $this->_scopeConfig->getValue(
            self::XML_PATH_CATEGORY_TAB_TWO_IS_ENABLED,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * Get Tab Category Three Is Enabled
     * @param null $store
     * @return mixed
     */
    public function getTabCategoryThreeIsEnabled($store = null)
    {

        return $this->_scopeConfig->getValue(
            self::XML_PATH_CATEGORY_TAB_THREE_IS_ENABLED,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * Get Tab Category Four Is Enabled
     * @param null $store
     * @return mixed
     */
    public function getTabCategoryFourIsEnabled($store = null)
    {

        return $this->_scopeConfig->getValue(
            self::XML_PATH_CATEGORY_TAB_FOUR_IS_ENABLED,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * Get Tab Category One Id
     * @param null $store
     * @return mixed
     */
    public function getTabCategoryOneId($store = null)
    {

        return $this->_scopeConfig->getValue(
            self::XML_PATH_CATEGORY_TAB_ONE_ID,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * Get Tab Category Two Id
     * @param null $store
     * @return mixed
     */
    public function getTabCategoryTwoId($store = null)
    {

        return $this->_scopeConfig->getValue(
            self::XML_PATH_CATEGORY_TAB_TWO_ID,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * Get Tab Category Three Id
     * @param null $store
     * @return mixed
     */
    public function getTabCategoryThreeId($store = null)
    {

        return $this->_scopeConfig->getValue(
            self::XML_PATH_CATEGORY_TAB_THREE_ID,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * Get Tab Category Four Id
     * @param null $store
     * @return mixed
     */
    public function getTabCategoryFourId($store = null)
    {

        return $this->_scopeConfig->getValue(
            self::XML_PATH_CATEGORY_TAB_FOUR_ID,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $store
        );
    }


    /**
     * Get Number Of Items Tab One
     * @param null $store
     * @return mixed
     */
    public function getNumberOfItemsCategoryTabOne($store = null)
    {
        return $this->_scopeConfig->getValue(
            self::XML_PATH_NUMBER_OF_ITEMS_CATEGORY_TAB_ONE,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * Get Number Of Items TabTwo
     * @param null $store
     * @return mixed
     */
    public function getNumberOfItemsCategoryTabTwo($store = null)
    {

        return $this->_scopeConfig->getValue(
            self::XML_PATH_NUMBER_OF_ITEMS_CATEGORY_TAB_TWO,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * Get Number of Cat Items
     * @param null $store
     * @return mixed
     */
    public function getNumberOfItemsCategoryTabThree($store = null)
    {

        return $this->_scopeConfig->getValue(
            self::XML_PATH_NUMBER_OF_ITEMS_CATEGORY_TAB_THREE,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * Get Number of Others Items
     * @param null $store
     * @return mixed
     */
    public function getNumberOfItemsCategoryTabFour($store = null)
    {

        return $this->_scopeConfig->getValue(
            self::XML_PATH_NUMBER_OF_ITEMS_CATEGORY_TAB_FOUR,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * Get Category Tab One Label
     * @param null $store
     * @return mixed
     */
    public function getCategoryTabOneLabel($store = null)
    {

        return $this->_scopeConfig->getValue(
            self::XML_PATH_CATEGORY_TAB_ONE_LABEL,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * Get Category Tab Two Label
     * @param null $store
     * @return mixed
     */
    public function getCategoryTabTwoLabel($store = null)
    {

        return $this->_scopeConfig->getValue(
            self::XML_PATH_CATEGORY_TAB_TWO_LABEL,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * Get Category Tab Three Label
     * @param null $store
     * @return mixed
     */
    public function getCategoryTabThreeLabel($store = null)
    {

        return $this->_scopeConfig->getValue(
            self::XML_PATH_CATEGORY_TAB_THREE_LABEL,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * Get Category Tab Four Label
     * @param null $store
     * @return mixed
     */
    public function getCategoryTabFourLabel($store = null)
    {

        return $this->_scopeConfig->getValue(
            self::XML_PATH_CATEGORY_TAB_FOUR_LABEL,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * Get config of category product tabs
     * @param $configPath
     * @param null $store
     * @return mixed
     */
    public function getTabsConfig($configPath, $store = null)
    {

        return $this->_scopeConfig->getValue(
            $configPath,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * Check weather module is enable or disable
     * @param null $store
     * @return bool
     */
    public function isEnabled($store = null)
    {
        $isModuleEnabled       = $this->isModuleEnabled();
        $isModuleOutputEnabled = $this->isModuleOutputEnabled();

        return $isModuleOutputEnabled && $isModuleEnabled && $this->getTabsConfig(self::GENERAL_IS_ENABLED, $store);
    }

    /**
     * Check module is enable in module list
     * @return mixed
     */
    public function isModuleEnabled()
    {
        $moduleName = "Jjcommerce_CategoryTabs";

        return $this->_moduleManager->isEnabled($moduleName);
    }

}
