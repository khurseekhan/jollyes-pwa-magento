<?php

/**
 * @category    Jjcommerce
 * @package     Jjcommerce_FeedStock
 * @author	Jjcommerce Team<support@2jdesign.co.uk>
 */

namespace Jjcommerce\FeedStock\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Encryption\EncryptorInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;

class Data extends \Magento\Framework\App\Helper\AbstractHelper {

    const STOCKIMPORTROOT = 'stockexport/general/file_path';

    public function getImportRoot()
    {
        return $this->scopeConfig->getValue(
            self::STOCKIMPORTROOT,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function isDebugMode()
    {
        return $this->scopeConfig->getValue(
            'stockexport/general/debug_mode',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }


}
