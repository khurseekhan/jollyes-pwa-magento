<?php

/**
 * @category    Jjcommerce
 * @package     Jjcommerce_FeedStock
 * @author	Jjcommerce Team<support@2jdesign.co.uk>
 */

namespace Jjcommerce\FeedStock\Model;

use Braintree\Exception;
use Magento\Framework\App\Filesystem\DirectoryList;

class ImportStock {

    protected $filesystem;

    protected $directoryList;
    protected $fileName = null;
    protected $_filePath;
    protected $_helper;
    protected $_dbResource;

    protected $fileHeader = [];

    protected $varDirectoryToRead;
    protected $varDirectoryToWrite;

    protected $stockStatusId;

    protected $sucessreportdata = [];
    protected $failurereportdata = [];

    protected $_stores = [];

    protected $enfieldId;

    /**
     * @var \Magento\Indexer\Model\IndexerFactory
     */
    protected $_indexerFactory;

    /**
     * @param \Magento\Framework\File\Csv $csvProcessor
     */
    public function __construct(
        \Magento\Framework\Filesystem $filesystem,
        DirectoryList $directoryList,
        \Jjcommerce\FeedStock\Helper\Data $helper,
        \Jjcommerce\FeedStock\Model\ResourceModel\StockUpdate $stockupdate,
        \Magento\Indexer\Model\IndexerFactory $indexerFactory
    ) {
        $this->filesystem = $filesystem;
        $this->varDirectoryToRead = $filesystem->getDirectoryRead(DirectoryList::PUB);
        $this->varDirectoryToWrite = $filesystem->getDirectoryWrite(DirectoryList::PUB);
        $this->_helper = $helper;

        $this->_dbResource = $stockupdate;

        $this->_filePath = trim($this->_helper->getImportRoot() , '/');
        //$this->log( __line__.' ======> '.$this->_filePath);
        $this->varDirectoryToWrite->create($this->_filePath,DirectoryList::PUB);

        $this->_indexerFactory = $indexerFactory;
    }



    public function processData()
    {
        try {

            // Collect latest uploaded stock feed file.
            $this->getLatestFile();
            //$this->log(__line__.' =========>  file:'.$this->fileName );
            if( is_null($this->fileName )){
                $this->log('no file found - '.date('Y-m-d H:i:s') );
                return;
            }

            $isReady = $this->stockFeedReady();
           // $this->log(__line__.' =========>  isready :'.$isReady);
            if (!$isReady) {
                //$this->log(__line__.' =========>  ready' );
                $this->getLock('updateStock');
               // $this->log(__line__.' =========> getLock ');
                $this->getLastBatchId();
               // $this->log(__line__.' =========> getLastBatchId ');
               // $this->initData();
                $this->readStockFile();
               // $this->log(__line__.' =========> readStockFile');
                $this->prepareData();
               // $this->log(__line__.' =========> prepareData  skip');
                $this->updateSheduleTable();
               // $this->log(__line__.' =========> updateSheduleTable ');
                $this->_archiveStockFile();
               // $this->log(__line__.' =========> _archiveStockFile');
                $this->createReport();
               // $this->log(__line__.' =========> createReport');
                $this->truncateBatchTable();
               // $this->log(__line__.' =========> truncateBatchTable');
                $this->indexInventoryStock();
              //  $this->log(__line__.' =========> indexInventoryStock');
                $this->releaseLock('updateStock');
              //  $this->log(__line__.' =========> releaseLock');
            } else {
                $this->log('New Stock feed not comes......');
            }
        }
        catch (Exception $e) {
            $this->log($e->getMessage() );
            echo  $e->getMessage();
        }
    }


    /**
     *  To update feed schedule table.
     *
     */
    public function updateSheduleTable()
    {
        $this->_dbResource->updateSheduleTable($this->fileName ."-".date('Y-m-d H:i:s'));
    }

    protected function _archiveStockFile()
    {

        $filePath = $this->varDirectoryToRead->getAbsolutePath($this->fileName); // /vagrant/pub/feed/customer_import/feed.csv
        $path =  $this->varDirectoryToRead->getRelativePath( $filePath); // feed/customer_import/feed.csv

        $content = $this->varDirectoryToRead->readFile($path, true);

        unlink($filePath);
        $processedPath = $this->processRoot($path);
        $this->varDirectoryToWrite->writeFile($processedPath, $content);

        return;
    }

    /**
     *  To generate report file
     *
     */
    public function createReport()
    {
        if (isset($this->sucessreportdata) && !empty($this->sucessreportdata)) {
            $filesuffix = time();
            $processedPath = $this->_filePath . '/feed_report/sucess_report_' . $filesuffix . '.txt';
            $content = implode("\r\t", $this->sucessreportdata);
            $this->varDirectoryToWrite->writeFile($processedPath, $content);

        }
        if (isset($this->failurereportdata) && !empty($this->failurereportdata)) {
            $filesuffix = time();
            $processedPath = $this->_filePath . '/feed_report/failure_report_' . $filesuffix . '.txt';
            $content = implode("\r\t", $this->failurereportdata);
            $this->varDirectoryToWrite->writeFile($processedPath, $content);
        }
        $time_end       = $this->microtime_float();
        echo "<br><br> end Time : ".$time_end;
        $this->log( 'End Time : ' .$time_end );
        $execution_time = $time_end - $this->time_start;
        echo '<br><br>Execution Time :' . $execution_time;

        $this->log('Execution Time ---' . $execution_time );
    }

    /**
     * To Truncate batch table
     *
     */
    public function truncateBatchTable()
    {
        try {
            $this->_dbResource->truncateBatch();
            $this->log("Batch Table truncate sucessfully...");
        }
        catch (Exception $e) {
            $this->log($e->getMessage());
        }
    }

    /**
     *  To check file lock
     *
     */
    public function isLocked($lockName = '')
    {
        if ($lockName != '') {
            $file = $this->_filePath . '/feed_lock/' . $lockName . '.lock';
            $filePath = $this->varDirectoryToRead->getAbsolutePath($file); // /vagrant/pub/feed/stockexport/feed_lock/updateStock.lock

            if (!file_exists($filePath))
                return false;

            $timeDifference = time() - filemtime(utf8_decode($filePath));
            if (is_file($filePath) && $timeDifference >= 1800) { //time for 30min
                //  @unlink($file);
                return false;
            }

            return file_exists($filePath);
        }
    }

    /**
     *  To create file lock
     *
     */
    public function getLock($lockName = '')
    {
        if ($lockName != '') {
            $file = $this->_filePath . '/feed_lock/' . $lockName . '.lock';
            $this->varDirectoryToWrite->writeFile($file,  time());
        }
    }
    /**
     *  To release lock file
     *
     */
    public function releaseLock($lockName = '')
    {
        if ($lockName != '') {
            $file = $this->_filePath . '/feed_lock/' . $lockName . '.lock';
            $filePath = $this->varDirectoryToRead->getAbsolutePath($file);
            unlink($filePath);
            $this->log('Release lock after.....');
        }
    }

    /**
     * Get latest stock feed file name
     *
     */
    public function getLatestFile()
    {
        $files = array();

        $configPaths = $this->varDirectoryToRead->search('{'.$this->_filePath. '/import/*}');

        foreach ($configPaths as $configPath) {

            if (strpos($configPath, 'processed') === false) {
                $path =  $this->varDirectoryToRead->getAbsolutePath( $configPath);
                $files[$configPath] = filemtime(utf8_decode($path));
            }
        }

        arsort($files);
        $newest         = array_slice($files, 0, 1);
        $this->fileName = key($newest);
    }

    /**
     *  To check whether new stock feed comes.
     *
     */
    public function stockFeedReady()
    {
        $file =  $this->_dbResource->getScheduleFile();
        return ($file === $this->fileName) ? 1 : 0;
    }


    /**
     * Get latest batch Id
     *
     */
    public function getLastBatchId()
    {
        $this->time_start = $this->microtime_float();
        $batchResult      =   $this->_dbResource->getBatchId();
        // echo '<br><br>Start Time : ' .$this->time_start;
         $this->log( 'Start Time : ' .$this->time_start );
        if ($batchResult) {
            $this->batch_id = $batchResult + 1;
        } else {
            $this->batch_id = 1;
        }
    }

    /**
     * Feel batch table of data to br update
     *
     */
    public function readStockFile()
    {

        $feedFile = $this->getProductSheet();
       // $this->log( __line__.'==== readStockFile : '.print_r( $feedFile,1) );
        foreach ($feedFile as $seg) {
            $serialize_data = serialize($seg);
            $this->_dbResource->setBatchId(array( 'batch_id' => $this->batch_id ,'content'=> $serialize_data ));
           // $this->log( __line__.'==== readStockFile : '.print_r( $seg,1) );
           // $this->updateThis($seg);
        }


    }

    public function updateThis($dataresult){

       // foreach ($dataresult as $row) {
          //  $this->log( __line__.'==== updateThis : '.print_r( $dataresult,1) );
            $this->updateData($dataresult);
      //  }

    }

    /**
     * Get stock feed file complete data
     *
     */
    public function getProductSheet()
    {
        $filePath = $this->varDirectoryToRead->getAbsolutePath($this->fileName);
        //$this->log( __line__.' ======> '.$filePath);
        $path = $this->varDirectoryToRead->getRelativePath($filePath);
        // $contentData = $this->varDirectoryToRead->readFile($path, true);
        $stockFileData = array();

        $rows = explode("\n", trim($this->varDirectoryToRead->readFile($path, true), "\n"));

        $this->fileHeader = explode(',', trim($rows[0]) );
        $enfieldKey = array_search ('Enfield',  $this->fileHeader);

       // $this->log( __line__.' ======> '.print_r($this->fileHeader,1));
        $this->fileHeader  = $this->_dbResource->getFileHeader($this->fileHeader);
        $this->enfieldId =   $this->fileHeader[$enfieldKey];

        //$this->log( __line__.' ======> '.print_r($this->fileHeader,1));

        unset( $rows[0] );

        foreach ($rows as $rline) {
            if(!trim($rline)){
                continue;
            }
            $row = explode(',', trim($rline) );

            // ========== lets find entity in catalog for sku ===========

            $productKeys = $this->_dbResource->getProductIdBySku($row[0]);

            //$this->log( __line__.' ======> '.$row[0].':'.$entityId);
            if( empty($productKeys) ){
                    $this->failurereportdata[] = $row[0]." : No product exist" ;
                    continue;
            }
            $entityId = $productKeys['entity_id'];
            $keyRow = array();
            $i = 0;
            foreach($row as $value){
               if($i==0){
                    $keyRow[$this->fileHeader[$i]] = $entityId;
                } else {
                    $keyRow[$this->fileHeader[$i]] = $value;
                }
                $i++;
            }
            if(isset($productKeys['row_id']) && is_array($productKeys)){
                $keyRow['entity_id'] = $productKeys['row_id'];
            }

            $stockFileData[] = $keyRow;
        }
       // $this->log( __line__.' ======> '.print_r($stockFileData,1));
        return $stockFileData;
    }


    /**
     *  Update stock and inventory and index on solr product which is change stock status
     *
     */
    public function prepareData()
    {
        try {

            $dataresult = $this->_dbResource->getDataByBatchId($this->batch_id);
            $this->initData();
            $this->_dbResource->truncateStore();
            foreach ($dataresult as $row) {
                $serdata = unserialize($row['content']);
               // $this->log( __line__.'==== prepare data : ' );
                $this->updateData($serdata);
            }
        }
        catch (Exception $e) {

        }
    }

    protected function indexInventoryStock()
    {
        $indexerIds = array( 'cataloginventory_stock' ) ;
        foreach($indexerIds as $indexerId) {
            $idx = $this->_indexerFactory->create()->load($indexerId);
            $idx->reindexAll($indexerId);
        }
    }
    protected function initData()
    {
        $this->productEntityTypeId =  $this->_dbResource->productEntityType();
        // $this->priceAttributeId =  $this->_dbResource->productPriceAttributeId($this->productEntityTypeId);
        $this->stockStatusId =  $this->_dbResource->productQtynStockStatusAttributeId($this->productEntityTypeId);
        $this->_stores =   $this->_dbResource->getStoresLocator( $this->fileHeader );

    }

    protected function updateData($data)
    {
        $entity_id = 0;
        try {
            // echo '<pre>'; print_r($data); echo '</pre>'; exit;
           // $this->log( __line__.'==== updateData : '.print_r( $data,1) );
            $entity_id = $data['sku']; // $this->_dbResource->getProductIdBySku(trim($data['sku']));
            unset( $data['sku'] );
            if( (int)$entity_id>0 ) {
                $this->updateStock($data, $entity_id);

                $this->sucessreportdata[] = $entity_id."  is successfully processed";
            }

        }catch(Exception $e) {

            $this->failurereportdata[] = $entity_id." : EXCEPTION - ".$e->getMessage();
        }

    }
    protected function updateStock($data,$entity_id)
    {
       //$this->log( __line__.'==== updateStock : '.$entity_id );
       // $stockAvailable =  0.00; //(int)$data['Total'];
        /*foreach($data as $storeId => $stock ){
            if( (int)$storeId>0 && !in_array($storeId,$this->_dbResource->getDisableStores()) ) {
                $stockAvailable+= $stock;
            }
        }*/
	
	// Global stock update start.
	$stockStatus = 1;
	/*
        // Update product main Qty with Enfield store for each SKU
        if(isset( $data[$this->enfieldId]) && $data[$this->enfieldId]>0 && !in_array($this->enfieldId,$this->_dbResource->getDisableStores())) {
            $data['Total'] = $data[$this->enfieldId];
            $stockStatus = 1;
        }else{
            $data['Total'] = 0;
            $stockStatus = 0;
        }

        $this->_dbResource->updateEntityStock($entity_id,$data );
	*/

	// Global stock update end.

        $this->_dbResource->updateEntityStockForStore($entity_id,$data );

        $this->_dbResource->updateEntityAttribute($data['entity_id'],$this->stockStatusId,$stockStatus,'int');
    }


    protected function processRoot($relativePath)
    {
        $eachPath = explode( '/',$relativePath );
        $i = count($eachPath );
        $fileName =   $eachPath[$i-1].'_'.date('Ymd-His');
        $eachPath[$i-1] = 'processed';
        $eachPath[$i] = $fileName;
        return implode('/',$eachPath);

    }

    public function log($message) {
        //\Zend\Log\Logger::INFO
        if($this->_helper->isDebugMode()) {
            $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/importstockfeed.log');
            $logger = new \Zend\Log\Logger();
            $logger->addWriter($writer);
            $logger->info($message);
        }
    }

    /**
     *  To calculate time
     *
     */
    public function microtime_float()
    {
        list($usec, $sec) = explode(" ", microtime());
        return ((float) $usec + (float) $sec);
    }


}
