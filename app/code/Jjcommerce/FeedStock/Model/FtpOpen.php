<?php
/**
 * @category    Jjcommerce
 * @package     Jjcommerce_FeedStocknPrice
 * @author	Jjcommerce Team<support@2jdesign.co.uk>
 */

namespace Jjcommerce\FeedStocknPrice\Model;

/**
 * FTP client
 */
class FtpOpen extends \Magento\Framework\Filesystem\Io\Ftp
{


    /**
     * Open a connection
     *
     * Possible argument keys:
     * - host        required
     * - port        default 21
     * - timeout     default 90
     * - user        default anonymous
     * - password    default empty
     * - ssl         default false
     * - passive     default false
     * - path        default empty
     * - file_mode   default FTP_BINARY
     *
     * @param array $args
     * @return true
     * @throws LocalizedException
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function open(array $args = [])
    {
        if (empty($args['host'])) {

            $this->log('Empty host specified');
            return false;
        }

        if (empty($args['port'])) {
            $args['port'] = 21;
        }

        if (empty($args['user'])) {
            $args['user'] = 'anonymous';
            $args['password'] = 'anonymous@noserver.com';
        }

        if (empty($args['password'])) {
            $args['password'] = '';
        }

        if (empty($args['timeout'])) {
            $args['timeout'] = 90;
        }

        if (empty($args['file_mode'])) {
            $args['file_mode'] = FTP_BINARY;
        }

        $this->_config = $args;

        if (empty($this->_config['ssl'])) {
            $this->_conn = @ftp_connect($this->_config['host'], $this->_config['port'], $this->_config['timeout']);
        } else {
            $this->_conn = @ftp_ssl_connect($this->_config['host'], $this->_config['port'], $this->_config['timeout']);
        }
        if (!$this->_conn) {
            $this->log('Could not establish FTP connection, invalid host or port');
            return false;
        }

        if (!@ftp_login($this->_conn, $this->_config['user'], $this->_config['password'])) {

            $this->close();
            $this->log('Invalid user name or password');
            return false;
        }

        if (!empty($this->_config['path'])) {
            if (!@ftp_chdir($this->_conn, $this->_config['path'])) {

                $this->close();
                $this->log('Invalid path');
                return false;
            }
        }

        if (!empty($this->_config['passive'])) {
            if (!@ftp_pasv($this->_conn, true)) {

                $this->close();
                $this->log('Invalid file transfer mode');
                return false;
            }
        }

        return true;
    }



    public function log($message) {
        //\Zend\Log\Logger::INFO

        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/importstockfeed.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info($message);
    }


}
