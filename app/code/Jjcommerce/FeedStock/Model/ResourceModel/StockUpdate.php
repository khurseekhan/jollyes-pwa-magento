<?php
/**
 * Copyright © 2015 Jjcommerce. All rights reserved.
 */

namespace Jjcommerce\FeedStock\Model\ResourceModel;

class StockUpdate extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected $dStore = [];
    /**
     * Initialize resources
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('jj_stock_batch', 'id');
    }

    public function getScheduleFile(){

    $storetable = $this->getTable('jj_stock_cron_schedule');
    $select = $this->getConnection()->select()->from(
        $storetable,
        ['file_name']
    );
   return $this->getConnection()->fetchOne($select);

   }

    public function getBatchId()
{

    $select = $this->getConnection()->select()->from(
        $this->getMainTable(),
        ['batch_id']
    )->order('batch_id DESC')->limit(1);

    return $this->getConnection()->fetchOne($select);
}
    public function setBatchId($data)
{

    $this->getConnection()->insert($this->getMainTable(), $data );
}

    public function getDataByBatchId($batchId)
{
    $select = $this->getConnection()->select()->from(
        $this->getMainTable(),
        ['content']
    )->where('batch_id="'.$batchId.'"');
    return $this->getConnection()->fetchAll($select);
}

    public function updateSheduleTable($fileName)
{
    $storetable = $this->getTable('jj_stock_cron_schedule');
    $this->getConnection()->insert($storetable , array('file_name'=> $fileName) );
}

public function getProductIdBySku($sku)
{
    $entitytable = $this->getTable('catalog_product_entity');
    $select = $this->getConnection()->select()->from(
        $entitytable,
        ['entity_id']
    )->where('sku="'.$sku.'"');
    $data = $this->getConnection()->fetchAll($select);
    if( empty($data) ){
        return [];
    }

    return $data[0];
}



    public function updateEntityAttribute($entity_id,$attribute_id,$value,$backendType='decimal')
{

    $storetable = $this->getTable('catalog_product_entity_'.$backendType);
    $select = $this->getConnection()->select()->from(
        $storetable,
        ['value_id']
    )->where('entity_id='.$entity_id." and attribute_id=".$attribute_id);

    $value_id = $this->getConnection()->fetchOne($select);
    if((int)$value_id>0) {
    $this->getConnection()->update($storetable , array('value'=> $value), 'entity_id='.$entity_id.' and attribute_id='.$attribute_id );
    }
    else{
        $data = array(
            'value' => $value,
            'entity_id' => $entity_id,
            'attribute_id' => $attribute_id,
            'store_id'  => 0
        );
        $this->getConnection()->insert($storetable, $data );
    }


}

    public function updateEntityStock($entity_id,$data)
{
    $stockAvailable =  (int)$data['Total'];

   if($stockAvailable>0)$stockStatus = 1;
   else $stockStatus = 0;

    $inventoryStockItemtable = $this->getTable('cataloginventory_stock_item');

    $select = $this->getConnection()->select()->from(
        $inventoryStockItemtable,
        ['item_id']
    )->where('product_id='.$entity_id)
        ->where('website_id=0');

    $item_id = $this->getConnection()->fetchOne($select);
    if((int)$item_id>0) {
      //  $this->log( __line__.'==== resource : updateStock : '.$entity_id );
    $this->getConnection()->update($inventoryStockItemtable , array('qty'=> $stockAvailable,'is_in_stock' => $stockStatus), 'product_id='.$entity_id );
    }
    else{
       // $this->log( __line__.'==== resource : insertStock : '.$entity_id );
        $dataInsert = array(
            'qty' => $stockAvailable,
            'product_id' => $entity_id,
            'stock_id' => 1,
            'is_in_stock' => $stockStatus
        );
        $this->getConnection()->insert($inventoryStockItemtable, $dataInsert );
    }

    $inventoryStockStatustable = $this->getTable('cataloginventory_stock_status');
    $select = $this->getConnection()->select()->from(
        $inventoryStockStatustable,
        ['product_id']
    )->where('product_id='.$entity_id);

    $product_id = $this->getConnection()->fetchOne($select);

    if((int)$product_id>0) {

   $this->getConnection()->update($inventoryStockStatustable , array('qty'=> $stockAvailable,'stock_status'=> $stockStatus), 'product_id='.$entity_id );
    }
    else{
        $dataInsert = array(
            'qty' => $stockAvailable,
            'product_id' => $entity_id,
            'stock_id' => 1,
            'website_id' => 0,
            'stock_status' => $stockStatus
        );
       $this->getConnection()->insert($inventoryStockStatustable, $dataInsert );
    }
}

public function getFileHeader($header){

        $store = $this->getStoresLocator($header );
        $newheader = array();

    foreach($header as $code  ) {
        if($code=='sku'){
            $newheader[] = $code;
            continue;
        }

        $keyCode = ucwords( strtolower($code)  );
        $keyCode = str_replace( ' ','_',  $keyCode );

        if(isset( $store[$keyCode ] ) && !is_null( $store[$keyCode ]) ){
            $newheader[] = $store[$keyCode ];
        }
        else{
            $newheader[] = 0;
        }
    }
    return $newheader;
}

    public  function getDisableStores()
    {

        if(empty($this->dStore)){
            $storeLocatortable = $this->getTable('jj_storelocator');
            $locators = array();

            $select = $this->getConnection()->select()->from(
                $storeLocatortable,
                ['shop_id']
            )->where('status<>1');
            $data = $this->getConnection()->fetchAll($select);
            foreach($data as $row ) {
                $this->dStore[] = $row['shop_id'];
            }
        }
        return $this->dStore;

    }

    public  function getStoresLocator( $codes )
{
    $storeLocatortable = $this->getTable('jj_storelocator');
    $locators = array();

    $select = $this->getConnection()->select()->from(
        $storeLocatortable,
        ['shop_id','name']
    );
    $data = $this->getConnection()->fetchAll($select);
    //print_r($data); exit;
    foreach($codes as $code ) {
        $code = ucwords( strtolower($code)  );
        $keyCode = str_replace( ' ','_',  $code );
        if($code=='Sku' || $code=='Total' || $code=='row_id')continue;

       /* $select = $this->getConnection()->select()->from(
            $storeLocatortable,
            ['shop_id']
        )->where('name=\'' . $code . '\'');
        $locators[$keyCode] = $this->getConnection()->fetchOne($select);*/
        foreach($data as $shopcode ) {

            $shopkeycode = ucwords( strtolower($shopcode['name'])  );
            $shopkeycode = str_replace( ' ','_',  $shopkeycode );

            if($shopkeycode== $keyCode ){
                $locators[$keyCode] = $shopcode['shop_id'];
            }
        }
    }

   return $locators;
}

public function updateEntityStockForStore($entity_id,$data)
{

    $storeStcktable = $this->getTable('jj_store_stock');
    $insdata = array();
    foreach($data as $storeId => $stock ) {
        if ($storeId == 'sku' || $storeId == 'Total' || $storeId == 'row_id') continue;

        // id, product_id, store_id, stock
        // =======================================================
        /* $select = $this->getConnection()->select()->from(
             $storeStcktable,
             ['id']
         )->where('product_id='.$entity_id)
             ->where('store_id='.$storeId);

         $item_id = '';//$this->getConnection()->fetchOne($select);
         unset( $select);*/

        //if((int)$item_id>0) {
        /*  $this->getConnection()->update($storeStcktable ,
               array('stock'=> $stock ),
              'product_id='.$entity_id.' and store_id ='.$storeId );
       }
       else{ */
        $insdata[] = array($stock,$entity_id,$storeId);
       /* $dataInsert = array(
            'stock' => $stock,
            'product_id' => $entity_id,
            'store_id' => $storeId
        );*/
        // $this->getConnection()->insert($storeStcktable, $dataInsert );
       //}
       // $this->log( __line__.'==== resource : storeStock : '.$entity_id );
        // =======================================================

    }
    $column  = array(
        'stock' ,'product_id' ,'store_id'
    );
    //$this->log( __line__.print_r($insdata,1) );
    $this->getConnection()->insertArray( $storeStcktable,$column,$insdata );
}

    public function productEntityType()
    {
        $entityTypetable = $this->getTable('eav_entity_type');
        $select = $this->getConnection()->select()->from(
            $entityTypetable,
            ['entity_type_id']
        )->where('entity_type_code="catalog_product"');
        return $this->getConnection()->fetchOne($select);
    }

    public function productPriceAttributeId($eav_entity_type)
    {
        $attributetable = $this->getTable('eav_attribute');
        $select = $this->getConnection()->select()->from(
            $attributetable,
            ['attribute_id']
        )->where('entity_type_id='.$eav_entity_type.' and attribute_code="price"');
        return  $this->getConnection()->fetchOne($select);
    }

    public function productQtynStockStatusAttributeId($eav_entity_type)
    {
        $attributetable = $this->getTable('eav_attribute');
        $select = $this->getConnection()->select()->from(
            $attributetable,
            ['attribute_id']
        )->where('entity_type_id='.$eav_entity_type.' and attribute_code="quantity_and_stock_status"');
        return  $this->getConnection()->fetchOne($select);
    }

    public function truncateStore(){
        $storeStcktable = $this->getTable('jj_store_stock');
        $this->getConnection()->query( ' TRUNCATE table '.$storeStcktable );
     }

    public function truncateBatch()
{
    $this->getConnection()->query( ' TRUNCATE table '.$this->getMainTable() );
}

    public function log($message) {
        //\Zend\Log\Logger::INFO

        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/importstockfeedResource.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info($message);
    }

}
