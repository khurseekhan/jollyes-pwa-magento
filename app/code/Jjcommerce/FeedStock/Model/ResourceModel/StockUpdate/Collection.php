<?php
/**
 * Copyright © 2015 Jjcommerce. All rights reserved.
 */

namespace Jjcommerce\FeedStock\Model\ResourceModel\StockUpdate;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Jjcommerce\FeedStock\Model\StockUpdate',
                     'Jjcommerce\FeedStock\Model\ResourceModel\StockUpdate');
    }
}
