<?php

/**
 * @category    Jjcommerce
 * @package     Jjcommerce_FeedStock
 * @author	Jjcommerce Team<support@2jdesign.co.uk>
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Jjcommerce_FeedStock',
    __DIR__
);
