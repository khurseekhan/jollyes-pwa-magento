<?php

/**
 * @category    Jjcommerce
 * @package     Jjcommerce_FeedStock
 * @author	Jjcommerce Team<support@2jdesign.co.uk>
 */

namespace Jjcommerce\FeedStock\Controller\Import;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;


/**
 * Pet information list
 *
 * @SuppressWarnings(PHPMD.LongVariable)
 */
class ImportFeed extends Action {

    protected $importStock = null;

    /**
     * @var \Magento\Indexer\Model\IndexerFactory
     */
    protected $_indexerFactory;
    /**
     * @var \Magento\Indexer\Model\Indexer\CollectionFactory
     */
    protected $_indexerCollectionFactory;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $registry;

    /**
     * Constructor
     *
     * @param Context $context
     * @param Config $config
     * @param Session $checkoutSession
     */
    public function __construct(
    Context $context, 
    \Jjcommerce\FeedStock\Model\ImportStock $importStock,
    \Magento\Indexer\Model\IndexerFactory $indexerFactory,
    \Magento\Indexer\Model\Indexer\CollectionFactory $indexerCollectionFactory,
    \Magento\Framework\Registry $registry
    ) {
        $this->importStock = $importStock;
        $this->_indexerFactory = $indexerFactory;
        $this->_indexerCollectionFactory = $indexerCollectionFactory;

        $this->registry = $registry;

        parent::__construct($context);
    }

    public function execute() {

        $this->importStock->processData();
        echo "</br>TESTED on ".date('d-m-y H:i:s');
    }


    public function log($message) {
        //\Zend\Log\Logger::INFO

        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/importfeed.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info($message);
    }

}
