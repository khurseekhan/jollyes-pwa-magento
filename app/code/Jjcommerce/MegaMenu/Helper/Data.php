<?php
/**
 * @category    Jjcommerce
 * @package     Jjcommerce\MegaMenu
 * @author	Jjcommerce Team<support@2jdesign.co.uk>
 */
namespace Jjcommerce\MegaMenu\Helper;


use Magento\Store\Model\Store;
use Magento\Store\Model\ScopeInterface;


class Data extends \Magento\Framework\App\Helper\AbstractHelper
{

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    const XML_TAX_ENABLE = 'tax/display/type';


    /**
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @codeCoverageIgnore
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->_storeManager = $storeManager;
        parent::__construct($context);
    }


    /**
     * Check if enabled
     *
     * @return string|null
     */
    public function getTaxType()
    {
        return $this->scopeConfig->getValue(
            self::XML_TAX_ENABLE,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

}
