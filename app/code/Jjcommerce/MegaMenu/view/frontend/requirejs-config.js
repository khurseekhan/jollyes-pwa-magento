/**
 * Jjcommerce_MegaMenu
 *
 * PHP version 5.x
 *
 * @category  Javascript
 * @package   Jjcommerce\MegaMenu
 * @author    Swapnil Tatkondawar <swapnilt@2jcommerce.in>
 * @copyright 2017 Copyright 2J Commerce, Inc. http://www.2jcommerce.in/
 * @license   http://www.2jcommerce.in/ Private
 * @link      http://www.jollyes.co.uk/
 */
var config = {
    map: {
        "*": {
            "menu": "Jjcommerce_MegaMenu/js/mage/menu",
        }
    }
};