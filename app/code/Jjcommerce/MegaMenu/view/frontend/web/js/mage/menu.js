/**
 * Jjcommerce_MegaMenu
 *
 * PHP version 5.x
 *
 * @category  Javascript
 * @package   Jjcommerce\MegaMenu
 * @author    Swapnil Tatkondawar <swapnilt@2jcommerce.in>
 * @copyright 2017 Copyright 2J Commerce, Inc. http://www.2jcommerce.in/
 * @license   http://www.2jcommerce.in/ Private
 * @link      http://www.jollyes.co.uk/
 */
define([
    "jquery",
    "matchMedia",
    "mage/menu"
], function ($, mediaCheck) {
    /**
     * Menu Widget - this widget is a wrapper for the jQuery UI Menu
     */

    var level = 0;
    var linkArr = [];
    var pageNameArr = [];

    $.widget('mage.megamenu', $.mage.menu, {
        options: {
            responsive: false,
            expanded: false,
            delay: 300
        },
        _create: function () {
            var self = this;

            this._super();
            $(window).on('resize', function () {
                self.element.find('.submenu-reverse').removeClass('submenu-reverse');
            });
        },

        _init: function () {
            this.delay = this.options.delay;

            if (this.options.expanded === true) {
                this.isExpanded();
            }

            if (this.options.responsive === true) {
                mediaCheck({
                    media: '(max-width: 767px)',
                    entry: $.proxy(function () {
                        this._toggleMobileMode();
                    }, this),
                    exit: $.proxy(function () {
                        this._toggleDesktopMode();
                    }, this)
                });
            }

            this._assignControls()._listen();
            this._setActiveMenu();
        },
        _resetPromotionalBanner: function (submenu) {
            //Reset Promotional Image
            var promotional_area = false;
            promotional_area     = submenu.find('img.image-content');

            //If promotional image present
            if (promotional_area.length) {
                promotional_area.attr('src', promotional_area.data('url-src'));
            }
        },
        _toggleDesktopMode: function () {
            this._super();

            var subMenus = this.element.find('.level-top');
            $.each(subMenus, $.proxy(function (index, item) {
                var menu = $(item).find('> .ui-menu');
                menu.find('.ui-menu-item:not(.level1)').show();
            }, this));

            //Unbinding this on desktop mode
            $('.level1 > a').unbind('click.togglemenu');

        },
        _toggleMobileMode: function () {
            $('.overlay-wrapper').attr('data-action','toggle-nav');
            $(this.element).off('mouseenter mouseleave');

            this._on({
                "click .ui-menu-item:has(a)": function (event) {
                    event.preventDefault();
                    var target = $(event.target).closest(".ui-menu-item");
                    this.changeImage(event, target);
                    if (!target.hasClass('level-top') || !target.has(".ui-menu").length) {
                        window.location.href = target.find('> a').attr('href');
                    }
                }
            });

            this._on({
                "click a.level-top > span": function (event) {
                    event.stopPropagation();
                    var target = $(event.target).closest(".ui-menu-item");
                    window.location.href = target.find('> a').attr('href');
                }
            });

            $(document).on('click','.nav-sections-item-switch',function(){
                $(".nav-toggle").click();
            });

            $(".sub-cat > li.ui-menu-item.parent > a > span").on("click ", function (event) {
                event.stopPropagation();
                var target = $(event.target).closest(".ui-menu-item");
                window.location.href = target.find('> a').attr('href');
                }
            );

            $(".back-link-wrapper .category-link > a").on("click ", function (event) {
                window.location.href = $(this).attr("href");
            });

            $(".back-link-wrapper > div.back-button > span").on("click ", function (event) {
                    event.stopPropagation();
                    $("ul.sub-cat > li.level" + level).hide();
                    level = level - 1;
                    var prevLevel = level - 1;
                    if(level == 0){
                        $(".navigation.megamenu > ul > li.level" + level).removeClass("open");
                        $(".navigation.megamenu > ul > li.level" + level + " > a").removeClass("ui-state-active");
                        $(".navigation.megamenu > ul > li.level" + level).show();
                        $(".back-link-wrapper").hide();
                        $(".navigation.megamenu > ul > li.level" + level).children("ul").hide();
                        linkArr.length = 0;
                        pageNameArr.length = 0;
                    }else{
                        if(level == 1){
                            $(this).closest("div.wrapper-sub-cat").find("ul.sub-cat").removeClass("current").show();
                            $(this).closest("div.wrapper-sub-cat").find("ul.sub-cat > li.level" + level).show();
                        }else{
			                $(this).closest("div.wrapper-sub-cat").find("ul.sub-cat.current > li.level" + level + ".first").first().show();
                            $(this).closest("div.wrapper-sub-cat").find("ul.sub-cat.current > li.level" + level + ".first").first().nextUntil("li.level" + prevLevel + ".last", "li.level" + level).show();
                        }

                        $(".category-link > a").attr("href",linkArr[prevLevel]);
                        $(".category-link > a").text("Go To " + pageNameArr[prevLevel] + " Page");
                        linkArr.splice(level, 1);
                        pageNameArr.splice(level, 1);
                    }
                }
            );

            $(".sub-cat > li.ui-menu-item.parent > a").on("click ", function (event) {
                event.preventDefault();
                event.stopPropagation();
                    $(this).closest(".wrapper-sub-cat").find("div.back-link-wrapper .category-link a").text("Go To "+$(this).children("span:not(.ui-menu-icon)").text() + " Page");
                    $(this).parent("li.ui-menu-item ").hide();
                    $(this).parent("li.ui-menu-item ").closest("ul.sub-cat").addClass("current");
                    $(this).parent("li.ui-menu-item ").closest("ul.sub-cat").siblings("ul.sub-cat").hide();
                    $(this).parent("li.ui-menu-item ").siblings("li.level" + level).hide();
                    level = level + 1;
                    $(this).parent("li.ui-menu-item ").nextUntil("li.level" + (level - 1), "li.level" + level).show();
                    $(".category-link > a").attr("href",$(this).parent("li").children("a").attr("href"));
                    linkArr.push($(this).parent("li").children("a").attr("href"));
                    pageNameArr.push($(this).parent("li").children("a").children("span").text());
                }
            );


            var subMenus = this.element.find('.level-top');
            $.each(subMenus, $.proxy(function (index, item) {
                var category = $(item).find('> a span').not('.ui-menu-icon').text(),
                    categoryUrl = $(item).find('> a').attr('href'),
                    menu = $(item).find('> .ui-menu');

                this.categoryLink = $('<a>')
                    .attr('href', categoryUrl)
                    .text($.mage.__('All ') + category);

                // Removing the all links tag
                this.categoryParent = $('<li>')
                    .addClass('ui-menu-item all-category')

                menu.find('.ui-menu-item:not(.level1)').hide();

                if (menu.find('.all-category').length === 0) {
                    menu.prepend(this.categoryParent);
                }

            }, this));
        },
        focus: function( event, item ) {
            this._super(event, item);
            var focused          = this.active;
            var promotional_area = false;
            var imageUrl         = focused.data('image-url');
            var submenu          = focused.parent().siblings('ul.sub-cat-image');
            if (imageUrl) {
                promotional_area = submenu.find('img.image-content');
                promotional_area.attr('src', imageUrl);
            } else {
                this._resetPromotionalBanner(submenu);
            }
        },
        changeImage: function(event, item) {
            var focused          = item;
            var promotional_area = false;
            var imageUrl         = focused.data('image-url');
            var submenu          = focused.parent().siblings('ul.sub-cat-image');
            if (imageUrl) {
                promotional_area = submenu.find('img.image-content');
                promotional_area.attr('src', imageUrl);
            }
        },
        _close: function (startMenu, submenu) {
            if($(window).width() > 767){
                 this._super(startMenu);
            }

            if ( !startMenu ) {
                startMenu = this.active ? this.active.parent() : this.element;
            }

        },
        _open: function( submenu ) {

            mediaCheck({
                media: '(max-width: 767px)',
                entry: $.proxy(function () {
                    level = level + 1;
                    pageNameArr.push(submenu.closest("li").children("a").children("span:not('.ui-menu-icon')").text());
                    linkArr.push(submenu.closest("li").children("a").attr("href"));
                    submenu.closest("li").addClass("open");
                    submenu.closest("li").children("ul").show();
                    submenu.closest("li").siblings("li").hide();
                    submenu.closest("li").find("ul.sub-cat").show();
                    submenu.closest("li").find("ul.sub-cat").children("li.level" + level).show();
                    $(".category-link > a").attr("href",submenu.closest("li").children("a").attr("href"));


                    if( level == 0){
                        $(".back-link-wrapper").hide();
                    }else{
                        submenu.find(".back-link-wrapper").show();
                        submenu.closest("li").find(".back-link-wrapper > .category-link a").text("Go To " + submenu.closest("li").children("a").children("span:not('.ui-menu-icon')").text() + " Page");
                    }

                }, this),
                exit: $.proxy(function () {
                    if(typeof this._super !== "undefined") {
                        this._super(submenu);
                    }
                    submenu.closest("li").siblings("li").show();
                    submenu
                    //Custom code added to show custom columns
                        .find('ul.sub-cat')
                        .show();
                    this._resetPromotionalBanner(submenu);
                    this.checkMenuHeight(submenu);
                    this.windScrolled(submenu);
                }, this)
            });
        },
        checkMenuHeight: function(submenu){
            var menuHeight = 0, fromtop = 0, widheight = 0, contentHeight = 0;
            menuHeight = submenu.find('.wrapper-content').innerHeight();
            fromtop = submenu.offset().top;
            widheight = $(window).height();
            if ($('.header-navigation').hasClass('sticky-header')) {
                fromtop = 115;
            }
            contentHeight = widheight - fromtop;
            if(menuHeight > contentHeight){
                submenu.css({
                    'max-height': contentHeight
                });
            }else{
                submenu.css({
                    'max-height': '10000%'
                });
            }
            submenu.addClass('outerHeight');
        },
        windScrolled: function(submenu){
            var self = this;
            $(window).scroll(function(){
                self.checkMenuHeight(submenu);
            });
        },
        refresh: function() {
            var menus,
                icon = this.options.icons.submenu,
                submenus = this.element.find( this.options.menus );

            this.element.toggleClass( "ui-menu-icons", !!this.element.find( ".ui-icon" ).length );

            // Initialize nested menus
            submenus.filter( ":not(.ui-menu)" )
                .addClass( "ui-menu ui-widget ui-widget-content ui-corner-all" )
                .hide()
                .attr({
                    role: this.options.role,
                    "aria-hidden": "true",
                    "aria-expanded": "false"
                })
                .each(function() {
                    var menu = $( this ),
                        item = menu.prev( "a" ),
                        submenuCarat = $( "<span>" )
                            .addClass( "ui-menu-icon ui-icon " + icon )
                            .data( "ui-menu-submenu-carat", true );

                    item
                        .attr( "aria-haspopup", "true" )
                        .prepend( submenuCarat );
                    menu.attr( "aria-labelledby", item.attr( "id" ) );
                });

            menus = submenus.add( this.element );

            // Don't refresh list items that are already adapted
            menus.children( ":not(.ui-menu-item):not(li.wrapper-block):not(li.wrapper-content):has(a)" )
                .addClass( "ui-menu-item" )
                .attr( "role", "presentation" )
                .children( "a" )
                .uniqueId()
                .addClass( "ui-corner-all" )
                .attr({
                    tabIndex: -1,
                    role: this._itemRole()
                });

            // Initialize unlinked menu-items containing spaces and/or dashes only as dividers
            menus.children( ":not(.ui-menu-item)" ).each(function() {
                var item = $( this );
                // hyphen, em dash, en dash
                if ( !/[^\-\u2014\u2013\s]/.test( item.text() ) ) {
                    item.addClass( "ui-widget-content ui-menu-divider" );
                }
            });

            // Add aria-disabled attribute to any disabled menu item
            menus.children( ".ui-state-disabled" ).attr( "aria-disabled", "true" );

            // If the active item has been removed, blur the menu
            if ( this.active && !$.contains( this.element[ 0 ], this.active[ 0 ] ) ) {
                this.blur();
            }
        }
    });

    return {
        menu: $.mage.megamenu,
        navigation: $.mage.navigation
    };
});
