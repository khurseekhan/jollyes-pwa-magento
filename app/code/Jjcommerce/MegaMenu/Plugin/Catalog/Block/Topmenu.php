<?php
/**
 * Jjcommerce_MegaMenu
 *
 * PHP version 5.x
 *
 * @category  PHP
 * @package   Jjcommerce\MegaMenu
 * @author    Swapnil Tatkondawar <swapnilt@2jcommerce.in>
 * @copyright 2017 Copyright 2J Commerce, Inc. http://www.2jcommerce.in/
 * @license   http://www.2jcommerce.in/ Private
 * @link      http://www.jollyes.co.uk/
 */

namespace Jjcommerce\MegaMenu\Plugin\Catalog\Block;

use Magento\Catalog\Plugin\Block\Topmenu as MagentoTopmenuPlugin;
use Magento\Catalog\Model\ResourceModel\Category\Collection;
use Magento\Framework\Exception\LocalizedException;
use Magento\Catalog\Model\Category;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Catalog\Model\Layer\Resolver;
use Magento\Catalog\Helper\Category as CategoryHelper;
use Magento\Catalog\Model\ResourceModel\Category\StateDependentCollectionFactory;
use Magento\Theme\Block\Html\Topmenu as ThemeTopmenu;
use Magento\Framework\Data\Tree\Node;
use Magento\Framework\UrlInterface;
use Magento\Framework\Registry;

class Topmenu extends MagentoTopmenuPlugin
{
    /**
     * Registry Key Node
     */
    const NODE_KEY = 'current_cms_hierarchy_main_node';

    /**
     * Store Manager
     *
     * @var StoreManagerInterface
     */
    private $_storeManager;

    /**
     * Layer Resolver
     *
     * @var Resolver
     */
    private $_layerResolver;

    /**
     * Core registry
     *
     * @var Registry
     */
    protected $registry;


    /**
     * Initialize dependencies.
     *
     * @param Registry              $registry                  Registry Instance
     * @param CategoryHelper        $catalogCategory           Category Helper
     * @param StateDependentCollectionFactory     $categoryCollectionFactory Category Collection
     * @param StoreManagerInterface $storeManager              Store Manager
     * @param Resolver              $layerResolver             Layer Resolver
     *
     */
    public function __construct(
        Registry $registry,
        CategoryHelper $catalogCategory,
        StateDependentCollectionFactory $categoryCollectionFactory,
        StoreManagerInterface $storeManager,
        Resolver $layerResolver
    ) {
        parent::__construct(
            $catalogCategory,
            $categoryCollectionFactory,
            $storeManager,
            $layerResolver
        );

        $this->registry       = $registry;
        $this->_storeManager  = $storeManager;
        $this->_layerResolver = $layerResolver;
    }//end __construct()


    /**
     * Build category tree for menu block.
     *
     * @param ThemeTopmenu $subject           Model Instance of TopMenu
     * @param string       $outermostClass    Outer Most Class
     * @param string       $childrenWrapClass Child Wrapper class
     * @param int          $limit             Column Break Limit
     *
     * @return void
     * @SuppressWarnings("PMD.UnusedFormalParameter")
     */
    public function beforeGetHtml(
        ThemeTopmenu $subject,
        $outermostClass = '',
        $childrenWrapClass = '',
        $limit = 0
    ) {
        $rootId  = $this->_storeManager->getStore()->getRootCategoryId();
        $storeId = $this->_storeManager->getStore()->getId();

        /** @var \Magento\Catalog\Model\ResourceModel\Category\Collection $collection */
        $collection      = $this->getCategoryTree($storeId, $rootId);
        $currentCategory = $this->getCurrentCategory();
        $mapping         = [$rootId => $subject->getMenu()];  // use nodes stack to avoid recursion
        foreach ($collection as $category) {
            if (!isset($mapping[$category->getParentId()])) {
                continue;
            }
            /** @var Node $parentCategoryNode */
            $parentCategoryNode = $mapping[$category->getParentId()];
            $categoryNode       = new Node(
                $this->getCategoryAsArray($category, $currentCategory),
                'id',
                $parentCategoryNode->getTree(),
                $parentCategoryNode
            );

            $parentCategoryNode->addChild($categoryNode);
            $mapping[$category->getId()] = $categoryNode; //add node in stack
        }
    }//end beforeGetHtml()


    /**
     * Get Category Tree
     *
     * @param int $storeId Store Id
     * @param int $rootId  Root Id
     *
     * @return Collection
     * @throws LocalizedException
     */
    protected function getCategoryTree($storeId, $rootId)
    {
        $collection = parent::getCategoryTree($storeId, $rootId);
        $collection->addAttributeToSelect(
            [
                'column_break',
                'featured_product_sku',
            ],
            'left'
        );
        return $collection;
    }//end getCategoryTree()


    /**
     * Convert category to array
     *
     * @param Category $category        Category Model
     * @param Category $currentCategory Current Category Model
     *
     * @return array
     */
    private function getCategoryAsArray($category, $currentCategory)
    {
        $hasActive = in_array(
            (string)$category->getId(),
            explode('/', $currentCategory->getPath()),
            true
        );

        return [
            'name'              => $category->getName(),
            'id'                => 'category-node-' . $category->getId(),
            'url'               => $this->catalogCategory->getCategoryUrl($category),
            'has_active'        => $hasActive,
            'is_active'         => $category->getId() == $currentCategory->getId(),
            'column_break'      => (bool) $category->getColumnBreak(),
            'featured_product_sku' => $category->getData('featured_product_sku'),
            'category_item'     => true
        ];
    }//end getCategoryAsArray()


    /**
     * Get current Category from catalog layer
     *
     * @return Category
     */
    private function getCurrentCategory()
    {
        $catalogLayer = $this->_layerResolver->get();

        if (!$catalogLayer) {
            return null;
        }

        return $catalogLayer->getCurrentCategory();
    }//end getCurrentCategory()



}//end class
