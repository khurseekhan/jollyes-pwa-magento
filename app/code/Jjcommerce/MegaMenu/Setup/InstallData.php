<?php
/**
 * Jjcommerce_MegaMenu
 *
 * PHP version 5.x
 *
 * @category  PHP
 * @package   Jjcommerce\MegaMenu
 * @author    Swapnil Tatkondawar <swapnilt@2jcommerce.in>
 * @copyright 2017 Copyright 2J Commerce, Inc. http://www.2jcommerce.in/
 * @license   http://www.2jcommerce.in/ Private
 * @link      http://www.jollyes.co.uk/
 */

namespace Jjcommerce\MegaMenu\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Catalog\Setup\CategorySetupFactory;
use Magento\Catalog\Model\Category;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;

class InstallData implements InstallDataInterface
{
    /**
     * Category setup factory
     *
     * @var CategorySetupFactory
     */
    protected $_categorySetupFactory;


    /**
     * InstallData constructor.
     *
     * @param CategorySetupFactory $categorySetupFactory Category Setup Factory
     */
    public function __construct(CategorySetupFactory $categorySetupFactory)
    {
        $this->_categorySetupFactory = $categorySetupFactory;
    }//end __construct()


    /**
     * Installs data for a module
     *
     * @param ModuleDataSetupInterface $setup   Setup Instance
     * @param ModuleContextInterface   $context Context Instance
     *
     * @return void
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer     = $setup;
        $categorySetup = $this->_categorySetupFactory->create(['setup' => $setup]);

        $installer->startSetup();

        $categorySetup->addAttribute(
            Category::ENTITY,
            'column_break',
            [
                'type'       => 'int',
                'label'      => 'Add Column Break',
                'input'      => 'boolean',
                'source'     => 'Magento\Eav\Model\Entity\Attribute\Source\Boolean',
                'required'   => false,
                'sort_order' => 1,
                'global'     => ScopedAttributeInterface::SCOPE_STORE,
                'group'      => 'General Information',
            ]
        );

        $categorySetup->addAttribute(
            Category::ENTITY,
            'featured_product_sku',
            [
                'type'       => 'varchar',
                'label'      => 'Featured Product SKU',
                'input'      => 'text',
                'required'   => false,
                'sort_order' => 2,
                'global'     => ScopedAttributeInterface::SCOPE_STORE,
                'group'      => 'General Information',
            ]
        );

        $installer->endSetup();
    }//end install()


}//end class
