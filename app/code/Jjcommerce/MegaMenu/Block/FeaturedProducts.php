<?php
/**
 * Jjcommerce_MegaMenu
 *
 * PHP version 5.x
 *
 * @category  PHP
 * @package   Jjcommerce\MegaMenu
 * @author    Swapnil Tatkondawar <swapnilt@2jcommerce.in>
 * @copyright 2017 Copyright 2J Commerce, Inc. http://www.2jcommerce.in/
 * @license   http://www.2jcommerce.in/ Private
 * @link      http://www.jollyes.co.uk/
 */
namespace Jjcommerce\MegaMenu\Block;

use Magento\Catalog\Block\Product\Context;
use Magento\Catalog\Model\Layer\Resolver;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\Catalog\Model\Product\Visibility;
use Magento\CatalogInventory\Helper\Stock;
use Magento\Framework\App\Http\Context as HttpContext;
use Magento\Customer\Model\Context as CustomerContext;
use Magento\Tax\Api\TaxCalculationInterface;
use Jjcommerce\MegaMenu\Helper\Data;


class FeaturedProducts extends \Magento\Catalog\Block\Product\AbstractProduct
{
    const CACHE_TAG = 'top_menu_featured_category_products';


    /** @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory */
    private $productCollectionFactory;

    /** @var \Magento\Catalog\Model\Product\Visibility $catalogProductVisibility */
    private $catalogProductVisibility;

    /** @var \Magento\Store\Model\StoreManagerInterface $storeManager */
    private $storeManager;

    /** @var \Magento\Catalog\Model\Layer $catalogLayer */
    private $catalogLayer;

    /** @var \Magento\Framework\Registry $registry */
    private $registry;

    /** @var \Magento\CatalogInventory\Helper\Stock $stockHelper */
    private $stockHelper;

    /**
     * @var \Magento\Framework\App\Http\Context
     */
    protected $_httpContext;

    /**
     * @var \Magento\Tax\Api\TaxCalculationInterface
     */
    protected $taxCalculation;

    protected $helper;


    /**
     * FeaturedProducts constructor.
     * @param Context $context
     * @param Data $dataHelper
     * @param CollectionFactory $productCollectionFactory
     * @param Visibility $catalogProductVisibility
     * @param Resolver $layerResolver
     * @param Stock $stockHelper
     * @param HttpContext $httpContext
     * @param array $data
     */
    public function __construct(
        Context $context,
        CollectionFactory $productCollectionFactory,
        Visibility $catalogProductVisibility,
        Resolver $layerResolver,
        Stock $stockHelper,
        HttpContext $httpContext,
        TaxCalculationInterface $taxCalculation,
        Data $helper,
        $data = []
    ) {
        $this->_httpContext = $httpContext;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->catalogProductVisibility = $catalogProductVisibility;
        $this->registry = $context->getRegistry();
        $this->storeManager = $context->getStoreManager();
        $this->catalogLayer = $layerResolver->get();
        $this->stockHelper = $stockHelper;
        $this->taxCalculation = $taxCalculation;
        $this->helper = $helper;
        parent::__construct(
            $context,
            $data
        );
    }

	/**
     * @return void
    */
    protected function _construct()
    {
        $this->addData(
            ['cache_lifetime' => 86400, 'cache_tags' => [self::CACHE_TAG]]
        );
    }

	 /**
     * Get Key pieces for caching block content
     *
     * @return array
     */
    public function getCacheKeyInfo()
    {
        $currentCategory = $this->getCurrentCategory();
        return [
            'TOP_MENU_FEATURED_CATEGORY_PRODUCTS_'.$currentCategory->getId(),
            $this->storeManager->getStore()->getId(),
            $this->_design->getDesignTheme()->getId(),
            $this->_httpContext->getValue(CustomerContext::CONTEXT_GROUP),
            'template' => $this->getTemplate()
        ];

    }

    protected function _beforeToHtml()
    {
        if (!isset($category)) {
            $currentCategory = $this->getCurrentCategory();
        }

        if ($currentCategory->getLevel()==2) {
            $productCollection = $this->createCollection();
            if ($productCollection) {
                $this->setProductCollection($productCollection);
                return $this;
            } else {
                parent::_beforeToHtml();
            }
        } else {
            parent::_beforeToHtml();
        }
    }

    /**
     * Prepare and return product collection
     *
     * @return \Magento\Catalog\Model\ResourceModel\Product\Collection
     */
    public function createCollection()
    {
        if (!isset($category)) {
            $currentCategory = $this->getCurrentCategory();
        }

        $categories = $currentCategory->getChildrenCategories();
        if ($categories && count($categories) < 1) {
            return false;
        }

        $limit = 1;

        $storeId = $this->getCurrentStoreId();

        /** @var $collection \Magento\Catalog\Model\ResourceModel\Product\Collection */
        $collection = $this->productCollectionFactory->create();
        $collection->setVisibility($this->catalogProductVisibility->getVisibleInCatalogIds());

        $collection = $this->_addProductAttributesAndPrices($collection)
            //->addCategoryFilter($currentCategory)
            ->addStoreFilter($storeId)
            //->addVisibleFilter()
            ->addAttributeToFilter('sku', $currentCategory->getData('featured_product_sku'))
            ->setPageSize($this->getPageSize());

        $this->stockHelper->addInStockFilterToCollection($collection);
        $collection->getSelect()->order($this->getSortBy())->limit($limit);

        return $collection;
    }


    /**
     * Get current store id
     * @return int
     */
    private function getCurrentStoreId()
    {
        return $this->storeManager->getStore()->getId();
    }


    /**
     * Return identifiers for produced content
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG];
    }

    public function getProductCalPrice($product)
    {
        $taxAttribute = $this->getAssociatedProducts($product);
        $productRateId = $taxAttribute['tax_class'];
        $rate = $this->taxCalculation->getCalculatedRate($productRateId);
        $finalPrice = '';

        $isTax = $this->helper->getTaxType();

        $_configurableMinimalPriceWithTax = $product->getMinimalPrice();
        $price = (isset($_configurableMinimalPriceWithTax)) ? $_configurableMinimalPriceWithTax : $product->getFinalPrice();
        if($isTax==2){
            $finalPrice = $price + ($price * ($rate / 100));
        }else{
            $finalPrice = $price ;
        }

        return $finalPrice;
    }

    /**
     * Retrieve grouped products
     *
     * @return array
     */
    public function getAssociatedProducts($product)
    {

        $associatedProducts = $product->getTypeInstance()->getAssociatedProducts($product);
        $products = array();

        foreach ($associatedProducts as $product) {
            $products = array('tax_class' => $product->getTaxClassId(),'id' => $product->getId());
            break;
        }
        return $products;
    }

}
