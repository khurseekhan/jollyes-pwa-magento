<?php
/**
 * Jjcommerce_MegaMenu
 *
 * PHP version 5.x
 *
 * @category  PHP
 * @package   Jjcommerce\MegaMenu
 * @author    Swapnil Tatkondawar <swapnilt@2jcommerce.in>
 * @copyright 2017 Copyright 2J Commerce, Inc. http://www.2jcommerce.in/
 * @license   http://www.2jcommerce.in/ Private
 * @link      http://www.jollyes.co.uk/
 */

namespace Jjcommerce\MegaMenu\Block\Html;

use Magento\Theme\Block\Html\Topmenu as MagentoTopmenu;
use Magento\Framework\Data\Tree\Node;
use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\Data\Tree\NodeFactory;
use Magento\Framework\Data\TreeFactory;
use Magento\Framework\Registry;
use Magento\Cms\Model\BlockRepository;
use Magento\Cms\Model\Template\FilterProvider;

class Topmenu extends MagentoTopmenu
{
    /**
     * Registry Key Node
     */
    const NODE_KEY = 'current_cms_hierarchy_main_node';

    /**
     * @var BlockRepository
     */
    protected $blockRepository;

    /**
     * @var string
     */
    protected $blockContent = null;

    /**
     * @var FilterProvider
     */
    protected $filterProvider;

    /**
     * Store manager
     *
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * Break Counter Keeping
     * Keeping 2 as default because
     *
     * 1 - Column for Categories
     * 2 - Column for promotional Image
     *
     * @var integer
     */
    protected $_breakCounter = 2;

    /**
     * Top Element Level 0 count
     *
     * @var integer
     */
    protected $_topElementCounter = 0;

    /**
     * Core registry
     *
     * @var Registry
     */
    protected $registry;

    /**
     * @var \Magento\Catalog\Model\CategoryFactory
     */
    protected $_categoryFactory;

    /**
     * @var \Magento\CatalogInventory\Helper\Stock
     */
    protected $_stockFilter;


    /**
     * Topmenu constructor.
     *
     * @param StoreManagerInterface $storeManager    Filter Block Instance
     * @param FilterProvider        $filterProvider  Filter Block Instance
     * @param BlockRepository       $blockRepository Block Repository Instance
     * @param Registry              $registry        Core Registry Instance
     * @param Context               $context         Context Instance
     * @param NodeFactory           $nodeFactory     Node Factory Instance
     * @param TreeFactory           $treeFactory     Tree Factory Instance
     * @param array                 $data            Array data
     *
     */
    public function __construct(
        FilterProvider $filterProvider,
        BlockRepository $blockRepository,
        Registry $registry,
        Context $context,
        NodeFactory $nodeFactory,
        TreeFactory $treeFactory,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Magento\CatalogInventory\Helper\Stock $stockFilter,
        array $data = []
    ) {
        $this->registry        = $registry;
        $this->blockRepository = $blockRepository;
        $this->filterProvider  = $filterProvider;
        $this->storeManager    = $context->getStoreManager();
        $this->_categoryFactory = $categoryFactory;
        $this->_stockFilter = $stockFilter;
        parent::__construct(
            $context,
            $nodeFactory,
            $treeFactory,
            $data
        );
    }//end __construct()


    /**
     * Recursively generates top menu html from data that is specified in $menuTree
     *
     * @param Node   $menuTree          Menu Tree
     * @param string $childrenWrapClass Children Wrap Class
     * @param int    $limit             Limit
     * @param array  $colBrakes         Column Break
     *
     * @return string
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function _getHtml(
        Node $menuTree,
        $childrenWrapClass,
        $limit,
        $colBrakes = []
    ) {
        $html          = '';
        $children      = $menuTree->getChildren();
        $parentLevel   = $menuTree->getLevel();
        $childLevel    = ($parentLevel === null) ? 0 : $parentLevel + 1;
        $counter       = 1;
        $itemPosition  = 1;
        $childrenCount = $children->count();

        $parentPositionClass     = $menuTree->getPositionClass();
        $itemPositionClassPrefix = ($parentPositionClass) ? $parentPositionClass . '-' : 'nav-';

        foreach ($children as $child) {
            $child->setLevel($childLevel);
            $child->setIsFirst($counter == 1);
            $child->setIsLast($counter == $childrenCount);
            $child->setPositionClass($itemPositionClassPrefix . $counter);

            $outermostClassCode = '';
            $outermostClass     = $menuTree->getOutermostClass();

            if ($childLevel == 0 && $outermostClass) {
                $outermostClassCode = ' class="' . $outermostClass . '" ';
                $child->setClass($outermostClass);
            }

            if ($colBrakes && $colBrakes[$counter]['colbrake']) {
                $html .= '</ul></li><li class="column"><ul>';
            }

            if ($child->getColumnBreak() && $childLevel > 0) {
                $this->_breakCounter++;
                $html .= '</ul>';
                $html .= '<ul class="sub-cat column-break">';
            }

            $html .= '<li' . $this->_getRenderedMenuItemAttributes($child) . ' data-level='.$child->getLevel().'>';
            $html .= '<a href="' . $child->getUrl() . '" ' .$outermostClassCode . '>';
            $html .= '<span>'.$this->escapeHtml($child->getName()).'</span>';
            $html .= '</a>';

            if ($childLevel > 0 && $child->hasChildren()) {
                $html .= '<span class="icon-megamenu icon-level-'.$child->getLevel().'"></span>';
            }

            if ($childLevel > 0) {
                $html .= '</li>';
            }

            $html .= $this->_addSubMenu($child, $childLevel, $childrenWrapClass, $limit);

            if ($childLevel == 0) {
                $this->_topElementCounter++;
                $html .= '</li>';
            }

            $itemPosition++;
            $counter++;
        }

        if ($colBrakes && $limit) {
            $html = '<li class="column"><ul>' . $html . '</ul></li>';
        }

        return $html;
    }//end _getHtml()


    /**
     * Add sub menu HTML code for current menu item
     *
     * @param Node   $child             Child Node
     * @param string $childLevel        Child Level
     * @param string $childrenWrapClass Child wrapper class
     * @param int    $limit             Column Limit
     *
     * @return string HTML code
     */
    protected function _addSubMenu($child, $childLevel, $childrenWrapClass, $limit)
    {
        $html = $content = '';
        if (!$child->hasChildren()) {
            return $html;
        }

        $colStops = null;
        if ($childLevel == 0 && $limit) {
            $colStops = $this->_columnBrake($child->getChildren(), $limit);
        }

        $content = $this->_getHtml($child, $childrenWrapClass, $limit, $colStops);

        if ($childLevel < 1) {
            $menuClasses  = 'level';
            $menuClasses .= $childLevel;
            $gridCount    = $this->getBreakCounterCount();
            //Deduct one column with its a CMS items menu
            //As no promotional image area is needed
            if (!(bool) $child->getCategoryItem()) {
                $gridCount -=1;
                $menuClasses .= ' cms-items';
            } else {
                $menuClasses .= ' category-items';
            }

            $menuClasses .= ' submenu subcat-col-'.$gridCount;

            $html .= '<ul class="'.$menuClasses.'">';
            $html .= '<li class="wrapper-content">';
            $html .= '<div class="wrapper-sub-cat">';
            $html .= '<div class="back-link-wrapper">';
            $html .= '<div class="back-button">';
            $html .= '<span class="back-to-prev" rel="back to menu">'.$this->escapeHtml(__('Back')).'</span>';
            $html .= '</div>';
            $html .= '<div class="category-link">';
            $html .= '<a href="' . $child->getUrl() . '">';
            $html .= '<span>'.$this->escapeHtml(__('Go to %1 Page', $child->getName())).'</span>';
            $html .= '</a>';
            $html .= '</div>';
            $html .= '</div>';
            $html .= '<ul class="sub-cat">';
        }

        if ($childLevel < 1) {

            $this->resetBreakCounterCount();
            $html .= $content;
            $html .= '</ul>';
            $html .= '</div>';
            $html .= '<div class="wrapper-content-block">';
            if ((bool) $child->getCategoryItem()) {
                $childId = (int) str_replace('category-node-', '', $child->getId());
                $featuredProductSku = $child->getData('featured_product_sku');
                if($featuredProductSku) {
                    $html .= '<div class="wrapper-featured-block">';
                    $html .= $this->_getFeaturedProductBlock($childId);
                    $html .= '</div>';
                }
            }
            $html .= '<div class="allshop-link-block">';
            $html .= '<a href="' . $child->getUrl() . '">';
            $html .= '<span>'.$this->escapeHtml(__('Shop %1 Category', $child->getName())).'</span>';
            $html .= '</a>';
            $html .= '</div>';
            $html .= '</div>';
            $html .= '</li>';
            $html .= '</ul>';
        }

        if (!(bool)$html) {
            return $content;
        }

        return $html;
    }//end _addSubMenu()


    /**
     * Returns array of menu item's attributes
     *
     * @param Node $item Category Item Instance
     *
     * @return array
     */
    protected function _getMenuItemAttributes(Node $item)
    {
        $result   = parent::_getMenuItemAttributes($item);

        return $result;
    }//end _getMenuItemAttributes()




    /**
     * Returns array of menu item's attributes
     *
     * @return string
     */
    protected function _getFeaturedProductBlock($categoryId)
    {
        $category = $this->getCategoryById($categoryId);

        return $this->getLayout()
            ->createBlock('Jjcommerce\MegaMenu\Block\FeaturedProducts')
            ->setData('current_category',$category)
            ->setTemplate('Jjcommerce_MegaMenu::featured_product.phtml')
            ->toHtml();

        //return "Featured Product Block";
    }//end _getFeaturedProductBlock()



    public function getCategoryById($categoryId)
    {
        if(!$categoryId) return false;

        $category = $this->_categoryFactory->create()->load($categoryId);

        return $category;
    }



    /**
     * Generates string with all attributes that should be present in menu item element
     *
     * @param array $attributes Attributes for the image tag
     *
     * @return string
     */
    protected function _getImageAttributes($attributes)
    {
        $html = '';
        foreach ($attributes as $attributeName => $attributeValue) {
            $html .= ' ' . $attributeName . '="' . str_replace('"', '\"', $attributeValue) . '"';
        }
        return $html;
    }//end _getImageAttributes()


    /**
     * Returns the counter number
     *
     * @return Integer
     */
    protected function getBreakCounterCount()
    {
        return $this->_breakCounter;
    }//end getBreakCounterCount()


    /**
     * Returns the counter number
     * for top level category
     *
     * @return integer
     */
    public function getTopCatCount()
    {
        return $this->_topElementCounter;
    }//end getTopCatCount()


    /**
     * Resets the counter number
     *
     * @return Integer
     */
    protected function resetBreakCounterCount()
    {
        $this->_breakCounter = 2;
    }//end resetBreakCounterCount()


    /**
     * Get Cache Key Value
     *
     * @return string
     */
    public function getCacheKey()
    {
        $value = parent::getCacheKey();
        /**
         * @var $node \Magento\VersionsCms\Model\Hierarchy\Node
         */
        $node = $this->registry->registry(self::NODE_KEY);
        if (is_object($node) && $node->getRequestUrl() !== 'shop') {
            $value .= '_'.strtoupper($node->getRequestUrl());
        }
        return $value;
    }//end getCacheKey()


}//end class