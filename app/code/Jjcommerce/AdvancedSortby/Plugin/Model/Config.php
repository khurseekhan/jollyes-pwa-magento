<?php
/**
 * Jjcommerce_AdvancedSortby
 *
 * PHP version 5.x
 *
 * @category  PHP
 * @package   Jjcommerce\AdvancedSortby
 * @author    Swapnil Tatkondawar <swapnilt@2jcommerce.in>
 * @copyright 2017 Copyright 2J Commerce, Inc. http://www.2jcommerce.in/
 * @license   http://www.2jcommerce.in/ Private
 * @link      http://www.jollyes.co.uk/
 */
namespace Jjcommerce\AdvancedSortby\Plugin\Model;

use Jjcommerce\AdvancedSortby\Helper\Data;

class Config
{
    /**
     * Store manager
     *
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_helper;

    /**
     * Config constructor.
     * @param Data $helper
     */
    public function __construct(
        Data $helper
    ) {
        $this->_helper = $helper;
    }

    /**
     * @param \Magento\Catalog\Model\Config $catalogConfig
     * @param $options
     * @return array
     */
    public function afterGetAttributeUsedForSortByArray(
        \Magento\Catalog\Model\Config $catalogConfig,
        $options)
    {
        if(!$this->_helper->isEnabled()){
            return $options;
        }

        //Remove specific default sorting options
        unset($options['position']);
        unset($options['name']);
        unset($options['price']);

        //New sorting options
        if($this->_helper->getNewOptionEnabled()){
            $customOption['new'] = __('%1', $this->_helper->getNewLabel());
        }

        if($this->_helper->getBestsellerOptionEnabled()){
            $customOption['bestseller'] = __('%1', $this->_helper->getBestsellerLabel());
        }

        if($this->_helper->getMostPopularOptionIsEnabled()){
            $customOption['most_popular'] = __('%1', $this->_helper->getMostpopularLabel());
        }

        if($this->_helper->getNameIsEnabled()){
            $customOption['name'] = __('%1', $this->_helper->getNameAZLabel());
            $customOption['name_desc'] = __('%1', $this->_helper->getNameZALabel());
        }

        if($this->_helper->getPriceOptionIsEnabled()){
            $customOption['price'] =  __('%1', $this->_helper->getPriceLowestLabel());
            $customOption['price_desc'] = __('%1', $this->_helper->getPriceHighestLabel());
        }

        //Merge default sorting options with custom options
        $options = array_merge($customOption, $options);

        return $options;
    }
}
