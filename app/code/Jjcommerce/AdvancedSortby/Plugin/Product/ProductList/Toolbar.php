<?php
/**
 * Jjcommerce_AdvancedSortby
 *
 * PHP version 5.x
 *
 * @category  PHP
 * @package   Jjcommerce\AdvancedSortby
 * @author    Swapnil Tatkondawar <swapnilt@2jcommerce.in>
 * @copyright 2017 Copyright 2J Commerce, Inc. http://www.2jcommerce.in/
 * @license   http://www.2jcommerce.in/ Private
 * @link      http://www.jollyes.co.uk/
 */
namespace Jjcommerce\AdvancedSortby\Plugin\Product\ProductList;

class Toolbar
{

    /**
     * Plugin
     *
     * @param \Magento\Catalog\Block\Product\ProductList\Toolbar $subject
     * @param \Closure $proceed
     * @param \Magento\Framework\Data\Collection $collection
     * @return \Magento\Catalog\Block\Product\ProductList\Toolbar
     */
    public function aroundSetCollection(
        \Magento\Catalog\Block\Product\ProductList\Toolbar $subject,
        \Closure $proceed,
        $collection
    ) {
        $currentOrder = $subject->getCurrentOrder();
        $currentDirection = $subject->getCurrentDirection();;
        $result = $proceed($collection);

        if ($currentOrder) {
            switch ($currentOrder) {
                case 'price_desc':
                    $subject->getCollection()->setOrder('price', 'desc');
                    break;
                case 'new':
                    if ($currentDirection == 'desc' ) {

                        $subject->getCollection()
                            ->getSelect()
                            ->order('e.created_at DESC');


                    } elseif ($currentDirection == 'asc' ) {

                        $subject->getCollection()
                            ->getSelect()
                            ->order('e.created_at ASC');

                    }

                    break;
                case 'name_desc':
                    $subject->getCollection()->setOrder('name', 'desc');
                    break;
                case 'price':
                    if ($currentDirection == 'desc' ) {
                        $subject->getCollection()
                            ->getSelect()
                            ->reset(\Magento\Framework\DB\Select::ORDER)
                            ->order('price_index.min_price ASC');

                    }else{
                        $subject->getCollection()->setOrder('price', 'asc');
                    }

                    break;
                case 'name':
                    $subject->getCollection()->setOrder('name', 'asc');
                    break;
                case 'bestseller':
					try{
						$query = $subject->getCollection()->getSelect()->__toString();
						
						$pos = strpos($query, 'catalog_product_bestseller');
						if ($pos === false) {
						$subject->getCollection()->getSelect()->joinLeft(
							'catalog_product_bestseller',
							'e.entity_id = catalog_product_bestseller.entity_id',
							array('qty_ordered'=>'catalog_product_bestseller.no_of_times_product_ordered'))
							->group('e.entity_id')
							->order('qty_ordered '.$this->getCurrentDirectionReverse($currentDirection));
						}						
					}
					catch (\Exception $e) {						
						echo "msg:-->".$e->getMessage();
					}				
					
                    break;
                case 'most_popular':
                    $subject->getCollection()->getSelect()->joinLeft(
                        ['review_entity_summary'],
                        'e.entity_id = review_entity_summary.entity_pk_value',
                        ['rating_summary'=>'review_entity_summary.rating_summary']
                        )->group('e.entity_id')
                        ->order('rating_summary '.$this->getCurrentDirectionReverse($currentDirection));
                    break;
                default:
                    $subject->getCollection()->setOrder($currentOrder, $currentDirection);
                    break;
            }
        }

        return $result;
    }

    public function getCurrentDirectionReverse($currentDirection) {
        if ($currentDirection == 'asc') {
            return 'DESC';
        } elseif ($currentDirection == 'desc') {
            return 'ASC';
        } else {
            return $currentDirection;
        }
    }
}
