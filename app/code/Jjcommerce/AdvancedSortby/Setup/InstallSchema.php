<?php
/**
 * Jjcommerce_AdvancedSortby
 *
 * @category  PHP
 * @package   Jjcommerce\AdvancedSortby
 * @author    Swapnil Tatkondawar <swapnilt@2jcommerce.in>
 * @copyright 2017 Copyright 2J Commerce, Inc. http://www.2jcommerce.in/
 * @license   http://www.2jcommerce.in/ Private
 * @link      http://www.jollyes.co.uk/
 */
namespace Jjcommerce\AdvancedSortby\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use \Magento\Framework\DB\Ddl\Table;

class InstallSchema implements InstallSchemaInterface
{
    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        if (!$installer->tableExists('catalog_product_bestseller')) {
            $table  = $installer->getConnection()
                ->newTable($installer->getTable('catalog_product_bestseller'))
                ->addColumn(
                    'id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                    'Id'
                )
                ->addColumn(
                    'entity_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    '11',
                    [ 'unsigned' => true, 'nullable' => false],
                    'Entity Id'
                )
                ->addColumn(
                    'sku',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    '255',
                    [],
                    'Product Sku'
                )->addColumn(
                    'no_of_times_product_ordered',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    '11',
                    [ 'unsigned' => true, 'nullable' => false],
                    'Product Qty Ordered'
                )->addIndex(
                    $installer->getIdxName('catalog_product_bestseller', ['entity_id']),
                    ['entity_id']
                )->addIndex(
                    $installer->getIdxName('catalog_product_bestseller', ['sku']),
                    ['sku']
                );
            $installer->getConnection()->createTable($table);
        }

        $installer->endSetup();
    }
}
