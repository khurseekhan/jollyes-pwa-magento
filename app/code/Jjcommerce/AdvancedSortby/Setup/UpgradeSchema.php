<?php
/**
 * Jjcommerce_AdvancedSortby
 *
 * @category  PHP
 * @package   Jjcommerce\AdvancedSortby
 * @author    Swapnil Tatkondawar <swapnilt@2jcommerce.in>
 * @copyright 2017 Copyright 2J Commerce, Inc. http://www.2jcommerce.in/
 * @license   http://www.2jcommerce.in/ Private
 * @link      http://www.jollyes.co.uk/
 */
namespace Jjcommerce\AdvancedSortby\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{

    /**
     * @param SchemaSetupInterface   $setup
     * @param ModuleContextInterface $context
     */
    public function upgrade(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {

        if (version_compare($context->getVersion(), '2.0.1') < 0) {
            $installer = $setup;
            $installer->startSetup();

            if (!$installer->tableExists('jj_sales_order_batch')) {
                $table  = $installer->getConnection()
                    ->newTable($installer->getTable('jj_sales_order_batch'))
                    ->addColumn(
                        'id',
                        \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                        null,
                        ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                        'Id'
                    )
                    ->addColumn(
                        'last_item_id',
                        \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                        '11',
                        [ 'unsigned' => true, 'nullable' => false],
                        'Entity Id'
                    );
                $installer->getConnection()->createTable($table);
            }

            $installer->endSetup();
        }
    }
}
