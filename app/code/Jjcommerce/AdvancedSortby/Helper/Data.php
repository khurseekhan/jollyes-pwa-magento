<?php
/**
 * Jjcommerce_AdvancedSortby
 *
 * PHP version 5.x
 *
 * @category  PHP
 * @package   Jjcommerce\AdvancedSortby
 * @author    Swapnil Tatkondawar <swapnilt@2jcommerce.in>
 * @copyright 2017 Copyright 2J Commerce, Inc. http://www.2jcommerce.in/
 * @license   http://www.2jcommerce.in/ Private
 * @link      http://www.jollyes.co.uk/
 */
namespace Jjcommerce\AdvancedSortby\Helper;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Helper\AbstractHelper;

class Data extends AbstractHelper
{
    /**
     * Core store config
     *
     * @var ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * New is Enabled or Not
     */
    const XML_PATH_IS_NEW_ENABLED = 'advanced_sortby_setting/new/is_new_enabled';

    /**
     * Bestseller is Enabled or Not
     */
    const XML_PATH_IS_BESTSELLER_ENABLED = 'advanced_sortby_setting/bestseller/is_bestseller_enabled';

    /**
     * Most Popular is Enabled or Not
     */
    const XML_PATH_IS_MOSTPOPULAR_ENABLED = 'advanced_sortby_setting/mostpopular/is_mostpopular_enabled';

    /**
     * Name is Enabled or Not
     */
    const XML_PATH_IS_NAME_ENABLED = 'advanced_sortby_setting/name/is_name_enabled';

    /**
     * Price is Enabled or Not
     */
    const XML_PATH_IS_PRICE_ENABLED = 'advanced_sortby_setting/price/is_price_enabled';


    /**
     * new_label
     */
    const XML_PATH_NEW_LABEL = 'advanced_sortby_setting/new/new_label';

    /**
     * bestseller_label
     */
    const XML_PATH_BESTSELLER_LABEL = 'advanced_sortby_setting/bestseller/bestseller_label';

    /**
     * mostpopular_label
     */
    const XML_PATH_MOSTPOPULAR_LABEL = 'advanced_sortby_setting/mostpopular/mostpopular_label';

    /**
     * Name A-Z Label
     */
    const XML_PATH_NAME_AZ_LABEL = 'advanced_sortby_setting/name/name_asc_label';

    /**
     * Name Z-A Label
     */
    const XML_PATH_NAME_ZA_LABEL = 'advanced_sortby_setting/name/name_desc_label';

    /**
     * Price Low to High Label
     */
    const XML_PATH_PRICE_LH_LABEL = 'advanced_sortby_setting/price/price_asc_label';

    /**
     * Price High to Low Label
     */
    const XML_PATH_PRICE_HL_LABEL = 'advanced_sortby_setting/price/price_desc_label';


    /**
     * "Enable Module" from system config
     */
    const GENERAL_IS_ENABLED = 'advanced_sortby_setting/general/is_enabled';


    /**
     * __construct
     * @param Context $context
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        Context $context,
        StoreManagerInterface $storeManager
    )
    {
        $this->_scopeConfig  = $context->getScopeConfig();
        $this->_storeManager = $storeManager;
        $this->logger        = $context->getLogger();

        parent::__construct($context);
    }

    /**
     * Get New Option Is Enabled or Not
     * @param null $store
     * @return mixed
     */
    public function getNewOptionEnabled($store = null)
    {

        return $this->_scopeConfig->getValue(
            self::XML_PATH_IS_NEW_ENABLED,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * Get Bestseller Option Is Enabled or Not
     * @param null $store
     * @return mixed
     */
    public function getBestsellerOptionEnabled($store = null)
    {

        return $this->_scopeConfig->getValue(
            self::XML_PATH_IS_BESTSELLER_ENABLED,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * Get Most-Popular Option Is Enabled or Not
     * @param null $store
     * @return mixed
     */
    public function getMostPopularOptionIsEnabled($store = null)
    {

        return $this->_scopeConfig->getValue(
            self::XML_PATH_IS_MOSTPOPULAR_ENABLED,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * Get Name Option Is Enabled or Not
     * @param null $store
     * @return mixed
     */
    public function getNameIsEnabled($store = null)
    {

        return $this->_scopeConfig->getValue(
            self::XML_PATH_IS_NAME_ENABLED,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * Get Price Option Is Enabled or Not
     * @param null $store
     * @return mixed
     */
    public function getPriceOptionIsEnabled($store = null)
    {

        return $this->_scopeConfig->getValue(
            self::XML_PATH_IS_PRICE_ENABLED,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * Get Category Tab One Label
     * @param null $store
     * @return mixed
     */
    public function getNewLabel($store = null)
    {

        return $this->_scopeConfig->getValue(
            self::XML_PATH_NEW_LABEL,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * Get Best Seller Label
     * @param null $store
     * @return mixed
     */
    public function getBestsellerLabel($store = null)
    {

        return $this->_scopeConfig->getValue(
            self::XML_PATH_BESTSELLER_LABEL,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * Get Most Popular Label
     * @param null $store
     * @return mixed
     */
    public function getMostpopularLabel($store = null)
    {

        return $this->_scopeConfig->getValue(
            self::XML_PATH_MOSTPOPULAR_LABEL,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * Get Name A-Z Label
     * @param null $store
     * @return mixed
     */
    public function getNameAZLabel($store = null)
    {

        return $this->_scopeConfig->getValue(
            self::XML_PATH_NAME_AZ_LABEL,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * Get Name Z-A Label
     * @param null $store
     * @return mixed
     */
    public function getNameZALabel($store = null)
    {

        return $this->_scopeConfig->getValue(
            self::XML_PATH_NAME_ZA_LABEL,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * Get Price Lowest Label
     * @param null $store
     * @return mixed
     */
    public function getPriceLowestLabel($store = null)
    {

        return $this->_scopeConfig->getValue(
            self::XML_PATH_PRICE_LH_LABEL,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * Get Price Highest Label
     * @param null $store
     * @return mixed
     */
    public function getPriceHighestLabel($store = null)
    {

        return $this->_scopeConfig->getValue(
            self::XML_PATH_PRICE_HL_LABEL,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * Get config of category product tabs
     * @param $configPath
     * @param null $store
     * @return mixed
     */
    public function getTabsConfig($configPath, $store = null)
    {

        return $this->_scopeConfig->getValue(
            $configPath,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * Check weather module is enable or disable
     * @param null $store
     * @return bool
     */
    public function isEnabled($store = null)
    {
        $isModuleEnabled       = $this->isModuleEnabled();
        $isModuleOutputEnabled = $this->isModuleOutputEnabled();

        return $isModuleOutputEnabled && $isModuleEnabled && $this->getTabsConfig(self::GENERAL_IS_ENABLED, $store);
    }

    /**
     * Check module is enable in module list
     * @return mixed
     */
    public function isModuleEnabled()
    {
        $moduleName = "Jjcommerce_AdvancedSortby";

        return $this->_moduleManager->isEnabled($moduleName);
    }

}
