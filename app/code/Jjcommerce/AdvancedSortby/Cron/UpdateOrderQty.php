<?php
/**
 * Jjcommerce_AdvancedSortby
 *
 * @category  PHP
 * @package   Jjcommerce\AdvancedSortby
 * @author    Swapnil Tatkondawar <swapnilt@2jcommerce.in>
 * @copyright 2017 Copyright 2J Commerce, Inc. http://www.2jcommerce.in/
 * @license   http://www.2jcommerce.in/ Private
 * @link      http://www.jollyes.co.uk/
 */
namespace Jjcommerce\AdvancedSortby\Cron;

use Jjcommerce\AdvancedSortby\Model\ResourceModel\UpdateProductQty;

class UpdateOrderQty {

    /**
     * @var UpdateProductQty
     */
    protected $_dbResource;


    /**
     * UpdateOrderQty constructor.
     * @param UpdateProductQty $resource
     */
    public function __construct(
        UpdateProductQty $resource
    )
    {
        $this->_dbResource = $resource;
    }


    /**
     *
     */
    public function update()
    {
        $this->_dbResource->updateProductQtyOrdered();
    }
}
