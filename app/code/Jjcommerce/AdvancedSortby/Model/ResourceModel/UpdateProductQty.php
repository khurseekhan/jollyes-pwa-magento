<?php
/**
 * Jjcommerce_AdvancedSortby
 *
 * @category  PHP
 * @package   Jjcommerce\AdvancedSortby
 * @author    Swapnil Tatkondawar <swapnilt@2jcommerce.in>
 * @copyright 2017 Copyright 2J Commerce, Inc. http://www.2jcommerce.in/
 * @license   http://www.2jcommerce.in/ Private
 * @link      http://www.jollyes.co.uk/
 */
namespace Jjcommerce\AdvancedSortby\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class UpdateProductQty extends AbstractDb
{
    /**
     * Initialize resources
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('catalog_product_bestseller', 'id');
    }


    /**
     *  Get updateProductQtyOrdered
     */
    public function updateProductQtyOrdered()
    {

        try{
            $lastUpdatedItemId = $this->getLastItemId();
            if($lastUpdatedItemId==false){
                $salesOrderItemTableName = $this->getTable('sales_order_item');
                $select = $this->getConnection()->select()->from(
                    $salesOrderItemTableName,
                    ['product_id', 'sku', 'qty_ordered']
                );
            }elseif($lastUpdatedItemId != $this->getLastSalesOrderItemId()){
                $salesOrderItemTableName = $this->getTable('sales_order_item');
                $select = $this->getConnection()->select()->from(
                    $salesOrderItemTableName,
                    ['product_id', 'sku', 'qty_ordered']
                )->where( 'item_id BETWEEN '.$lastUpdatedItemId.' AND '.$this->getLastSalesOrderItemId());
            }else{
                echo "Last Order Item Id is not changed yet.";
                die;
            }

            $result = $this->getConnection()->fetchAll($select);
            if(count($result)>0) {
                foreach($result as $row){

                    if(isset($row['product_id'])){
                        $entityId =  $row['product_id'];
                    }else{
                        continue;
                    }

                    if($this->isProductExist($entityId)){
                        $salesOrderedQty = $this->getTotalSalesItemQtyOrdered($entityId);
                        if($this->getProductTotalQty($entityId) < $salesOrderedQty){
                            $this->getConnection()->update('catalog_product_bestseller' , array('no_of_times_product_ordered'=> $salesOrderedQty), 'entity_id='.$entityId);
                        }
                    }else{
                        $data = array(
                            'entity_id' => $row['product_id'],
                            'sku' => $row['sku'],
                            'no_of_times_product_ordered' => $row['qty_ordered'],
                        );
                        $this->getConnection()->insert('catalog_product_bestseller', $data );
                    }
                }
            }

            $this->updateLastOrderItemId();

        }catch(Exception $e){
            $this->_objectManager->get('Psr\Log\LoggerInterface')->critical($e);
        }

    }

    /**
     * @return string
     */
    public function getLastItemId()
    {

        $select = $this->getConnection()->select()->from(
            'jj_sales_order_batch',
            ['last_item_id']
        )->order('last_item_id DESC')->limit(1);

        return $this->getConnection()->fetchOne($select);
    }

    /**
     * @param $entityId
     * @return bool
     */
    public function isProductExist($entityId){

        $tableName = $this->getTable('catalog_product_bestseller');

        $select = $this->getConnection()->select()->from(
            $tableName,
            ['entity_id']
        )->where('entity_id='.$entityId);

        $productId = $this->getConnection()->fetchOne($select);

        if((int)$productId > 0)
        {
            return true;
        }

        return false;
    }

    /**
     * @param $entityId
     * @return int
     */
    public function getTotalSalesItemQtyOrdered($entityId){

        $tableName = $this->getTable('sales_order_item');

        $select = $this->getConnection()->select()->from(
            $tableName,
            ['product_id','sku','qty_ordered']
        )->where('product_id='.$entityId);

        $result = $this->getConnection()->fetchAll($select);

        if(count($result) > 0)
        {
            $totalQtyOrdered = 0;
            //$productData = [];
            foreach($result as $row){

                $totalQtyOrdered = $totalQtyOrdered + $row['qty_ordered'];
            }

            //$productData[$entityId] = $totalQtyOrdered;
        }

        return $totalQtyOrdered;
    }

    /**
     * Get Total Ordered quantity of product
     * @param $entityId
     * @return string
     */
    public function getProductTotalQty($entityId){

        $tableName = $this->getTable('catalog_product_bestseller');

        $select = $this->getConnection()->select()->from(
            $tableName,
            ['entity_id','sku','no_of_times_product_ordered']
        )->where('entity_id='.$entityId);

        $result = $this->getConnection()->fetchRow($select);

        if(count($result) > 0)
        {
            return $result['no_of_times_product_ordered'];
        }

        return '';
    }

    /**
     * @return mixed
     */
    public function getLastSalesOrderItemId(){

        $tableName = $this->getTable('sales_order_item');

        $select = $this->getConnection()->select()->from(
            $tableName,
            ['item_id','product_id','sku','qty_ordered']
        )->order('item_id DESC')->limit(1);

        $result = $this->getConnection()->fetchRow($select);

        return $result['item_id'];
    }

    public function updateLastOrderItemId(){
        $tableName = $this->getTable('sales_order_item');

        $select = $this->getConnection()->select()->from(
            $tableName,
            ['item_id','product_id','sku','qty_ordered']
        )->order('item_id DESC')->limit(1);

        $result = $this->getConnection()->fetchRow($select);

        $data = ['last_item_id'=>$result['item_id']];

        $selectBatch = $this->getConnection()->select()->from(
            'jj_sales_order_batch',
            ['last_item_id']
        )->order('last_item_id DESC')->limit(1);

        $lastItemId =  $this->getConnection()->fetchOne($selectBatch);

        if($lastItemId){
            $this->getConnection()->update('jj_sales_order_batch' , $data, 'id=4');

        }else{
            $this->getConnection()->insert('jj_sales_order_batch', $data );
        }

    }


}
