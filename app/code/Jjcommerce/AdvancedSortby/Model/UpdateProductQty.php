<?php
/**
 * Jjcommerce_AdvancedSortby
 *
 * @category  PHP
 * @package   Jjcommerce\AdvancedSortby
 * @author    Swapnil Tatkondawar <swapnilt@2jcommerce.in>
 * @copyright 2017 Copyright 2J Commerce, Inc. http://www.2jcommerce.in/
 * @license   http://www.2jcommerce.in/ Private
 * @link      http://www.jollyes.co.uk/
 */
namespace Jjcommerce\AdvancedSortby\Model;

use Magento\Framework\Model\AbstractModel;

class UpdateProductQty extends AbstractModel
{

    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ){
        parent::__construct($context,$registry,$resource,$resourceCollection,$data);
    }

    /**
     * Constructor
     *
     * @return void
     */

    protected function _construct()
    {
        parent::_construct();
        $this->_init('Jjcommerce\AdvancedSortby\Model\ResourceModel\UpdateProductQty');
    }


}
