<?php
namespace Jjcommerce\ClickAndCollect\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;

class SalesQuoteToOrder implements ObserverInterface
{
    /**
     * discount to order
     *
     * @param EventObserver $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {

        $quote = $observer->getQuote();
        $discountData = $quote->getJjDiscountLabel();
        
        //Set discount data to order
        $order = $observer->getOrder();
        $order->setData('jj_discount_label', $discountData);

        return $this;
    }

    public function log($message){
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/jjdiscount.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info($message);
    }
}
