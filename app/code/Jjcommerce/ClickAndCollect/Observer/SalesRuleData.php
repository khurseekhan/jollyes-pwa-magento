<?php
namespace Jjcommerce\ClickAndCollect\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;

class SalesRuleData implements ObserverInterface
{

    protected $discountDes;
    protected $description = array();
    protected $disData;
    /**
     * discount to order
     *
     * @param EventObserver $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {

        $quote = $observer->getEvent()->getQuote();
        $item = $observer->getEvent()->getItem();
        $rule = $observer->getEvent()->getRule();
        $result = $observer->getEvent()->getResult();
        $address = $observer->getEvent()->getAddress();


        $ruleLabel = $rule->getStoreLabel($address->getQuote()->getStore());
        $label = '';
        if(!isset($this->discountDes[$rule->getId()])){
            $this->discountDes[$rule->getId()]['amount'] = $result->getAmount();
        }else{
            $this->discountDes[$rule->getId()]['amount'] = $result->getAmount();
        }

        if ($ruleLabel) {
            $label = $ruleLabel;
        } else {
            if (strlen($address->getCouponCode())) {
                $label = $address->getCouponCode();
            }
        }

        if (strlen($label)) {
            $this->description[$rule->getId()] = $label.' - '.$this->discountDes[$rule->getId()]['amount'];
        }
        $this->disData = implode($this->description);
        $quote->setData('jj_discount_label',$this->disData);
        return $this;
    }

    public function log($message){
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/jjdiscount1.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info($message);
    }
}
