<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Jjcommerce\ClickAndCollect\Observer;

use Magento\Framework\Event\ObserverInterface;

class SalesOrderEmailSendBefore implements ObserverInterface
{
    protected $jhelper;

    public function __construct(
        \Magento\Framework\Json\Helper\Data $jhelper
    ) {

        $this->jhelper = $jhelper;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {

        $transport      = $observer->getEvent()->getTransport();
        if($transport->getData('order')->getStorelocatorId()>0) {

            $storelocatorDescription = $this->jhelper->jsonDecode($transport->getData('order')->getStorelocatorDescription());

            foreach ($storelocatorDescription as $key => $value) {
                $transport->setData('slocator_' . $key, $value);
            }
        }
    }

    public function log($message) {
        //\Zend\Log\Logger::INFO

        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/SalesOrderEmailSendBefore.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info($message);
    }


}
