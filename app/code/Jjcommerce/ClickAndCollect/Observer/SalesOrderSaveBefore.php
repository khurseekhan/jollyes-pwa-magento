<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Jjcommerce\ClickAndCollect\Observer;

use Magento\Framework\Event\ObserverInterface;

class SalesOrderSaveBefore implements ObserverInterface
{
    protected  $_onepage = null;
    protected $storeLocators = null;
    protected $_jhelper = null;

    public function __construct(
        \Magento\Checkout\Model\Type\Onepage  $onepage,
         \Magento\Framework\Json\Helper\Data $jhelper
    ) {

        $this->_onepage = $onepage;
        $this->_quote = $onepage->getQuote();
        $this->_jhelper = $jhelper;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {

       $order = $observer->getEvent()->getOrder();
      if($this->_quote && !$order->getStorelocatorId() ) {

          $locatorId = $this->_quote->getStorelocatorId();
          $order->setStorelocatorId($locatorId);

          $desc = $this->_quote->getStorelocatorDescription();
          $order->setStorelocatorDescription($desc);

          if($locatorId>0) {

              $descData = $this->_jhelper->jsonDecode($desc);

              $shippingDescription = 'Click and Collect';
              $type = 'Click and Collect';
              if ($descData['is_in_stock'] == 1) {
                  $shippingDescription .= ' - Express Delivery';
                  $type = 'express';
              } else {
                  $shippingDescription .= ' - Standard Delivery';
                  $type = 'standard';
              }

              // storelocator_msg
              $order->setStorelocatorMsg($descData['message']);

              // storelocator_name
              $order->setStorelocatorName($descData['name']);

              // storelocator_phone
              $order->setStorelocatorPhone($descData['phone']);

              // storelocator_type
              $order->setCcType($type);

              $pickupaddress = $descData['address'].','. $descData['address2'].','. $descData['city'].','.$descData['zipcode'].','.'United Kingdom';

              // storelocator_address
              $order->setStorelocatorAddress($pickupaddress);

              $shippingAddress = $this->_quote->getShippingAddress();
              $order->setShippingDescription($shippingDescription);
          }

          if($this->_quote){
              $discountData = $this->_quote->getJjDiscountLabel();
              $order->setJjDiscountLabel($discountData);
          }
      }
    }
    
    public function log($message) {
        //\Zend\Log\Logger::INFO

        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/salesordergrid.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info($message);
    }
}
