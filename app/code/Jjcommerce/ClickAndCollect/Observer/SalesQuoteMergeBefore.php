<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Jjcommerce\ClickAndCollect\Observer;

use Magento\Framework\Event\ObserverInterface;

class SalesQuoteMergeBefore implements ObserverInterface
{

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
       
        $sourceQuote = $observer->getEvent()->getSource();
        $quote = $observer->getEvent()->getQuote();

        $quote->setData( 'delivery_type',  $sourceQuote->getData( 'delivery_type' ) );
        $quote->setData( 'storelocator_id', $sourceQuote->getData( 'storelocator_id' ));
        $quote->setData( 'storelocator_description', $sourceQuote->getData( 'storelocator_description' ));
       // $quote->setData('jj_discount_label', $quote->getJjDiscountLabel());
    }

}
