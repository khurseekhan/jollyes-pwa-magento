<?php


namespace Jjcommerce\ClickAndCollect\Setup;


use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;

use Magento\Quote\Setup\QuoteSetupFactory;
use Magento\Sales\Setup\SalesSetupFactory;
use Magento\Eav\Setup\EavSetupFactory;


class UpgradeData implements UpgradeDataInterface
{

    const SCHEMA_ATTRIBUTE = 'delivery_type';
    const STOREPICKUPONLY  =  'Collect only';  // 'In Store only';


    private $eavSetupFactory;
    /**
     * Quote setup factory
     *
     * @var QuoteSetupFactory
     */
    protected $_quoteSetupFactory;

    /**
     * Sales setup factory
     *
     * @var SalesSetupFactory
     */
    protected $_salesSetupFactory;

    /**
     * Init
     *
     * @param EavSetupFactory $eavSetupFactory
     * @param QuoteSetupFactory $quoteSetupFactory
     * @param SalesSetupFactory $salesSetupFactory
     */
    public function __construct(
        QuoteSetupFactory $quoteSetupFactory,
        EavSetupFactory $eavSetupFactory,
        SalesSetupFactory $salesSetupFactory
    ) {

        $this->_quoteSetupFactory = $quoteSetupFactory;
        $this->_salesSetupFactory = $salesSetupFactory;
        $this->eavSetupFactory = $eavSetupFactory;
    }
    /**
     * Upgrades data for a module
     *
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface   $context
     *
     * @return void
     */
    public function upgrade(
        ModuleDataSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $quoteAddressTable = 'quote_address';
        $quoteTable = 'quote';
        $orderTable = 'sales_order';
        $invoiceTable = 'sales_invoice';
        $creditmemoTable = 'sales_creditmemo';

        if (version_compare($context->getVersion(), '2.1.0', '<')) {

            $textOptions = ['type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 'visible' => true, 'required' => false];
            /** @var \Magento\Quote\Setup\QuoteSetup $quoteSetup */
            $quoteSetup = $this->_quoteSetupFactory->create(['setup' => $setup]);
            $quoteSetup->addAttribute('quote', 'delivery_type', $textOptions);
        }

        if (version_compare($context->getVersion(), '2.1.1', '<')) {

            /*  $attributeLabel = 'Delivery Type';
              $attributeGroup = 'Default';

              $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
              $eavSetup->addAttribute(
                  \Magento\Catalog\Model\Product::ENTITY,
                  self::SCHEMA_ATTRIBUTE,
                  [
                      'type' => 'int',
                      'label' => $attributeLabel,
                      'input' => 'select',
                      'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                      'visible' => true,
                      'required' => false,
                      'user_defined' => false,
                      'default' => '',
                      'searchable' => false,
                      'filterable' => false,
                      'comparable' => false,
                      'visible_on_front' => false,
                      'used_in_product_listing' => true,
                      'unique' => false,
                      'option' => [ 'values' => ['In Store only','Both Store or Home', 'Home Delivery only'] ]
                  ]
              );

            $entityTypeId = $eavSetup->getEntityTypeId(\Magento\Catalog\Model\Product::ENTITY);

            $attributeSetId = $eavSetup->getDefaultAttributeSetId($entityTypeId);
            $eavSetup->addAttributeToSet($entityTypeId, $attributeSetId, $attributeGroup, self::SCHEMA_ATTRIBUTE);
            */
            //Setup two columns for quote, quote_address and order
            //Quote address tables
            $setup->getConnection()
                ->addColumn(
                    $setup->getTable($quoteAddressTable),
                    'jj_discount_amount',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                        '10,2',
                        'default' => 0.00,
                        'nullable' => true,
                        'comment' =>'Discount Amount'
                    ]
                );
            $setup->getConnection()
                ->addColumn(
                    $setup->getTable($quoteAddressTable),
                    'jj_discount_label',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'nullable' => true,
                        'comment' =>'Discount Label'
                    ]
                );
            //Quote tables
            $setup->getConnection()
                ->addColumn(
                    $setup->getTable($quoteTable),
                    'jj_discount_amount',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                        '10,2',
                        'default' => 0.00,
                        'nullable' => true,
                        'comment' =>'Discount Amount'
                    ]
                );

            $setup->getConnection()
                ->addColumn(
                    $setup->getTable($quoteTable),
                    'jj_discount_label',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'nullable' => true,
                        'comment' =>'Discount Label'
                    ]
                );
            //Order tables
            $setup->getConnection()
                ->addColumn(
                    $setup->getTable($orderTable),
                    'jj_discount_amount',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                        '10,2',
                        'default' => 0.00,
                        'nullable' => true,
                        'comment' =>'Discount Amount'
                    ]
                );

            $setup->getConnection()
                ->addColumn(
                    $setup->getTable($orderTable),
                    'jj_discount_label',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'nullable' => true,
                        'comment' =>'Discount Label'
                    ]
                );
            //Invoice tables
            $setup->getConnection()
                ->addColumn(
                    $setup->getTable($invoiceTable),
                    'jj_discount_amount',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                        '10,2',
                        'default' => 0.00,
                        'nullable' => true,
                        'comment' =>'Discount Amount'
                    ]
                );
            $setup->getConnection()
                ->addColumn(
                    $setup->getTable($invoiceTable),
                    'jj_discount_label',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'nullable' => true,
                        'comment' =>'Discount Label'
                    ]
                );
            //Credit memo tables
            $setup->getConnection()
                ->addColumn(
                    $setup->getTable($creditmemoTable),
                    'jj_discount_amount',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                        '10,2',
                        'default' => 0.00,
                        'nullable' => true,
                        'comment' =>'Discount Amount'
                    ]
                );
            $setup->getConnection()
                ->addColumn(
                    $setup->getTable($creditmemoTable),
                    'jj_discount_label',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'nullable' => true,
                        'comment' =>'Discount Label'
                    ]
                );
        }
        if (version_compare($context->getVersion(), '2.1.2', '<')) {

            $setup->getConnection()
                ->addColumn(
                    $setup->getTable($orderTable),
                    'storelocator_msg',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'nullable' => true,
                        'comment' =>'Storelocation message'
                    ]
                );
            $setup->getConnection()
                ->addColumn(
                    $setup->getTable($orderTable),
                    'storelocator_name',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'nullable' => true,
                        'comment' =>'Storelocation name'
                    ]
                );
            $setup->getConnection()
                ->addColumn(
                    $setup->getTable($orderTable),
                    'storelocator_phone',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'nullable' => true,
                        'comment' =>'Storelocation phone'
                    ]
                );
            $setup->getConnection()
                ->addColumn(
                    $setup->getTable($orderTable),
                    'storelocator_address',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'nullable' => true,
                        'comment' =>'Storelocation address'
                    ]
                );
            // =================================================================

            $setup->getConnection()
                ->addColumn(
                    $setup->getTable($invoiceTable),
                    'storelocator_msg',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'nullable' => true,
                        'comment' =>'Storelocation message'
                    ]
                );

            $setup->getConnection()
                ->addColumn(
                    $setup->getTable($invoiceTable),
                    'storelocator_name',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'nullable' => true,
                        'comment' =>'Storelocation name'
                    ]
                );
            $setup->getConnection()
                ->addColumn(
                    $setup->getTable($invoiceTable),
                    'storelocator_phone',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'nullable' => true,
                        'comment' =>'Storelocation phone'
                    ]
                );
            $setup->getConnection()
                ->addColumn(
                    $setup->getTable($invoiceTable),
                    'storelocator_address',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'nullable' => true,
                        'comment' =>'Storelocation address'
                    ]
                );
            // =================================================================

            $setup->getConnection()
                ->addColumn(
                    $setup->getTable($creditmemoTable),
                    'storelocator_msg',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'nullable' => true,
                        'comment' =>'Storelocation message'
                    ]
                );

            $setup->getConnection()
                ->addColumn(
                    $setup->getTable($creditmemoTable),
                    'storelocator_name',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'nullable' => true,
                        'comment' =>'Storelocation name'
                    ]
                );
            $setup->getConnection()
                ->addColumn(
                    $setup->getTable($creditmemoTable),
                    'storelocator_phone',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'nullable' => true,
                        'comment' =>'Storelocation phone'
                    ]
                );
            $setup->getConnection()
                ->addColumn(
                    $setup->getTable($creditmemoTable),
                    'storelocator_address',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'nullable' => true,
                        'comment' =>'Storelocation address'
                    ]
                );

        }

        if (version_compare($context->getVersion(), '2.1.3', '<')) {

            $setup->getConnection()
                ->addColumn(
                    $setup->getTable($orderTable),
                    'cc_type',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'nullable' => true,
                        'comment' =>'Click & Collect Type'
                    ]
                );


        }
    }


}
