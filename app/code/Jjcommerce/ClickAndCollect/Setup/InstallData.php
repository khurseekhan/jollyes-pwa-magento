<?php
/**
 * @category    Jjcommerce
 * @package     Jjcommerce_ClickAndCollect
 * @author	Jjcommerce Team<support@2jdesign.co.uk>
 */
namespace Jjcommerce\ClickAndCollect\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Quote\Setup\QuoteSetupFactory;
use Magento\Sales\Setup\SalesSetupFactory;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

/**
 * @codeCoverageIgnore
 */
class InstallData implements InstallDataInterface
{
     /**
     * Quote setup factory
     *
     * @var QuoteSetupFactory
     */
    protected $_quoteSetupFactory;

    /**
     * Sales setup factory
     *
     * @var SalesSetupFactory
     */
    protected $_salesSetupFactory;

    /**
     * Init
     *
     * @param EavSetupFactory $eavSetupFactory
     * @param QuoteSetupFactory $quoteSetupFactory
     * @param SalesSetupFactory $salesSetupFactory
     */
    public function __construct(
        QuoteSetupFactory $quoteSetupFactory,
        SalesSetupFactory $salesSetupFactory
    ) {

        $this->_quoteSetupFactory = $quoteSetupFactory;
        $this->_salesSetupFactory = $salesSetupFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $intOptions = ['type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, 'visible' => true, 'required' => false];
        $textOptions = ['type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 'visible' => true, 'required' => false];

        /** @var \Magento\Quote\Setup\QuoteSetup $quoteSetup */
        $quoteSetup = $this->_quoteSetupFactory->create(['setup' => $setup]);
        $quoteSetup->addAttribute('quote', 'storelocator_id', $intOptions);
        $quoteSetup->addAttribute('quote', 'storelocator_description', $textOptions);

        /** @var \Magento\Sales\Setup\SalesSetup $salesSetup */
        $salesSetup = $this->_salesSetupFactory->create(['setup' => $setup]);
        $salesSetup->addAttribute('order', 'storelocator_id', $intOptions);
        $salesSetup->addAttribute('order', 'storelocator_description', $textOptions);

        $connection = $setup->getConnection();
        $tableName = $setup->getTable('sales_order_grid');

        $connection->addColumn(  $tableName, "storelocator_id", [
            'TYPE' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            'LENGTH' => '12',
            'COMMENT' => 'store locator id'
        ] );
        $connection->addColumn(  $tableName, "storelocator_description", [
            'TYPE' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            'COMMENT' => 'click and collect description'
        ] );

    }
}
