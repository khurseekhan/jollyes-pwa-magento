var config = {

    map: {
        '*': {
            'Magento_Checkout/js/view/shipping':'Jjcommerce_ClickAndCollect/js/view/shipping'
        }
    },

    'config': {
        'mixins': {
            'Magento_Checkout/js/view/shipping': {
                'Jjcommerce_ClickAndCollect/js/view/shipping-payment-mixin': true
            },
            'Magento_Checkout/js/view/payment': {
                'Jjcommerce_ClickAndCollect/js/view/shipping-payment-mixin': true
            }
        }
    }
}