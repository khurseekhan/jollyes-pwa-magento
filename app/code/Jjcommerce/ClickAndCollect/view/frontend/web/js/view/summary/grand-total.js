/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
/*global define*/
define(
    [
        'Magento_Checkout/js/view/summary/abstract-total',
        'Magento_Checkout/js/model/totals',
        'Magento_Checkout/js/model/quote'
    ],
    function (Component, totals, quote) {
        "use strict";
        return Component.extend({
            defaults: {
                template: 'Jjcommerce_ClickAndCollect/summary/grand-total'
            },
            totals: quote.getTotals(),
            isDisplayed: function() {
                return true;
                //this.isFullMode();
            },
            getPureValue: function() {

              /*  var totals = quote.getTotals();
                var segment = window.checkoutConfig.totalsData.total_segments;
                for(var i =0;i < segment.length;i++){
                    if(segment[i]['code']=='grand_total'){
                        return segment[i].value ;
                    }
                }

                return quote.grand_total;*/
                var price = 0;
                if (this.totals()) {
                    price = totals.getSegment('grand_total').value;
                }
                return price;
            },
            getValue: function() {
                return this.getFormattedPrice(this.getPureValue());
            }
        });
    }
);
