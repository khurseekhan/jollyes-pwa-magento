/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
/*global define*/
define(
    [
        'Magento_Checkout/js/view/summary/abstract-total',
        'Magento_Checkout/js/model/totals',
        'Magento_Checkout/js/model/quote'
    ],
    function (Component, totals, quote) {
        "use strict";
        return Component.extend({
            defaults: {
               // template: 'Magento_SalesRule/summary/discount'
                template: 'Jjcommerce_ClickAndCollect/summary/discount'
            },
            totals: quote.getTotals(),
            isDisplayed: function() {
                return false;
               // return  this.getPureValue() != 0;


            },
            getCouponCode: function() {
                if (!this.totals()) {
                    return null;
                }
                return this.totals()['coupon_code'];
            },
            getPureValue: function() {
                var price = 0;
                if (this.totals() && this.totals().discount_amount) {
                    price = parseFloat(this.totals().discount_amount);
                }
                return price;
            },
            getValue: function() {
                return this.getFormattedPrice(this.getPureValue());
            },
            getDiscount: function(){
                if (!this.totals()) {
                    return null;
                }
                var titleArr = totals.getSegment('discount').title;
                var disTitleArray = [];
                var disAmountArray = [];

                var regExp = /\(([^)]+)\)/;
                var matches = regExp.exec(titleArr);
                var matchesCount = matches[1].split(",");

                for(var i=0;i< matchesCount.length;i++){
                    var disTitle = matchesCount[i].split(" - ");
                    disTitleArray[i] = disTitle;
                    if(disTitle.length==1){
                        disTitleArray[i].push(this.totals().discount_amount);
                    }

                }

               return disTitleArray;
            },
            getDiscountTitle: function(){
                //console.log(this.getDiscount());
                var discountArry = [];
                var discountArry = this.getDiscount();

                var discountTitle = [];
                for(var i=0; i < discountArry.length;i++){
                    if(discountArry.length==1){
                        discountTitle[i] = discountArry[i][0];
                    }else {
                        for (var j = 0; j < discountArry[i].length; j++) {
                            discountTitle[j] = discountArry[j][0];
                        }
                    }
                    return discountTitle;
                }
               // return discountTitle;
            },

            getDiscountAmount: function(){
                var discountArry = this.getDiscount();
                var discountAmount = [];
                for(var i=0;i< discountArry.length;i++){
                    if(discountArry.length==1){
                        discountAmount[i] = this.getFormattedPrice(parseFloat(discountArry[i][1]));
                        return discountAmount;
                    }else {
                        for (var j = 0; j < discountArry[i].length; j++) {
                            discountAmount[j] = this.getFormattedPrice(parseFloat(discountArry[j][1]));
                        }
                        return discountAmount;
                    }

                    //return discountAmount = this.getFormattedPrice(parseFloat(discountArry[i][1]));
                }

               // return false;
            }
        });
    }
);
