define(
    [
        'jquery',
        'ko',
        'uiComponent',
        'underscore',
        'Magento_Customer/js/model/customer',
        'Magento_Checkout/js/model/step-navigator',
        'mage/storage'
    ],
    function (
        $,
        ko,
        Component,
        _,
        customer,
        stepNavigator,
        storage
    ) {
        'use strict';

        return Component.extend({
            defaults: {
                template: 'Jjcommerce_ClickAndCollect/deliverystep'
            },

            //add here your logic to display step,
            isVisible: ko.observable(window.store_only_product==0),

            /**
             *
             * @returns {*}
             */
            initialize: function () {
                this._super();
                if(window.store_only_product>0) {
                    this.updateDeliverytype();
                }
                //if(!window.isCustomerLoggedIn){
                //this.isVisible(true);
                // register your step
                if(window.store_only_product==0) {
                    stepNavigator.registerStep(
                        //step code will be used as step content id in the component template
                        'delivery',
                        //step alias
                        null,
                        //step title value
                        'Delivery Method',
                        //observable property with logic when display step or hide step
                        this.isVisible,
                        _.bind(this.navigate, this),

                        /**
                         * sort order value
                         * 'sort order value' < 10: step displays before shipping step;
                         * 10 < 'sort order value' < 20 : step displays between shipping and payment step
                         * 'sort order value' > 20 : step displays after payment step
                         */
                        5
                    );
                }
                //}

                return this;
            },

            collectBystore: function () {
                return (window.currentDelivery == 'collectByStore') ?true:false;
            },

            homeDelivery: function () {
                return (window.currentDelivery == 'home_delivery') ?true:false;
            },

            selectDeliveryByHome: function (deliveryby) {
                window.currentDelivery = 'home_delivery';
                $("body").addClass('home_delivery').removeClass('collectByStore');
                this.navigateToNextStep();
                return true;
            },
            selectDeliveryByStore: function (deliveryby) {
                window.currentDelivery = 'collectByStore';
                $("body").addClass('collectByStore').removeClass('home_delivery');
                this.navigateToNextStep();
                return true;
            },


            /**
             * The navigate() method is responsible for navigation between checkout step
             * during checkout. You can add custom logic, for example some conditions
             * for switching to your custom step
             */
            navigate: function () {
                var self = this;
            },

            updateDeliverytype: function(){

                var data = {'current_delivery':window.currentDelivery};
                return storage.post(
                    'clickncollect/checkout/collect/current_delivery/'+window.currentDelivery,
                    JSON.stringify(data),
                    false
                ).done(function (response) {
                    if (response.error) {
                        alert(response.error);
                    }

                }).fail(function () {

                });

            },

            /**
             * @returns void
             */
            navigateToNextStep: function () {

                if(window.currentDelivery==""){
                   alert('Delivery type is not selected' );
                   return '';
                }

                if(window.currentDelivery=="home_delivery"){
                    $("#checkout-shipping-method-load .clickShipping").hide();
                    $("#checkout-shipping-method-load .homeShipping").show();
                    $('.homeShipping .radio' ).eq(0).attr('checked','checked').click();
                    $("#shipping .step-title").text('Shipping Address');
                }else{
                    $("#checkout-shipping-method-load .homeShipping").hide();
                    $("#checkout-shipping-method-load .clickShipping").show();
                    $("#s_method_clickandcollect").attr("checked","checked").click();
                    $("#shipping .step-title").text('Billing Address');
                }
                //window.location.href = window.location+'#shipping='+ window.currentDelivery;
                this.updateDeliverytype();
                stepNavigator.next();
            },

            /**
             * @return {Boolean}
             */
            isAvailable: function () {
                return window.isExtensionEnabled
            }
        });
    }
);