define([
    'ko',
    'Magento_Checkout/js/view/summary/abstract-total',
    'Magento_Checkout/js/model/quote',
    'Magento_Catalog/js/price-utils',
    'Magento_Checkout/js/model/totals'
], function (ko, Component, quote, priceUtils, totals) {
    'use strict';

    return Component.extend({
        defaults: {
            isFullTaxSummaryDisplayed: window.checkoutConfig.isFullTaxSummaryDisplayed || false,
            template: 'Jjcommerce_ClickAndCollect/summary/jjdiscount'
        },
        totals: quote.getTotals(),
        discountData: window.checkoutConfig.jj_discount_label,
        isMultipleDiscount: false,

        getDiscountFullData: function() {
            if (!this.totals()) {
                return null;
            }
            var titleArr = totals.getSegment('jj_discount').title;
            var disTitleArray = [];
            var disAmountArray = [];

            if (titleArr.length > 0) {
                titleArr = titleArr;
            } else {
                titleArr = this.discountData;
            }

            if (titleArr.indexOf(',') != -1) {
                var matchesCount = titleArr.split(",");
                this.isMultipleDiscount = true;
            } else {
                var matchesCount = titleArr;
                this.isMultipleDiscount = false;
            }

            if (this.isMultipleDiscount) {
                for(var i=0;i< matchesCount.length;i++){
                    var disTitle = matchesCount[i].split(" - ");
                    disTitleArray[i] = disTitle;
                }
            }else{
                var disTitle = matchesCount.split(" - ");
                disTitleArray = disTitle;
            }
            return disTitleArray;
        },

        getDiscountLabels: function(){
            var discountArry = [];
            var discountArry = this.getDiscountFullData();
            var discountTitle = [];
            if(this.isMultipleDiscount){
                for(var i=0; i<discountArry.length; i++){
                    discountTitle[i] = discountArry[i][0];
                }
                return discountTitle;
            }else{
                discountTitle[0] = discountArry[0];
                return discountTitle;
            }

        },

        getDiscountPrices: function(){
            var discountArry = [];
            var discountArry = this.getDiscountFullData();
            var discountAmount = [];
            if(this.isMultipleDiscount) {
                for (var i = 0; i<discountArry.length; i++) {
                    discountAmount[i] = "-"+this.getFormattedPrice(parseFloat(discountArry[i][1]));
                }
                return discountAmount;

            }else{
                discountAmount[0] = "-"+this.getFormattedPrice(parseFloat(discountArry[1]));
                return discountAmount;
            }
            // return false;
        },

        isDisplayed: function () {
            // return true;
            return this.getValue() != 0;
        },

        getValue: function() {
            var price = 0;
            if (this.totals() && this.getDiscountPrice()) {
                // price = totals.getSegment('jj_discount').value;
                var discount = this.discountData;
                if (discount.length > 0) {
                    price = discount.split(" - ");
                    price = price[1];
                }else {
                    price = 0;
                }
            }
            return price;
        }
    });
});