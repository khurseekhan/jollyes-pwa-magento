/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
/*browser:true*/
/*global define*/
define([
    'jquery',
    'uiComponent',
    'ko',
    'Magento_Checkout/js/model/step-navigator',
    'Magento_Customer/js/model/customer',
    'Magento_Customer/js/action/check-email-availability',
    'Magento_Customer/js/action/login',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/checkout-data',
    'Magento_Checkout/js/model/full-screen-loader',
    'mage/validation',
	'mage/storage',
    'Magento_Checkout/js/model/url-builder'
], function ($, Component, ko,stepNavigator, customer, checkEmailAvailability, loginAction, quote, checkoutData, fullScreenLoader, validation, storage, urlBuilder) {
    'use strict';

    var validatedEmail = checkoutData.getValidatedEmailValue();

    if (validatedEmail && !customer.isLoggedIn()) {
        quote.guestEmail = validatedEmail;
    }

    return Component.extend({
        defaults: {
            template: 'Jjcommerce_AdvancedCheckout/form/element/email',
            email: checkoutData.getInputFieldEmailValue(),
            emailFocused: false,
            isLoading: false,
            isPasswordVisible: false,
			isTradeButtonVisible: false,
            isLoginVisible: true,
            listens: {
                email: 'emailHasChanged',
                emailFocused: 'validateEmail'
            }
        },
        checkDelay: 2000,
        checkRequest: null,
        isEmailCheckComplete: null,
		isTradeEmailCheckComplete: null,
        isCustomerLoggedIn: customer.isLoggedIn,
        forgotPasswordUrl: window.checkoutConfig.forgotPasswordUrl,
        emailCheckTimeout: 0,

        /**
         * Initializes observable properties of instance
         *
         * @returns {Object} Chainable.
         */
        initObservable: function () {
            this._super()
                .observe(['email', 'emailFocused', 'isLoading', 'isPasswordVisible', 'isTradeButtonVisible' , 'isLoginVisible']);

            return this;
        },

        /**
         * Callback on changing email property
         */
        emailHasChanged: function () {
            var self = this;

            clearTimeout(this.emailCheckTimeout);

            if (self.validateEmail()) {
                quote.guestEmail = self.email();
                checkoutData.setValidatedEmailValue(self.email());
            }
            this.emailCheckTimeout = setTimeout(function () {
                if (self.validateEmail()) {
                    $("#guest-button button").removeAttr("disabled");
                    self.checkEmailAvailability();
                } else {
                    $("#guest-button").removeClass("guest-text next-but");
                    $("#guest-button button > span").text("Next");
                    $("#guest-button button").attr("disabled","disabled");
                    self.isPasswordVisible(false);
					self.isTradeButtonVisible(false);
                }
            }, self.checkDelay);

            checkoutData.setInputFieldEmailValue(self.email());
        },

        /**
         * Check email existing.
         */
        checkEmailAvailability: function () {
            var self = this;
            this.validateRequest();
            this.isEmailCheckComplete = $.Deferred();
            this.isLoading(true);
            this.checkRequest = checkEmailAvailability(this.isEmailCheckComplete, this.email());

            $.when(this.isEmailCheckComplete).done(function () {
                $("#guest-button").removeClass("guest-text next-but");
                $("#guest-button button > span").text("Next");
                self.isPasswordVisible(false);
                self.isLoginVisible(true);
                self.tradeEmailCheck();
            }).fail(function () {
                $("#guest-button").addClass("guest-text");
                $("#guest-button button > span").text("Checkout as a Guest");
                self.isPasswordVisible(true);
                self.isLoginVisible(false);
                self.tradeEmailCheck();
            }).always(function () {
                self.isLoading(false);
            });
        },

        /**
         * If request has been sent -> abort it.
         * ReadyStates for request aborting:
         * 1 - The request has been set up
         * 2 - The request has been sent
         * 3 - The request is in process
         */
        validateRequest: function () {
            if (this.checkRequest != null && $.inArray(this.checkRequest.readyState, [1, 2, 3])) {
                this.checkRequest.abort();
                this.checkRequest = null;
            }
        },

        /**
         * Local email validation.
         *
         * @param {Boolean} focused - input focus.
         * @returns {Boolean} - validation result.
         */
        validateEmail: function (focused) {
            var loginFormSelector = 'form[data-role=email-with-possible-login]',
                usernameSelector = loginFormSelector + ' input[name=username]',
                loginForm = $(loginFormSelector),
                validator;

            loginForm.validation();

            if (focused === false && !!this.email()) {
                return !!$(usernameSelector).valid();
            }

            validator = loginForm.validate();

            return validator.check(usernameSelector);
        },

        /**
         * Log in form submitting callback.
         *
         * @param {HTMLElement} loginForm - form element.
         */
        login: function (loginForm) {
            var loginData = {},
                formDataArray = $(loginForm).serializeArray();

            formDataArray.forEach(function (entry) {
                loginData[entry.name] = entry.value;
            });

            if (this.isPasswordVisible() && $(loginForm).validation() && $(loginForm).validation('isValid')) {
                fullScreenLoader.startLoader();
                loginAction(loginData).always(function() {
                    fullScreenLoader.stopLoader();
                });
            }
        },

        /**
         * @returns void
         */
        navigateToNextStep: function () {
            if (this.validateEmailInformation()){
            stepNavigator.next();
            }
        },

        /**
         * @return {Boolean}
         */
        validateEmailInformation: function () {
            var loginFormSelector = 'form[data-role=email-with-possible-login]',
                loginFormValidation = $(loginFormSelector + ' input[name=username]');

            if (!customer.isLoggedIn()) {
                $(loginFormSelector).validation();
            }

            if ($(loginFormValidation).validation() && !$(loginFormValidation).validation('isValid')) {
                return false;
            }

            return true;
        },
        tradeEmailCheck: function(){
				var self = this;
                var isTradeEmailCheckComplete = $.Deferred();
                storage.post(
                urlBuilder.createUrl('/customers/isEmailAvailable', {}),
                JSON.stringify({
                    customerEmail:  this.email(),
                    websiteId : window.tradeStoreId
                }),
                false
            ).done(
                function (isTradeEmailAvailable) {
                    if (isTradeEmailAvailable) {
                        isTradeEmailCheckComplete.resolve();
                    } else {
                        isTradeEmailCheckComplete.reject();
                    }
                }
            ).fail(
                function () {
                    $.Deferred().reject();
                }
            );


             $.when(isTradeEmailCheckComplete).done(function () {
                self.isTradeButtonVisible(false);
				$("#guest-button").removeClass("next-but");
            }).fail(function () {
                self.isTradeButtonVisible(true);
				$("#guest-button").addClass("next-but");
            }).always(function () {
               self.isLoading(false);
            });
        }
    });
});
