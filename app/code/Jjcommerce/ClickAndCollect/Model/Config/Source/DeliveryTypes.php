<?php

namespace Jjcommerce\ClickAndCollect\Model\Config\Source;


use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;
use Magento\Framework\DB\Ddl\Table;

class DeliveryTypes extends AbstractSource
{
    protected $_options;

    /*
    public function __construct(BrandsRepositoryInterface $brandsRepository)
    {
        $this->brandRepository = $brandsRepository;
    }*/

    public function getAllOptions()
    {
            $this->_options  =array(
                array( 'label' => 'Store Pickup',  'value' => 'Store Pickup' ),
                array( 'label' => 'Home Delivery',  'value' => 'Home Delivery' )
            );

        return $this->_options;
    }

    public function getOptionText($value)
    {
        foreach ($this->getAllOptions() as $option) {
            if ($option['value'] == $value) {
                return $option['label'];
            }
        }
        return false;
    }

    public function getFlatColumns()
    {
        $attributeCode = $this->getAttribute()->getAttributeCode();
        return [
            $attributeCode => [
                'unsigned' => false,
                'default' => null,
                'extra' => null,
                'type' => Table::TYPE_TEXT,
                'nullable' => true,
                'comment' => 'Product ' . $attributeCode . ' column',
            ],
        ];
    }

}