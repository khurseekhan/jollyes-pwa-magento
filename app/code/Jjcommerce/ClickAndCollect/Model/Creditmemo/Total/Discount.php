<?php

namespace Jjcommerce\ClickAndCollect\Model\Creditmemo\Total;

use Magento\Sales\Model\Order\Creditmemo\Total\AbstractTotal;

class Discount extends AbstractTotal
{
    /**
     * @param \Magento\Sales\Model\Order\Creditmemo $creditmemo
     * @return $this
     */
    public function collect(\Magento\Sales\Model\Order\Creditmemo $creditmemo)
    {
        $creditmemo->getJjDiscountAmount(0);
        $creditmemo->getJjDiscountLabel('');

        $amount = $creditmemo->getOrder()->getJjDiscountAmount();
        $creditmemo->setJjDiscountAmount($amount);
        $label = $creditmemo->getOrder()->getJjDiscountLabel();
        $creditmemo->setJjDiscountLabel($amount);

        return $this;
    }
}
