<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Jjcommerce\ClickAndCollect\Model\Quote\Total;

use Magento\Store\Model\ScopeInterface;

class Discount extends \Magento\Quote\Model\Quote\Address\Total\AbstractTotal
{

    /**
     * Collect grand total address amount
     *
     * @param \Magento\Quote\Model\Quote $quote
     * @param \Magento\Quote\Api\Data\ShippingAssignmentInterface $shippingAssignment
     * @param \Magento\Quote\Model\Quote\Address\Total $total
     * @return $this
     */
    protected $quoteValidator = null;
    protected $rule;
    protected $discountData;

    protected $_ruleDiscountAmount = [];

    protected $_ruleDiscountLabel = [];
    protected $rulesDisStr;
    protected $jjRuleDiscount = [];
    protected $ismultiDis = false;

    public function __construct(
        \Magento\Quote\Model\QuoteValidator $quoteValidator,
        \Magento\SalesRule\Model\Rule $rule,
        \Magento\SalesRule\Model\Rule\Action\Discount\Data $discountData
    )
    {
        $this->quoteValidator = $quoteValidator;
        $this->rule = $rule;
        $this->discountData = $discountData;
    }

    public function collect(
        \Magento\Quote\Model\Quote $quote,
        \Magento\Quote\Api\Data\ShippingAssignmentInterface $shippingAssignment,
        \Magento\Quote\Model\Quote\Address\Total $total
    )
    {
        parent::collect($quote, $shippingAssignment, $total);
        if (!count($shippingAssignment->getItems())) {
            return $this;
        }

        $discount = $this->discountData;
        $rule = $this->rule;


        $ruleData = $quote->getShippingAddress()->getDiscountDescription();

        if(!empty($ruleData)){
                $this->rulesDisStr = $ruleData;
        }else{
            $this->rulesDisStr = '';
        }
       // print_R($ruleData);
        $discount = $quote->getData('jj_discount_label');

      //  $quote->setData('jj_discount_label',$this->rulesDisStr);
      //  $total->setData('jj_discount_label',$this->rulesDisStr);
        if($quote && empty($discount)) {
           // $quote->setData('jj_discount_label',$this->rulesDisStr);
          //  $total->setData('jj_discount_label',$this->rulesDisStr);
        }else if($quote && !empty($discount)){
            if(strlen($this->rulesDisStr) > strlen($discount)) {
              //  $quote->setData('jj_discount_label', $this->rulesDisStr);
               // $total->setData('jj_discount_label', $this->rulesDisStr);
            }else{
                $isDiscount = $this->isDiscountQuote($this->rulesDisStr, $quote->getData('jj_discount_label'));
                if($isDiscount){
                   // $quote->setData('jj_discount_label', $this->rulesDisStr);
                   // $total->setData('jj_discount_label', $this->rulesDisStr);
                }else{
                   // $quote->setData('jj_discount_label', $quote->getData('jj_discount_label'));
                   // $total->setData('jj_discount_label', $total->getData('jj_discount_label'));
                }

            }
        }

        return $this;
    }

    protected function isDiscountQuote($defaultDiscount, $quoteDiscount){
        $discountSting = strcmp($defaultDiscount,$quoteDiscount);
        if($discountSting > 0){
            return true;
        }else{
            return false;
        }
    }

    /**
     * @param \Magento\Quote\Model\Quote $quote
     * @param \Magento\Quote\Model\Quote\Address\Total $total
     * @return array
     */
    public function fetch(\Magento\Quote\Model\Quote $quote, \Magento\Quote\Model\Quote\Address\Total $total)
    {

        $discountLabel = $quote->getJjDiscountLabel();
       $result = [];
            $result = [
                'code' => 'jj_discount',
                'title' => $discountLabel,
                'value' => 0
            ];

        return $result;
    }

    /**
     * label
     *
     * @return \Magento\Framework\Phrase
     */
    public function getLabel()
    {
        return __('jj_discount');
    }

    /**
     * @param \Magento\Quote\Model\Quote\Address\Total $total
     */
    protected function clearValues(\Magento\Quote\Model\Quote\Address\Total $total)
    {
        $total->setTotalAmount('subtotal', 0);
        $total->setBaseTotalAmount('subtotal', 0);
        $total->setTotalAmount('tax', 0);
        $total->setBaseTotalAmount('tax', 0);
        $total->setTotalAmount('discount_tax_compensation', 0);
        $total->setBaseTotalAmount('discount_tax_compensation', 0);
        $total->setTotalAmount('shipping_discount_tax_compensation', 0);
        $total->setBaseTotalAmount('shipping_discount_tax_compensation', 0);
        $total->setSubtotalInclTax(0);
        $total->setBaseSubtotalInclTax(0);

    }

    public function log($message){
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/jjdiscountdata.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info($message);
    }
}
