<?php
namespace Jjcommerce\ClickAndCollect\Model;

use Magento\Checkout\Model\ConfigProviderInterface;

class DiscountDataConfigProvider implements ConfigProviderInterface
{
    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    protected $rulesDisStr;

    /**
     * @var \Magento\Quote\Model\QuoteRepository
     */
     protected $quoteRepository;

    /**
     * @var \Magento\Quote\Model\QuoteFactory
     */
     protected $_quoteFactory;

    /**
     * DiscountDataConfigProvider constructor.
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Quote\Model\QuoteRepository $quoteRepository
     * @param \Magento\Quote\Model\QuoteFactory $quoteFactory
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Quote\Model\QuoteRepository $quoteRepository,
        \Magento\Quote\Model\QuoteFactory $quoteFactory,
        \Psr\Log\LoggerInterface $logger

    )
    {
        $this->checkoutSession = $checkoutSession;
        $this->quoteRepository = $quoteRepository;
        $this->_quoteFactory = $quoteFactory;
        $this->logger = $logger;
    }

    /**
     * @return array
     */
    public function getConfig()
    {
        try{
            $customConfig = [];
            $quoteId = $this->checkoutSession->getQuote()->getId();

            $quote = $this->_quoteFactory->create()->load($quoteId);

            $ruleData = $quote->getShippingAddress()->getDiscountDescription();
            if(!empty($ruleData)) {
                $this->rulesDisStr = $ruleData;
            }else{
                $this->rulesDisStr = '';
            }

            $customConfig['jj_discount_label'] = $this->rulesDisStr ;
            $discount = $quote->getData('jj_discount_label');
            // $quote->setData('jj_discount_label',$this->rulesDisStr);

            if($quote && (empty($discount))) {
                $quote->setData('jj_discount_label',$this->rulesDisStr);
            }else if($quote && !empty($discount)){
                if(strlen($this->rulesDisStr) > strlen($discount)){
                    $quote->setData('jj_discount_label',$this->rulesDisStr);
                }else{
                    $isDiscount = $this->isDiscountQuote($this->rulesDisStr, $quote->getData('jj_discount_label'));
                    if($isDiscount){
                        $quote->setData('jj_discount_label', $this->rulesDisStr);
                    }else{
                        $quote->setData('jj_discount_label', $quote->getData('jj_discount_label'));
                    }
                }
            }

            $quote->save($quote);

        }catch(Exception $e){
            throw new Zend_Measure_Exception($e->getMessage(), $e->getCode(), $e);
        }

        return $customConfig;
    }

    protected function isDiscountQuote($defaultDiscount, $quoteDiscount){
        $discountSting = strcmp($defaultDiscount,$quoteDiscount);
        if($discountSting > 0){
            return true;
        }else{
            return false;
        }
    }

}
