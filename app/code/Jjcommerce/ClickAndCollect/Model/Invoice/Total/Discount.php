<?php

namespace Jjcommerce\ClickAndCollect\Model\Invoice\Total;

use Magento\Sales\Model\Order\Invoice\Total\AbstractTotal;

class Discount extends AbstractTotal
{
    /**
     * @param \Magento\Sales\Model\Order\Invoice $invoice
     * @return $this
     */
    public function collect(\Magento\Sales\Model\Order\Invoice $invoice)
    {
        $invoice->getJjDiscountAmount(0);
        $invoice->getJjDiscountLabel('');

        $amount = $invoice->getOrder()->getJjDiscountAmount();
        $invoice->setJjDiscountAmount($amount);
        $label = $invoice->getOrder()->getJjDiscountLabel();
        $invoice->setJjDiscountLabel($amount);

        return $this;
    }
}
