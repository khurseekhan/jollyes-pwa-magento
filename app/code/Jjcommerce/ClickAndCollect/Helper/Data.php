<?php
namespace Jjcommerce\ClickAndCollect\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

class Data extends AbstractHelper
{
    const XML_PATH_CARRIER_CLICK_AND_COLLECT_ENABLE = 'clickandcollect/general/active';

    const XML_PATH_STANDARD_DELIVERY_ENABLE = 'clickandcollect/general/active_standard_delivery';

    /**
     * Data constructor.
     * @param \Magento\Framework\App\Helper\Context $context
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context
    ) {
        parent::__construct($context);
    }

    /**
     * Check if enabled
     *
     * @return string|null
     */
    public function isStandardCollectOptionEnabled()
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_STANDARD_DELIVERY_ENABLE,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

     /**
     * Check if enabled
     *
     * @return string|null
     */
    public function isExtensionEnabled()
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_CARRIER_CLICK_AND_COLLECT_ENABLE,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
}
