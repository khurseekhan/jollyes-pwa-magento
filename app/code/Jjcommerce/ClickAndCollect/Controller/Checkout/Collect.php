<?php

/**
 * @category    Jjcommerce
 * @package     Jjcommerce_ClickAndCollect
 * @author    Jjcommerce Team<support@2jdesign.co.uk>
 */

namespace Jjcommerce\ClickAndCollect\Controller\Checkout;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;


/**
 * Click and collect store information
 *
 * @SuppressWarnings(PHPMD.LongVariable)
 */
class Collect extends Action
{


    protected $jhelper;

    protected $_onepagecheckout;

    /**
     * @var \Magento\Quote\Api\CartRepositoryInterface
     */
    protected $quoteRepository;

    protected $storeLocators = null;

    /**
     * Constructor
     *
     * @param Context $context
     * @param Config $config
     * @param Session $checkoutSession
     */
    public function __construct(
        Context $context,
        \Magento\Quote\Api\CartRepositoryInterface $quoteRepository,
        \Jjcommerce\StoreLocator\Model\Storelocator $storelocator,
        \Magento\Checkout\Model\Type\Onepage $onepage,
        \Magento\Framework\Json\Helper\Data $jhelper
    )
    {
        $this->storeLocators = $storelocator;
        $this->jhelper = $jhelper;
        $this->_onepagecheckout = $onepage;
        $this->quoteRepository = $quoteRepository;
        parent::__construct($context);
    }

    public function execute()
    {
        $quote = $this->_onepagecheckout->getQuote();
        $storeId = $this->getRequest()->getParam('store_id');
        $localTime = $this->getRequest()->getParam('local_time');

        $currentDelivery = $this->getRequest()->getParam('current_delivery');

        $country = 'FR'; // Some country code
        $postcode = '75000'; // Some postcode
        $regionId = '0'; // Some region id
        if ($currentDelivery) {
            //  $this->log( __line__);
            $quote->setData('delivery_type', $currentDelivery);
            
            // Remove storelocator and description when select home_delivery
            if($currentDelivery == 'home_delivery'){
                $quote->setData('storelocator_description','');
                $quote->setData('storelocator_id',NULL);
            }

            /****start of collecting rates and Set Shipping & Payment Method****/
            $method = 'clickandcollect'; // Used shipping method
            $shippingAddress = $quote->getShippingAddress();
            $shippingAddress->setCountryId($country)
                ->setRegionId($regionId)
                ->setPostcode($postcode)
                ->setTelephone('9999999999')
                ->setShippingMethod($method)
                ->setCollectShippingRates(true);
            $shippingAddress->save();
            /****end of collecting rates and Set Shipping & Payment Method****/

            $this->quoteRepository->save($quote);

            $responceData = ['status' => 'success'];

            $this->getResponse()->representJson($this->jhelper->jsonEncode($responceData));
            return;
        }

        /****start of setting temporary billing address to make a click n collect payment****/
        $billingAddress = $quote->getBillingAddress();
        $billingAddress->setCountryId($country)
            ->setRegionId($regionId)
            ->setPostcode($postcode)
            ->setTelephone('9999999999');
        $billingAddress->save();
        /****end of setting temporary billing address to make a click n collect payment**/

        //  $this->log( __line__);
        if (!$storeId) {
            $responceData = ['error' => 'Store not selected'];

            $this->getResponse()->representJson($this->jhelper->jsonEncode($responceData));
            return;
        }

        $store = $this->storeLocators->getCollection()
            ->addFieldToSelect(array('name', 'address', 'address2', 'zipcode', 'city', 'phone', 'store_url'))
            ->addFieldToFilter('shop_id', $storeId)
            ->addFieldToFilter('status', 1)->getFirstItem();
        $storekey = str_replace(' ', '_', strtolower($store['name']));

        $flag = 1;
        $items = $quote->getAllVisibleItems();
        $itemStock = array();
        foreach ($items as $item) {

            if ($item->getHasChildren()) {
                foreach ($item->getChildren() as $child) {
                    $itemstock = $this->storeLocators->getStockByProductId($child->getProduct());
                    if ($itemstock) {
                        foreach ($itemstock as $stock) {
                            if ($stock['shop_id'] == $storeId && $stock['stock'] < $item->getQty()) {
                                $flag = null;
                                break;
                            }
                        }
                    } else {
                        $flag = null;
                        break;
                    }
                }
            } else {

                $itemstock = $this->storeLocators->getStockByProductId($item->getProduct());
                if ($itemstock) {
                    foreach ($itemstock as $stock) {
                        if ($stock['shop_id'] == $storeId && $stock['stock'] < $item->getQty()) {
                            $flag = null;
                            break;
                        }
                    }
                } else {
                    $flag = null;
                    break;
                }
            }
        }
        $message = $this->getMessage($flag);


        $description = $this->jhelper->jsonEncode(
            array('store_id' => $storeId,
                'name' => $store['name'],
                'address' => $store['address'],
                'address2' => $store['address2'],
                'zipcode' => $store['zipcode'],
                'city' => $store['city'],
                'phone' => $store['phone'],
                'store_url' => $store['store_url'],
                'local_time' => $localTime,
                'is_in_stock' => $flag,
                'message' => $message
            ));

        $quote->setData('storelocator_id', $storeId);
        $quote->setData('storelocator_description', $description);
        $this->quoteRepository->save($quote);

        $responceData = [];

        $this->getResponse()->representJson($this->jhelper->jsonEncode($responceData));
        return;
    }

    public function getMessage($stockStatus)
    {

        if (!$stockStatus) return 'Your order will be ready for collection in 7 days';

        if ($stockStatus) {
            $hour = date('H');
            $minutes = date('i');


            if (($hour + 3) > 17) return 'Your order will be ready for collection at 9am tomorrow';
            if ($hour < 6) return 'Your order will be ready for collection at 9am';

            $time = (($hour + 3) > 12) ? (($hour + 3) - 12) . ':' . $minutes . 'pm' : ($hour + 3) . ':' . $minutes . 'am';
            return 'Your order will be ready for collection at ' . $time;
        }
    }

    public function log($message)
    {
        //\Zend\Log\Logger::INFO

        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/saveStore.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info($message);
    }

}
