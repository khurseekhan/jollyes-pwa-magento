<?php
/**
 * @category    Jjcommerce
 * @package     Jjcommerce_ClickAndCollect
 * @author	Jjcommerce Team<support@2jdesign.co.uk>
 */


namespace Jjcommerce\ClickAndCollect\Block\Onepage;

use Magento\Checkout\Block\Checkout\LayoutProcessorInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;

class LayoutProcessor implements LayoutProcessorInterface
{


    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;
    /**
     * @var PriceCurrencyInterface
     */
    protected $priceCurrency;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;



    public function __construct(
        ScopeConfigInterface $scopeConfig,
        PriceCurrencyInterface $priceCurrency,
        \Magento\Checkout\Model\Session $checkoutSession,
        StoreManagerInterface $storeManager
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->priceCurrency = $priceCurrency;
        $this->checkoutSession = $checkoutSession;
        $this->storeManager = $storeManager;

    }

    public function process($jsLayout)
    {

        //if ($this->scopeConfig->isSetFlag('jjcommerce_checkout/general/enabled', ScopeInterface::SCOPE_STORE))

        //  if ($this->checkoutSession->getQuote()->isVirtual()) {

        $shippingAddress = &$jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress'];
        $shippingAddress['component'] = 'Jjcommerce_ClickAndCollect/js/view/shipping';

        $summary = &$jsLayout['components']['checkout']['children']['sidebar']['children']['summary']['children'];
        $summary['totals']['component'] = 'Jjcommerce_ClickAndCollect/js/view/summary/totals';

        $summary['totals']['component'] = 'Jjcommerce_ClickAndCollect/js/view/summary/totals';
        $summary['totals']['children']['shipping']['component'] = 'Jjcommerce_ClickAndCollect/js/view/summary/shipping';
        $summary['totals']['children']['grand-total']['component'] = 'Jjcommerce_ClickAndCollect/js/view/summary/grand-total';
        //$summary['totals']['children']['grand-total']['component'] = 'Magento_Tax/js/view/checkout/summary/grand-total';
       $summary['totals']['children']['tax']['component'] = 'Jjcommerce_ClickAndCollect/js/view/summary/tax';
       $summary['totals']['children']['discount']['component'] = 'Jjcommerce_ClickAndCollect/js/view/summary/discount';
    //   $summary['totals']['children']['customerbalance']['component'] = 'Jjcommerce_ClickAndCollect/js/view/summary/customer-balance';
       // $summary['totals']['children']['jj_discount']['component'] = 'Jjcommerce_ClickAndCollect/js/view/summary/jjdiscount';


        //  $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['component']
        // = 'Jjcommerce_ClickAndCollect/js/view/payment';


        return $jsLayout;
    }

}
