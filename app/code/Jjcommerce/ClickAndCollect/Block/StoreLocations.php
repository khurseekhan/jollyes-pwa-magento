<?php
/**
 * @category    Jjcommerce
 * @package     Jjcommerce_ClickAndCollect
 * @author	Jjcommerce Team<support@2jdesign.co.uk>
 */
          
namespace Jjcommerce\ClickAndCollect\Block;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Jjcommerce\ClickAndCollect\Helper\Data;
use Jjcommerce\ProductSticker\Helper\Data as StickerHelper;

class StoreLocations extends \Magento\Framework\View\Element\Template
{

    protected $storeLocators = null;

    protected $storeLocatorInventory = null;

    protected $onestep = null;

    protected $_storeOnlyProduct = 0;

    /**
     * @var PriceCurrencyInterface
     */
    protected $priceCurrency;

    /**
     * @var \Jjcommerce\ClickAndCollect\Helper\Data
     */
    protected $dataHelper;

    protected $stickerHelper;

    /**
     * StoreLocations constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\Data\Form\FormKey $formKey
     * @param \Jjcommerce\StoreLocator\Model\Storelocator $storelocator
     * @param \Jjcommerce\StoreLocator\Model\CataloginventoryStoreStock $cataloginventoryStoreStock
     * @param \Magento\Checkout\Model\Type\Onepage $onepage
     * @param PriceCurrencyInterface $priceCurrency
     * @param Data $dataHelper
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Data\Form\FormKey $formKey,
        \Jjcommerce\StoreLocator\Model\Storelocator $storelocator,
        \Jjcommerce\StoreLocator\Model\CataloginventoryStoreStock $cataloginventoryStoreStock,
        \Magento\Checkout\Model\Type\Onepage $onepage,
        PriceCurrencyInterface $priceCurrency,
        Data $dataHelper,
        StickerHelper $stickerHelper,
        array $data = []
    ) {
        $this->formKey = $formKey;
        $this->storeLocators = $storelocator;
        $this->storeLocatorInventory = $cataloginventoryStoreStock;
        $this->onestep = $onepage;
        $this->priceCurrency = $priceCurrency;
        $this->dataHelper = $dataHelper;
        $this->getItemWithStorePickupOnly();
        $this->stickerHelper = $stickerHelper;
        parent::__construct($context, $data);
    }

    /**
     * @return string[]
     */
    public function getShippingAmountEstimate()
    {
        return array(
            'orders_over'  =>  $this->priceCurrency->getCurrencySymbol().'29 - Free' ,
            'orders_under' =>  $this->priceCurrency->getCurrencySymbol().'29 - '.$this->priceCurrency->getCurrencySymbol().'4.95',
            'weekend'      =>   '-'.$this->priceCurrency->getCurrencySymbol().'10.50'
        );
    }

    /**
     * @return string
     */
    public function getDeliveryType()
    {
        $quoteDeliveryType =  $this->onestep->getQuote()->getData('delivery_type');
        $defaultType = '';
        if($this->getStoreOnlyProduct()>0){
            $defaultType = 'collectByStore';
        }

        return ($quoteDeliveryType)?$quoteDeliveryType:$defaultType;
    }

    public function getItemWithStorePickupOnly()
    {
        $storeId = $this->onestep->getQuote()->getStoreId();
        $productIds = [];
        $items = $this->onestep->getQuote()->getAllVisibleItems();
        foreach( $items as $item ) {
            if ($item->getHasChildren()) {
                foreach ($item->getChildren() as $child) {
                    $productIds[]  =  $child->getProductId();
                }
            }
            else{
                $productIds[]  =  $item->getProductId();
            }
        }
        $this->_storeOnlyProduct =  $this->storeLocators->countProductWithStorePickupOnly($productIds,$storeId);

    }

    public function getStoreOnlyProduct(){
        return $this->_storeOnlyProduct;
    }

    public function getStoreInventory()
    {

        $stores[] = null;
        $storeset = 0;
        $noStockAvailable = false;
        $stores = array('0' => array('id' => '', 'name' => 'Please select store', 'status' => null));

        $items = $this->onestep->getQuote()->getAllVisibleItems();
            foreach( $items as $item ) {

                if ($item->getHasChildren()) {
                    foreach ($item->getChildren() as $child) {

                        $itemstock = $this->storeLocators->getStockByProductId($child->getProduct());
                        if ($itemstock) {
                            $storeset = 1;
                            foreach ($itemstock as $stock) {
                                $status = ($stock['stock'] >= $item->getQty()) ? 1 : null;
                                if (!isset($stores[$stock['shop_id']])) {
                                    $stores[$stock['shop_id']] = array('id' => $stock['shop_id'], 'name' => $stock['name'], 'status' => $status);
                                    if (is_null($status)) {
                                        $stores[$stock['shop_id']]['shop_name'] = $stock['name'];
                                        $stores[$stock['shop_id']]['missingProducts'][$item->getId()]['name'] = $item->getName();
                                    }
                                } else {
                                    if (is_null($status)) {
                                        $stores[$stock['shop_id']]['status'] = $status;
                                        $stores[$stock['shop_id']]['shop_name'] = $stock['name'];
                                        $stores[$stock['shop_id']]['missingProducts'][$item->getId()]['name'] = $item->getName();

                                    }
                                }
                            }
                        }
                        else{
                            $noStockAvailable = true;
                        }
                    }
                } else {

                    $itemstock = $this->storeLocators->getStockByProductId($item->getProduct());

                    if ($itemstock) {
                        $storeset = 1;
                        foreach ($itemstock as $stock) {
                            $status = ($stock['stock'] >= $item->getQty()) ? 1 : null;
                            if (!isset($stores[$stock['shop_id']])) {
                                $stores[$stock['shop_id']] = array('id' => $stock['shop_id'], 'name' => $stock['name'], 'status' => $status);
                                if (is_null($status)) {
                                    $stores[$stock['shop_id']]['shop_name'] = $stock['name'];
                                    $stores[$stock['shop_id']]['missingProducts'][$item->getId()]['name'] = $item->getName();
                                }
                            } else {
                                if (is_null($status)) {
                                    $stores[$stock['shop_id']]['status'] = $status;
                                    $stores[$stock['shop_id']]['shop_name'] = $stock['name'];
                                    $stores[$stock['shop_id']]['missingProducts'][$item->getId()]['name'] = $item->getName();
                                }
                            }
                        }
                    }
                    else{
                        $noStockAvailable = true;
                    }
                }
            }

        $standardDeliveryEnable = $this->dataHelper->isStandardCollectOptionEnabled();
        if ($standardDeliveryEnable) {
            $collection = $this->storeLocators->getCollection();
            foreach ( $collection as $store ) {
                if (!isset($stores[ $store->getShopId() ]) || $noStockAvailable ) {
                    $stores[$store->getShopId()] = array('id' => $store->getShopId(), 'name' => $store->getName(), 'status' => null);
                }
            }
        }

        foreach( $stores as $key => $data ) {
            $storeName = str_replace( ' ','_',   strtolower($data['name']) );
            if($storeName != $this->stickerHelper->getWarehouseName()) {
                $fdata[] = $data;
            }
        }
        return array( 'stores' =>$fdata  );
    }

    protected function getProductStock($product)
    {
        $collection =  $this->storeLocatorInventory->getCollection()
            ->addFieldToSelect( array('store_id','stock') )
            ->addFieldToFilter( 'product_id',$product->getId());

        $data = array();
        foreach( $collection as $row ){
            $data[ $row->getStoreId() ] = $row->getStock();
         }

       return $data;
    }

    /**
     * To check weather extension is enabled or disabled
     * @return boolean
     */
    public function isExtensionEnabled()
    {
        return $this->dataHelper->isExtensionEnabled();
    }

    public function log($message) {
        //\Zend\Log\Logger::INFO

        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/storelocator.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info($message);
    }
     
}