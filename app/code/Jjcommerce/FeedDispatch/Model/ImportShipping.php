<?php

/**
 * @category    Jjcommerce
 * @package     Jjcommerce_FeedDispatch
 * @author	Jjcommerce Team<support@2jdesign.co.uk>
 */

namespace Jjcommerce\FeedDispatch\Model;

use Braintree\Exception;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Sales\Model\Service\InvoiceService;
use Magento\Sales\Model\Order\ShipmentFactory;
use Magento\Sales\Model\Order\CreditmemoFactory;
use Magento\Sales\Model\Order\Invoice;
use Magento\Framework\Registry;

class ImportShipping {
    
    protected $filesystem;

    protected $directoryList;
    protected $fileName = null;
    protected $_filePath;
    protected $_helper;
    protected $_dbResource;

    protected $fileHeader = [];

    protected $varDirectoryToRead;
    protected $varDirectoryToWrite;

    protected $sucessreportdata = [];
    protected $failurereportdata = [];

    protected $_order = null;
    protected $_transaction;

    /**
     * @var ShipmentFactory
     */
    protected $shipmentFactory;

    /**
     * @var Registry
     */
    protected $registry;
    /**
     * @var InvoiceService
     */
    private $invoiceService;

    /**
     * @var \Magento\Sales\Api\CreditmemoFactory
     */
    protected $creditmemoFactory;

    /**
     * @var \Magento\Sales\Api\CreditmemoManagementInterface
     */
    protected $creditmemoManagement;

     const CONFIRMSTATUS = 'confirmed';
    /**
     * @param \Magento\Framework\File\Csv $csvProcessor
     */
    public function __construct(
    \Magento\Framework\Filesystem $filesystem,
    DirectoryList $directoryList,
    \Jjcommerce\FeedDispatch\Helper\Data $helper,
    \Jjcommerce\FeedDispatch\Model\ResourceModel\ShippingUpdate $shippingupdate,
    \Magento\Sales\Model\Order $order,
    Registry $registry,
    ShipmentFactory $shipmentFactory,
    InvoiceService $invoiceService,
    \Magento\Framework\DB\Transaction $transaction,
    CreditmemoFactory $creditmemoFactory,
    \Magento\Sales\Api\CreditmemoManagementInterface $creditmemoManagement
    ) {
        $this->filesystem = $filesystem;
        $this->varDirectoryToRead = $filesystem->getDirectoryRead(DirectoryList::PUB);
        $this->varDirectoryToWrite = $filesystem->getDirectoryWrite(DirectoryList::PUB);

        $this->directoryList = $directoryList;
        $this->_helper = $helper;

        $this->_dbResource = $shippingupdate;

        $this->_filePath = $this->_helper->getImportRoot();
       // $this->fileHeader = unserialize($this->_helper->getImportFeedHeader() );

        $this->_order =   $order;

        $this->registry = $registry;
        $this->shipmentFactory = $shipmentFactory;
        $this->invoiceService = $invoiceService;
        $this->_transaction = $transaction;

        $this->creditmemoFactory = $creditmemoFactory;
        $this->creditmemoManagement = $creditmemoManagement;
    }
   // 10094/PNK|1|495|150|STD|1|0|HDNL|21|1900-1-1|AND#ARG#


    public function processData()
    {

        try {
            if ($this->isLocked('updateShipment')) {
                $this->log('Lock File exists...');
               return;
            }

            // Collect latest uploaded shipment feed file.
            $this->getLatestFile();

            if( is_null($this->fileName )){
                return;
            }

            $isReady = $this->shipmentFeedReady();

           if (!$isReady) {
               $this->getLock('updateShipment');
               $this->getLastBatchId();

               $this->readShipmentFile();
                $this->prepareData();

                $this->updateSheduleTable();
                $this->_archiveShipmentFile();

                $this->createReport();
                $this->truncateBatchTable();
                $this->releaseLock('updateShipment');
                $this->log(  'Shipment is updated......' );
            } else {
               $this->log('New Shipment feed not comes......');
            }
        }
        catch (Exception $e) {
            $this->log($e->getMessage() );
        }
    }


    /**
     *  To update feed schedule table.
     *
     */
    public function updateSheduleTable()
    {
        $this->_dbResource->updateSheduleTable($this->fileName ."-".date('Y-m-d H:i:s'));
    }

    protected function _archiveShipmentFile()
    {

        $filePath = $this->varDirectoryToRead->getAbsolutePath($this->fileName); // /vagrant/pub/feed/customer_import/feed.csv
        $path =  $this->varDirectoryToRead->getRelativePath( $filePath); // feed/customer_import/feed.csv

        $content = $this->varDirectoryToRead->readFile($path, true);

        unlink($filePath);
        $processedPath = $this->processRoot($path);
        $this->varDirectoryToWrite->writeFile($processedPath, $content);

        return;
    }

    /**
     *  To generate report file
     *
     */
    public function createReport()
    {
        if (isset($this->sucessreportdata) && !empty($this->sucessreportdata)) {
            $filesuffix = time();
            $processedPath = $this->_filePath . '/feed_report/sucess_report_' . $filesuffix . '.txt';
            $content = implode("\r\t", $this->sucessreportdata);
            $this->varDirectoryToWrite->writeFile($processedPath, $content);

        }
        if (isset($this->failurereportdata) && !empty($this->failurereportdata)) {
            $filesuffix = time();
            $processedPath = $this->_filePath . '/feed_report/failure_report_' . $filesuffix . '.txt';
            $content = implode("\r\t", $this->failurereportdata);
            $this->varDirectoryToWrite->writeFile($processedPath, $content);
        }
        $time_end       = $this->microtime_float();
        $execution_time = $time_end - $this->time_start;
        $this->log('Execution Time ---' . $execution_time );
    }

    /**
     * To Truncate batch table
     *
     */
    public function truncateBatchTable()
    {
        try {
            $this->_dbResource->truncateBatch();
            $this->log("Batch Table truncate sucessfully...");
        }
        catch (Exception $e) {
            $this->log($e->getMessage());
        }
    }

    /**
     *  To check file lock
     *
     */
    public function isLocked($lockName = '')
    {
        if ($lockName != '') {
            $file = $this->_filePath . '/feed_lock/' . $lockName . '.lock';
            $filePath = $this->varDirectoryToRead->getAbsolutePath($file); // /vagrant/pub/feed/shipmentimport/feed_lock/updateShipment.lock

            if (!file_exists($filePath))
                return false;

            $timeDifference = time() - filemtime(utf8_decode($filePath));
            if (is_file($filePath) && $timeDifference >= 1800) { //time for 30min
                //  @unlink($file);
                return false;
            }

            return file_exists($filePath);
        }
    }

    /**
     *  To create file lock
     *
     */
    public function getLock($lockName = '')
    {
        if ($lockName != '') {
            $file = $this->_filePath . '/feed_lock/' . $lockName . '.lock';
            $this->varDirectoryToWrite->writeFile($file,  time());
        }
    }
    /**
     *  To release lock file
     *
     */
    public function releaseLock($lockName = '')
    {
        if ($lockName != '') {
            $file = $this->_filePath . '/feed_lock/' . $lockName . '.lock';
            $filePath = $this->varDirectoryToRead->getAbsolutePath($file);
            unlink($filePath);
            $this->log('Release lock after.....');
        }
    }

    /**
     * Get latest shipment feed file name
     *
     */
    public function getLatestFile()
    {
        $files = array();

        if($this->_filePath==''){
            return;
        }

        $configPaths = $this->varDirectoryToRead->search('{'.$this->_filePath. '/import/*}');
        foreach ($configPaths as $configPath) {

            if (strpos($configPath, 'processed') === false  ) {
                $path =  $this->varDirectoryToRead->getAbsolutePath( $configPath);
                $files[$configPath] = filemtime(utf8_decode($path));
            }
        }

        arsort($files);
        $newest         = array_slice($files, 0, 1);
        $this->fileName = key($newest);

    }

    /**
     *  To check whether new shipment feed comes.
     *
     */
    public function shipmentFeedReady()
    {
        $file =  $this->_dbResource->getScheduleFile();
        return ($file === $this->fileName) ? 1 : 0;
    }


    /**
     * Get latest batch Id
     *
     */
    public function getLastBatchId()
    {
        $this->time_start = $this->microtime_float();
        $batchResult      =   $this->_dbResource->getBatchId();

        if ($batchResult) {
            $this->batch_id = $batchResult + 1;
        } else {
            $this->batch_id = 1;
        }
    }

    /**
     * Feel batch table of data to br update
     *
     */
    public function readShipmentFile()
    {

        $feedFile = $this->getOrderItemSheet();

        foreach ($feedFile as $seg) {
            $serialize_data = serialize($seg);
            $this->_dbResource->setBatchId(array( 'batch_id' => $this->batch_id ,'content'=> $serialize_data ));
        }
    }

    /**
     * Get shipment feed file complete data
     *
     */
    public function getOrderItemSheet()
    {
        $filePath = $this->varDirectoryToRead->getAbsolutePath($this->fileName);
        $path = $this->varDirectoryToRead->getRelativePath($filePath);
        $contentData = $this->varDirectoryToRead->readFile($path, true);
        $shipmentFileData = array();

        $rows = explode("\n", trim($contentData, "\n"));
        $this->fileHeader = explode("\t", trim( strtolower( $rows[0]) ) );
        unset($rows[0]);

        foreach ($rows as $rline) {
            if(!trim($rline)){
                continue;
            }
            $row = explode("\t", trim($rline) );

            $keyRow = array();
            $i = 0;
            foreach($row as $value){
                $keyRow[$this->fileHeader[$i]] = $value;
                $i++;
            }

            $shipmentFileData[] = $keyRow;
        }

        return $shipmentFileData;
    }

    /*
     *  lets update order shipment
    */

    public function prepareData()
    {
        try {

            $dataresult = $this->_dbResource->getDataByBatchId($this->batch_id);
            $orderData = array();

            foreach ($dataresult as $row) {
                $serdata = unserialize($row['content']);

                if(!isset($orderData[$serdata['order_id']])) {
                    $orderData[$serdata['order_id']] = array($serdata);
                }
                else {
                    $orderData[$serdata['order_id']][] = $serdata;
                }
            }

            $this->updateShipment($orderData);

        }
        catch (Exception $e) {
            $this->failurereportdata[] = $e->getMessage();
        }
    }



    protected function updateShipment($orderdata)
    {
        $entity_id = 0;
        try {
            $transactionSave = null;
            foreach ($orderdata as $orderId => $data) {

                $customer_code= null;
                $order = $this->_order->getCollection()->addFieldToFilter('increment_id', array('eq' => $orderId))->getFirstItem();

                if (!$order || !$order->getId()) {
                    $this->failurereportdata[] = $orderId.'No order for this code found';
                    continue;
                }

                $order->load($order->getId());
                $trackingCode = null;
                $invoiceData = array();


                foreach ($data as $orderItemData) {

                   // 80 : item shipped, 220 : shipping completed

                       $itemId = $this->_dbResource->getOrderItem($order->getId(), $orderItemData['sku']);

                       if(isset($orderItemData['tracking_code'])) {
                           $trackingCode = $orderItemData['tracking_code'];
                       }
                       $invoiceData[$itemId] = $orderItemData['qty'];

                }

                if(!count($invoiceData) ){
                    continue;
                }

                if ($order->canInvoice()) {
                $invoice = $this->invoiceService->prepareInvoice($order, $invoiceData);
                if (!$invoice) {
                    $this->failurereportdata[] = $orderId. 'Invoice not prepared';
                    continue;
                }
                if (!$invoice->getTotalQty()) {
                    $this->failurereportdata[] = $orderId. 'Can\'t create an invoice without products.';
                    continue;
                }
                if($this->registry->registry('current_invoice') ) {
                    $this->registry->unregister('current_invoice');
                }
                $this->registry->register('current_invoice', $invoice);
                $invoice->addComment('The shipment is updating via feed.');

                $invoice->setCustomerNote('The shipment is updating via feed.');
                $invoice->register();
                $invoice->getOrder()->setIsInProcess(true);
                    $transactionSave = $this->_transaction->addObject(
                        $invoice
                    )->addObject(
                        $invoice->getOrder()
                    );
                }

                if ($order->canShip()) {
                    $shipment = false;

                    $shipment = $this->_prepareShipment($order, $invoiceData,$trackingCode);
                    if ($shipment) {

                        $transactionSave = $this->_transaction->addObject(
                            $shipment
                        )->addObject($order);
                    }
                }
                if($transactionSave) {

                    try {
                        $transactionSave->save();
                    }catch (Exception $e){
                        $this->failurereportdata[] = 'error while saving Transaction: '.$e->getMessage();
                    }

                }

                $this->sucessreportdata[] =$orderId.' You created the invoice.';
        }
        }catch(Exception $e) {
            $this->failurereportdata[] = $e->getMessage() ;
        }

    }

    /**
     * Prepare shipment
     *
     * @param \Magento\Sales\Model\Order\Invoice $invoice
     * @return \Magento\Sales\Model\Order\Shipment|false
     */
    protected function _prepareShipment($order,$itemData,$trackingCode=null)
    {
        $trackingData = array('parent_id'=> 0,'order_id' => $order->getId(), 'number' => $trackingCode,'carrier_code' =>$order->getShippingMethod());

        $shipment = $this->shipmentFactory->create(
            $order,
            $itemData,
            ($trackingCode)?array( $trackingData ):null
        );

        if (!$shipment->getTotalQty()) {
            $this->failurereportdata[] = $order->getIncrementId().'  '. 'shipping could not generated';
            return false;
        }
        return $shipment->register();
    }

    protected function processRoot($relativePath)
    {
        $eachPath = explode( '/',$relativePath );
        $i = count($eachPath );
        $fileName =   $eachPath[$i-1].'_'.date('Ymd-His');
        $eachPath[$i-1] = 'processed';
        $eachPath[$i] = $fileName;
        return implode('/',$eachPath);

    }

    public function log($message) {
        //\Zend\Log\Logger::INFO

        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/importshipmentfeed.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info($message);
    }

    /**
     *  To calculate time
     *
     */
    public function microtime_float()
    {
        list($usec, $sec) = explode(" ", microtime());
        return ((float) $usec + (float) $sec);
    }



 }
