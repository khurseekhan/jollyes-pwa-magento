<?php
/**
 * Copyright © 2015 Jjcommerce. All rights reserved.
 */

namespace Jjcommerce\FeedDispatch\Model\ResourceModel;

class ShippingUpdate extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resources
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('jj_shipment_batch', 'id');
    }

    public function getScheduleFile(){

    $storetable = $this->getTable('jj_shipment_cron_schedule');
    $select = $this->getConnection()->select()->from(
        $storetable,
        ['file_name']
    );
   return $this->getConnection()->fetchOne($select);

   }

    public function getBatchId()
{

    $select = $this->getConnection()->select()->from(
        $this->getMainTable(),
        ['batch_id']
    )->order('batch_id DESC')->limit(1);

    return $this->getConnection()->fetchOne($select);
}
    public function setBatchId($data)
{

    $this->getConnection()->insert($this->getMainTable(), $data );
}

    public function getDataByBatchId($batchId)
{
    $select = $this->getConnection()->select()->from(
        $this->getMainTable(),
        ['content']
    )->where('batch_id="'.$batchId.'"');
    return $this->getConnection()->fetchAll($select);
}

    public function updateSheduleTable($fileName)
{
    $storetable = $this->getTable('jj_shipment_cron_schedule');
    $this->getConnection()->insert($storetable , array('file_name'=> $fileName) );
}

    public function truncateBatch()
{
    $this->getConnection()->query( ' TRUNCATE '.$this->getMainTable() );
}
    public function getOrderItem($orderid, $itemSku)
{
    $itemTable = $this->getTable('sales_order_item');
    $select = $this->getConnection()->select()->from(
        $itemTable,
        ['item_id']
    )->where('order_id='.$orderid.' and parent_item_id is null and sku="'.$itemSku.'"');
    return $this->getConnection()->fetchOne($select);
}

}
