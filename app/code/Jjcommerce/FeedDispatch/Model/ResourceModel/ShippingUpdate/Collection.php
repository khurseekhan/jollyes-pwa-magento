<?php
/**
 * Copyright © 2015 Jjcommerce. All rights reserved.
 */

namespace Jjcommerce\FeedDispatch\Model\ResourceModel\ShippingUpdate;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Jjcommerce\FeedDispatch\Model\ShippingUpdate',
                     'Jjcommerce\FeedDispatch\Model\ResourceModel\ShippingUpdate');
    }
}
