<?php

/**
 * @category    Jjcommerce
 * @package     Jjcommerce_FeedDispatch
 * @author	Jjcommerce Team<support@2jdesign.co.uk>
 */

namespace Jjcommerce\FeedDispatch\Controller\Import;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;


/**
 *
 * @SuppressWarnings(PHPMD.LongVariable)
 */
class ImportFeed extends Action {

    protected $importShipment = null;


    /**
     * Constructor
     *
     * @param Context $context
     * @param Config $config
     * @param Session $checkoutSession
     */
    public function __construct(
    Context $context,
    \Jjcommerce\FeedDispatch\Model\ImportShipping $importShipment,
    \Magento\Sales\Model\Order $order
    ) {
        $this->importShipment = $importShipment;
        parent::__construct($context);
    }

    public function execute() {

       $this->importShipment->processData();
        echo 'TESTED!';
    }


    public function log($message) {
        //\Zend\Log\Logger::INFO

        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/importshipmentfeed.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info($message);
    }

}
