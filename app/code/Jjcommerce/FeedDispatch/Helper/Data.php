<?php

/**
 * @category    Jjcommerce
 * @package     Jjcommerce_FeedDispatch
 * @author	Jjcommerce Team<support@2jdesign.co.uk>
 */

namespace Jjcommerce\FeedDispatch\Helper;


class Data extends \Magento\Framework\App\Helper\AbstractHelper {

    const SHIPMENTIMPORTROOT = 'feeddispatch/general/file_path';

    public function getImportRoot()
    {
        return $this->scopeConfig->getValue(
            self::SHIPMENTIMPORTROOT,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }


}
