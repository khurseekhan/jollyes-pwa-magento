<?php

/**
 * @category    Jjcommerce
 * @package     Jjcommerce_FeedDispatch
 * @author	Jjcommerce Team<support@2jdesign.co.uk>
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Jjcommerce_FeedDispatch',
    __DIR__
);
