<?php
/**
 * Jjcommerce_AdvancedReport
 *
 * PHP version 5.x
 *
 * @category  XML
 * @package   Jjcommerce\AdvancedReport
 * @author    Khushbu Dhoot <khushbu@2jcommerce.in>
 * @copyright 2017 Copyright 2J Commerce, Inc. http://www.2jcommerce.in/
 * @license   http://www.2jcommerce.in/ Private
 * @link      http://www.jollyes.co.uk
 */

/**
 * Sales report admin controller
 *
 * @author     Magento Core Team <core@magentocommerce.com>
 */
namespace Jjcommerce\AdvancedReport\Controller\Adminhtml\Report;

/**
 * @SuppressWarnings(PHPMD.NumberOfChildren)
 */
abstract class AdvancedReport extends \Magento\Reports\Controller\Adminhtml\Report\AbstractReport
{
    /**
     * Add report/sales breadcrumbs
     *
     * @return $this
     */
    public function _initAction()
    {
        parent::_initAction();
        $this->_addBreadcrumb(__('Sales'), __('Item Wise Sales Report'));
        return $this;
    }

    /**
     * Determine if action is allowed for reports module
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        switch ($this->getRequest()->getActionName()) {
            case 'advancedreport':
                return $this->_authorization->isAllowed('Jjcommerce_AdvancedReport::salesroot_advancedreport');
                break;
            default:
                return $this->_authorization->isAllowed('Magento_Reports::salesroot');
                break;
        }
    }
}
