<?php
/**
 * Jjcommerce_AdvancedReport
 *
 * PHP version 5.x
 *
 * @category  XML
 * @package   Jjcommerce\AdvancedReport
 * @author    Khushbu Dhoot <khushbu@2jcommerce.in>
 * @copyright 2017 Copyright 2J Commerce, Inc. http://www.2jcommerce.in/
 * @license   http://www.2jcommerce.in/ Private
 * @link      http://www.jollyes.co.uk
 */
namespace Jjcommerce\AdvancedReport\Controller\Adminhtml\Report\AdvancedReport;

use Jjcommerce\AdvancedReport\Model\Flag;

class AdvancedReport extends \Jjcommerce\AdvancedReport\Controller\Adminhtml\Report\AdvancedReport
{
    /**
     * Sales report action
     *
     * @return void
     */
    public function execute()
    {
        //$this->_showLastExecutionTime(Flag::ADVANCE_REPORT_ORDER_FLAG_CODE, 'advancedreport');
        $this->_initAction()->_setActiveMenu(
            'Jjcommerce_AdvancedReport::report_salesroot_advancedreport'
        )->_addBreadcrumb(
            __('Item Wise Sales Report'),
            __('Item Wise Sales Report')
        );
        $this->_view->getPage()->getConfig()->getTitle()->prepend(__('Item Wise Sales Report'));

        $gridBlock = $this->_view->getLayout()->getBlock('adminhtml_advancedreport_advancedreport.grid');
        $filterFormBlock = $this->_view->getLayout()->getBlock('grid.filter.form.advanced');

        $this->_initReportAction([$gridBlock, $filterFormBlock]);

        $this->_view->renderLayout();
    }
}
