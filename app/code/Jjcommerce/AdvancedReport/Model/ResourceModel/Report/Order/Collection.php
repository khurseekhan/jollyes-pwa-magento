<?php
/**
 * Jjcommerce_AdvancedReport
 *
 * PHP version 5.x
 *
 * @category  XML
 * @package   Jjcommerce\AdvancedReport
 * @author    Khushbu Dhoot <khushbu@2jcommerce.in>
 * @copyright 2017 Copyright 2J Commerce, Inc. http://www.2jcommerce.in/
 * @license   http://www.2jcommerce.in/ Private
 * @link      http://www.jollyes.co.uk
 */
namespace Jjcommerce\AdvancedReport\Model\ResourceModel\Report\Order;

/**
 * Report item wise order collection
 *
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Collection extends \Magento\Sales\Model\ResourceModel\Report\Collection\AbstractCollection
{
    /**
     * Period format
     *
     * @var string
     */
    protected $_periodFormat;

    /**
     * Aggregated Data Table
     *
     * @var string
     */
    protected $_aggregationTable = 'sales_order_item';

    /**
     * Selected columns
     *
     * @var array
     */
    protected $_selectedColumns = [];

    /**
     * @param \Magento\Framework\Data\Collection\EntityFactory $entityFactory
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategy
     * @param \Magento\Framework\Event\ManagerInterface $eventManager
     * @param \Magento\Sales\Model\ResourceModel\Report $resource
     * @param \Magento\Framework\DB\Adapter\AdapterInterface $connection
     */
    public function __construct(
        \Magento\Framework\Data\Collection\EntityFactory $entityFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategy,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Magento\Sales\Model\ResourceModel\Report $resource,
        \Magento\Framework\DB\Adapter\AdapterInterface $connection = null
    ) {
        $resource->init($this->_aggregationTable);
        parent::__construct($entityFactory, $logger, $fetchStrategy, $eventManager, $resource, $connection);
    }

    /**
     * Get selected columns
     *
     * @return array
     */
    protected function _getSelectedColumns()
    {
        $connection = $this->getConnection();
        if ('month' == $this->_period) {
            $this->_periodFormat = $connection->getDateFormatSql('so.created_at', '%Y-%m');
        } elseif ('year' == $this->_period) {
            $this->_periodFormat = $connection->getDateExtractSql(
                'so.created_at',
                \Magento\Framework\DB\Adapter\AdapterInterface::INTERVAL_YEAR
            );
        } else {
            $this->_periodFormat = $connection->getDateFormatSql('so.created_at', '%Y-%m-%d');
        }

        if (!$this->isTotals()) {
            $this->_selectedColumns = [
                'created_at' => $this->_periodFormat,
                'item_id' => 'item_id',
                'order_id' => 'so.increment_id',
                'cc_name' => 'so.storelocator_name',
                'cc_type_name' => 'so.cc_type',
                'customername' => 'CONCAT(soa.firstname, " ",soa.lastname)',
                'customeremail' => 'so.customer_email',
                'product_desc' => 'soi.name',
                'sku' => 'soi.sku',
                'qty_ordered' => 'FLOOR(soi.qty_ordered)',
                'subtot' => 'soi.base_row_total_incl_tax',
                'taxamount' => 'soi.tax_amount',
                'tax_percent' => 'soi.tax_percent',
                'discountamount' => 'soi.discount_amount',
                'rowtotal'=>'(soi.base_row_total_incl_tax-soi.discount_amount)',
                'coupon_code'=>'so.coupon_code',
                'b_postcode' => 'soa.postcode',
                'b_country'  => 'soa.country_id'
            ];
        }

        if ($this->isTotals()) {
            $this->_selectedColumns = $this->getAggregatedColumns();
        }

        return $this->_selectedColumns;
    }

    /**
     * Apply custom columns before load
     *
     * @return $this
     */
    protected function _beforeLoad()
    {
        $this->getSelect()
            ->from(['soi' => $this->getResource()->getMainTable()], $this->_getSelectedColumns());

        $this->getSelect()->joinInner(
                ['so' => 'sales_order'],
                'soi.order_id = so.entity_id AND soi.parent_item_id is null'
            );

        $this->getSelect()->joinInner(
            ['soa' => 'sales_order_address'],
            'soa.parent_id = so.entity_id AND soa.address_type = "billing"',
            ['soa.firstname', 'soa.lastname']

        );

        if (!$this->isTotals()) {
            $this->getSelect()->group("so.created_at");
            $this->getSelect()->group("item_id");
        }

        return parent::_beforeLoad();
    }

    protected function _applyDateRangeFilter()
    {
        // Remember that field PERIOD is a DATE(YYYY-MM-DD) in all databases
        if ($this->_from !== null) {
            $this->getSelect()->where('so.created_at >= ?', $this->_from. ' 00:00:00');
        }
        if ($this->_to !== null) {
            $this->getSelect()->where('so.created_at <= ?', $this->_to. ' 23:59:59');
        }

        return $this;
    }

    /**
     * Apply stores filter to select object
     *
     * @param \Magento\Framework\DB\Select $select
     * @return $this
     */
    protected function _applyStoresFilterToSelect(\Magento\Framework\DB\Select $select)
    {
        $nullCheck = false;
        $storeIds = $this->_storesIds;

        if (!is_array($storeIds)) {
            $storeIds = [$storeIds];
        }

        $storeIds = array_unique($storeIds);

        if ($index = array_search(null, $storeIds)) {
            unset($storeIds[$index]);
            $nullCheck = true;
        }

        if ($nullCheck) {
            $select->where('so.store_id IN(?) OR so.store_id IS NULL', $storeIds);
        } else {
            $select->where('so.store_id IN(?)', $storeIds);
        }

        return $this;
    }
     public function setDataRange($from=null,$to=null ){
        $this->_from = $from;
        $this->_to = $to;
        return $this;
    }
    public function setStoreFilter($store){
        $this->_storesIds = $store;
        return $this;
    }

}
