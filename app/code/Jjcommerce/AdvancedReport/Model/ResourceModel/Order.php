<?php
/**
 * Jjcommerce_AdvancedReport
 *
 * PHP version 5.x
 *
 * @category  XML
 * @package   Jjcommerce\AdvancedReport
 * @author    Khushbu Dhoot <khushbu@2jcommerce.in>
 * @copyright 2017 Copyright 2J Commerce, Inc. http://www.2jcommerce.in/
 * @license   http://www.2jcommerce.in/ Private
 * @link      http://www.jollyes.co.uk
 */
namespace Jjcommerce\AdvancedReport\Model\ResourceModel;

/**
 * Order entity resource model
 */
class Order extends \Magento\Sales\Model\ResourceModel\Report\AbstractReport
{
    /**
     * @var \Magento\Sales\Model\ResourceModel\Report\Order\CreatedatFactory
     */
    protected $_orderFactory;


    /**
     * @param \Magento\Framework\Model\ResourceModel\Db\Context $context
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate
     * @param \Magento\Reports\Model\FlagFactory $reportsFlagFactory
     * @param \Magento\Framework\Stdlib\DateTime\Timezone\Validator $timezoneValidator
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $dateTime
     * @param \Magento\Sales\Model\ResourceModel\Report\Order\Createdat $createdat
     * @param string $connectionName
     */
    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
        \Magento\Reports\Model\FlagFactory $reportsFlagFactory,
        \Magento\Framework\Stdlib\DateTime\Timezone\Validator $timezoneValidator,
        \Magento\Framework\Stdlib\DateTime\DateTime $dateTime,
        \Magento\Sales\Model\ResourceModel\Report\Order\Createdat $createdat,
        $connectionName = null
    ) {
        parent::__construct(
            $context,
            $logger,
            $localeDate,
            $reportsFlagFactory,
            $timezoneValidator,
            $dateTime,
            $connectionName
        );
        $this->_orderFactory = $createdat;

    }

    /**
     * Model initialization
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('sales_order_item', 'id');
    }

    /**
     * Aggregate Orders data
     *
     * @param string|int|\DateTime|array|null $from
     * @param string|int|\DateTime|array|null $to
     * @return $this
     */
    public function aggregate($from = null, $to = null)
    {
        $this->_orderFactory->create()->aggregate($from, $to);
        //$this->_updateDatFactory->create()->aggregate($from, $to);
        $this->_setFlagData(\Jjcommerce\AdvancedReport\Model\Flag::ADVANCE_REPORT_ORDER_FLAG_CODE);
        return $this;
    }
}
