<?php
/**
 * Jjcommerce_AdvancedReport
 *
 * PHP version 5.x
 *
 * @category  XML
 * @package   Jjcommerce\AdvancedReport
 * @link      http://www.jollyes.co.uk
 */

Namespace Jjcommerce\AdvancedReport\Cron;

use Jjcommerce\AdvancedReport\Model\ResourceModel\Report\Order\Collection;
use Magento\Framework\App\Filesystem\DirectoryList;

class AdvanceReportByCron{
    protected $totalsCollection;
    protected $_columns;
    protected $_directory;
    protected $_path = 'media/itemwise';
    protected $_RowTotal;

    /**
     * @var \Jjcommerce\AdvancedReport\Controller\Adminhtml\Report\AdvancedReport\ExportSalesItemCsv
     */
    private $exportSalesItemCsv;
    /**
     * @var Magento\Reports\Model\ResourceModel\Report\Collection\Factory
     */
    private $resourceFactory;
    /**
     * @var Magento\Store\Model\StoreManagerInterface
     */
    private $storeManagerInterface;
    /**
     * @var \Magento\Framework\App\Response\Http\FileFactory
     */
    private $fileFactory;

    public function __construct(
        Collection $exportSalesItemCollection,
        \Magento\Reports\Model\ResourceModel\Report\Collection\Factory  $resourceFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManagerInterface,
        \Magento\Framework\Filesystem  $fileFactory
    ){
        $this->exportSalesItemCsv = $exportSalesItemCollection;
        $this->resourceFactory = $resourceFactory;
        $this->storeManagerInterface = $storeManagerInterface;
        $this->_filesystem =     $fileFactory;
        $this->_directory = $this->_filesystem->getDirectoryWrite(DirectoryList::PUB);
    }

    public function   getItemWiseSalesReport( ){


        $period = $this->getPeriod();

        $stores = $this->storeManagerInterface->getStores();
        $storeIds = [];
        foreach($stores as $store) {
            $storeIds[] = $store->getStoreId();
        }

        $this->totalsCollection =  $this->exportSalesItemCsv
                             ->setDataRange($period['from'],$period['to'])
                             ->setStoreFilter($storeIds);


        if ($this->totalsCollection->load()->getSize() >0  ) {
            $filePath =  $this->getCsvFile();
        }

    }

    public function getPeriod(){
       $from =  date('Y-m-d',strtotime("-7 days"));
       $to =  date('Y-m-d',strtotime("-1 days"));

        return array( 'from' => $from,
            'to' => $to );
    }

    /**
     * Retrieve a file container array by grid data as CSV
     *
     * Return array with keys type and value
     *
     * @return array
     */
    public function getCsvFile()
    {
        $fileName = 'item_wise_sales_report_'.date('Y-m-d').'.csv'; 

        $file = $this->_path . '/' . $fileName;

        $this->_directory->create($this->_path);
        $stream = $this->_directory->openFile($file, 'w+');

        $stream->lock();
        $stream->writeCsv($this->_getExportHeaders());
       // $this->_exportIterateCollection('_exportCsvItem', [$stream]);
        $totalRecords = 0;
        foreach($this->totalsCollection as $item){
            $this->_exportCsvItem($item, $stream );
            $totalRecords++;
        }

        if ($totalRecords>0) {
            $this->_RowTotal = $this->_RowTotal/$totalRecords;
            $stream->writeCsv($this->_getExportTotals());
        }

        $stream->unlock();
        $stream->close();
        return $file;
    }

    public function _getExportHeaders(){

        return [ 'Period', 'Order Number','Click & Collect Store','Click & Collect Type',
            'Bill To Name','Email Address','Reward Card Number','Product Description','SKU',
            'Quantity','Sub Total','Tax Amount','Tax Percent','Discount Amount','Row Total',
            'Voucher Code', 'Billing Postcode', 'Billing Country'];

    }

     protected function _exportCsvItem(
        \Magento\Framework\DataObject $item,
        \Magento\Framework\Filesystem\File\WriteInterface $stream
    ) {
        $row = [];
        foreach ($this->getColumns() as $column) {          

                if('tax_percent'==$column){
                $this->_RowTotal +=  $item->getData( $column );
                    $row[] = round($item->getData( $column ),2) .'%';
                }elseif('created_at'==$column){  
                $createdAt = $item->getData( $column ); 
                    $row[] =  date("M d, Y", strtotime($createdAt));

                }else{
                    $row[] = $item->getData( $column );
                }           
        }
        $stream->writeCsv($row);
    }

    /**
     * Retrieve Totals row array for Export
     *
     * @return string[]
     */
    protected function _getExportTotals()
    {

        $row = [];
        foreach ($this->getColumns() as $column) {
            if('tax_percent'==$column){
                $row[] = $this->_RowTotal.'%';
            }elseif('created_at' == $column ){
                $row[] = 'Total';
            }
            else{

                $row[] = null;
            }
        }
        return $row;
    }

    public function getColumns(){

        if($this->_columns==null){

            $this->_columns = array(
                'created_at','order_id','cc_name','cc_type_name','customername','customeremail','reward_card_number',
                'product_desc','sku','qty_ordered','subtot','taxamount','tax_percent','discountamount',
                'rowtotal','coupon_code','b_postcode','b_country'
            );
        }
        return $this->_columns;

    }

}
