<?php
/**
 * Jjcommerce_AdvancedReport
 *
 * PHP version 5.x
 *
 * @category  XML
 * @package   Jjcommerce\AdvancedReport
 * @author    Khushbu Dhoot <khushbu@2jcommerce.in>
 * @copyright 2017 Copyright 2J Commerce, Inc. http://www.2jcommerce.in/
 * @license   http://www.2jcommerce.in/ Private
 * @link      http://www.jollyes.co.uk
 */

namespace Jjcommerce\AdvancedReport\Block\Adminhtml\Advancedreport\Advancedreport;

class Grid extends \Magento\Reports\Block\Adminhtml\Grid\AbstractGrid
{
    /**
     * GROUP BY criteria
     *
     * @var string
     */
    protected $_columnGroupBy = 'period';

    /**
     * {@inheritdoc}
     * @codeCoverageIgnore
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setCountTotals(true);
    }

    /**
     * {@inheritdoc}
     */
    public function getResourceCollectionName()
    {
        return 'Jjcommerce\AdvancedReport\Model\ResourceModel\Report\Order\Collection';
    }



    /**
     * {@inheritdoc}
     *
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareColumns()
    {
        $currencyCode = $this->getCurrentCurrencyCode();
        $rate = $this->getRate($currencyCode);

        $this->addColumn(
            'created_at',
            [
                'header' => __('Period'),
                'index' => 'created_at',
                'sortable' => false,
                'period_type' => $this->getPeriodType(),
                'renderer' => 'Magento\Reports\Block\Adminhtml\Sales\Grid\Column\Renderer\Date',
                'totals_label' => __('Total'),
                'html_decorators' => ['nobr'],
                'header_css_class' => 'col-period',
                'column_css_class' => 'col-period'
            ]
        );

        $this->addColumn(
            'order_id',
            [
                'header' => __('Order Number'),
                'index' => 'order_id',
                'sortable' => false,

            ]
        );

        $this->addColumn(
            'cc_name',
            [
                'header' => __('Click & Collect Store'),
                'index' => 'cc_name',
                'sortable' => false,

            ]
        );

        $this->addColumn(
            'cc_type_name',
            [
                'header' => __('Click & Collect Type'),
                'index' => 'cc_type_name',
                'sortable' => false,

            ]
        );

        $this->addColumn(
            'customername',
            [
                'header' => __('Bill To Name'),
                'index' => 'customername',
                'sortable' => false,

            ]
        );

        $this->addColumn(
            'customeremail',
            [
                'header' => __('Email Address'),
                'index' => 'customeremail',
                'sortable' => false,

            ]
        );

        $this->addColumn(
            'reward_card_number',
            [
                'header' => __('Reward Card Number'),
                'index' => 'reward_card_number',
                'renderer' => 'Jjcommerce\AdvancedReport\Block\Adminhtml\Advancedreport\Grid\Column\Renderer\Reward',
                'sortable' => false,
                'totals_label' => __(''),
                'html_decorators' => ['nobr'],
            ]
        );

        $this->addColumn(
            'product_desc',
            [
                'header' => __('Product Description'),
                'index' => 'product_desc',
                'sortable' => false,

            ]
        );

        $this->addColumn(
            'sku',
            [
                'header' => __('SKU'),
                'index' => 'sku',
                'sortable' => false,

            ]
        );

        $this->addColumn(
            'qty_ordered',
            [
                'header' => __('Quantity'),
                'index' => 'qty_ordered',
                'sortable' => false,

            ]
        );

        $this->addColumn(
            'subtot',
            [
                'header' => __('Sub Total'),
                'index' => 'subtot',
                //'type' => 'currency',
                'currency_code' => $currencyCode,
                'rate' => $rate,
                'sortable' => false,

            ]
        );

        $this->addColumn(
            'taxamount',
            [
                'header' => __('Tax Amount'),
                'index' => 'taxamount',
                'decimals'          => 2,
                'sortable' => false,

            ]
        );


        $this->addColumn(
            'tax_percent',
            [
                'header' => __('Tax Percent'),
                'index' => 'tax_percent',
                'sortable' => false,
                'renderer' => 'Jjcommerce\AdvancedReport\Block\Adminhtml\Advancedreport\Grid\Column\Renderer\Percent',
                'total'  => 'avg',
            ]
        );


        $this->addColumn(
            'discountamount',
            [
                'header' => __('Discount Amount'),
                'index' => 'discountamount',
                'sortable' => false,

            ]
        );


        $this->addColumn(
            'rowtotal',
            [
                'header' => __('Row Total'),
                'index' => 'rowtotal',
                //'type' => 'currency',
                //'currency_code' => $currencyCode,
                //'rate' => $rate,
                'sortable' => false,

            ]
        );


        $this->addColumn(
            'coupon_code',
            [
                'header' => __('Voucher Code'),
                'index' => 'coupon_code',
                'sortable' => false,

            ]
        );

        $this->addColumn(
            'b_postcode',
            [
                'header' => __('Billing Postcode'),
                'index' => 'b_postcode',
                'sortable' => false,

            ]
        );

        $this->addColumn(
            'b_country',
            [
                'header' => __('Billing Country'),
                'index' => 'b_country',
                'sortable' => false,

            ]
        );

        $this->addExportType('*/*/exportSalesItemCsv', __('CSV'));
        $this->addExportType('*/*/exportSalesItemExcel', __('Excel XML'));

        return parent::_prepareColumns();
    }

    /**
     * @return array
     */
    public function getCountTotals()
    {
        if (!$this->getTotals()) {
            $filterData = $this->getFilterData();
            $totalsCollection = $this->_resourceFactory->create(
                $this->getResourceCollectionName()
            )->setPeriod(
                    $filterData->getData('period_type')
                )->setDateRange(
                    $filterData->getData('from', null),
                    $filterData->getData('to', null)
                )->addStoreFilter(
                    $this->_getStoreIds()
                )->setAggregatedColumns(
                    $this->_getAggregatedColumns()
                )->isTotals(
                    true
                );

            $this->_addOrderStatusFilter($totalsCollection, $filterData);

            if ($totalsCollection->load()->getSize() < 1 || !$filterData->getData('from')) {
                $this->setTotals(new \Magento\Framework\DataObject());
                $this->setCountTotals(false);
            } else {
                foreach ($totalsCollection->getItems() as $item) {
                    $this->setTotals($item);
                    break;
                }
            }
        }
        return parent::getCountTotals();
    }


    }
