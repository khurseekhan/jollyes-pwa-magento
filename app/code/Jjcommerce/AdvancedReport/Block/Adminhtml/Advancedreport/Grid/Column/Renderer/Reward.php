<?php
/**
 * Jjcommerce_AdvancedReport
 *
 * PHP version 5.x
 *
 * @category  XML
 * @package   Jjcommerce\AdvancedReport
 * @author    Khushbu Dhoot <khushbu@2jcommerce.in>
 * @copyright 2017 Copyright 2J Commerce, Inc. http://www.2jcommerce.in/
 * @license   http://www.2jcommerce.in/ Private
 * @link      http://www.jollyes.co.uk
 */
namespace Jjcommerce\AdvancedReport\Block\Adminhtml\Advancedreport\Grid\Column\Renderer;

use Magento\Customer\Api\CustomerRepositoryInterface;

/**
 * Adminhtml grid item renderer reward
 */
class Reward extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{

    /**
     * @var CustomerRepositoryInterface
     */
    protected $_customerRepository;


    /**
     * Constructor
     *
     * @param \Magento\Backend\Block\Context $context
     * @param CustomerRepositoryInterface $customerRepositoryInterface
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Context $context,
        CustomerRepositoryInterface $customerRepositoryInterface,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_customerRepository = $customerRepositoryInterface;
    }


    /**
     * Renders grid column
     *
     * @param \Magento\Framework\DataObject $row
     * @return string
     */
    public function render(\Magento\Framework\DataObject $row)
    {
        $cardNumber = '';
        $customerId =   $row->getData('customer_id');
        if($customerId){
            $customer   =   $this->_customerRepository->getById($customerId);
            if($customer->getCustomAttribute('reward_card_number')){
                $cardNumber =   $customer->getCustomAttribute('reward_card_number')->getValue();
            }
        }

        return $cardNumber;
    }
}
