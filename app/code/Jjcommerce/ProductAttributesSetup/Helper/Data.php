<?php
/**
 * Jjcommerce_ProductAttributesSetup
 *
 * PHP version 5.x
 *
 * @category  PHP
 * @package   Jjcommerce\ProductAttributesSetup
 * @author    Swapnil Tatkondawar <swapnilt@2jcommerce.in>
 * @copyright 2017 Copyright 2J Commerce, Inc. http://www.2jcommerce.in/
 * @license   http://www.2jcommerce.in/ Private
 * @link      http://www.jollyes.co.uk
 */
namespace Jjcommerce\ProductAttributesSetup\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

class Data extends AbstractHelper
{
    /**
     * Data constructor.
     * @param \Magento\Framework\App\Helper\Context $context
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context
    ) {
        parent::__construct($context);
    }

}
