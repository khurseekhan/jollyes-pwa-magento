<?php
/**
 * Jjcommerce_ProductAttributesSetup
 *
 * PHP version 5.x
 *
 * @category  PHP
 * @package   Jjcommerce\ProductAttributesSetup
 * @author    Swapnil Tatkondawar <swapnilt@2jcommerce.in>
 * @copyright 2017 Copyright 2J Commerce, Inc. http://www.2jcommerce.in/
 * @license   http://www.2jcommerce.in/ Private
 * @link      http://www.jollyes.co.uk
 */
namespace Jjcommerce\ProductAttributesSetup\Setup;

use Magento\Catalog\Api\Data\ProductAttributeInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Eav\Setup\EavSetup;

class UpgradeData implements UpgradeDataInterface
{
    const PRODUCT_GROUP = 'Jjcommerce Attributes';

    /**
     * @var EavSetupFactory
     */
    protected $eavSetupFactory;

    /**
     * UpgradeData constructor
     *
     * @param EavSetupFactory $eavSetupFactory
     */
    public function __construct(EavSetupFactory $eavSetupFactory)
    {
        $this->eavSetupFactory = $eavSetupFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        /** @var \Magento\Eav\Setup\EavSetup $eavSetup */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

        if (version_compare($context->getVersion(), '2.0.1', '<')) {
            $this->createSizeGuideAttribute($eavSetup);
            $this->createNutritionAttribute($eavSetup);
        }

        if(version_compare($context->getVersion(), '2.0.2', '<')){
            $this->createBannerMessageAttribute($eavSetup);
        }

        $setup->endSetup();
    }

    /**
     * @param \Magento\Eav\Setup\EavSetup $eavSetup
     * @return void
     */
    private function createSizeGuideAttribute($eavSetup)
    {
        $attributeCode = 'size_guide';
        $attributeLabel = 'Size Guide';

        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            $attributeCode,
            [
                'type' => 'text',
                'label' => $attributeLabel,
                'input' => 'textarea',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'wysiwyg_enabled' => true,
                'visible' => true,
                'required' => false,
                'group' => self::PRODUCT_GROUP,
                'visible_on_front' => true,
                'used_in_product_listing' => false,
            ]
        );

        $entityTypeId = $eavSetup->getEntityTypeId(\Magento\Catalog\Model\Product::ENTITY);

        $attributeSetId = $eavSetup->getDefaultAttributeSetId($entityTypeId);
        $eavSetup->addAttributeToSet($entityTypeId, $attributeSetId, self::PRODUCT_GROUP, $attributeCode);
    }

    /**
     * @param \Magento\Eav\Setup\EavSetup $eavSetup
     * @return void
     */
    private function createNutritionAttribute($eavSetup)
    {
        $attributeCode = 'nutrition';
        $attributeLabel = '	Nutrition';

        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            $attributeCode,
            [
                'type' => 'text',
                'label' => $attributeLabel,
                'input' => 'textarea',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'wysiwyg_enabled' => true,
                'visible' => true,
                'required' => false,
                'group' => self::PRODUCT_GROUP,
                'visible_on_front' => true,
                'used_in_product_listing' => false,
            ]
        );

        $entityTypeId = $eavSetup->getEntityTypeId(\Magento\Catalog\Model\Product::ENTITY);

        $attributeSetId = $eavSetup->getDefaultAttributeSetId($entityTypeId);
        $eavSetup->addAttributeToSet($entityTypeId, $attributeSetId, self::PRODUCT_GROUP, $attributeCode);
    }

    /**
     * @param \Magento\Eav\Setup\EavSetup $eavSetup
     * @return void
     */
    private function createBannerMessageAttribute($eavSetup)
    {
        $attributeCode = 'banner_message';
        $attributeLabel = 'Banner Message';

        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            $attributeCode,
            [
                'type' => 'text',
                'label' => $attributeLabel,
                'input' => 'textarea',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'wysiwyg_enabled' => true,
                'visible' => true,
                'required' => false,
                'group' => self::PRODUCT_GROUP,
                'visible_on_front' => true,
                'used_in_product_listing' => false,
            ]
        );

        $entityTypeId = $eavSetup->getEntityTypeId(\Magento\Catalog\Model\Product::ENTITY);

        $attributeSetId = $eavSetup->getDefaultAttributeSetId($entityTypeId);
        $eavSetup->addAttributeToSet($entityTypeId, $attributeSetId, self::PRODUCT_GROUP, $attributeCode);
    }


}
