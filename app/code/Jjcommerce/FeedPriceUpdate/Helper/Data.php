<?php

/**
 * @category    Jjcommerce
 * @package     Jjcommerce_FeedPriceUpdate
 * @author	Jjcommerce Team<support@2jdesign.co.uk>
 */

namespace Jjcommerce\FeedPriceUpdate\Helper;


class Data extends \Magento\Framework\App\Helper\AbstractHelper {

    const DATAIMPORTROOT = 'torexfeed/general/file_path';
    const DATAIMPORTHEADER = 'torexfeed/general/header';

    public function getImportRoot()
    {
        return $this->scopeConfig->getValue(
            self::DATAIMPORTROOT,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getImportFeedHeader()
    {
        return $this->scopeConfig->getValue(
            self::DATAIMPORTHEADER,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
}
