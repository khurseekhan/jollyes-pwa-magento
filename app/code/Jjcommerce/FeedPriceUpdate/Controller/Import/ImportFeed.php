<?php

/**
 * @category    Jjcommerce
 * @package     Jjcommerce_FeedPriceUpdate
 * @author	Jjcommerce Team<support@2jdesign.co.uk>
 */

namespace Jjcommerce\FeedPriceUpdate\Controller\Import;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;


/**
 * Pet information list
 *
 * @SuppressWarnings(PHPMD.LongVariable)
 */
class ImportFeed extends Action {

    protected $importPrice = null;

    /**
     * @var \Magento\Indexer\Model\IndexerFactory
     */
    protected $_indexerFactory;
    /**
     * @var \Magento\Indexer\Model\Indexer\CollectionFactory
     */
    protected $_indexerCollectionFactory;
    
    /**
     * Constructor
     *
     * @param  \Jjcommerce\FeedPriceUpdate\Model\ImportPrice $importPrice
     * @param  \Magento\Indexer\Model\IndexerFactory  $indexerFactory
     * @param  \Magento\Indexer\Model\Indexer\CollectionFactory $indexerCollectionFactory
     */
    public function __construct(
    Context $context, 
    \Jjcommerce\FeedPriceUpdate\Model\ImportPrice $importPrice,
    \Magento\Indexer\Model\IndexerFactory $indexerFactory,
    \Magento\Indexer\Model\Indexer\CollectionFactory $indexerCollectionFactory
    ) {
        $this->importPrice = $importPrice;
        $this->_indexerFactory = $indexerFactory;
        $this->_indexerCollectionFactory = $indexerCollectionFactory;

        parent::__construct($context);
    }

    public function execute() {       

        $this->importPrice->processData();
        echo 'TESTED!';
    }


    public function log($message) {
        //\Zend\Log\Logger::INFO

        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/importfeed.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info($message);
    }

}
