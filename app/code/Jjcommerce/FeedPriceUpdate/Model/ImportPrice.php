<?php

/**
 * @category    Jjcommerce
 * @package     Jjcommerce_FeedPriceUpdate
 * @author	Jjcommerce Team<support@2jdesign.co.uk>
 */

namespace Jjcommerce\FeedPriceUpdate\Model;

use Magento\Framework\App\Filesystem\DirectoryList;

class ImportPrice {
    
    protected $filesystem;

    protected $directoryList;
    protected $fileName = null;
    protected $_filePath;
    protected $_helper;
    protected $_dbResource;

    protected $fileHeader = [];

    protected $varDirectoryToRead;
    protected $varDirectoryToWrite;

    protected $productEntityTypeId;
    protected $priceAttributeId;
    protected $priceStatusId;

    protected $sucessreportdata = [];
    protected $failurereportdata = [];
    /**
     * @var \Magento\Indexer\Model\IndexerFactory
     */
    protected $_indexerFactory;

    /**
     * @param \Magento\Framework\File\Csv $csvProcessor
     */
    public function __construct(
    \Magento\Framework\Filesystem $filesystem,
    DirectoryList $directoryList,
    \Jjcommerce\FeedPriceUpdate\Helper\Data $helper,
    \Jjcommerce\FeedPriceUpdate\Model\ResourceModel\PriceUpdate $priceupdate,
    \Magento\Indexer\Model\IndexerFactory $indexerFactory
    ) {
        $this->filesystem = $filesystem;
        $this->varDirectoryToRead = $filesystem->getDirectoryRead(DirectoryList::PUB);
        $this->varDirectoryToWrite = $filesystem->getDirectoryWrite(DirectoryList::PUB);

        $this->directoryList = $directoryList;
        $this->_helper = $helper;

        $this->_dbResource = $priceupdate;


        $this->_filePath = $this->_helper->getImportRoot();
        // $this->_helper->getImportFeedHeader();

       // $this->fileHeader = unserialize('a:5:{i:0;s:3:"sku";i:1;s:17:"special_from_date";i:2;s:15:"special_to_date";i:3;s:13:"special_price";i:4;s:5:"price";}');
        $this->fileHeader = unserialize($this->_helper->getImportFeedHeader() );
        $this->_indexerFactory = $indexerFactory;
    }
   // 10094/PNK|1|495|150|STD|1|0|HDNL|21|1900-1-1|AND#ARG#

    public function processData()
    {

        try {
            if ($this->isLocked('updatePrice')) {
                $this->log('Lock File exists...');
                return;
            }

            // Collect latest uploaded price feed file.
            $this->getLatestFile();
            if( is_null($this->fileName )){
                return;
            }

            $isReady = $this->priceFeedReady();
           if (!$isReady) {
               $this->getLock('updatePrice');
               $this->getLastBatchId();
               $this->readPriceFile();
                $this->prepareData();
                $this->updateSheduleTable();
                $this->_archivePriceFile();
                $this->createReport();
                $this->truncateBatchTable();
                $this->indexInventoryPrice();
                $this->releaseLock('updatePrice');

                $this->log('Product is updated......');
            } else {

               $this->log('New Price feed not comes......');
            }
        }
        catch (Exception $e) {
            $this->log($e->getMessage() );
            echo  $e->getMessage();
        }
    }


    /**
     *  To update feed schedule table.
     *
     */
    public function updateSheduleTable()
    {
        $this->_dbResource->updateSheduleTable($this->fileName ."-".date('Y-m-d H:i:s'));
    }

    protected function _archivePriceFile()
    {

        $filePath = $this->varDirectoryToRead->getAbsolutePath($this->fileName); // /vagrant/pub/feed/customer_import/feed.csv
        $path =  $this->varDirectoryToRead->getRelativePath( $filePath); // feed/customer_import/feed.csv

        $content = $this->varDirectoryToRead->readFile($path, true);

        unlink($filePath);
        $processedPath = $this->processRoot($path);
        $this->varDirectoryToWrite->writeFile($processedPath, $content);

        return;
    }

    /**
     *  To generate report file
     *
     */
    public function createReport()
    {
        if (isset($this->sucessreportdata) && !empty($this->sucessreportdata)) {
            $filesuffix = time();
            $processedPath = $this->_filePath . '/feed_report/sucess_report_' . $filesuffix . '.txt';
            $content = implode("\r\t", $this->sucessreportdata);
            $this->varDirectoryToWrite->writeFile($processedPath, $content);

        }
        if (isset($this->failurereportdata) && !empty($this->failurereportdata)) {
            $filesuffix = time();
            $processedPath = $this->_filePath . '/feed_report/failure_report_' . $filesuffix . '.txt';
            $content = implode("\r\t", $this->failurereportdata);
            $this->varDirectoryToWrite->writeFile($processedPath, $content);
        }
        $time_end       = $this->microtime_float();
        $execution_time = $time_end - $this->time_start;
        $this->log('Execution Time ---' . $execution_time );
    }

    /**
     * To Truncate batch table
     *
     */
    public function truncateBatchTable()
    {
        try {
            $this->_dbResource->truncateBatch();
            $this->log("Batch Table truncate sucessfully...");
        }
        catch (Exception $e) {
            $this->log($e->getMessage());
        }
    }

    /**
     *  To check file lock
     *
     */
    public function isLocked($lockName = '')
    {
        if ($lockName != '') {
            $file = $this->_filePath . '/feed_lock/' . $lockName . '.lock';
            $filePath = $this->varDirectoryToRead->getAbsolutePath($file); // /vagrant/pub/feed/priceexport/feed_lock/updatePrice.lock

            if (!file_exists($filePath))
                return false;

            $timeDifference = time() - filemtime(utf8_decode($filePath));
            if (is_file($filePath) && $timeDifference >= 1800) { //time for 30min
                //  @unlink($file);
                return false;
            }

            return file_exists($filePath);
        }
    }

    /**
     *  To create file lock
     *
     */
    public function getLock($lockName = '')
    {
        if ($lockName != '') {
            $file = $this->_filePath . '/feed_lock/' . $lockName . '.lock';
            $this->varDirectoryToWrite->writeFile($file,  time());
        }
    }
    /**
     *  To release lock file
     *
     */
    public function releaseLock($lockName = '')
    {
        if ($lockName != '') {
            $file = $this->_filePath . '/feed_lock/' . $lockName . '.lock';
            $filePath = $this->varDirectoryToRead->getAbsolutePath($file);
            unlink($filePath);
            $this->log('Release lock after.....');
        }
    }

    /**
     * Get latest price feed file name
     *
     */
    public function getLatestFile()
    {
        $files = array();

        $configPaths = $this->varDirectoryToRead->search('{'.$this->_filePath. '/import/*}');

        foreach ($configPaths as $configPath) {

            if (strpos($configPath, 'processed') === false) {
                $path =  $this->varDirectoryToRead->getAbsolutePath( $configPath);
                $files[$configPath] = filemtime(utf8_decode($path));
            }
        }

        arsort($files);
        $newest         = array_slice($files, 0, 1);
        $this->fileName = key($newest);
    }

    /**
     *  To check whether new price feed comes.
     *
     */
    public function priceFeedReady()
    {
        $file =  $this->_dbResource->getScheduleFile();
        return ($file === $this->fileName) ? 1 : 0;
    }


    /**
     * Get latest batch Id
     *
     */
    public function getLastBatchId()
    {
        $this->time_start = $this->microtime_float();
        $batchResult      =   $this->_dbResource->getBatchId();

        if ($batchResult) {
            $this->batch_id = $batchResult + 1;
        } else {
            $this->batch_id = 1;
        }
    }

    /**
     * Feel batch table of data to br update
     *
     */
    public function readPriceFile()
    {

        $feedFile = $this->getProductSheet();

        foreach ($feedFile as $seg) {
          //  $concat = array_combine($this->fileHeader, $seg);
            $serialize_data = serialize($seg);
            $this->_dbResource->setBatchId(array( 'batch_id' => $this->batch_id ,'content'=> $serialize_data ));
        }
    }

    /**
     * Get price feed file complete data
     *
     */
    public function getProductSheet()
    {
        $filePath = $this->varDirectoryToRead->getAbsolutePath($this->fileName);
        $path = $this->varDirectoryToRead->getRelativePath($filePath);
        $contentData = $this->varDirectoryToRead->readFile($path, true);
        $priceFileData = array();

        $rows = explode("\n", trim($contentData, "\n"));
        foreach ($rows as $rline) {
            if(!trim($rline)){
                continue;
            }
            $rline = trim($rline);
            $rline = trim($rline,',');

            $row = explode(',', trim($rline) );


            $keyRow = array();
            $i = 0;

            foreach($row as $value){
                if(!isset($this->fileHeader[$i])) {
                    continue;
                }
                $keyRow[$this->fileHeader[$i]] = $value;
                $i++;
            }

            $priceFileData[] = $keyRow;
        }
        return $priceFileData;
    }


    /**
     *  Update price and inventory and index on solr product which is change price status
     *
     */
    public function prepareData()
    {
        try {

            $dataresult = $this->_dbResource->getDataByBatchId($this->batch_id);
            $this->initData();
            foreach ($dataresult as $row) {
                $serdata = unserialize($row['content']);
                $this->updateData($serdata);
            }
        }
        catch (Exception $e) {
            $this->initData();
        }
    }

protected function indexInventoryPrice()
{
    // cataloginventory_price indexer
    $indexerIds = array( 'catalog_product_price' ) ;
    foreach($indexerIds as $indexerId) {
        $idx = $this->_indexerFactory->create()->load($indexerId);
        $idx->reindexAll($indexerId);
    }
}
    protected function initData()
{
    $this->productEntityTypeId =  $this->_dbResource->productEntityType();
}

    protected function updateData($data)
    {
        $entity_id = 0;
        try {

            $entity_id = $this->_dbResource->getProductIdBySku(trim($data['sku']));
            if( (int)$entity_id>0 && $data['price']>0.00 ) {
                $this->updatePrice($data, $entity_id);
                if($data['price']>$data['special_price']) {
                    $this->updateSpecialPrice($data, $entity_id);
                }
                else{
                    $this->failurereportdata[] = $entity_id." [".$data['sku']."] : special price couldn't process, greater than final price.";
                }
                $this->sucessreportdata[] = $entity_id." [".$data['sku']."] is successfully processed";
            }
            else{
                $this->failurereportdata[] = $data['sku']." : No product exist or price might zero ( ".$data['price']." )" ;
            }
        }catch(Exception $e) {
            $this->failurereportdata[] = $entity_id.','.$data['sku']." : EXCEPTION - ".$e->getMessage();
        }

    }
    
    /*
     * sku, special_from_date, special_to_date, special_price, price
     * 7541,2014-12-09,        2015-02-09,      11.658,        15.742
     * Array ( [sku] => 22104 [special_from_date] => 2013-03-28 [special_to_date] => 2024-03-27 [special_price] => 19.158 [price] => 20.825 )
     */
   
    protected function updatePrice($data,$entity_id)
    {
        
        $price =$data['price'];
        $priceAttributeId =  $this->_dbResource->productAttributeId($this->productEntityTypeId);
        $this->_dbResource->updateEntityAttribute($entity_id,$priceAttributeId,$price);
    }  
    
    protected function updateSpecialPrice($data, $entity_id)
    {
        $spFromDateAttributeId =  $this->_dbResource->productAttributeId($this->productEntityTypeId, 'special_from_date');
        $spToDateAttributeId =  $this->_dbResource->productAttributeId($this->productEntityTypeId,'special_to_date');
        $spriceAttributeId =  $this->_dbResource->productAttributeId($this->productEntityTypeId,'special_price');

        $price = $data['special_price'];
        $spFrom = (!$price)?NULL: $data['special_from_date'];
        $spTo = (!$price)?NULL:$data['special_to_date'];

        if(!$price) $price = 'NULL';
        /*$price =$data['special_price'];
        $spFrom =$data['special_from_date'];
        $spTo =$data['special_to_date'];*/

       // if((float)$price>0.00) {
            $this->_dbResource->updateEntityAttribute($entity_id, $spriceAttributeId, $price);
            $this->_dbResource->updateEntityAttribute($entity_id, $spFromDateAttributeId, $spFrom, 'datetime');
            $this->_dbResource->updateEntityAttribute($entity_id, $spToDateAttributeId, $spTo, 'datetime');
        //}
    }

    protected function processRoot($relativePath)
    {
        $eachPath = explode( '/',$relativePath );
        $i = count($eachPath );
        $fileName =   $eachPath[$i-1].'_'.date('Ymd-His');
        $eachPath[$i-1] = 'processed';
        $eachPath[$i] = $fileName;
        return implode('/',$eachPath);

    }

    public function log($message) {
        //\Zend\Log\Logger::INFO

        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/importpricefeed.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info($message);
    }

    /**
     *  To calculate time
     *
     */
    public function microtime_float()
    {
        list($usec, $sec) = explode(" ", microtime());
        return ((float) $usec + (float) $sec);
    }


 }
