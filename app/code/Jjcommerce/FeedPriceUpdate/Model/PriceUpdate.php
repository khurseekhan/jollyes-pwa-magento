<?php
/**
 * @category    Jjcommerce
 * @package     Jjcommerce_FeedPriceUpdate
 * @author	Jjcommerce Team<support@2jdesign.co.uk>
 */

namespace Jjcommerce\FeedPriceUpdate\Model;


class PriceUpdate extends \Magento\Framework\Model\AbstractModel
{

    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []    
    ){
       parent::__construct($context,$registry,$resource,$resourceCollection,$data);
    }
     
     /**
     * Constructor
     *
     * @return void
     */
    
    protected function _construct()
    {
        parent::_construct();
        $this->_init('Jjcommerce\FeedPriceUpdate\Model\ResourceModel\PriceUpdate');
    }    
    

}
