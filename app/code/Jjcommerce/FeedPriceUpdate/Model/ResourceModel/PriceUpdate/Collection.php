<?php
/**
 * @category    Jjcommerce
 * @package     Jjcommerce_FeedPriceUpdate
 * @author	Jjcommerce Team<support@2jdesign.co.uk>
 */

namespace Jjcommerce\FeedPriceUpdate\Model\ResourceModel\PriceUpdate;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Jjcommerce\FeedPriceUpdate\Model\PriceUpdate',
                     'Jjcommerce\FeedPriceUpdate\Model\ResourceModel\PriceUpdate');
    }
}
