<?php
/**
 * @category    Jjcommerce
 * @package     Jjcommerce_FeedPriceUpdate
 * @author	Jjcommerce Team<support@2jdesign.co.uk>
 */

namespace Jjcommerce\FeedPriceUpdate\Model\ResourceModel;

class PriceUpdate extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resources
     *
     * @return void     
     */
    protected function _construct()
    {
        $this->_init('jj_price_batch', 'id');
    }

    public function getScheduleFile(){

    $storetable = $this->getTable('jj_price_cron_schedule');
    $select = $this->getConnection()->select()->from(
        $storetable,
        ['file_name']
    );
   return $this->getConnection()->fetchOne($select);

   }

    public function getBatchId()
{

    $select = $this->getConnection()->select()->from(
        $this->getMainTable(),
        ['batch_id']
    )->order('batch_id DESC')->limit(1);

    return $this->getConnection()->fetchOne($select);
}
    public function setBatchId($data)
{

    $this->getConnection()->insert($this->getMainTable(), $data );
}

    public function getDataByBatchId($batchId)
{
    $select = $this->getConnection()->select()->from(
        $this->getMainTable(),
        ['content']
    )->where('batch_id="'.$batchId.'"');
    return $this->getConnection()->fetchAll($select);
}

    public function updateSheduleTable($fileName)
{
    $storetable = $this->getTable('jj_price_cron_schedule');
    $this->getConnection()->insert($storetable , array('file_name'=> $fileName) );
}

public function getProductIdBySku($sku)
{
    $entitytable = $this->getTable('catalog_product_entity');
    $select = $this->getConnection()->select()->from(
        $entitytable,
        ['entity_id']
    )->where('sku="'.$sku.'"');
    return $this->getConnection()->fetchOne($select);
}



    public function updateEntityAttribute($entity_id,$attribute_id,$value,$backendType='decimal')
{

    $storetable = $this->getTable('catalog_product_entity_'.$backendType);

    $select = $this->getConnection()->select()->from(
        $storetable,
        ['value_id']
    )->where('entity_id='.$entity_id." and attribute_id=".$attribute_id);

    $value_id = $this->getConnection()->fetchOne($select);
    if((int)$value_id>0) {
        if($backendType=='decimal') {
            $updQuery = "UPDATE `" . $storetable . "` SET `value` = " . $value . " WHERE (entity_id=" . $entity_id . " and attribute_id=" . $attribute_id . ")";
            $this->getConnection()->query($updQuery);
        }else {
            $this->getConnection()->update($storetable, array('value' => $value), 'entity_id=' . $entity_id . ' and attribute_id=' . $attribute_id);
        }
    }
    else{
        if($backendType=='decimal') {
            $updQuery = "INSERT INTO `catalog_product_entity_decimal` (`entity_id`,`attribute_id`,`value`,`store_id`) VALUES ( ".$entity_id.','.$attribute_id.','. $value ." , 0)";
            $this->getConnection()->query($updQuery);

        }else {
            $data = array(
                'value' => $value,
                'entity_id' => $entity_id,
                'attribute_id' => $attribute_id,
                'store_id' => 0
            );
            $this->getConnection()->insert($storetable, $data);
        }
    }
}   

    public function productEntityType()
    {
        $entityTypetable = $this->getTable('eav_entity_type');
        $select = $this->getConnection()->select()->from(
            $entityTypetable,
            ['entity_type_id']
        )->where('entity_type_code="catalog_product"');
        return $this->getConnection()->fetchOne($select);
    }

    public function productAttributeId($eav_entity_type,$attribute_code='price')
    {
        $attributetable = $this->getTable('eav_attribute');
        $select = $this->getConnection()->select()->from(
            $attributetable,
            ['attribute_id']
        )->where('entity_type_id='.$eav_entity_type.' and attribute_code="'.$attribute_code.'"');
        return  $this->getConnection()->fetchOne($select);
    }
    

    public function truncateBatch()
{
    $this->getConnection()->query( ' TRUNCATE '.$this->getMainTable() );
}

}