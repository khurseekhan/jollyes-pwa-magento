<?php
/**
 * @category    Jjcommerce
 * @package     Jjcommerce_FeedPriceUpdate
 * @author	Jjcommerce Team<support@2jdesign.co.uk>
 */
namespace Jjcommerce\FeedPriceUpdate\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use \Magento\Framework\DB\Ddl\Table;

class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        if (!$installer->tableExists('jj_price_batch')) {
            $table  = $installer->getConnection()
                ->newTable($installer->getTable('jj_price_batch'))
                ->addColumn(
                    'id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                    'Id'
                )
                ->addColumn(
                    'batch_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    '11',
                    [ 'unsigned' => true, 'nullable' => false],
                    'Batch Id'
                )
                ->addColumn(
                    'content',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    '64k',
                    [],
                    'content'
                )
                ->addColumn(
                    'created_at',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                    null,
                    ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
                    'Creation Time'
                )
                ->addColumn(
                    'status',
                    \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                    '1',
                    ['default' => 0],
                    'status'
                );
            $installer->getConnection()->createTable($table);
        }

        if (!$installer->tableExists('jj_price_cron_schedule')) {
            $table  = $installer->getConnection()
                ->newTable($installer->getTable('jj_price_cron_schedule'))
                ->addColumn(
                    'id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                    'Id'

                )->addColumn(
                    'file_name',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    '255',
                    [],
                    'file name'
                )
                ->addColumn(
                    'update_at',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                    null,
                    ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
                    'Update Time'
                )
                ->addColumn(
                    'status',
                    \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                    '1',
                    ['default' => 0],
                    'status'
                );
            $installer->getConnection()->createTable($table);
        }
        $installer->endSetup();
    }
}
