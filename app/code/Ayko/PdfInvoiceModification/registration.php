<?php
/**
 * Ayko_PdfInvoiceModification
 *
 * PHP version 5.x
 *
 * @category  PHP
 * @package   Ayko\PdfInvoiceModification
 * @author    Ayko Development Team <ayko@twojay.co>
 * @copyright Copyright (c) 2020 Ayko Limited (http://www.ayko.co/)
 */
\Magento\Framework\Component\ComponentRegistrar::register(
        \Magento\Framework\Component\ComponentRegistrar::MODULE,
        'Ayko_PdfInvoiceModification',
        __DIR__
    );