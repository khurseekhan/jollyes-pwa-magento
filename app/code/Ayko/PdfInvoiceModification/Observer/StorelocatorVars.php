<?php
/**
 * Ayko_PdfInvoiceModification
 *
 * PHP version 5.x
 *
 * @category  PHP
 * @package   Ayko\PdfInvoiceModification
 * @author    Ayko Development Team <support@twojay.co>
 * @copyright Copyright (c) 2020 Ayko Limited (http://www.ayko.co/)
 */
namespace Ayko\PdfInvoiceModification\Observer;
use Magento\Framework\Event\Observer;

class StorelocatorVars implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * Add custom variables for store locator.
     *
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        $order = $observer->getSource();

        if (!$order instanceof \Magento\Sales\Model\Order) {
            $order = $order->getOrder();
        }

        if ($order->getIsVirtual())
            return;

        if ($order->getStorelocatorId()) {
            $storeData = json_decode($order->getStorelocatorDescription(), true);
            foreach ($storeData as $varName => $value ) {
                $observer->getVariableList()->setData($varName, $value);
            }
        }
    }
}



