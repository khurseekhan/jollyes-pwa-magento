<?php
/**
 * Ayko_PdfInvoiceModification
 *
 * PHP version 5.x
 *
 * @category  PHP
 * @package   Ayko\PdfInvoiceModification
 * @author    Ayko Development Team <support@twojay.co>
 * @copyright Copyright (c) 2020 Ayko Limited (http://www.ayko.co/)
 */
namespace Ayko\PdfInvoiceModification\Sales\Model\Order\Pdf;
use TwoJay\Loyalty\Model\LoyaltyCoupons;

class PetClubVoucher extends \Magento\Sales\Model\Order\Pdf\Total\DefaultTotal
{
    /**
     * Get array of arrays with loyalty coupons total information for display in PDF
     *
     * @return array
     */
    public function getTotalsForDisplay()
    {
        $amount = $this->getOrder()->formatPriceTxt($this->getAmount());
        if ($this->getAmountPrefix()) {
            $amount = $this->getAmountPrefix() . $amount;
        }

        $title = __($this->getTitle());
        if ($this->getTitleSourceField()) {
            $label = $title . ' [' . $this->_getTitleDescription() . ']:';
        } else {
            $label = $title . ':';
        }

        $fontSize = $this->getFontSize() ? $this->getFontSize() : 7;
        $total = ['amount' => $amount, 'label' => $label, 'font_size' => $fontSize];
        return [$total];
    }

    /**
     * Get title description from source field
     *
     * @return mixed
     */
    private function _getTitleDescription()
    {
        $voucherData = json_decode($this->getTitleDescription(), true);
        $titleDesc = '';
        if ($voucherData) {
            $titleDesc = implode(", ", array_column($voucherData, LoyaltyCoupons::CODE));
        }
        return $titleDesc;
    }

}



