<?php
/**
 * Ayko_SegmentioAnalytics
 *
 * @category  Ayko
 * @package   Ayko_SegmentioAnalytics
 * @author    Ayko Development Team <support@ayko.co>
 * @copyright Copyright (c) 2019 Ayko (http://www.ayko.com/)
 *
 */
namespace Ayko\SegmentioAnalytics\Observer\Customer;

use Magento\Framework\App\RequestInterface;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer as EventObserver;
use Ayko\SegmentioAnalytics\Api\Segment\AnalyticsInterface;
use Magento\Framework\Message\ManagerInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;

class SignInTrack implements ObserverInterface
{
    /**
     * @var AnalyticsInterface
     */
    protected $analytics;

    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @var ManagerInterface
     */
    private $messageManager;

    /**
     * @var CustomerRepositoryInterface
     */
    protected $customerRepository;

    /**
     * SignInTrack constructor.
     * @param AnalyticsInterface $analytics
     * @param RequestInterface $request
     * @param ManagerInterface $messageManager
     * @param CustomerRepositoryInterface $customerRepository
     */
    public function __construct(
        AnalyticsInterface $analytics,
        RequestInterface $request,
        ManagerInterface $messageManager,
        CustomerRepositoryInterface $customerRepository
    ) {
        $this->analytics = $analytics;
        $this->request = $request;
        $this->messageManager = $messageManager;
        $this->customerRepository = $customerRepository;
    }

    /**
     * @param EventObserver $observer
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute(
        EventObserver $observer
    ) {
//        $registerPostParams = $this->getPostParams();
//        $postEmail = isset($registerPostParams['login']['username']) ? $registerPostParams['login']['username'] : $registerPostParams['email'];

        // Get customer email from observer
        $customerEmail = $observer->getEvent()->getCustomer()->getEmail();

        $customer = $this->getCustomer($customerEmail);
        $this->analytics->getSignInTrack($customer);
    }

    /**
     * @return array
     */
    protected function getPostParams()
    {
        return $this->request->getPost() ?? [];
    }

    /**
     * @param $email
     * @return CustomerInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function getCustomer($email)
    {
        try {
            return $this->customerRepository->get($email);
        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
        }
    }
}
