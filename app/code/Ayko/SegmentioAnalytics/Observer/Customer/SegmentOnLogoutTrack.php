<?php
/**
 * Ayko_SegmentioAnalytics
 *
 * @category  Ayko
 * @package   Ayko_SegmentioAnalytics
 * @author    Ayko Development Team <support@ayko.co>
 * @copyright Copyright (c) 2019 Ayko (http://www.ayko.com/)
 *
 */
namespace Ayko\SegmentioAnalytics\Observer\Customer;

use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer as EventObserver;
use Ayko\SegmentioAnalytics\Api\Segment\AnalyticsInterface;
use Ayko\SegmentioAnalytics\Model\ConfigInterface;
use Magento\Framework\Message\ManagerInterface;

class SegmentOnLogoutTrack implements ObserverInterface
{
    /**
     * @var AnalyticsInterface
     */
    protected $analytics;

    /**
     * @var ManagerInterface
     */
    private $messageManager;

    /**
     * @var CustomerRepositoryInterface
     */
    protected $customerRepository;

    /**
     * SegmentOnLogoutTrack constructor.
     * @param AnalyticsInterface $analytics
     * @param ManagerInterface $messageManager
     * @param CustomerRepositoryInterface $customerRepository
     */
    public function __construct(
        AnalyticsInterface $analytics,
        ManagerInterface $messageManager,
        CustomerRepositoryInterface $customerRepository
    ) {
        $this->analytics = $analytics;
        $this->messageManager = $messageManager;
        $this->customerRepository = $customerRepository;
    }

    /**
     * @param EventObserver $observer
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute(
        EventObserver $observer
    ) {
        $event = $observer->getEvent();
        $customer = $this->getCustomer($event->getCustomer()->getEmail());
        $this->analytics->getSignOutTrack($customer);
    }

    /**
     * @param $email
     * @return CustomerInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function getCustomer($email)
    {
        try {
            return $this->customerRepository->get($email);
        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
        }
    }
}
