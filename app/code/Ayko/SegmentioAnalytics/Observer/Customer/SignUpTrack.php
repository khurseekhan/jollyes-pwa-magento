<?php
/**
 * Ayko_SegmentioAnalytics
 *
 * @category  Ayko
 * @package   Ayko_SegmentioAnalytics
 * @author    Ayko Development Team <support@ayko.co>
 * @copyright Copyright (c) 2019 Ayko (http://www.ayko.com/)
 *
 */
namespace Ayko\SegmentioAnalytics\Observer\Customer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer as EventObserver;
use Ayko\SegmentioAnalytics\Api\Segment\AnalyticsInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Message\ManagerInterface;

class SignUpTrack implements ObserverInterface
{
    /**
     * @var AnalyticsInterface
     */
    protected $analytics;

    /**
     * @var CustomerRepositoryInterface
     */
    protected $_customerRepository;

    /**
     * @var RequestInterface
     */
    protected $_request;

    /**
     * @var ManagerInterface
     */
    private $messageManager;

    /**
     * SignUpTrack constructor.
     * @param AnalyticsInterface $analytics
     * @param CustomerRepositoryInterface $customerRepository
     * @param RequestInterface $request
     * @param ManagerInterface $messageManager
     */
    public function __construct(
        AnalyticsInterface $analytics,
        CustomerRepositoryInterface $customerRepository,
        RequestInterface $request,
        ManagerInterface $messageManager
    ) {
        $this->analytics = $analytics;
        $this->_request = $request;
        $this->_customerRepository = $customerRepository;
        $this->messageManager = $messageManager;
    }

    /**
     * @param EventObserver $observer
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute(
        EventObserver $observer
    ) {
        $registerPostParams = $this->getPostParams();
        $postEmail = isset($registerPostParams['email']) ? $registerPostParams['email'] : '';
        $customer = $this->getCustomer($postEmail);
        $registerData = $observer->getData('register_data');
        $this->analytics->getSignUpTrack($customer, $registerData);
    }

    /**
     * @return array
     */
    protected function getPostParams()
    {
        return $this->_request->getPost() ?? [];
    }

    /**
     * @param $email
     * @return CustomerInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function getCustomer($email)
    {
        try {
            return $this->_customerRepository->get($email);
        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
        }
    }
}
