<?php
/**
 * Ayko_SegmentioAnalytics
 *
 * @category  Ayko
 * @package   Ayko_SegmentioAnalytics
 * @author    Ayko Development Team <support@ayko.co>
 * @copyright Copyright (c) 2019 Ayko (http://www.ayko.com/)
 *
 */
namespace Ayko\SegmentioAnalytics\Observer\Order;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer as EventObserver;
use Ayko\SegmentioAnalytics\Api\Segment\AnalyticsInterface;

class SuccessOrderTrack implements ObserverInterface
{
    /**
     * @var AnalyticsInterface
     */
    protected $analytics;

    /**
     * SignInTrack constructor.
     * @param AnalyticsInterface $analytics
     */
    public function __construct(
        AnalyticsInterface $analytics
    ) {
        $this->analytics = $analytics;
    }

    /**
     * @param EventObserver $observer
     */
    public function execute(
        EventObserver $observer
    ) {
        $orderIds = $observer->getEvent()->getOrderIds();
        if (is_array($orderIds)) {
            $this->analytics->getOrderSuccessTrack($orderIds);
        }
    }
}
