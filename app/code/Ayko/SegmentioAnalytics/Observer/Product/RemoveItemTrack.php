<?php
/**
 * Ayko_SegmentioAnalytics
 *
 * @category  Ayko
 * @package   Ayko_SegmentioAnalytics
 * @author    Ayko Development Team <support@ayko.co>
 * @copyright Copyright (c) 2019 Ayko (http://www.ayko.com/)
 *
 */
namespace Ayko\SegmentioAnalytics\Observer\Product;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer as EventObserver;
use Ayko\SegmentioAnalytics\Api\Segment\AnalyticsInterface;
use Magento\Checkout\Model\Session as CheckoutSession;

class RemoveItemTrack implements ObserverInterface
{
    /**
     * @var AnalyticsInterface
     */
    protected $analytics;

    /**
     * @var CheckoutSession
     */
    protected $checkoutSession;

    /**
     * RemoveItemTrack constructor.
     * @param AnalyticsInterface $analytics
     * @param CheckoutSession $checkoutSession
     */
    public function __construct(
        AnalyticsInterface $analytics,
        CheckoutSession $checkoutSession
    ) {
        $this->analytics = $analytics;
        $this->checkoutSession = $checkoutSession;
    }

    /**
     * @param EventObserver $observer
     */
    public function execute(
        EventObserver $observer
    ) {
        $quote = $this->checkoutSession->getQuote();
        $quoteItem = $observer->getEvent()->getQuoteItem();
        $this->analytics->removeItemTrack($quote, $quoteItem);
    }
}
