<?php
/**
 * Ayko_SegmentioAnalytics
 *
 * @category  Ayko
 * @package   Ayko_SegmentioAnalytics
 * @author    Ayko Development Team <support@ayko.co>
 * @copyright Copyright (c) 2019 Ayko (http://www.ayko.com/)
 *
 */
namespace Ayko\SegmentioAnalytics\Observer\Product;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer as EventObserver;
use Ayko\SegmentioAnalytics\Api\Segment\AnalyticsInterface;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Catalog\Model\ProductFactory;
use Magento\Catalog\Helper\Data;
use Magento\Framework\Registry;
use Magento\Framework\App\RequestInterface;

class AddToCartTrack implements ObserverInterface
{
    /**
     * @var AnalyticsInterface
     */
    protected $analytics;

    /**
     * @var CheckoutSession
     */
    protected $checkoutSession;

    /**
     * @var ProductFactory
     */
    protected $productFactory;

    /**
     * @var Data
     */
    protected $breadcrumbData;

    /**
     * @var Registry
     */
    protected $_coreRegistry;

    /**
     * @var RequestInterface
     */
    protected $_request;

    /**
     * AddToCartTrack constructor.
     * @param AnalyticsInterface $analytics
     * @param CheckoutSession $checkoutSession
     * @param ProductFactory $productFactory
     * @param Data $breadcrumbData
     * @param Registry $coreRegistry
     * @param RequestInterface $request
     */
    public function __construct(
        AnalyticsInterface $analytics,
        CheckoutSession $checkoutSession,
        ProductFactory $productFactory,
        Data $breadcrumbData,
        Registry $coreRegistry,
        RequestInterface $request
    ) {
        $this->_request = $request;
        $this->analytics = $analytics;
        $this->checkoutSession = $checkoutSession;
        $this->productFactory = $productFactory;
        $this->breadcrumbData = $breadcrumbData;
        $this->_coreRegistry = $coreRegistry;
    }

    /**
     * @param EventObserver $observer
     */
    public function execute(
        EventObserver $observer
    ) {
        $product = $observer->getEvent()->getProduct();
        $quote = $this->checkoutSession->getQuote();
        $quoteItem = $observer->getEvent()->getQuoteItem();
        $this->analytics->addToCartTrack($product, $quote, $quoteItem);
    }
}
