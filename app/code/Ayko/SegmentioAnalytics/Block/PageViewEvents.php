<?php
/**
 * Ayko_SegmentioAnalytics
 *
 * @category  Ayko
 * @package   Ayko_SegmentioAnalytics
 * @author    Ayko Development Team <support@ayko.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.ayko.com/)
 *
 */

namespace Ayko\SegmentioAnalytics\Block;

use Magento\Framework\App\Request\Http;
use Magento\Framework\App\Response\RedirectInterface;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Checkout\Model\Session;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Store\Model\StoreManagerInterface;
use Ayko\SegmentioAnalytics\Model\Segment\Analytics;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\ObjectManagerInterface;
use Magento\Catalog\Model\ProductRepository;
use Magento\Store\Model\ScopeInterface;
use Ayko\SegmentioAnalytics\Helper\Data;

/**
 * Class Analytics
 * @package Ayko\SegmentioAnalytics\Block
 */
class PageViewEvents extends Template
{
    const AUTHENTICATED = 1;

    const NOT_AUTHENTICATED = 0;

    /**
     * @var Session
     */
    protected $checkoutSession;

    /**
     * @var Registry
     */
    protected $registry;

    /**
     * @var
     */
    protected $request;

    /**
     * @var RedirectInterface
     */
    private $redirect;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var CustomerSession
     */
    private $customerSession;

    /**
     * @var Analytics
     */
    private $analytics;

    /**
     * Instance of serializer.
     *
     * @var Json
     */
    private $serializer;

    /**
     * @var ObjectManagerInterface
     */
    protected $objectManagerInterface;

    /**
     * @var ProductRepository
     */
    protected $productRepository;

    /**
     * @var Data
     */
    protected $helper;

    /**
     * PageViewEvents constructor.
     * @param Session $checkoutSession
     * @param CustomerSession $customerSession
     * @param Http $request
     * @param Registry $registry
     * @param RedirectInterface $redirect
     * @param StoreManagerInterface $storeManager
     * @param Analytics $analytics
     * @param Json|null $serializer
     * @param ObjectManagerInterface $objectManagerInterface
     * @param ProductRepository $productRepository
     * @param Context $context
     * @param Data $helper
     */
    public function __construct(
        Session $checkoutSession,
        CustomerSession $customerSession,
        Http $request,
        Registry $registry,
        RedirectInterface $redirect,
        StoreManagerInterface $storeManager,
        Analytics $analytics,
        Json $serializer = null,
        ObjectManagerInterface $objectManagerInterface,
        ProductRepository $productRepository,
        Context $context,
        Data $helper
    )
    {
        $this->objectManagerInterface = $objectManagerInterface;
        $this->serializer             = $serializer ?: $this->objectManagerInterface->get(Json::class);
        $this->checkoutSession        = $checkoutSession;
        $this->customerSession        = $customerSession;
        $this->registry               = $registry;
        $this->redirect               = $redirect;
        $this->request                = $request;
        $this->storeManager           = $storeManager;
        $this->analytics              = $analytics;
        $this->productRepository      = $productRepository;
        $this->helper = $helper;
        parent::__construct(
            $context
        );

    }

    /**
     *  Get Identity Event Data
     * @return string
     */
    public function getIdentityData()
    {
        $customer            = $this->customerSession->getCustomer();
        $loyaltyNumber       = ($customer->getLoyaltyNumber())?:null;
        $contactByEmail      = ($customer->getData('contact_by_email')) ? true : false;
        $contactBySms        = ($customer->getData('contact_by_sms')) ? true : false;
        $date                = date_create($customer->getCreatedAt());
        $createdAt           = date_format($date, "Y/m/d H:i:s");
        $trackingInformation = [
            "userId" => ($loyaltyNumber) ? $loyaltyNumber :'',
            "email" => $customer->getEmail(),
            "first_name" => $customer->getFirstname(),
            "last_name" => $customer->getLastname(),
            "phone" => $this->analytics->getTelephone($customer),
            "created_at" => $createdAt,
            // "loyalty_points_balance" => 0,
            // "loyalty_voucher_balance" => 0,
            "opt_in_email" => $contactByEmail,
            "opt_in_sms" => $contactBySms,
            "signup_source" => Analytics::SOURCE_TYPE
        ];
        $tracking            = $this->serializer->serialize($trackingInformation);

        return $tracking;
    }

    /**
     * @return string
     */
    public function getIdentifier()
    {
        $loyaltyNumber = $this->customerSession->getCustomer()->getLoyaltyNumber();

        return ($loyaltyNumber) ?: null;
    }

    /**
     * @return array
     */
    public function getTrackingData()
    {
        $request = $this->request->getFullActionName();
        $value   = [];
        switch ($request) {
            case "customer_account_create":
                $value = ["page_type" => "Signup", "authenticated" => self::NOT_AUTHENTICATED];
                break;
            case "customer_account_login":
                $value = ["page_type" => "Login", "authenticated" => self::NOT_AUTHENTICATED];
                break;
            case "customer_account_index":
                $value = ["page_type" => "Dashboard", "authenticated" => self::AUTHENTICATED];
                break;
            case "petsinfo_pets_index":
                $value = ["page_type" => "My Pets", "authenticated" => self::AUTHENTICATED];
                break;
            case "loyalty_point_history":
                $value = ["page_type" => "PetCLUB Points", "authenticated" => self::AUTHENTICATED];
                break;
            case "loyalty_voucher_history":
                $value = ["page_type" => "PetCLUB Vouchers", "authenticated" => self::AUTHENTICATED];
                break;
            case "loyalty_earnpoints_index":
                $value = ["page_type" => "Earn PetCLUB Points", "authenticated" => self::AUTHENTICATED];
                break;
            case "customer_account_edit":
                $value = ["page_type" => "Account Information", "authenticated" => self::AUTHENTICATED];
                break;
            case "customer_address_index":
                $value = ["page_type" => "Address Book", "authenticated" => self::AUTHENTICATED];
                break;
            case "sales_order_history":
                $value = ["page_type" => "Online Orders", "authenticated" => self::AUTHENTICATED];
                break;
            case "review_customer_index":
                $value = ["page_type" => "Product Reviewed", "authenticated" => self::AUTHENTICATED];
                break;
            case "magento_customerbalance_info_index":
                $value = ["page_type" => "Store Credit", "authenticated" => self::AUTHENTICATED];
                break;
            case "newsletter_manage_index":
                $value = ["page_type" => "Newsletter Subscription", "authenticated" => self::AUTHENTICATED];
                break;
            case "catalog_product_view":
                $product              = $this->getProduct();
                $standardCategoryPath = $product->getStandardCategoryPath();
                $value = $this->getStandardCategoryPath($standardCategoryPath);
                break;
            case "catalog_category_view":
                $standardCategoryPath = $this->getCategoryLevels();
                $value = $this->getStandardCategoryPath($standardCategoryPath);
                break;
            default:
                $value = false;
                break;
        }

        return $value;
    }

    /**
     * Return the category levels
     *
     * @return array
     */
    public function getCategoryLevels()
    {
        return $this->helper->getCategoryLevels();
    }

    /**
     * @param $categories
     * @return array
     */
    protected  function getStandardCategoryPath($categories)
    {
        if (isset($categories) && !is_array($categories)) {
            $categories = explode(",", $categories);
        }

        $categoryData = [
            "category_level_1" => trim($categories['0'] ?? ''),
            "category_level_2" => trim($categories['1'] ?? ''),
            "category_level_3" => trim($categories['2'] ?? '')
        ];

        $productStandardCategory = $this->serializer->serialize($categoryData);
        $value                   = ["page_type" => "Product Page", "standard_category" => $productStandardCategory];

        return $value;
    }

    /**
     * Return the current GET/POST parameters
     *
     * @return array
     */
    protected function getParams()
    {
        return $this->helper->getParams();
    }

    /**
     * Get the referrer url
     *
     * @return string
     */
    protected function getReferrer()
    {
        return $this->redirect->getRedirectUrl();
    }

    /**
     *  Get Current Product
     * @return object
     */
    protected function getProduct()
    {
        return $this->registry->registry('current_product');
    }

    /**
     * Get config
     * @param $path
     * @param null $store
     * @return mixed
     */
    public function getConfig($path, $store = null)
    {
        return $this->_scopeConfig->getValue(
            $path,
            ScopeInterface::SCOPE_STORE,
            $store
        );
    }
}
