<?php
/**
 * Ayko_SegmentioAnalytics
 *
 * @category  Ayko
 * @package   Ayko_SegmentioAnalytics
 * @author    Ayko Development Team <support@ayko.co>
 * @copyright Copyright (c) 2019 Ayko (http://www.ayko.com/)
 *
 */
namespace Ayko\SegmentioAnalytics\Block;

use Magento\Framework\View\Element\Template;
use Ayko\SegmentioAnalytics\Helper\Data;
use Magento\Store\Model\ScopeInterface;

class Analytics extends Template
{
    /**
     * @var Data
     */
    private $_analyticsData;

    /**
     * Analytics constructor.
     * @param Template\Context $context
     * @param Data $analyticsData
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        Data $analyticsData,
        array $data = []
    ) {
        $this->_analyticsData = $analyticsData;
        parent::__construct($context, $data);
    }

    /**
     * Get config
     *
     * @param string $path
     * @return mixed
     */
    public function getConfig($path)
    {
        return $this->_scopeConfig->getValue(
            $path,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Render Segment Io Analytics tracking scripts
     *
     * @return string
     */
    protected function _toHtml()
    {
        if (!$this->_analyticsData->isAnalyticsAvailable()) {
            return '';
        }

        return parent::_toHtml();
    }
}
