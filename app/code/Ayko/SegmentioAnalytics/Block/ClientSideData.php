<?php
/**
 * Ayko_SegmentioAnalytics
 *
 * @category  Ayko
 * @package   Ayko_SegmentioAnalytics
 * @author    Ayko Development Team <support@ayko.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.ayko.com/)
 *
 */

namespace Ayko\SegmentioAnalytics\Block;

use Magento\Catalog\Model\ProductFactory;
use Magento\Eav\Api\AttributeRepositoryInterface;
use Magento\Eav\Model\Entity\Collection\AbstractCollection;
use Magento\Framework\App\Request\Http;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\Response\RedirectInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Catalog\Block\Product\ListProduct;
use Magento\Checkout\Model\Session;
use Magento\Quote\Model\Quote\Item;
use Magento\SalesRule\Model\Coupon;
use Magento\SalesRule\Model\Rule;
use Magento\Catalog\Model\ProductRepository;
use Ayko\SegmentioAnalytics\Helper\Data;

/**
 * Class Analytics
 * @package Ayko\SegmentioAnalytics\Block
 */
class ClientSideData extends Template
{

    protected $product;

    /**
     * @var Session
     */
    protected $checkoutSession;

    /**
     * @var ListProduct
     */
    protected $listProduct;

    /**
     * @var AttributeRepositoryInterface
     */
    protected $eavAttributeRepository;

    /**
     * @var Registry
     */
    protected $registry;

    /**
     * @var Http
     */
    protected $httpRequest;

    /**
     * @var ProductRepository
     */
    protected $productRepository;

    /**
     * @var RedirectInterface
     */
    private $redirect;

    /**
     * @var ProductFactory
     */
    protected $productFactory;

    /**
     * @var Coupon
     */
    protected $coupon;

    /**
     * @var Rule
     */
    protected $saleRule;

    /**
     * @var \Magento\Quote\Model\Cart\CartTotalRepository
     */
    protected $cartTotalRepository;

    /**
     * @var \Magento\Catalog\Helper\ImageFactory
     */
    protected $imageHelperFactory;

    /**
     * @var Data
     */
    protected $helper;

    /**
     * @var Http|RequestInterface
     */
    private $request;

    /**
     * ClientSideData constructor.
     * @param Session $checkoutSession
     * @param ListProduct $listProduct
     * @param Http $httpRequest
     * @param AttributeRepositoryInterface $eavAttributeRepositoryInterface
     * @param Registry $registry
     * @param Coupon $coupon
     * @param Rule $saleRule
     * @param RedirectInterface $redirect
     * @param Context $context
     * @param \Magento\Catalog\Helper\ImageFactory $imageHelperFactory
     * @param \Magento\Quote\Model\Cart\CartTotalRepository $cartTotalRepository
     * @param ProductRepository $productRepository
     * @param Data $helper
     * @param RequestInterface $request
     */
    public function __construct(
        Session $checkoutSession,
        ListProduct $listProduct,
        Http $httpRequest,
        AttributeRepositoryInterface $eavAttributeRepositoryInterface,
        Registry $registry,
        Coupon $coupon,
        Rule $saleRule,
        RedirectInterface $redirect,
        Context $context,
        \Magento\Catalog\Helper\ImageFactory $imageHelperFactory,
        \Magento\Quote\Model\Cart\CartTotalRepository $cartTotalRepository,
        ProductRepository $productRepository,
        Data $helper,
        RequestInterface $request
    )
    {
        $this->checkoutSession        = $checkoutSession;
        $this->listProduct            = $listProduct;
        $this->eavAttributeRepository = $eavAttributeRepositoryInterface;
        $this->registry               = $registry;
        $this->redirect               = $redirect;
        $this->httpRequest            = $httpRequest;
        $this->coupon                 = $coupon;
        $this->saleRule               = $saleRule;
        $this->imageHelperFactory     = $imageHelperFactory;
        $this->cartTotalRepository    = $cartTotalRepository;
        $this->productRepository     = $productRepository;
        $this->helper = $helper;
        $this->request = $request;
        parent::__construct(
            $context
        );

    }

    /**
     * Return the current Cart/Basket ID
     *
     * @return int
     */
    public function getCartId()
    {
        return $this->getQuote()->getId();
    }

    /**
     * @return string
     */
    public function getCouponCode()
    {
        return $this->getQuote()->getCouponCode();
    }

    /**
     * @return mixed
     */
    public function getTax()
    {
        return $this->getQuote()->getShippingAddress()->getData('tax_amount');
    }

    /**
     * @return string
     */
    public function getCurrency()
    {
        return $this->getQuote()->getQuoteCurrencyCode();
    }

    /**
     * @return float
     */
    public function getTotal()
    {
        return $this->getQuote()->getGrandTotal();
    }

    /**
     * @return Item[]
     */
    public function getCartItems()
    {
        return $this->getQuote()->getAllVisibleItems();
    }

    /**
     * @return float
     */
    public function getshipping()
    {
        return $this->getQuote()->getShippingAddress()->getShippingAmount();
    }

    /**
     * @return \Magento\Quote\Model\Quote
     */
    public function getQuote()
    {
        return $this->checkoutSession->getQuote();
    }

    /**
     * @return float|null
     * @throws NoSuchEntityException
     */
    public function getDiscountAmount()
    {
        $totals = $this->cartTotalRepository->get($this->getQuote()->getId());
        return abs($totals->getDiscountAmount());
    }

    /**
     * @param null $product
     * @return string
     */
    public function getImage($product = null)
    {
        if ($product) {
            return $this->imageHelperFactory->create()
                ->init($product, 'product_base_image')->getUrl();
        }
        return '';
    }

    /**
     * Return the current product collection from the category page
     *
     * @return AbstractCollection
     */
    public function getProductList()
    {
        return $this->listProduct->getLoadedProductCollection();
    }

    /**
     * Return the attribute label from the attribute code and selected id
     *
     * @param $code
     * @param $id
     * @return mixed
     * @throws NoSuchEntityException
     */
    public function getAttributeLabel($code, $id)
    {
        $attribute = $this->eavAttributeRepository->get(
            \Magento\Catalog\Model\Product::ENTITY,
            $code
        );

        $key = array_search($id, array_column($attribute->getSource()->getAllOptions(), 'value'));
        return $attribute->getSource()->getAllOptions()[$key]['label'];
    }

    /**
     * Return the category levels
     *
     * @return array
     */
    public function getCategoryLevels()
    {
        return $this->helper->getCategoryLevels();
    }

    /**
     * Get default categories for Product
     *
     * @param null $product
     * @return array
     */
    public function getProductStandardCategories($product = null)
    {
        $categories = [];
        if ($product) {

            $categoryPath = $product->getData('standard_category_path');
            if(empty($categoryPath)){
                $categoryPath = $product->getData('category_path');
            }
            if (isset($categoryPath)) {
                $categories = explode(",", $categoryPath);
            }
        }
        return $categories;
    }

    /**
     * Return the current GET/POST parameters
     *
     * @return array
     */
    public function getParams()
    {
        return $this->helper->getParams();
    }

    /**
     * Check to see if we have any selected filters on the category page
     *
     * @return bool
     */
    public function hasFilters()
    {
        if (sizeof($this->getParams()) > 2) {
            return true;
        }
        return false;
    }

    /**
     * Strip out elements from array that are not filters
     *
     * @return array
     */
    public function getFilters()
    {
        $filters = $this->getParams();
        unset($filters['id']);
        unset($filters['am_base_price']);

        return $filters;
    }

    /**
     * Get the current product object
     *
     * @return mixed
     */
    public function getProductInfo()
    {
        return $this->registry->registry('current_product');
    }

    /**
     * @param $couponCode
     * @return float
     */
    public function getDiscount($couponCode)
    {
        $ruleId = $this->coupon->loadByCode($couponCode)->getRuleId();
        $rule   = $this->saleRule->load($ruleId);
        return $rule->getDiscountAmount();
    }

    /**
     * Get the referrer url
     *
     * @return string
     */
    public function getReferrer()
    {
        return $this->request->getServer('HTTP_REFERER');
    }

    /**
     * Get store URL
     *
     * @return mixed
     * @throws NoSuchEntityException
     */
    public function getStoreUrl()
    {
        return str_replace(['https://', 'http://'], ['', ''], $this->_storeManager->getStore()->getBaseUrl());
    }

    /**
     * Get product object by id
     *
     * @param $id
     * @return mixed
     * @throws NoSuchEntityException
     */
    public function getProductById($id)
    {
        return $this->productRepository->getById($id);
    }

    /**
     * @param $product
     * @return string
     */
    public function getImageFullPath($productImage)
    {
        return $this->helper->getImageFullPath($productImage);
    }
}
