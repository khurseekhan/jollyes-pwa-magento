var config = {
    config: {
        mixins: {
            'Magento_Catalog/js/catalog-add-to-cart': {
                'Ayko_SegmentioAnalytics/js/catalog-add-to-cart-mixin': true
            }
        }
    },
    deps: [
        "Ayko_SegmentioAnalytics/js/segment-analytics"
    ]

};