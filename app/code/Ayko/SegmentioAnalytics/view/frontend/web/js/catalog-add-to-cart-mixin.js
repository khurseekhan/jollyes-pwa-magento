define([
    'jquery'
], function ($) {
    'use strict';

    return function (widget) {

        var summary_count = 0;

        $.widget('mage.catalogAddToCart', widget, {
            /**
             * Handler for the form 'submit' event
             *
             * @param {jQuery} form
             */
            submitForm: function (form) {

                //console.log(form.closest('.product-item-info'));

                this.ajaxSubmit(form); // original code
                this.segmentTracking(); // add every thing you want
            },
            /**
             * Do something
             */
            segmentTracking: function () {

                $(document).ajaxComplete(function (event, xhr, settings) {

                    if (settings.url.indexOf("sections=cart") > 0 && settings.type === "GET") {
                        let cartObj = xhr.responseJSON;

                        if (summary_count !== cartObj.cart.summary_count) {
                            summary_count = cartObj.cart.summary_count;
                            let productObj = [];
                            cartObj.cart.items.forEach(function (element) {
                                productObj.push(getProductData(element));
                            });
                            analytics.track('Cart Viewed', getSegmentHeader(productObj));
                        }

                    }
                });

            },
        });
        return $.mage.catalogAddToCart;


        function getSegmentHeader(products) {
            return {
                products: products
            }
        }

        function getProductData(arrayElements) {
            let priceExtract = arrayElements.product_price.substring(arrayElements.product_price.indexOf("£") + 1);
            //console.log(priceExtract);
            return {
                product_id: arrayElements.product_id,
                name: arrayElements.name,
                sku: arrayElements.product_sku,
                category_level_1: arrayElements.category_level_1,
                category_level_2: arrayElements.category_level_2,
                category_level_3: arrayElements.category_level_3,
                brand: arrayElements.brand,
                price: parseFloat(priceExtract),
                url: arrayElements.product_url,
                image_url: arrayElements.image,
                lifestage: arrayElements.lifestage,
                breed_size: arrayElements.breed_size,
                dietary_needs: arrayElements.dietary_need,
                main_flavour: arrayElements.main_flavour,
                colour: arrayElements.colour,
                quantity: arrayElements.qty,
                pack_weight: arrayElements.pack_weight,
                revenue: parseFloat(arrayElements.product_price_value)
            };
        }
    }
});