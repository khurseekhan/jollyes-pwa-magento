/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

require([
    'jquery',
    'jquery/ui',
    'mage/validation',
    'mage/storage',
    'jquery/jquery-storageapi'
], function ($) {
    'use strict';

    var storage

    // Detect changes to history
    (function (history) {
        var pushState = history.pushState;
        history.pushState = function (state) {
            if (typeof history.onpushstate == "function") {
                history.onpushstate({state: state});
            }
            // whatever else you want to do
            // maybe call onhashchange e.handler
            return pushState.apply(history, arguments);
        }
    })(window.history);

    $(document).ready(function () {

        /**
         * Segment Tracking
         * Sort by Options
         */
        let sorter = $('#sorter');
        let selectedSort = sorter.find(":selected").text().trim();

        sorter.change(function () {
            selectedSort = $(this).find(":selected").text().trim();
        });

        /**
         * Get the header for the return object
         *
         * @param products
         * @param isProduct
         * @param filterOptions
         * @param sortOptions
         * @param isProduct
         */
        function getSegmentHeader(products, isProduct = false, filterOptions = false, sortOptions = false) {
            if (isProduct === false) {

                if (filterOptions !== false && sortOptions === false) {
                    return {
                        cart_id: segmentData.cart_id,
                        category_level_1: segmentData.category_level_1,
                        category_level_2: segmentData.category_level_2,
                        category_level_3: segmentData.category_level_3,
                        filters: Object.assign([], filterOptions),
                        products: Object.assign([], products)
                    }
                } else if (sortOptions !== false && filterOptions === false) {
                    return {
                        cart_id: segmentData.cart_id,
                        category_level_1: segmentData.category_level_1,
                        category_level_2: segmentData.category_level_2,
                        category_level_3: segmentData.category_level_3,
                        sort: sortOptions,
                        products: Object.assign([], products)
                    }
                } else if (filterOptions !== false && sortOptions !== false) {
                    return {
                        cart_id: segmentData.cart_id,
                        category_level_1: segmentData.category_level_1,
                        category_level_2: segmentData.category_level_2,
                        category_level_3: segmentData.category_level_3,
                        filters: Object.assign([], filterOptions),
                        sort: sortOptions,
                        products: Object.assign([], products)
                    }
                } else {
                    return {
                        cart_id: segmentData.cart_id,
                        category_level_1: segmentData.category_level_1,
                        category_level_2: segmentData.category_level_2,
                        category_level_3: segmentData.category_level_3,
                        products: Object.assign([], products)
                    }
                }

            }
        }


        /**
         * Get the product data from the node
         *
         * @param productObj
         * @returns {{image_url: *, pack_weight: *, url: *, category_level_1: *, category_level_3: *, colour: *, category_level_2: *, breed_size: *, price: *, name: *, lifestage: *, id: *, position: *, sku: *, main_flavour: *, brand: *, dietary_needs: *}}
         */
        function getProductData(productObj) {
            return {
                product_id: (productObj.data('product-id')).toString(),
                sku: (productObj.data('product-sku')).toString(),
                category_level_1: productObj.data('product-category-level-1'),
                category_level_2: productObj.data('product-category-level-2'),
                category_level_3: productObj.data('product-category-level-3'),
                name: productObj.data('product-name'),
                image_url: productObj.data('product-imageurl'),
                lifestage: productObj.data('product-lifestage'),
                breed_size: productObj.data('product-breedsize'),
                dietary_needs: productObj.data('product-dietaryneeds'),
                main_flavour: productObj.data('product-mainflavour'),
                colour: productObj.data('product-colour'),
                pack_weight: productObj.data('product-packweight'),
                brand: productObj.data('product-brand'),
                price: parseFloat(productObj.data('product-price')),
                position: productObj.data('product-position'),
                url: productObj.find('.product-item-photo').attr('href')
            };
        }

        /**
         * Segment Tracking
         * Product Listing Viewed
         */
        function productListViewed() {
            if(typeof analytics != "undefined") {
                if ($('body').hasClass('catalog-category-view') || $('body').hasClass('catalogsearch-result-index')) {
                    let products = [], filters = [];
                    $("li.product-item > div.product-item-info").each(function (index) {
                        if ($(this).data('product-id')) {
                            products.push(getProductData($(this)));
                        }
                    });
                    analytics.track('Product List Viewed', getSegmentHeader(products, false, false, selectedSort));

                    // Get filters if filters are applied on page-load
                    $('#am-shopby-container .items > li').each(function () {
                        let type = $(this).find('.filter-label').text();
                        let value = $(this).find('.filter-value').text();
                        if (type.length > 0) {
                            let filter = {
                                type: type,
                                value: value
                            };
                            filters.push(filter);
                        }
                    });

                    // Send filtered event to Segment if filters are non-empty
                    if(filters.length > 0 && typeof analytics != "undefined") {
                        analytics.track('Product List Filtered', getSegmentHeader(products, false, filters, selectedSort));
                    }

                }

                if ($('body').hasClass('customer-account-logoutsuccess')) {
                    analytics.reset();
                }
            }
        }

        productListViewed();

        $(document).on('click','.styled-sorter-options li a',function(){
            if(!$(".sidebar.sidebar-main .filter-content > div").hasClass("am-filter-current")){
                selectedSort = $(this).text().trim();
                productListViewed();
            }
        })

            .on('change','.product-options-wrapper .super-attribute-select',function(){
                var getConfigOptionVal = $("input[name='selected_configurable_option']").val();
                var getCategoryPath = $("input[name='category_path']").val();
                var base_url = window.location.origin;
                var serviceUrl = base_url+'/segment/product/view';
                if(getConfigOptionVal){
                    $.ajax({
                        url:serviceUrl,
                        type: 'POST',
                        dataType: 'json',
                        data: {'product_id': getConfigOptionVal, 'standard_category': getCategoryPath},
                        success: function(result){
                            if(typeof analytics != "undefined") {
                                analytics.track('Product Viewed', result);
                            }
                        }
                    });
                }
            });

        /**
         * Segment Tracking
         * Product List Viewed - Filtered
         */

        $(document).ajaxComplete(function (event, xhr, settings) {

            if (settings.url.indexOf("amshopby%5B") > 0 && settings.type === "GET") {

                let products = [], brandFilter, filters = [];

                // Get visible products
                $("li.product-item > div.product-item-info").each(function (index) {
                    if ($(this).data('product-id')) {
                        products.push(getProductData($(this)));
                    }
                });

                // Get filters
                $('#am-shopby-container .items > li').each(function () {
                    let type = $(this).find('.filter-label').text();
                    let value = $(this).find('.filter-value').text();
                    if (type.length > 0) {
                        let filter = {
                            type: type,
                            value: value
                        };
                        filters.push(filter);
                    }
                });

                // Get sort by option
                selectedSort = $('#sorter').find(":selected").text().trim();

                // Send data to Segment
                if(filters.length > 0 && typeof analytics != "undefined") {
                    analytics.track('Product List Filtered', getSegmentHeader(products, false, filters, selectedSort));
                } else {
                    if($(".filter-options input[type='radio']").length > 0) {
                        productListViewed();
                    }
                }

            }

        });

        /**
         * Segment Tracking
         * mini cart Clicked
         */
        $('.showcart').click(function (ev) {
            if (!$(this).hasClass('active') && typeof analytics != "undefined") {
                let productObj = [];
                storage = $.initNamespaceStorage('mage-cache-storage').localStorage;
                let cartStorage = storage.get('cart');
                if (cartStorage) {
                    cartStorage.items.forEach(function (element) {
                        productObj.push(getLocalStorageData(element));
                    });
                    analytics.track('Cart Viewed', getSegmentHeaderCart(productObj));
                } else {
                    analytics.track('Cart Viewed', '');
                }
            }
        });

        /**
         *
         * @param products
         * @returns {{products: *}}
         */
        function getSegmentHeaderCart(products) {
            return {
                products: products
            }
        }

        /**
         *
         * @param arrayElements
         * @returns {{quantity: *, image_url: *, pack_weight: *, url: *, category_level_1: *, category_level_3: *, colour: *, revenue: *, category_level_2: *, breed_size: *, price: *, product_id: *, name: *, lifestage: *, sku: *, main_flavour: *, brand: *, dietary_needs: *}}
         */
        function getLocalStorageData(arrayElements) {
            let priceExtract = arrayElements.product_price.substring(arrayElements.product_price.indexOf("£") + 1);
            //console.log(priceExtract);
            return {
                product_id: arrayElements.product_id,
                name: arrayElements.name,
                sku: arrayElements.product_sku,
                category_level_1: arrayElements.category_level_1,
                category_level_2: arrayElements.category_level_2,
                category_level_3: arrayElements.category_level_3,
                brand: arrayElements.brand,
                price: parseFloat(priceExtract),
                url: arrayElements.product_url,
                image_url: arrayElements.image,
                lifestage: arrayElements.lifestage,
                breed_size: arrayElements.breed_size,
                dietary_needs: arrayElements.dietary_need,
                main_flavour: arrayElements.main_flavour,
                colour: arrayElements.colour,
                quantity: arrayElements.qty,
                pack_weight: arrayElements.pack_weight,
                revenue: parseFloat(arrayElements.product_price_value)
            };
        }

    });


});

