<?php
/**
 * Ayko_SegmentioAnalytics
 *
 * @category  Ayko
 * @package   Ayko_SegmentioAnalytics
 * @author    Ayko Development Team <support@ayko.co>
 * @copyright Copyright (c) 2019 Ayko (http://www.ayko.com/)
 *
 */
namespace Ayko\SegmentioAnalytics\Console\Command;

use Ayko\SegmentioAnalytics\Model\StandardCategory;
use Symfony\Component\Console\Command\Command as SymfonyCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\Framework\App\State;

class GenerateStandardCategoryPathCommand extends SymfonyCommand
{
    const NAME = 'ayko:generate:standard-category-path';

    /**
     * @var \Magento\Framework\App\State
     */
    protected $_appState;
    /**
     * @var StandardCategory
     */
    private $standardCategory;

    /**
     * SetStandardCategoryCommand constructor.
     * @param State $appState
     * @param StandardCategory $standardCategory
     */
    public function __construct(
        State $appState,
        StandardCategory $standardCategory
    ) {
        parent::__construct();
        $this->_appState = $appState;
        $this->standardCategory = $standardCategory;
    }

    protected function configure()
    {
        $this->setName(static::NAME)
            ->setDescription('This is used to set standard category path to products having standard category');

        parent::configure();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->_appState->setAreaCode('adminhtml');
        $output->writeln("<info>Started</info>");
        $this->standardCategory->updateStandardCategoryPath();
        $output->writeln("<info>Finished</info>");

    }
}
