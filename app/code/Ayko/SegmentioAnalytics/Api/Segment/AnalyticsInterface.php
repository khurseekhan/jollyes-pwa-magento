<?php
/**
 * Ayko_SegmentioAnalytics
 *
 * @category  Ayko
 * @package   Ayko_SegmentioAnalytics
 * @author    Ayko Development Team <support@ayko.co>
 * @copyright Copyright (c) 2019 Ayko (http://www.ayko.com/)
 *
 */
namespace Ayko\SegmentioAnalytics\Api\Segment;

interface AnalyticsInterface
{
    /**
     * @return mixed
     */
    public function sendInit();

    /**
     * @param $data
     * @param $registerData
     * @return mixed
     */
    public function sendIdentify($data, $registerData);

    /**
     * @param $data
     * @return mixed
     */
    public function sendSegmentTrack($data);

    /**
     * @param $data
     * @return mixed
     */
    public function getSignInTrack($data);

    /**
     * @param $data
     * @return mixed
     */
    public function getSignOutTrack($data);

    /**
     * @param $data
     * @param $registerData
     * @return mixed
     */
    public function getSignUpTrack($data, $registerData);

    /**
     * @param $product
     * @param $quote
     * @param $quoteItem
     * @return mixed
     */
    public function addToCartTrack($product, $quote, $quoteItem);

    /**
     * @param $quote
     * @param $quoteItem
     * @return mixed
     */
    public function removeItemTrack($quote, $quoteItem);

    /**
     * @param $order
     * @return mixed
     */
    public function getOrderSuccessTrack($order);
}
