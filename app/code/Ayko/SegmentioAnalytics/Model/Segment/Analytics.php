<?php
/**
 * Ayko_SegmentioAnalytics
 *
 * @category  Ayko
 * @package   Ayko_SegmentioAnalytics
 * @author    Ayko Development Team <support@ayko.co>
 * @copyright Copyright (c) 2019 Ayko (http://www.ayko.com/)
 *
 */

namespace Ayko\SegmentioAnalytics\Model\Segment;

use Ayko\SegmentioAnalytics\Api\Segment\AnalyticsInterface;
use Ayko\SegmentioAnalytics\Model\ConfigInterface;
use Magento\Customer\Model\Customer;
use Psr\Log\LoggerInterface;
use Magento\Sales\Model\OrderFactory;
use Magento\Customer\Model\Session;
use Magento\Catalog\Model\ProductRepository;
use Segment;
use Magento\Framework\Serialize\Serializer\Json;
use Ayko\SegmentioAnalytics\Model\AnalyticsTrackingFactory;
use Magento\Framework\Registry;
use Magento\Catalog\Model\Session as CatalogSession;
use Magento\Customer\Api\AddressRepositoryInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Quote\Model\QuoteIdMaskFactory;
use Magento\Store\Model\StoreManagerInterface;
use Ayko\SegmentioAnalytics\Helper\Data;
use Magento\Framework\Stdlib\CookieManagerInterface;
use Magento\Catalog\Helper\Data as taxHelper;

class Analytics extends AbstractEntryPoint implements AnalyticsInterface
{
    const SOURCE_TYPE = "WEB";
    const EVENT_ACCOUNT_CREATE = "Account Created";
    const EVENT_SIGN_IN = "Signed In";
    const EVENT_SIGN_OUT = "Signed Out";
    const EVENT_ADD_TO_CART = "Product Added";
    const EVENT_REMOVED_PRODUCT = "Product Removed";
    const EVENT_COMPLETE_ORDER = "Order Completed";
    const EVENT_PRODUCT_PURCHASED = "Product Purchased";
    const EVENT_RESET_PASSWORD = "Password Reset";
    const URL_EXTENSION = ".html";
    const EVENT_TRACKING_MODE_ENABLE = true;
    const EVENT_TRACKING_MODE_DISABLE = false;
    const MODULE_ENABLE = true;
    const MODULE_DISABLE = false;
    const COOKIE_NAME = 'ajs_anonymous_id';

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var OrderFactory
     */
    protected $orderFactory;

    /**
     * @var Session
     */
    protected $customerSession;

    /**
     * @var ProductRepository
     */
    protected $productRepository;

    /**
     * Instance of serializer.
     *
     * @var Json
     */
    private $serializer;

    /**
     * @var AnalyticsTrackingFactory
     */
    protected $analyticsTrackingFactory;

    /**
     * @var Registry
     */
    protected $registry;

    /**
     * @var CatalogSession
     */
    protected $session;

    /**
     * @var AddressRepositoryInterface
     */
    protected $addressRepository;

    /**
     * @var CustomerRepositoryInterface
     */
    protected $customerRepository;

    /**
     * @param QuoteIdMaskFactory
     */
    protected $quoteIdMaskFactory;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var Data
     */
    protected $helper;

    /**
     * @var CookieManagerInterface
     */
    protected $cookieManager;

    /**
     * @var taxHelper
     */
    protected $taxHelper;

    /**
     * Analytics constructor.
     * @param LoggerInterface $logger
     * @param ConfigInterface $config
     * @param OrderFactory $orderFactory
     * @param Session $customerSession
     * @param ProductRepository $productRepository
     * @param Json|null $serializer
     * @param AnalyticsTrackingFactory $analyticsTrackingFactory
     * @param Registry $registry
     * @param CatalogSession $session
     * @param AddressRepositoryInterface $addressRepository
     * @param CustomerRepositoryInterface $customerRepository
     * @param QuoteIdMaskFactory $quoteIdMaskFactory
     * @param StoreManagerInterface $storeManager
     * @param Data $helper
     * @param CookieManagerInterface $cookieManager
     * @param taxHelper $taxHelper
     */
    public function __construct(
        LoggerInterface $logger,
        ConfigInterface $config,
        OrderFactory $orderFactory,
        Session $customerSession,
        ProductRepository $productRepository,
        Json $serializer,
        AnalyticsTrackingFactory $analyticsTrackingFactory,
        Registry $registry,
        CatalogSession $session,
        AddressRepositoryInterface $addressRepository,
        CustomerRepositoryInterface $customerRepository,
        QuoteIdMaskFactory $quoteIdMaskFactory,
        StoreManagerInterface $storeManager,
        Data $helper,
        CookieManagerInterface $cookieManager,
        taxHelper $taxHelper
    )
    {
        parent::__construct($config);
        $this->serializer = $serializer;
        $this->logger = $logger;
        $this->orderFactory = $orderFactory;
        $this->customerSession = $customerSession;
        $this->productRepository = $productRepository;
        $this->analyticsTrackingFactory = $analyticsTrackingFactory;
        $this->registry = $registry;
        $this->session = $session;
        $this->addressRepository = $addressRepository;
        $this->customerRepository = $customerRepository;
        $this->quoteIdMaskFactory = $quoteIdMaskFactory;
        $this->storeManager = $storeManager;
        $this->helper = $helper;
        $this->cookieManager = $cookieManager;
        $this->taxHelper = $taxHelper;
    }

    /**
     * @return string|null
     */
    public function getCookieId()
    {
        return str_replace('"', '', $this->cookieManager->getCookie(self::COOKIE_NAME));
    }

    /**
     * Send Init
     *
     * @return string
     */
    public function sendInit()
    {
        try {
            $debugModeEnabled = [];
            $segmentKey = $this->getSegmentKey();

            if($this->config->isLogEnabled()) {
                $debugModeEnabled = [
                    "debug" => true,
                    "error_handler" => function ($code, $msg) { error_log($msg); }
                ];
            }

            Segment::init($segmentKey, $debugModeEnabled);

        } catch (\Exception $e) {
            $this->logger->critical($e->getMessage());
        }
    }

    /**
     * @param $customer
     * @param $registerData
     * @return array|mixed
     */
    public function sendIdentify($customer, $registerData = null)
    {
        $loyaltyNumber = $this->getCustomAttributeValue($customer, 'loyalty_number');
        $contactByEmail = $this->getCustomAttributeValue($customer, 'contact_by_email');
        $contactBySms = $this->getCustomAttributeValue($customer, 'contact_by_sms');
        $date = date_create($customer->getCreatedAt());
        $createdAt = date_format($date, "Y/m/d H:i:s");

        try {
            $trackingData = [
                "anonymousId" => $this->getCookieId(),
                "userId" => ($loyaltyNumber) ? $loyaltyNumber : null,
                "traits" => [
                    "user_id" => ($loyaltyNumber) ? $loyaltyNumber : null,
                    "email" => ($customer->getEmail()) ?: '',
                    "first_name" => ($customer->getFirstname()) ?: '',
                    "last_name" => ($customer->getLastname()) ?: '',
                    "phone" => $this->getTelephone($customer),
                    "created_at" => $createdAt,
                    "opt_in_email" => ($contactByEmail)? true : false,
                    "opt_in_sms" => ($contactBySms)? true : false,
                    "signup_source" => self::SOURCE_TYPE
                ]
            ];

            Segment::identify($trackingData);

        } catch (\Exception $e) {
            $this->logger->critical($e->getMessage());
        }
    }

    /**
     * @param $data
     * @return void
     */
    public function sendSegmentTrack($data)
    {
        try {
            Segment::track($data);
        } catch (\Exception $e) {
            $this->logger->critical($e->getMessage());
        }
    }

    /**
     * @param $customer
     * @return bool
     */
    public function getSignInTrack($customer)
    {
        if(!$this->isModuleEnabled()) {
            return $this;
        }
        $loyaltyNumber = $this->getCustomAttributeValue($customer, 'loyalty_number');
        try {
            $trackingData = [
                "anonymousId" => $this->getCookieId(),
                "userId" => ($loyaltyNumber) ?: null,
                "event" => self::EVENT_SIGN_IN,
                "properties" => [
                    "user_id" => ($loyaltyNumber) ?: null,
                    "email" => ($customer->getEmail()) ?: '',
                    "source" => self::SOURCE_TYPE
                ]
            ];

            $this->sendInit();
            $this->sendIdentify($customer);
            $this->sendSegmentTrack($trackingData);

        } catch (\Exception $e) {
            $this->logger->critical($e->getMessage());
        }
    }

    /**
     * @param $customer
     * @return bool
     */
    public function getSignOutTrack($customer)
    {
        if(!$this->isModuleEnabled()) {
            return $this;
        }
        $loyaltyNumber = $this->getCustomAttributeValue($customer, 'loyalty_number');
        try {
            $trackingData = [
                "anonymousId" => $this->getCookieId(),
                "userId" => ($loyaltyNumber) ?: null,
                "event" => self::EVENT_SIGN_OUT,
                "properties" => [
                    "user_id" => ($loyaltyNumber) ?: null,
                    "email" => ($customer->getEmail()) ?: '',
                    "source" => self::SOURCE_TYPE
                ]
            ];

            $this->sendInit();
            $this->sendSegmentTrack($trackingData);

        } catch (\Exception $e) {
            $this->logger->critical($e->getMessage());
        }
    }

    /**
     * @param $customer
     * @param $registerData
     * @return mixed|void
     */
    public function getSignUpTrack($customer, $registerData)
    {
        if(!$this->isModuleEnabled()) {
            return $this;
        }

        try {
            $loyaltyNumber = $this->getCustomAttributeValue($customer, 'loyalty_number');
            $trackingData = [
                "anonymousId" => $this->getCookieId(),
                "userId" => (isset($loyaltyNumber)) ? $loyaltyNumber : null,
                "event" => self::EVENT_ACCOUNT_CREATE,
                "properties" => [
                    "user_id" => (isset($loyaltyNumber)) ? $loyaltyNumber : null,
                    "source" => self::SOURCE_TYPE,
                    "first_name" => ($customer->getFirstname()) ?: '',
                    "last_name" => ($customer->getLastname()) ?: '',
                    "email" => ($customer->getEmail()) ?: '',
                    "phone" => $this->getTelephone($customer)
                ]
            ];

            $this->sendInit();
            $this->sendIdentify($customer, $registerData);
            $this->sendSegmentTrack($trackingData);

        } catch (\Exception $e) {
            $this->logger->critical($e->getMessage());
        }
    }


    /**
     * @param $customer
     * @param $registerData
     * @return mixed|void
     */
    public function resetPasswordEvent($customer, $registerData)
    {
        if(!$this->isModuleEnabled()) {
            return $this;
        }

        try {
            $loyaltyNumber = $this->getCustomAttributeValue($customer, 'loyalty_number');
            $trackingData = [
                "anonymousId" => $this->getCookieId(),
                "userId" => (isset($loyaltyNumber)) ? $loyaltyNumber : null,
                "event" => self::EVENT_RESET_PASSWORD,
                "properties" => [
                    "user_id" => (isset($loyaltyNumber)) ? $loyaltyNumber : null,
                    "source" => self::SOURCE_TYPE,
                    "first_name" => ($customer->getFirstname()) ?: '',
                    "last_name" => ($customer->getLastname()) ?: '',
                    "email" => ($customer->getEmail()) ?: '',
                    "phone" => $this->getTelephone($customer)
                ]
            ];

            $this->sendInit();
            $this->sendSegmentTrack($trackingData);

        } catch (\Exception $e) {
            $this->logger->critical($e->getMessage());
        }
    }

    /**
     * @param $product
     * @param $quote
     * @param $quoteItem
     * @return $this
     */
    public function addToCartTrack($product, $quote, $quoteItem)
    {
        if(!$this->isModuleEnabled()) {
            return $this;
        }

        try {
            $categoryPath = $product->getData('standard_category_path');
            list($category1, $category2, $category3) = $this->getStandardCategoryPathName($categoryPath);
            $quoteItem->setCategoryPath($categoryPath);
            $customer = $this->getCustomer();
            $loyaltyNumber = $customer->getLoyaltyNumber();
            $userId = ($loyaltyNumber) ? $loyaltyNumber : null;
            $trackingData = $this->getTrackingData(
                $quote,
                $quoteItem,
                $userId,
                $eventName = self::EVENT_ADD_TO_CART,
                $category1,
                $category2,
                $category3
            );

            $this->sendInit();
            $this->sendSegmentTrack($trackingData);

        } catch (\Exception $e) {
            $this->logger->critical($e->getMessage());
        }
    }

    /**
     * @param $quote
     * @param $quoteItem
     * @return bool
     */
    public function removeItemTrack($quote, $quoteItem)
    {
        if(!$this->isModuleEnabled()) {
            return $this;
        }

        $categoryPath = $quoteItem->getData('category_path');
        list($category1, $category2, $category3) = $this->getStandardCategoryPathName($categoryPath);
        try {
            $customer = $this->getCustomer();
            $loyaltyNumber = $customer->getLoyaltyNumber();
            $userId = ($loyaltyNumber) ? $loyaltyNumber : null;
            $trackingData = $this->getTrackingData(
                $quote,
                $quoteItem,
                $userId,
                $eventName = self::EVENT_REMOVED_PRODUCT,
                $category1,
                $category2,
                $category3
            );

            $this->sendInit();
            $this->sendSegmentTrack($trackingData);

        } catch (\Exception $e) {
            $this->logger->critical($e->getMessage());
        }
    }

    /**
     * @param $orderIds
     * @return mixed|void
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getOrderSuccessTrack($orderIds)
    {
        if(!$this->isModuleEnabled()) {
            return $this;
        }

        $order = $this->orderFactory->create();
        $order->load($orderIds);
        $customer = $this->getCustomer();
        $loyaltyNumber = $customer->getLoyaltyNumber();
        $userId = ($loyaltyNumber) ? $loyaltyNumber : null;
        $items = $order->getAllItems();
        $products = [];
        $configurableCategoryPath = [];
        foreach ($items as $item) {
            $categoryPath = $item->getData('category_path');
            if ($item->getProductType() == 'configurable') {
                $configurableCategoryPath[$item->getId()] = $categoryPath;
                continue;
            }
            if (!$categoryPath) {
                $categoryPath = (key_exists($item->getParentItemId(),$configurableCategoryPath)) ? $configurableCategoryPath[$item->getParentItemId()] : '';
            }

            list($category1, $category2, $category3) = $this->getStandardCategoryPathName($categoryPath);
            $productName = $item->getName();
            $product = $this->getProduct($item);
            $productId = $product->getId();
            $price = $this->getPriceIncTax($product);
            $products[] = [
                "product_id" => $productId,
                "sku" => $product->getSku(),
                "category_level_1" => trim($category1),
                "category_level_2" => trim($category2),
                "category_level_3" => trim($category3),
                "name" => ($productName) ?: '',
                "brand" => (html_entity_decode($product->getAttributeText('brands'), ENT_QUOTES, 'UTF-8')) ?: '',
                "price" => $price,
                "lifestage" => ($product->getAttributeText('life_stage')) ?: '',
                "breed_size" => ($product->getAttributeText('breed_size')) ?: '',
                "dietary_needs" => ($product->getAttributeText('dietary_requirement')) ?: '',
                "main_flavour" => ($product->getAttributeText('main_flavour')) ?: '',
                "colour" => ($product->getAttributeText('colour')) ?: '',
                "pack_weight" => ($product->getWeight()) ?: '',
                "url" => ($item->getProduct()->getProductUrl()) ?: '',
                "image_url" => $this->getImageFullPath($product),
                "revenue" => (float)$product->getFinalPrice(),
                "quantity" => (int)$item->getQtyOrdered(),
                "discount" => (float)$item->getDiscountAmount()
            ];

            $productPurchasedTrackData[] = [
                "order_id" => $order->getId(),
                "card_id" => $order->getQuoteId(),
                "product_id" => $productId,
                "sku" => $product->getSku(),
                "category_level_1" => trim($category1),
                "category_level_2" => trim($category2),
                "category_level_3" => trim($category3),
                "name" => ($productName) ?: '',
                "brand" => (html_entity_decode($product->getAttributeText('brands'), ENT_QUOTES, 'UTF-8')) ?: '',
                "price" => $price,
                "lifestage" => ($product->getAttributeText('life_stage')) ?: '',
                "breed_size" => ($product->getAttributeText('breed_size')) ?: '',
                "dietary_needs" => ($product->getAttributeText('dietary_requirement')) ?: '',
                "main_flavour" => ($product->getAttributeText('main_flavour')) ?: '',
                "colour" => ($product->getAttributeText('colour')) ?: '',
                "pack_weight" => ($product->getWeight()) ?: '',
                "quantity" => (int)$item->getQtyOrdered(),
                "revenue" => (float)$product->getFinalPrice(),
                "discount" => (float)$item->getDiscountAmount(),
                "currency" => $order->getStoreCurrencyCode(),
                "url" => ($item->getProduct()->getProductUrl()) ?: '',
                "image_url" => $this->getImageFullPath($product),
            ];
        }

        $trackingData = [
            "anonymousId" => $this->getCookieId(),
            "userId" => $userId,
            "event" => self::EVENT_COMPLETE_ORDER,
            "properties" => [
                "order_id" => $order->getId(),
                "total" => (float)$order->getGrandTotal(),
                "shipping" => (float)$order->getShippingAmount(),
                "tax" => (float)$order->getTaxAmount(),
                "discount" => (float)$order->getDiscountAmount(),
                "coupon" => ($order->getCouponCode()) ?: '',
                "currency" => $order->getStoreCurrencyCode(),
                "delivery_method" => ($order->getShippingMethod()) ?: '',
                "delivery_store" => ($order->getData('storelocator_name')) ?: '',
                "products" => $products
            ]
        ];

        try {
            $this->sendInit();
            $this->sendSegmentTrack($trackingData);
            $this->productPurchasedTrack($productPurchasedTrackData, $userId);

        } catch (\Exception $e) {
            $this->logger->critical($e->getMessage());
        }
    }

    /**
     * @param array $productPurchasedTrackData
     * @param $userId
     */
    protected function productPurchasedTrack(array $productPurchasedTrackData, $userId)
    {
        try {
            foreach ($productPurchasedTrackData as $productPurchasedTrack) {
                $trackingData = [
                    "userId" => $userId,
                    "anonymousId" => $this->getCookieId(),
                    "event" => self::EVENT_PRODUCT_PURCHASED,
                    "properties" => $productPurchasedTrack
                ];

                $this->sendInit();
                $this->sendSegmentTrack($trackingData);
            }
        } catch (\Exception $e) {
            $this->logger->critical($e->getMessage());
        }
    }


    /**
     * Get Attribute Option value
     *
     * @param $product
     * @param $attributeName
     * @param $optionId
     * @return mixed
     */
    public function getAttributeOptionValue($product, $attributeName, $optionId)
    {
        $attr = $product->getResource()->getAttribute($attributeName);
        $optionText = null;
        if ($attr->usesSource()) {
            $optionText = $attr->getSource()->getOptionText($optionId);
            if (empty($optionText)) {
                $optionText = null;
            }
        }

        return $optionText;
    }

    /**
     * Get Customer object
     *
     * @return \Magento\Customer\Model\Customer
     */
    protected function getCustomer()
    {
        $customer = $this->customerSession->getCustomer();
        $customerId = $customer->getId();
        if (!$customerId) {
            $customer = $this->customerSession;
        }

        return $customer;
    }

    /**
     * @param $id
     * @return \Magento\Catalog\Api\Data\ProductInterface|mixed
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getProductById($id)
    {
        return $this->productRepository->getById($id);
    }

    /**
     * @return mixed
     */
    protected function getAnalyticsTracking()
    {
        return $this->analyticsTrackingFactory->create();
    }

    /**
     * @param $message
     */
    protected function log($message)
    {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/segment_analytics.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info($message);
    }

    /**
     * @param $customer
     * @param $attributeCode
     * @return bool
     */
    public function getCustomAttributeValue($customer, $attributeCode)
    {
        $attributeValue = '';

        if(!$customer->getCustomAttributes()) {
            return false;
        }

        foreach ($customer->getCustomAttributes() as $attribute) {
            if($attribute->getAttributeCode() == $attributeCode) {
                $attributeValue = $attribute->getValue();
            }
        }

        return $attributeValue;
    }

    /**
     * @param \Magento\Customer\Model\Data\Customer $customer
     * @return string|string[]|null
     */
    public function getTelephone($customer)
    {
        if($customer instanceof \Magento\Customer\Model\Data\Customer)
            $billingAddressId = $customer->getDefaultBilling();

        $telephone = '';
        foreach ($customer->getAddresses() as $address) {
            if (isset($billingAddressId) && $billingAddressId !== $address->getId()) {
                continue;
            }

            $telephone = $this->helper->getTelephoneNumber($address);
            break;
        }

        return $telephone;
    }

    /**
     * @param $product
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getImageFullPath($product)
    {
        $image = ($product->getImage())?:$product->getSmallImage();

        return $this->storeManager->getStore()->getBaseUrl() . 'catalog/product' . $image;
    }

    /**
     * @param $product
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getProductUrl($product)
    {
        if ($product->getUrlKey()) {
            return $this->storeManager->getStore()->getBaseUrl() . $product->getUrlKey() . self::URL_EXTENSION;
        }
    }

    /**
     * @return bool
     */
    private function isModuleEnabled()
    {
        return ($this->config->isModuleEnabled()) ? self::MODULE_ENABLE: self::MODULE_DISABLE;
    }

    /**
     * @param $categoryPath
     * @return array
     */
    protected function getStandardCategoryPathName($categoryPath)
    {
        $category1 = '';
        $category2 = '';
        $category3 = '';

        if (isset($categoryPath)) {
            $categories = explode(",", $categoryPath);
            $category1 = (isset($categories['0'])) ? $categories['0'] : '';
            $category2 = (isset($categories['1'])) ? $categories['1'] : '';
            $category3 = (isset($categories['2'])) ? $categories['2'] : '';
        }

        return array($category1, $category2, $category3);
    }

    /**
     * @param $quote
     * @param $quoteItem
     * @param string $userId
     * @param $eventName
     * @param $category1
     * @param $category2
     * @param $category3
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function getTrackingData($quote, $quoteItem, $userId, $eventName, $category1, $category2, $category3): array
    {
        $product = $this->getProduct($quoteItem);
        $productId = $product->getId();
        $price = $this->getPriceIncTax($product);

        $trackingData = [
            "anonymousId" => $this->getCookieId(),
            "userId" => $userId,
            "event" => $eventName,
            "properties" => [
                "cart_id" => $quote->getId(),
                "product_id" => $productId,
                "sku" => $product->getSku(),
                "quantity" => $quoteItem->getQty(),
                "category_level_1" => trim($category1),
                "category_level_2" => trim($category2),
                "category_level_3" => trim($category3),
                "name" => ($product->getName()) ?: '',
                "brand" => (html_entity_decode($product->getAttributeText('brands'), ENT_QUOTES, 'UTF-8')) ?: '',
                "price" => $price,
                "lifestage" => ($product->getAttributeText('life_stage')) ?: '',
                "breed_size" => ($product->getAttributeText('breed_size')) ?: '',
                "dietary_needs" => ($product->getAttributeText('dietary_requirement')) ?: '',
                "main_flavour" => ($product->getAttributeText('main_flavour')) ?: '',
                "colour" => ($product->getAttributeText('colour')) ?: '',
                "pack_weight" => ($product->getWeight()) ?: '',
                "url" => ($quoteItem->getProduct()->getProductUrl()) ?: '',
                "image_url" => $this->getImageFullPath($product),
            ]
        ];

        return $trackingData;
    }


    /**
     * @param $product
     * @return float
     */
    protected function getPriceIncTax($product): float
    {
        $price = $this->taxHelper->getTaxPrice($product, $product->getFinalPrice(), true);

        return $price;
    }

    /**
     * @param $quoteItem
     * @return mixed
     */
    protected function getProduct($quoteItem)
    {
        $product = $quoteItem->getProduct();

        if($quoteItem->getProductType()=='configurable') {
            $product =  $quoteItem->getOptionByCode('simple_product')->getProduct();
        }

        return $product;
    }
}
