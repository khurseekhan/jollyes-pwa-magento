<?php
/**
 * Ayko_SegmentioAnalytics
 *
 * @category  Ayko
 * @package   Ayko_SegmentioAnalytics
 * @author    Ayko Development Team <support@ayko.co>
 * @copyright Copyright (c) 2019 Ayko (http://www.ayko.com/)
 *
 */
namespace Ayko\SegmentioAnalytics\Model\Segment;

use Ayko\SegmentioAnalytics\Model\ConfigInterface;

/**
 * Class AbstractEntryPoint
 */
abstract class AbstractEntryPoint
{
    /**
     * @var ConfigInterface
     */
    protected $config;

    /**
     * @var LoggerInterface
     */
    private $logger;

   /**
    * AbstractEntryPoint constructor.
    * @param ConfigInterface $config
    */
    public function __construct(
        ConfigInterface $config
    ) {
        $this->config = $config;
    }

    /**
     * Get API user name
     *
     * @return string
     */
    public function getSegmentKey()
    {
        return $this->config->getSegmentKey();
    }
}
