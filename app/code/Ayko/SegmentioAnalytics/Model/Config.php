<?php
/**
 * Ayko_SegmentioAnalytics
 *
 * @category  Ayko
 * @package   Ayko_SegmentioAnalytics
 * @author    Ayko Development Team <support@ayko.co>
 * @copyright Copyright (c) 2019 Ayko (http://www.ayko.com/)
 *
 */
namespace Ayko\SegmentioAnalytics\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;

class Config implements ConfigInterface
{
    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(ScopeConfigInterface $scopeConfig)
    {
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * {@inheritdoc}
     */
    public function isModuleEnabled()
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_ENABLED,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Get the write key from config
     *
     * @return bool
     */
    public function getSegmentKey()
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_SERVER_SIDE_KEY,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Get the client write key from config
     *
     * @return string
     */
    public function getClientWriteKey()
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_CLIENT_SIDE_KEY,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Get the mode from config
     *
     * @return bool
     */
    public function getTrackingMode()
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_TRACK_MODE,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * check if module log mode is enabled or not from configuration
     *
     * @return string
     */
    public function isLogEnabled()
    {
        return $this->scopeConfig->getValue(
            self::XPATH_LOG_DEBUG,
            ScopeInterface::SCOPE_STORE
        );
    }
}
