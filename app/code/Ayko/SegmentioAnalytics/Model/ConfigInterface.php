<?php
/**
 * Ayko_SegmentioAnalytics
 *
 * @category  Ayko
 * @package   Ayko_SegmentioAnalytics
 * @author    Ayko Development Team <support@ayko.co>
 * @copyright Copyright (c) 2019 Ayko (http://www.ayko.com/)
 *
 */
namespace Ayko\SegmentioAnalytics\Model;

interface ConfigInterface
{
    /**
     * Enabled config path
     */
    const XML_PATH_ENABLED = 'segmentio_analytics/general/enabled';

    /**
     * Segment Server Side Key config path
     */
    const XML_PATH_SERVER_SIDE_KEY = 'segmentio_analytics/general/server_side_key';

    /**
     * Segment Client Side Key config path
     */
    const XML_PATH_CLIENT_SIDE_KEY = 'segmentio_analytics/general/client_side_key';

    /**
     * Tracking Mode config path
     */
    const XML_PATH_TRACK_MODE = 'segmentio_analytics/general/tracking_mode';

    /**
     *  Non Standard Category Mode config path
     */
    const XML_PATH_NON_STANDARD_CATEGORIES = "segmentio_analytics/general/non_standard_category";

    /**
     *  Non Standard Third Level Category  config path
     */
    const XML_PATH_NON_STANDARD_THIRD_LEVEL_CATEGORY = "segmentio_analytics/general/non_standard_third_level_category";

    /**
     *  Standard Categories Third Level config path
     */
    const XML_PATH_STANDARD_CATEGORY_THIRD_LEVEL = "segmentio_analytics/general/standard_category_third_level";

    /**
     *  Standard Categories Level config path
     */
    const XML_PATH_STANDARD_CATEGORIES_LEVEL= "segmentio_analytics/general/standard_categories_level";

    /**
     *  Enable loyalty log debugging
     */
    const XPATH_LOG_DEBUG = "segmentio_analytics/general/log_enabled";

    /**
     * Check if contacts module is enabled
     *
     * @return bool
     */
    public function isModuleEnabled();

    /**
     * Get Segment Key
     *
     * @return string
     */
    public function getSegmentKey();

    /**
     * @return string
     */
    public function getClientWriteKey();

    /**
     *  Get Mode
     * @return boolean
     */
    public function getTrackingMode();

    /**
     * Check if logger is enabled
     *
     * @return bool
     */
    public function isLogEnabled();
}
