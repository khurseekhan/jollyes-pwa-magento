<?php
/**
 * Ayko_SegmentioAnalytics
 *
 * @category  Ayko
 * @package   Ayko_SegmentioAnalytics
 * @author    Ayko Development Team <support@ayko.co>
 * @copyright Copyright (c) 2019 Ayko (http://www.ayko.com/)
 *
 */
namespace Ayko\SegmentioAnalytics\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;

class TrackMode implements ArrayInterface
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => 'event', 'label' => __('On the Fly Mode')],
            ['value' => 'cron', 'label' => __('On Cron Mode')]
        ];
    }
}
