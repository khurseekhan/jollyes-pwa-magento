<?php
/**
 * Ayko_SegmentioAnalytics
 *
 * @category  Ayko
 * @package   Ayko_SegmentioAnalytics
 * @author    Ayko Development Team <support@ayko.co>
 * @copyright Copyright (c) 2019 Ayko (http://www.ayko.com/)
 *
 */
namespace Ayko\SegmentioAnalytics\Model\ResourceModel\AnalyticsTracking;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * Identifier field name for collection items
     *
     * Can be used by collections with items without defined
     *
     * @var string
     */
    protected $_idFieldName = 'id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Ayko\SegmentioAnalytics\Model\AnalyticsTracking::class,
            \Ayko\SegmentioAnalytics\Model\ResourceModel\AnalyticsTracking::class
        );
    }

    /**
     * Define resource model
     *
     * @return string
     */
    public function getIdFieldName()
    {
        return $this->_idFieldName;
    }
}
