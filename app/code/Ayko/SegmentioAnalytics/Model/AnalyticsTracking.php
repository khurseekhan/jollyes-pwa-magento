<?php
/**
 * Ayko_SegmentioAnalytics
 *
 * @category  Ayko
 * @package   Ayko_SegmentioAnalytics
 * @author    Ayko Development Team <support@ayko.co>
 * @copyright Copyright (c) 2019 Ayko (http://www.ayko.com/)
 *
 */
namespace Ayko\SegmentioAnalytics\Model;

use Magento\Framework\Model\AbstractModel;

class AnalyticsTracking extends AbstractModel
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Ayko\SegmentioAnalytics\Model\ResourceModel\AnalyticsTracking::class);
    }
}
