<?php
/**
 * Ayko_SegmentioAnalytics
 *
 * @category  Ayko
 * @package   Ayko_SegmentioAnalytics
 * @author    Ayko Development Team <support@ayko.co>
 * @copyright Copyright (c) 2019 Ayko (http://www.ayko.com/)
 *
 */
namespace Ayko\SegmentioAnalytics\Model;

use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory;
use Magento\Catalog\Model\Product\Action;
use Magento\Framework\Exception\InputException;
use Magento\Store\Model\StoreManagerInterface;
use Ayko\SegmentioAnalytics\Helper\Data;
use Magento\Eav\Model\ResourceModel\Entity\Attribute;
use Psr\Log\LoggerInterface;
use Magento\Store\Model\Store;
/**
 * Class StandardCategory
 * @package Ayko\SegmentioAnalytics\Model
 */
class StandardCategory
{
    const STANDARD_CATEGORY_LEVEL = 3;

    const STANDARD_CATEGORY_ENABLED = 1;

    const INCLUDE_IN_MENU_ACTIVE = 1;

    const IS_ACTIVE_CATEGORY = 1;

    const NON_STANDARD_THIRD_LEVEL_CATEGORY = 63;

    const NON_STANDARD_CATEGORIES = 'Shop by Popular Brands';

    const CATEGORIES_LEVEL = ['3','4'];

    const ENTITY_TYPE = 'catalog_category';

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var Collection
     */
    private $collection;

    /**
     * @var CollectionFactory
     */
    protected $categoryCollectionFactory;

    /**
     * @var Action
     */
    protected $modelProductAction;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var Data
     */
    protected $helper;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Category\Collection
     */
    private $categoryCollection;

    /**
     * @var Attribute
     */
    private $eavAttribute;


    /**
     * StandardCategory constructor.
     * @param CollectionFactory $categoryCollectionFactory
     * @param Action $modelProductAction
     * @param StoreManagerInterface $storeManager
     * @param Data $helper
     * @param Attribute $eavAttribute
     * @param LoggerInterface $logger
     */
    public function __construct(
        CollectionFactory $categoryCollectionFactory,
        Action $modelProductAction,
        StoreManagerInterface $storeManager,
        Data $helper,
        Attribute $eavAttribute,
        LoggerInterface $logger
    ) {
        $this->categoryCollectionFactory = $categoryCollectionFactory;
        $this->modelProductAction = $modelProductAction;
        $this->storeManager = $storeManager;
        $this->helper = $helper;
        $this->eavAttribute = $eavAttribute;
        $this->logger = $logger;
    }

    /**
     * Update Product attribute Standard Category Path based on Standard Category
     */
    public function updateStandardCategoryPath()
    {
        $categoryCollection = $this->getCategoryCollection();
        if (!$categoryCollection->getSize()) {
            return;
        }
        /** @var $category Category */
        foreach ($categoryCollection as $category) {
            $productIds = $category->getProductCollection()->getAllIds();
            if (!$productIds) {
                continue;
            }

            $standardCategoryPath = implode(",", array_column($this->getStandardPath($category), "label"));
            $storeId = $this->getStoreId();
            $this->updateStandardCategoryPathAttribute($productIds, $standardCategoryPath, $storeId);
        }

    }

    /**
     * @return \Magento\Catalog\Model\ResourceModel\Category\Collection
     */
    protected function getCategoryCollection()
    {
        $this->categoryCollection = $this->categoryCollectionFactory->create();
        $this->categoryCollection
            ->addFieldtoFilter('include_in_menu', ['eq' => self::INCLUDE_IN_MENU_ACTIVE])
            ->addFieldtoFilter('is_active',['eq'=>self::IS_ACTIVE_CATEGORY])
            ->addFieldtoFilter('level',['in'=>[self::CATEGORIES_LEVEL]])
            ->addFieldtoFilter('standard_category', ['eq' => self::STANDARD_CATEGORY_ENABLED]);

        $this->categoryCollection->getSelect()->joinLeft(
            ['ccp' => 'catalog_category_product'],
            'ccp.category_id=e.entity_id',
            ['product_ids' => new \Zend_Db_Expr('group_concat(`ccp`.product_id)')]
        )->group('e.entity_id');

        return $this->categoryCollection;
    }

    /**
     * @return Collection|\Magento\Catalog\Model\ResourceModel\Category\Collection
     */
    protected function getStandardCategoryCollection()
    {
        $nameAttributeId = $this->getAttributeIdByCode('name');

        $this->collection = $this->categoryCollectionFactory->create();

        $this->collection->addFieldtoFilter('include_in_menu',['eq'=>self::INCLUDE_IN_MENU_ACTIVE])
            ->addFieldtoFilter('is_active',['eq'=>self::IS_ACTIVE_CATEGORY])
            ->addFieldtoFilter('parent_id',['neq'=>self::NON_STANDARD_THIRD_LEVEL_CATEGORY])
            ->addFieldtoFilter('level',['eq'=>self::STANDARD_CATEGORY_LEVEL]);

        $this->collection->getSelect()->joinInner(
            ['ccev' => 'catalog_category_entity_varchar'],
            'ccev.entity_id=e.entity_id AND ccev.attribute_id='.$nameAttributeId.' and ccev.value != "'.self::NON_STANDARD_CATEGORIES.'"',
            ['ccev.value as name']
        );

        $subQuery = $this->getSubQuery($this->collection);
        $this->collection->getSelect()->joinInner(['sq' => $subQuery], 'e.parent_id = sq.entity_id', []);

        return $this->collection;
    }

    /**
     * @param $collection
     * @return mixed
     */
    protected function getSubQuery($collection)
    {
        $isActiveAttributeId = $this->getAttributeIdByCode('is_active');
        return $collection->getConnection()->select()->from(
            ['cce' => 'catalog_category_entity'],
            ['cce.entity_id']
        )->joinInner(
            ['is_active_parent' => 'catalog_category_entity_int'],
            'is_active_parent.entity_id = cce.entity_id and is_active_parent.attribute_id= '.$isActiveAttributeId.' and is_active_parent.store_id='.Store::DEFAULT_STORE_ID.' and is_active_parent.value = '.self::IS_ACTIVE_CATEGORY.'',
            []
        )->columns([
            'is_active_parent.value'
        ]);
    }

    /**
     * @param $attributeCode
     * @return int
     */
    public function getAttributeIdByCode($attributeCode)
    {
        return $this->eavAttribute->getIdByCode('catalog_category', $attributeCode);
    }

    /**
     *  Enabled Standard Category Based on Category Level
     */
    public function assignStandardCategories()
    {
        $categories = $this->getStandardCategoryCollection()->getItems();
        $this->processCategories($categories);
    }

    public function processCategories($categories)
    {
        foreach ($categories as $category) {
            if (!$this->isNonStandardCategory($category))
                $this->saveStandardCategory($category);
        }
    }

    /**
     * Check weather current category is non standard category or not
     * @param $category
     * @return bool
     */
    protected function isNonStandardCategory($category)
    {
        return ($category->getName() === self::NON_STANDARD_CATEGORIES) ? true : false ;
    }

    /**
     * @param $category
     * @return array
     */
    public function getStandardPath($category)
    {
        $path = [];
        $pathInStore = $category->getPathInStore();
        $pathIds = array_reverse(explode(',', $pathInStore));
        $rootIds = $this->getRootIds();
        $categories = $category->getParentCategories();
        // add category path breadcrumb
        foreach ($pathIds as $categoryId) {
            if (isset($categories[$categoryId]) && $categories[$categoryId]->getName()
                && !in_array($categoryId,$rootIds)) {
                $path['category' . $categoryId] = [
                    'label' => $categories[$categoryId]->getName()
                ];
            }
        }

        return $path;
    }

    /**
     * @return int
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getStoreId()
    {
        return $this->storeManager->getStore()->getId();
    }

    /**
     * Return ids of root categories as array
     *
     * @return array
     */
    public function getRootIds()
    {
        $ids = [\Magento\Catalog\Model\Category::TREE_ROOT_ID];
        foreach ($this->storeManager->getGroups() as $store) {
            $ids[] = $store->getRootCategoryId();
        }
        return $ids;
    }

    /**
     * @param \Magento\Framework\DataObject $category
     * @throws InputException
     */
    protected function saveStandardCategory(\Magento\Framework\DataObject $category)
    {
        try {
            if ($category->hasChildren()) {
                $this->processCategories($category->getChildrenCategories());
            }
            $category->setData('standard_category', self::STANDARD_CATEGORY_ENABLED);
            $category->getResource()->saveAttribute($category, 'standard_category');

        } catch (\Exception $e) {
            $this->logger->critical($e->getMessage());
        }
    }

    /**
     * Update product standard_category_path attribute
     * @param array $productIds
     * @param string $standardCategoryPath
     * @param int $storeId
     * @throws InputException
     */
    protected function updateStandardCategoryPathAttribute(array $productIds, string $standardCategoryPath, int $storeId)
    {
        try {
            $this->modelProductAction
                ->updateAttributes(
                    $productIds,
                    ['standard_category_path' => $standardCategoryPath],
                    $storeId
                );
        } catch (\Exception $e) {
            $this->logger->critical($e->getMessage());
        }
    }
}
