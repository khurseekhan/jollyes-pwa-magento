<?php
/**
 * Ayko_SegmentioAnalytics
 *
 * @category  Ayko
 * @package   Ayko_SegmentioAnalytics
 * @author    Ayko Development Team <support@ayko.co>
 * @copyright Copyright (c) 2019 Ayko (http://www.ayko.com/)
 *
 */
namespace Ayko\SegmentioAnalytics\ViewModel;

use Magento\Catalog\Helper\Data;
use Ayko\SegmentioAnalytics\Model\ConfigInterface;
use Magento\Catalog\Model\Category;
use Magento\Checkout\Model\Session;
use Magento\Eav\Api\AttributeRepositoryInterface;
use Magento\Framework\DataObject;
use Magento\Framework\View\Element\Block\ArgumentInterface;
use Magento\Framework\Json\Helper\Data as JsonHelper;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Registry;
use Magento\Quote\Model\Cart\CartTotalRepository;
use Ayko\SegmentioAnalytics\Helper\Data as DataHelper;

class Analytics extends DataObject implements ArgumentInterface
{
    /**
     * Catalog data.
     *
     * @var Data
     */
    private $catalogData;

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var JsonHelper
     */
    protected $jsonHelper;

    /**
     * @var Http
     */
    protected $request;

    /**
     * @var AttributeRepositoryInterface
     */
    private $eavAttributeRepository;

    /**
     * @var Session
     */
    private $checkoutSession;

    /**
     * @var CartTotalRepository
     */
    private $cartTotalRepository;

    /**
     * @var Registry
     */
    private $registry;

    /**
     * @var DataHelper
     */
    private $helper;

    /**
     * Analytics constructor.
     * @param Session $checkoutSession
     * @param Data $catalogData
     * @param ConfigInterface $scopeConfig
     * @param JsonHelper $jsonHelper
     * @param AttributeRepositoryInterface $eavAttributeRepositoryInterface
     * @param Http $request
     * @param CartTotalRepository $cartTotalRepository
     * @param Registry $registry
     * @param DataHelper $helper
     */
    public function __construct(
        Session $checkoutSession,
        Data $catalogData,
        ConfigInterface $scopeConfig,
        JsonHelper $jsonHelper,
        AttributeRepositoryInterface $eavAttributeRepositoryInterface,
        Http $request,
        CartTotalRepository $cartTotalRepository,
        Registry $registry,
        DataHelper $helper
    )
    {
        parent::__construct();

        $this->catalogData            = $catalogData;
        $this->scopeConfig            = $scopeConfig;
        $this->jsonHelper            = $jsonHelper;
        $this->eavAttributeRepository = $eavAttributeRepositoryInterface;
        $this->request               = $request;
        $this->cartTotalRepository    = $cartTotalRepository;
        $this->registry               = $registry;
        $this->checkoutSession        = $checkoutSession;
        $this->helper = $helper;
    }

    /**
     * @return bool
     */
    public function isAnalyticsAvailable()
    {
        return $this->scopeConfig->isModuleEnabled();
    }

    /**
     * @return string
     */
    public function getBreadCrumbData()
    {
        $categories = $this->catalogData->getBreadcrumbPath();
        foreach ($categories as $value) {
            $data[] = $value['label'];
        }

        if ($this->request->getFullActionName() == "catalog_product_view") {
            $data = $this->getProductStandardCategories();
        }

        return implode(',', $data);
    }

    /**
     * Return the attribute label from the attribute code and selected id
     *
     * @param $code
     * @param $product
     * @return mixed
     */
    public function getAttributeLabel($code, $product)
    {
        try {
            $attribute = $this->eavAttributeRepository->get(
                \Magento\Catalog\Model\Product::ENTITY,
                $code
            );
            $key       = array_search($product->getData($code), array_column($attribute->getSource()->getAllOptions(), 'value'));
            return $attribute->getSource()->getAllOptions()[$key]['label'];
        } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
            return false;
        }
    }

    /**
     * Return the current Cart/Basket ID
     *
     * @return int
     */
    public function getCartId()
    {
        return $this->checkoutSession->getQuote()->getId();
    }

    /**
     * Return the category levels
     *
     * @return Category|DataObject
     */
    public function getCategoryLevels()
    {
       return $this->helper->getCategoryLevels();
    }


    /**
     * Get default categories for Product
     *
     * @param null $product
     * @return array
     */
    public function getProductStandardCategories($product = null)
    {
        if (!$product) {
            $product = $this->registry->registry('product');
        }

        $categories = [];
        if ($product) {
            $categoryPath = $product->getData('standard_category_path');
            if(empty($categoryPath)){
                $categoryPath = $product->getData('category_path');
            }
            if (isset($categoryPath)) {
                $categories = explode(",", $categoryPath);
            }
        }
        return $categories;
    }

}
