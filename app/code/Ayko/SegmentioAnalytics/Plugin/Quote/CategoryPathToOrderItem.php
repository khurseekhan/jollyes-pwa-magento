<?php
/**
 * Ayko_SegmentioAnalytics
 *
 * @category  Ayko
 * @package   Ayko_SegmentioAnalytics
 * @author    Ayko Development Team <support@ayko.co>
 * @copyright Copyright (c) 2019 Ayko (http://www.ayko.com/)
 *
 */
namespace Ayko\SegmentioAnalytics\Plugin\Quote;

use Magento\Quote\Model\Quote\Item\ToOrderItem;
use Magento\Quote\Model\Quote\Item\AbstractItem;

class CategoryPathToOrderItem
{
    /**
     * @param ToOrderItem $subject
     * @param \Closure $proceed
     * @param AbstractItem $item
     * @param array $additional
     * @return \Magento\Sales\Model\Order\Item
     */
    public function aroundConvert(
        ToOrderItem $subject,
        \Closure $proceed,
        AbstractItem $item,
        $additional = []
    ) {
        /** @var $orderItem \Magento\Sales\Model\Order\Item */
        $orderItem = $proceed($item, $additional);
        $orderItem->setCategoryPath($item->getCategoryPath());
        return $orderItem;
    }
}
