<?php

namespace Ayko\SegmentioAnalytics\Plugin\Checkout\CustomerData;

use Magento\Checkout\Model\Session;
use Ayko\SegmentioAnalytics\Helper\Data;

class CartExtra {

    /**
     * @var Session
     */
    protected $checkoutSession;

    /**
     * @var Data
     */
    protected $helper;

    /**
     * CartExtra constructor.
     * @param Session $checkoutSession
     * @param Data $helper
     */
    public function __construct(
        Session $checkoutSession,
        Data $helper
    )
    {
        $this->checkoutSession = $checkoutSession;
        $this->helper = $helper;
    }

    /**
     * @param \Magento\Checkout\CustomerData\Cart $subject
     * @param array $result
     * @return array
     */
    public function afterGetSectionData(\Magento\Checkout\CustomerData\Cart $subject, array $result)
    {
        if (is_array($result['items'])) {
            foreach ($result['items'] as $key => $itemAsArray) {
                $quoteItem = $this->checkoutSession->getQuote()->getItemById($itemAsArray['item_id']);
                $product = $this->getProduct($quoteItem);
                $result['items'][$key]['brand'] = (html_entity_decode($product->getAttributeText('brands'), ENT_QUOTES, 'UTF-8'))?:'';
                $result['items'][$key]['lifestage'] = ($product->getAttributeText('life_stage'))?:'';
                $result['items'][$key]['breed_size'] = ($product->getAttributeText('breed_size'))?:'';
                $result['items'][$key]['dietary_need'] = ($product->getAttributeText('dietary_requirement'))?:'';
                $result['items'][$key]['main_flavour'] = ($product->getAttributeText('main_flavour'))?:'';
                $result['items'][$key]['pack_weight'] = ($product->getAttributeText('pack_weight'))?:'';
                $result['items'][$key]['colour'] = ($product->getAttributeText('colour'))?:'';
                $categories = [];
                $categoryPath = $quoteItem->getData('category_path');
                if (isset($categoryPath)) {
                    $categories = explode(",", $categoryPath);
                }
                $result['items'][$key]['category_level_1'] = (isset($categories[0])) ? trim($categories[0]) : '';
                $result['items'][$key]['category_level_2'] = (isset($categories[1])) ? trim($categories[1]) : '';
                $result['items'][$key]['category_level_3'] = (isset($categories[2])) ? trim($categories[2]) : '';
                $result['items'][$key]['image'] = $this->helper->getImageFullPath($product->getData('thumbnail'));

                $result['items'][$key]['name'] =  $product->getName();
                $result['items'][$key]['product_id'] =  $product->getId();
            }
        }

        return $result;
    }

    /**
     * @param $quoteItem
     * @return mixed
     */
    protected function getProduct($quoteItem)
    {
        $product = $quoteItem->getProduct();

        if($quoteItem->getProductType()=='configurable') {
            $product =  $quoteItem->getOptionByCode('simple_product')->getProduct();
        }

        return $product;
    }
}
