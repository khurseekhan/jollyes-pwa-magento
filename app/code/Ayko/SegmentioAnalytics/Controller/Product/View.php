<?php
/**
 * Ayko_SegmentioAnalytics
 *
 * @category  Ayko
 * @package   Ayko_SegmentioAnalytics
 * @author    Ayko Development Team <support@ayko.co>
 * @copyright Copyright (c) 2019 Ayko (http://www.ayko.com/)
 *
 */
namespace Ayko\SegmentioAnalytics\Controller\Product;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Catalog\Model\ProductRepository;
use Magento\Framework\Controller\Result\JsonFactory;
use Ayko\SegmentioAnalytics\ViewModel\Analytics;
use Ayko\SegmentioAnalytics\Helper\Data;
use Magento\Catalog\Helper\Data as taxHelper;

class View extends Action {

    /**
     * @var ProductRepository
     */
    protected $productRepository;

    /**
     * @var JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var Analytics
     */
    protected $analytics;

    /**
     * @var Data
     */
    protected $helper;

    /**
     * @var taxHelper
     */
    protected $taxHelper;

    /**
     * View constructor.
     * @param Context $context
     * @param ProductRepository $productRepository
     * @param JsonFactory $resultJsonFactory
     * @param Analytics $analytics
     * @param Data $helper
     * @param taxHelper $taxHelper
     */
    public function __construct(
        Context $context,
        ProductRepository $productRepository,
        JsonFactory $resultJsonFactory,
        Analytics $analytics,
        Data $helper,
        taxHelper $taxHelper
    ) {
        parent::__construct($context);
        $this->productRepository = $productRepository;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->analytics = $analytics;
        $this->helper = $helper;
        $this->taxHelper = $taxHelper;
    }

    public function execute()
    {
        $result = $this->resultJsonFactory->create();
        if ($this->getRequest()->isAjax()) {
            $post = $this->_request->getParams();
            $product = $this->productRepository->getById($post['product_id']);
            $categories = explode(',',$post['standard_category']);
            $productInformation= [
                'cart_id' => $this->analytics->getCartId(),
                'product_id' => $product->getId(),
                'sku' => $product->getSku(),
                'category_level_1' => (isset($categories[0])) ? trim($categories[0]) : '',
                'category_level_2' => (isset($categories[1])) ? trim($categories[1]) : '',
                'category_level_3' => (isset($categories[2])) ? trim($categories[2]) : '',
                'name' => $product->getName(),
                'image_url' => $this->helper->getImageFullPath($product->getThumbnail()),
                'lifestage' => trim($this->analytics->getAttributeLabel('life_stage', $product)),
                'breed_size' => trim($this->analytics->getAttributeLabel('breed_size', $product)),
                'dietary_needs' => trim($this->analytics->getAttributeLabel('dietary_requirement', $product)),
                'main_flavour' => trim($this->analytics->getAttributeLabel('main_flavour', $product)),
                'colour' => trim($this->analytics->getAttributeLabel('colour', $product)),
                'pack_weight' => $product->getWeight(),
                'brand' => trim($this->analytics->getAttributeLabel('brands', $product)),
                'price' =>  $this->taxHelper->getTaxPrice($product, $product->getFinalPrice(), true),
                'url' => $product->getProductUrl()
            ];

            return $result->setData($productInformation);
        }

    }
}