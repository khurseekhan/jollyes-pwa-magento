<?php
/**
 * Ayko_SegmentioAnalytics
 *
 * @category  Ayko
 * @package   Ayko_SegmentioAnalytics
 * @author    Ayko Development Team <support@ayko.co>
 * @copyright Copyright (c) 2019 Ayko (http://www.ayko.com/)
 *
 */
namespace Ayko\SegmentioAnalytics\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{
    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        /**
         * Create table 'segment_io_analytics_tracking'
         */
        $table = $installer->getConnection()
            ->newTable($installer->getTable('segment_io_analytics_tracking'))
            ->addColumn(
                'id',
                Table::TYPE_INTEGER,
                null,
                [
                    'identity' => true,
                    'unsigned' => true,
                    'nullable' => false,
                    'primary'  => true,
                ],
                'Id'
            )
            ->addColumn(
                'event_type',
                Table::TYPE_TEXT,
                '255',
                [],
                'Event Type'
            )
            ->addColumn(
                'tracking_information',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                '64k',
                [],
                'Tracking Information'
            )
            ->addColumn(
                'status',
                Table::TYPE_SMALLINT,
                null,
                [
                    'nullable' => false,
                    'default'  => '0',
                ],
                'Status'
            )
            ->addIndex(
                $installer->getIdxName('segment_io_analytics_tracking', ['id']),
                ['id']
            )
            ->addIndex(
                $installer->getIdxName('segment_io_analytics_tracking', ['event_type']),
                ['event_type']
            )
            ->addIndex(
                $installer->getIdxName('segment_io_analytics_tracking', ['status']),
                ['status']
            )
            ->setComment('Segment Io Analytics Tracking');
        $installer->getConnection()->createTable($table);

        $installer->endSetup();
    }
}
