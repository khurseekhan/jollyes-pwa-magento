<?php
/**
 * Ayko_SegmentioAnalytics
 *
 * @category  Ayko
 * @package   Ayko_SegmentioAnalytics
 * @author    Ayko Development Team <support@ayko.co>
 * @copyright Copyright (c) 2019 Ayko (http://www.ayko.com/)
 *
 */
namespace Ayko\SegmentioAnalytics\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Catalog\Setup\CategorySetupFactory;
use Magento\Catalog\Model\Category;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Catalog\Model\Product;
use Magento\Eav\Model\Entity\Attribute\Source\Boolean;

class UpgradeData implements UpgradeDataInterface
{
    /**
     * Category setup factory
     *
     * @var CategorySetupFactory
     */
    protected $_categorySetupFactory;

    /**
     * @var EavSetupFactory
     */
    protected $eavSetupFactory;

    /**
     * UpgradeData constructor.
     * @param CategorySetupFactory $categorySetupFactory
     * @param EavSetupFactory $eavSetupFactory
     */
    public function __construct(
        CategorySetupFactory $categorySetupFactory,
        EavSetupFactory $eavSetupFactory
    ) {
        $this->_categorySetupFactory = $categorySetupFactory;
        $this->eavSetupFactory = $eavSetupFactory;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        /** @var \Magento\Eav\Setup\EavSetup $eavSetup */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

        if (version_compare($context->getVersion(), '2.0.1', '<')) {
            $categorySetup = $this->_categorySetupFactory->create(['setup' => $setup]);
            $installer->startSetup();
            $categorySetup->addAttribute(
                Category::ENTITY,
                'standard_category',
                [
                    'type'       => 'int',
                    'label'      => 'Standard Category',
                    'input'      => 'boolean',
                    'source'     => Boolean::class,
                    'required'   => false,
                    'sort_order' => 1,
                    'global'     => ScopedAttributeInterface::SCOPE_STORE,
                ]
            );

            $this->createStandardCategoryAttribute($eavSetup);
        }

        if (version_compare($context->getVersion(), '2.0.3', '<')) {

            $entityType = $eavSetup->getEntityTypeId('catalog_product');

            $eavSetup->updateAttribute($entityType, 'weight', 'used_in_product_listing',true, null);
            $eavSetup->updateAttribute($entityType, 'breed_size', 'used_in_product_listing',true, null);
            $eavSetup->updateAttribute($entityType, 'dietary_requirement', 'used_in_product_listing',true, null);
        }

        $installer->endSetup();
    }

    /**
     * @param \Magento\Eav\Setup\EavSetup $eavSetup
     * @return void
     */
    private function createStandardCategoryAttribute($eavSetup)
    {
        $attributeCode = 'standard_category_path';
        $attributeLabel = 'Standard Category Path';

        $eavSetup->addAttribute(
            Product::ENTITY,
            $attributeCode,
            [
                'type' => 'text',
                'label' => $attributeLabel,
                'input' => 'text',
                'global' => ScopedAttributeInterface::SCOPE_STORE,
                'required' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => true
            ]
        );
    }
}
