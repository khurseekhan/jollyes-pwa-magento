<?php
/**
 * Ayko_SegmentioAnalytics
 *
 * @category  Ayko
 * @package   Ayko_SegmentioAnalytics
 * @author    Ayko Development Team <support@ayko.co>
 * @copyright Copyright (c) 2019 Ayko (http://www.ayko.com/)
 *
 */

namespace Ayko\SegmentioAnalytics\Cron;

use Ayko\SegmentioAnalytics\Model\ConfigInterface;
use Ayko\SegmentioAnalytics\Model\StandardCategory;
use Psr\Log\LoggerInterface;
use Magento\Catalog\Model\Product\Action;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory;
use Ayko\SegmentioAnalytics\Helper\Data;

/**
 * Class UpdateStandardCategoryPathAttribute
 * @package Ayko\SegmentioAnalytics\Cron
 */
class UpdateStandardCategoryPathAttribute
{
    const CRON_JOB_CODE = 'set_standard_category';

    /**
     * @var ConfigInterface
     */
    protected $config;

    /**
     * @var StandardCategory
     */
    private $standardCategory;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var Action
     */
    protected $modelProductAction;

    /**
     * @var CollectionFactory
     */
    protected $categoryCollectionFactory;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Category\Collection
     */
    private $categoryCollection;

    /**
     * @var Data
     */
    private $helper;

    /**
     * UpdateStandardCategoryPathAttribute constructor.
     * @param ConfigInterface $config
     * @param StandardCategory $standardCategory
     * @param LoggerInterface $logger
     * @param Action $modelProductAction
     * @param CollectionFactory $categoryCollectionFactory
     * @param Data $helper
     */
    public function __construct(
        ConfigInterface $config,
        StandardCategory $standardCategory,
        LoggerInterface $logger,
        Action $modelProductAction,
        CollectionFactory $categoryCollectionFactory,
        Data $helper
    ) {
        $this->config = $config;
        $this->standardCategory = $standardCategory;
        $this->logger = $logger;
        $this->modelProductAction = $modelProductAction;
        $this->categoryCollectionFactory = $categoryCollectionFactory;
        $this->helper = $helper;
    }

    /**
     * @return $this
     */
    public function execute()
    {
        if (!$this->config->isModuleEnabled()) {
            return $this;
        }

        try {
            $categoryCollection = $this->getCategoryCollection();
            if ($categoryCollection->getSize()) {
                foreach ($categoryCollection as $category) {
                        $standardCategoryPath = implode(",", array_column($this->standardCategory->getStandardPath($category), "label"));
                        $productIds = ($category->getProductIds()) ? explode(',', $category->getProductIds()):'';
                        if ($productIds && is_array($productIds)) {
                            $this->modelProductAction
                                ->updateAttributes(
                                    $productIds,
                                    ['standard_category_path' => $standardCategoryPath],
                                    $this->standardCategory->getStoreId()
                                );
                        }
                }
            }

            return $this;
        } catch (\Exception $e) {
            $this->logger->error(__class__ . ': ' . $e->getMessage());
        }
    }

    /**
     * @return Collection
     */
    protected function getCategoryCollection()
    {
        $this->categoryCollection = $this->categoryCollectionFactory->create();
        $this->categoryCollection
            ->addFieldtoFilter('include_in_menu', ['eq' => StandardCategory::INCLUDE_IN_MENU_ACTIVE])
            ->addFieldtoFilter('is_active', ['eq' => StandardCategory::IS_ACTIVE_CATEGORY])
            ->addFieldtoFilter('level', ['in' => [StandardCategory::CATEGORIES_LEVEL]])
            ->addFieldtoFilter('standard_category', ['eq' => StandardCategory::STANDARD_CATEGORY_ENABLED])
            ->addFieldToFilter('updated_at',
                $this->helper->dateFilter(self::CRON_JOB_CODE)
            );
        $this->categoryCollection->getSelect()->joinLeft(
            ['ccp' => 'catalog_category_product'],
            'ccp.category_id=e.entity_id',
            ['product_ids' => new \Zend_Db_Expr('group_concat(`ccp`.product_id)')]
        )->group('e.entity_id');

        return $this->categoryCollection;
    }

}
