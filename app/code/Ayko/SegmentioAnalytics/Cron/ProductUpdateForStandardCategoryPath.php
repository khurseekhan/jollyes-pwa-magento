<?php
/**
 * Ayko_SegmentioAnalytics
 *
 * @category  Ayko
 * @package   Ayko_SegmentioAnalytics
 * @author    Ayko Development Team <support@ayko.co>
 * @copyright Copyright (c) 2019 Ayko (http://www.ayko.com/)
 *
 */

namespace Ayko\SegmentioAnalytics\Cron;

use Ayko\SegmentioAnalytics\Model\ConfigInterface;
use Ayko\SegmentioAnalytics\Model\StandardCategory;
use Psr\Log\LoggerInterface;
use Magento\Catalog\Model\Product\Action;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\Catalog\Model\CategoryFactory;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory as CategoryCollection;
use Ayko\SegmentioAnalytics\Helper\Data;

/**
 * Class ProductUpdateForStandardCategoryPath
 * @package Ayko\SegmentioAnalytics\Cron
 */
class ProductUpdateForStandardCategoryPath
{
    const CRON_JOB_CODE = 'update_products_for_standard_category';

    const STANDARD_CATEGORY_COLLECTION_LIMIT = 1;
    /**
     * @var ConfigInterface
     */
    protected $config;

    /**
     * @var StandardCategory
     */
    private $standardCategory;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var Action
     */
    protected $modelProductAction;

    /**
     * @var Collection
     */
    private $collection;

    /**
     * @var CollectionFactory
     */
    protected $productCollectionFactory;

    /**
     * @var CategoryFactory
     */
    protected $categoryFactory;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory
     */
    protected $categoryCollectionFactory;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Category\Collection
     */
    private $categoryCollection;

    /**
     * @var Data
     */
    private $helper;

    /**
     * ProductUpdateForStandardCategoryPath constructor.
     * @param ConfigInterface $config
     * @param StandardCategory $standardCategory
     * @param LoggerInterface $logger
     * @param Action $modelProductAction
     * @param CollectionFactory $productCollectionFactory
     * @param CategoryFactory $categoryFactory
     * @param CategoryCollection $categoryCollectionFactory
     * @param Data $helper
     */
    public function __construct(
        ConfigInterface $config,
        StandardCategory $standardCategory,
        LoggerInterface $logger,
        Action $modelProductAction,
        CollectionFactory $productCollectionFactory,
        CategoryFactory $categoryFactory,
        CategoryCollection $categoryCollectionFactory,
        Data $helper
    )
    {
        $this->config = $config;
        $this->standardCategory = $standardCategory;
        $this->logger = $logger;
        $this->modelProductAction = $modelProductAction;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->categoryFactory = $categoryFactory;
        $this->categoryCollectionFactory = $categoryCollectionFactory;
        $this->helper = $helper;
    }

    /**
     * @return $this
     */
    public function execute()
    {
        if (!$this->config->isModuleEnabled()) {
            return $this;
        }
        $productCollection = $this->getProductCollection();
        if ($productCollection->getSize()) {
            foreach ($productCollection as $product) {
                $this->updateProductStandardCategoryPath($product);
            }
        }

        return $this;
    }

    /**
     * @return Collection
     */
    protected function getProductCollection()
    {
        if (is_null($this->collection)) {
            $this->collection = $this->productCollectionFactory->create();
            $this->collection->addFieldToFilter('updated_at',
                $this->helper->dateFilter(self::CRON_JOB_CODE)
            );
            $this->collection->getSelect()->joinLeft(
                ['ccp' => 'catalog_category_product'],
                'ccp.product_id=e.entity_id',
                ['category_ids' => new \Zend_Db_Expr('group_concat(`ccp`.category_id)')]
            )->group('e.entity_id');
        }

        return $this->collection;
    }

    /**
     *  Update Standard Category Path product attribute based on Standard Category
     * @param $product
     */
    protected function updateProductStandardCategoryPath($product)
    {
        try {
            $categoryIds = $product->getData('category_ids');
            if (count($categoryIds) <= 0) {
                return;
            }

            $standardCategoryPath = $this->getStandardCategoryPathFromCollection($categoryIds);
            $this->modelProductAction
                ->updateAttributes(
                    [$product->getEntityId()],
                    ['standard_category_path' => $standardCategoryPath],
                    $this->standardCategory->getStoreId()
                );

        } catch (\Exception $exception) {
            $this->logger->error(__class__ . ': ' . $exception->getMessage());
        }
    }


    /**
     * @param $categoryIds
     * @return \Magento\Catalog\Model\ResourceModel\Category\Collection
     */
    protected function getStandardCategoryPathFromCollection($categoryIds)
    {
        $this->categoryCollection = $this->categoryCollectionFactory->create();
        $this->categoryCollection
            ->addFieldtoFilter('include_in_menu',['eq'=>StandardCategory::INCLUDE_IN_MENU_ACTIVE])
            ->addFieldtoFilter('is_active',['eq'=>StandardCategory::IS_ACTIVE_CATEGORY])
            ->addFieldtoFilter('standard_category',['eq'=>StandardCategory::STANDARD_CATEGORY_ENABLED])
            ->addFieldtoFilter('level',['in'=>[StandardCategory::CATEGORIES_LEVEL]])
            ->addFieldtoFilter('entity_id',['in'=>[$categoryIds]])
            ->setPageSize(self::STANDARD_CATEGORY_COLLECTION_LIMIT);

        $standardCategoryPath = implode(",", array_column($this->standardCategory->getStandardPath($this->categoryCollection->getFirstItem()), "label"));

        return $standardCategoryPath;
    }

}
