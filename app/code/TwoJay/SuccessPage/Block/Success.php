<?php
/**
 * TwoJay_SuccessPage
 *
 * @category  PHP
 * @package   TwoJay_SuccessPage
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2018 TwoJay (http://www.twojay.co/
 **/
namespace TwoJay\SuccessPage\Block;

use Magento\Customer\Model\Context;
use Magento\Sales\Model\Order;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;

/**
 * Class Success
 * @package TwoJay\SuccessPage\Block
 */
class Success extends \Magento\Checkout\Block\Onepage\Success
{
    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;

    /**
     * @var \Magento\Sales\Model\Order\Config
     */
    protected $_orderConfig;

    /**
     * @var \Magento\Framework\App\Http\Context
     */
    protected $httpContext;

    /**
     * @var \Magento\Sales\Api\Data\OrderInterface
     */
    protected $order;

    /**
     * @var \Magento\Framework\Pricing\Helper\Data
     */
    protected $_price;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterfaceFactory
     */
    protected $_productRepositoryFactory;

    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Magento\Catalog\Helper\ImageFactory
     */
    protected $imageHelperFactory;

    /**
     * Success constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param Order\Config $orderConfig
     * @param \Magento\Framework\App\Http\Context $httpContext
     * @param Order $order
     * @param \Magento\Framework\Pricing\Helper\Data $price
     * @param \Magento\Catalog\Api\ProductRepositoryInterfaceFactory $productRepositoryFactory
     * @param \Magento\Catalog\Helper\ImageFactory $imageHelperFactory
     * @param ScopeConfigInterface $scopeConfig
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Sales\Model\Order\Config $orderConfig,
        \Magento\Framework\App\Http\Context $httpContext,
        \Magento\Sales\Model\Order $order,
        \Magento\Framework\Pricing\Helper\Data $price,
        \Magento\Catalog\Api\ProductRepositoryInterfaceFactory $productRepositoryFactory,
        \Magento\Catalog\Helper\ImageFactory $imageHelperFactory,
        ScopeConfigInterface $scopeConfig,
        array $data = []
    ) {
        parent::__construct($context,$checkoutSession,$orderConfig,$httpContext,$data);
        $this->_checkoutSession = $checkoutSession;
        $this->_orderConfig = $orderConfig;
        $this->_isScopePrivate = true;
        $this->httpContext = $httpContext;
        $this->order = $order;
        $this->_price = $price;
        $this->_productRepositoryFactory = $productRepositoryFactory;
        $this->imageHelperFactory = $imageHelperFactory;
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @get order items
     * @return string
     */
    public function getOrderItems()
    {
        $lastorderId = $this->_checkoutSession->getLastOrderId();
        $order = $this->order->load($lastorderId);
        $htmlArray = [];
        foreach ($order->getAllItems() as $item) {

           $product = $this->_productRepositoryFactory->create()->getById($item->getProductId());
            $imageUrl = $this->imageHelperFactory->create()
                ->init($product, 'product_thumbnail_image')->getUrl();

           $html = '<div id="product-info">';
           $html .= '<div id="product_image">'."<img src='". $imageUrl."'>".'</div>';
           $html .= '<div id="product-detail">';
           $html .= '<div id="product_name">'.$item->getName().'</div>';
           $html .= '<div id="product_price">'.$this->_price->currency($item->getPrice(),true, false).'</div>';
           $html .= '</div>';
           $html .= '</div>';
           $htmlArray[] = $html;
        }

        return $htmlArray;
    }

    /**
     * @get store owner email id
     * @return mixed
     */
    public function getStoreEmail()
    {
        return $this->scopeConfig->getValue('trans_email/ident_general/email',ScopeInterface::SCOPE_STORE);
    }

    /**
     * Get Last Real Order
     * @return Order
     */
    public function getOrder() {
        return $this->_checkoutSession->getLastRealOrder();
    }
}

