<?php
/**
 * TwoJay_SuccessPage
 *
 * @category  PHP
 * @package   TwoJay_SuccessPage
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2018 TwoJay (http://www.twojay.co/
 **/
namespace TwoJay\SuccessPage\Block;

use Magento\Framework\View\Element\Template;

/**
 * Class Registration
 * @package TwoJay\SuccessPage\Block
 */
class Registration extends \Magento\Checkout\Block\Registration
{
    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $customerSession;

    /**
     * @var \Magento\Customer\Model\Registration
     */
    protected $registration;

    /**
     * @var \Magento\Customer\Api\AccountManagementInterface
     */
    protected $accountManagement;

    /**
     * @var \Magento\Sales\Api\OrderRepositoryInterface
     */
    protected $orderRepository;

    /**
     * @var \Magento\Sales\Model\Order\Address\Validator
     */
    protected $addressValidator;

    /**
     * @var \Magento\Framework\Data\Form\FormKey
     */
    protected $_formKey;

    /**
     * Registration constructor.
     * @param Template\Context $context
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Customer\Model\Registration $registration
     * @param \Magento\Customer\Api\AccountManagementInterface $accountManagement
     * @param \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
     * @param \Magento\Sales\Model\Order\Address\Validator $addressValidator
     * @param \Magento\Framework\Data\Form\FormKey $formKey
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Customer\Model\Registration $registration,
        \Magento\Customer\Api\AccountManagementInterface $accountManagement,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Magento\Sales\Model\Order\Address\Validator $addressValidator,
        \Magento\Framework\Data\Form\FormKey $formKey,
        array $data = []
    ) {
        $this->customerSession = $customerSession;
        $this->_formKey = $formKey;
        parent::__construct($context,$checkoutSession,$customerSession,$registration,$accountManagement,$orderRepository,$addressValidator,$data);
    }

    /**
     * Validate order addresses
     *
     * @return bool
     */
    protected function validateAddresses()
    {
        $order = $this->orderRepository->get($this->checkoutSession->getLastOrderId());
        $addresses = $order->getAddresses();
        foreach ($addresses as $address) {
            $result = $this->addressValidator->validateForCustomer($address);
            if (is_array($result) && !empty($result)) {
                return false;
            }
        }
        return true;
    }

    /**
     * @check customer loggedIn or not
     * @return bool
     */
    public function isCustomerLoggedIn()
    {
       return $this->customerSession->isLoggedIn();
    }

    /**
     * @get Form Key
     *
     * @return string
     */
    public function getFormKey()
    {
        return $this->_formKey->getFormKey();
    }

    /**
     * @get guest first name
     * @return string
     */
    public function getCustomerFirstName()
    {

        $order = $this->orderRepository->get($this->checkoutSession->getLastOrderId());
        return $order->getBillingAddress()->getFirstname();
    }

    /**
     * @get guest Last name
     * @return string
     */
    public function getCustomerLastName()
    {
        $order = $this->orderRepository->get($this->checkoutSession->getLastOrderId());
        return $order->getBillingAddress()->getLastname();
    }
}
