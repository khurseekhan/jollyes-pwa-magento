<?php
/**
 * TwoJay_SuccessPage
 *
 * @category  PHP
 * @package   TwoJay_SuccessPage
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2018 TwoJay (http://www.twojay.co/
 **/
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'TwoJay_SuccessPage',
    __DIR__
);
