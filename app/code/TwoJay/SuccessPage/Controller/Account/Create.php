<?php
/**
 * TwoJay_SuccessPage
 *
 * @category  PHP
 * @package   TwoJay_SuccessPage
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2018 TwoJay (http://www.twojay.co/
 **/

namespace TwoJay\SuccessPage\Controller\Account;

use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\Data\OrderAddressInterface;
use Magento\Customer\Api\Data\AddressInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Quote\Model\Quote\Address as QuoteAddress;
use Magento\Quote\Model\Quote\AddressFactory as QuoteAddressFactory;
use Magento\Sales\Model\Order\OrderCustomerExtractor;
use Magento\Framework\UrlFactory;
use Magento\Customer\Model\Account\Redirect as AccountRedirect;
use TwoJay\Loyalty\Api\Loyalty\RegisterInterface;
use Magento\Framework\Json\Helper\Data as JsonHelper;
use TwoJay\Loyalty\Helper\Data as LoyaltyHelper;


/**
 * Class Create
 * @package TwoJay\SuccessPage\Controller\Account
 */
class Create extends \Magento\Framework\App\Action\Action implements \Magento\Sales\Api\OrderCustomerManagementInterface
{
    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $customerSession;

    /**
     * @var AccountRedirect
     */
    private $accountRedirect;

    /**
     * @var \Magento\Customer\Api\AccountManagementInterface
     */
    protected $accountManagement;

    /**
     * @var \Magento\Sales\Api\OrderCustomerManagementInterface
     */
    protected $orderCustomerService;

    /**
     * @var QuoteAddressFactory
     */
    private $quoteAddressFactory;

    /**
     * @var OrderCustomerExtractor
     */
    private $customerExtractor;

    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $urlModel;

    /**
     * @var RegisterInterface
     */
    protected $_register;

    /**
     * @var JsonHelper
     */
    protected $_jsonHelper;

    /**
     * @var LoyaltyHelper
     */
    protected $loyaltyHelper;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;

    /**
     * Create constructor.
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Customer\Api\AccountManagementInterface $accountManagement
     * @param \Magento\Sales\Api\OrderCustomerManagementInterface $orderCustomerService
     * @param \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
     * @param UrlFactory $urlFactory
     * @param AccountRedirect $accountRedirect
     * @param QuoteAddressFactory|null $quoteAddressFactory
     * @param OrderCustomerExtractor|null $orderCustomerExtractor
     * @param RegisterInterface $register
     * @param JsonHelper $jsonHelper
     * @param LoyaltyHelper $loyaltyHelper
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Customer\Api\AccountManagementInterface $accountManagement,
        \Magento\Sales\Api\OrderCustomerManagementInterface $orderCustomerService,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        UrlFactory $urlFactory,
        AccountRedirect $accountRedirect,
        QuoteAddressFactory $quoteAddressFactory = null,
        OrderCustomerExtractor $orderCustomerExtractor = null,
        RegisterInterface $register,
        JsonHelper $jsonHelper,
        LoyaltyHelper $loyaltyHelper,
        \Magento\Framework\Message\ManagerInterface $messageManager
    )
    {
        $this->checkoutSession = $checkoutSession;
        $this->customerSession = $customerSession;
        $this->accountManagement = $accountManagement;
        $this->orderCustomerService = $orderCustomerService;
        $this->orderRepository = $orderRepository;
        $this->quoteAddressFactory = $quoteAddressFactory
            ?: ObjectManager::getInstance()->get(QuoteAddressFactory::class);
        $this->customerExtractor = $orderCustomerExtractor
            ?? ObjectManager::getInstance()->get(OrderCustomerExtractor::class);

        $this->urlModel = $urlFactory->create();
        $this->accountRedirect = $accountRedirect;
        $this->_register = $register;
        $this->_jsonHelper = $jsonHelper;
        $this->loyaltyHelper = $loyaltyHelper;
        $this->messageManager = $messageManager;
        parent::__construct($context);
    }

    /**
     * Execute request
     *
     * @throws AlreadyExistsException
     * @throws NoSuchEntityException
     * @throws \Exception
     * @return \Magento\Framework\Controller\Result\Json
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($this->customerSession->isLoggedIn()) {
            $resultRedirect->setPath('*/*/');
            return $resultRedirect;
        }
        $orderId = $this->checkoutSession->getLastOrderId();
        $isSuccess = $this->getRequest()->getParam('is_success');
        // @codingStandardsIgnoreEnd
        if (isset($isSuccess) && $isSuccess == 1) {
            $url = $this->urlModel->getUrl('*/*/index', ['_secure' => true]);
        } else {
            $url = $this->urlModel->getUrl('*/*/index', ['_secure' => true]);
        }
        if (!$orderId) {
            $resultRedirect->setPath('*/*/');
            return $resultRedirect;
        }
        try {
            $this->create($orderId);
            $resultRedirect = $this->accountRedirect->getRedirect();

        } catch (\Exception $e) {
            $this->messageManager->addException($e, $e->getMessage());
            $resultRedirect->setUrl($this->_redirect->error($url));
        }
        return $resultRedirect;
    }

    /**
     * {@inheritdoc}
     */
    public function create($orderId)
    {
        $order = $this->orderRepository->get($orderId);
        if ($order->getCustomerId()) {
            throw new AlreadyExistsException(
                __('This order already has associated customer account')
            );
        }
        $customer = $this->customerExtractor->extract($orderId);
        /** @var AddressInterface[] $filteredAddresses */
        $filteredAddresses = [];
        foreach ($customer->getAddresses() as $address) {
            if ($this->needToSaveAddress($order, $address)) {
                $filteredAddresses[] = $address;
            }
        }

        $customer->setAddresses($filteredAddresses);

        $password = $this->getRequest()->getParam('password');
        $confirmation = $this->getRequest()->getParam('password_confirmation');
        $firstname = $this->getRequest()->getParam('firstname');
        $lastname = $this->getRequest()->getParam('lastname');
        $contact_by_email = $this->getRequest()->getParam('contact_by_email');
        $contact_in_app = $this->getRequest()->getParam('contact_in_app');
        $contact_by_sms = $this->getRequest()->getParam('contact_by_sms');
        $customer->setCustomAttribute('contact_by_email', $contact_by_email);
        $customer->setCustomAttribute('contact_in_app', $contact_in_app);
        $customer->setCustomAttribute('contact_by_sms', $contact_by_sms);

        $redirectUrl = $this->customerSession->getBeforeAuthUrl();

        // check whether the password and confirm password field matches or not
        $this->checkPasswordConfirmation($password, $confirmation);

        $customer = $this->accountManagement->createAccount($customer, $password, $redirectUrl);
        $order = $this->orderRepository->get($orderId);
        $order->setCustomerId($customer->getId());
        $order->setCustomerIsGuest(0);
        $order->setCustomerFirstname($firstname);
        $order->setCustomerLastname($lastname);
        $this->orderRepository->save($order);
        $this->customerSession->setCustomerDataAsLoggedIn($customer);

        // call loyalty system for customer registration
        if ($customer->getAddresses()) {
            $orderAddress = $order->getBillingAddress();
        } else {
            $orderAddress = $order->getShippingAddress();
        }

        $fullStreetAddress = '';
        if (!empty($orderAddress->getStreet())) {
            $streetAddress = $orderAddress->getStreet();
            foreach ($streetAddress as $streetAddress) {
                if (!empty($fullStreetAddress)) {
                    $fullStreetAddress .= ", " . trim($streetAddress);
                } else {
                    $fullStreetAddress .= trim($streetAddress);
                }
            }
        }

        if ($this->loyaltyHelper->isModuleEnabled()) {

            $registrationArray = [
                "FirstName" => $firstname,
                "LastName" => $lastname,
                "Birthdate" => "",
                "isPasswordHashed" => false,
                "Pwd" => $password,
                "FavouriteStoreID" => $order->getStoreId(),
                "Email" => $order->getCustomerEmail(),
                "MobilePhone" => $orderAddress->getTelephone(),
                "AcceptTandC" => true,
                "AcceptPrivacy" => true,
                "OptInInternal" => true,
                "OptInExternal" => true,
                "ContactbyEmail" => $contact_by_email,
                "ContactbySMS" => $contact_by_sms,
                "ActivateDate" => $order->getCreatedAt(),
                "Address" => $fullStreetAddress,
                "PostCode" => $orderAddress->getPostcode(),
                "City" => $orderAddress->getCity(),
                "SignupChannel" => "WEB"
            ];

            $this->loyaltyHelper->log('Order success page customer register loyalty request:');
            $this->loyaltyHelper->log(print_r($registrationArray, 1));

            $registerResponse = $this->_register->register($registrationArray);

            // check for 503 service unavailable
            if (!$this->loyaltyHelper->isJson($registerResponse) && strpos($registerResponse, '503') !== false) {
                $this->loyaltyHelper->insertToLoyaltyLogs('Customer', 'Success Page Register',
                    $order->getCustomerEmail(), 'Unable to register due to loyalty API HTTP Error 503.', null, null, $customer->getId());
            } else {
                if ($registerResponse) {
                    $registerResponse = $this->_jsonHelper->jsonDecode($registerResponse);
                    $this->loyaltyHelper->log('Order success page customer register loyalty response:');
                    $this->loyaltyHelper->log(print_r($registerResponse, 1));
                }
            }
        }

        $this->messageManager->addSuccess(
            __('Customer account with email %1 created successfully.',
                $order->getCustomerEmail())
        );

        return $customer;
    }

    /**
     * @param OrderInterface $order
     * @param AddressInterface $address
     *
     * @return bool
     */
    private function needToSaveAddress(
        OrderInterface $order,
        AddressInterface $address
    ): bool
    {
        /** @var OrderAddressInterface|null $orderAddress */
        $orderAddress = null;
        if ($address->isDefaultBilling()) {
            $orderAddress = $order->getBillingAddress();
        } elseif ($address->isDefaultShipping()) {
            $orderAddress = $order->getShippingAddress();
        }
        if ($orderAddress) {
            $quoteAddressId = $orderAddress->getData('quote_address_id');
            if ($quoteAddressId) {
                /** @var QuoteAddress $quote */
                $quote = $this->quoteAddressFactory->create()
                    ->load($quoteAddressId);
                if ($quote && $quote->getId()) {
                    return true;
                }
            }

            return true;
        }

        return false;
    }

    /**
     * Make sure that password and password confirmation matched
     *
     * @param string $password
     * @param string $confirmation
     * @return void
     * @throws InputException
     */
    protected function checkPasswordConfirmation($password, $confirmation)
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($password != $confirmation) {
            $this->messageManager->addError(
                __(
                    'Please make sure your passwords match.'
                )
            );
            // @codingStandardsIgnoreEnd
            $url = $this->urlModel->getUrl('checkout/onepage/success', ['_secure' => true]);
            $resultRedirect->setUrl($this->_redirect->success($url));

            return false;
        }

        return true;
    }
}

