<?php
/**
 * TwoJay_BraintreeOrderStatusFix
 *
 * @category  TwoJay
 * @package   TwoJay_BraintreeOrderStatusFix
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2018 Twojay (http://www.twojay.co/
 *
 */
namespace TwoJay\BraintreeOrderStatusFix\Plugin\Order\Payment\State;

use Magento\Framework\App\ObjectManager;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\Data\OrderPaymentInterface;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\StatusResolver;
use Magento\Sales\Model\Order\Payment\State\CaptureCommand as SalesCaptureCommand;

class CaptureCommand {

    /**
     * @var StatusResolver
     */
    private $statusResolver;

    /**
     * @param StatusResolver|null $statusResolver
     */
    public function __construct(StatusResolver $statusResolver = null)
    {
        $this->statusResolver = $statusResolver
            ? : ObjectManager::getInstance()->get(StatusResolver::class);
    }

    /**
     * @param SalesCaptureCommand $captureCommand
     * @param \Closure $proceed
     * @param $payment
     * @param $amount
     * @param OrderInterface $order
     * @return mixed
     */
    public function aroundExecute(
        SalesCaptureCommand $captureCommand,
        \Closure $proceed,
        $payment,
        $amount,
        OrderInterface $order
    )
    {
        $message = $proceed($payment, $amount, $order);
        $status = null;
        $state = Order::STATE_PROCESSING;

        if ($payment->getIsTransactionPending()) {
            $state = Order::STATE_PAYMENT_REVIEW;
        }

        if ($payment->getIsFraudDetected()) {
            $state = Order::STATE_PAYMENT_REVIEW;
            $status = Order::STATUS_FRAUD;
        }

        if (!isset($status)) {
            if($payment->getMethod()=="braintree"){
                if (!$status) {
                    $status = $order->getConfig()->getStateDefaultStatus($state);
                }
            }else{
                $status = $this->statusResolver->getOrderStatusByState($order, $state);
            }
        }

        $order->setState($state);
        $order->setStatus($status);

        return $message;
    }
}
