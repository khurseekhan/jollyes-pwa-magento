<?php
/**
 * TwoJay_BraintreeOrderStatusFix
 *
 * @category  TwoJay
 * @package   TwoJay_BraintreeOrderStatusFix
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2018 Twojay (http://www.twojay.co/
 *
 */
use \Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(
    ComponentRegistrar::MODULE,
    'TwoJay_BraintreeOrderStatusFix',
    __DIR__
);