<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_CartUpdate
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */
namespace TwoJay\CartUpdate\Plugin\Model;

/**
 * Plugin for Estimated collect total mask.
 */
class EstimateCalculation
{
    /**
     * Quote repository.
     *
     * @var \Magento\Quote\Api\CartRepositoryInterface
     */
    protected $cartRepository;

    /**
     * @param \Magento\Quote\Api\CartRepositoryInterface $cartRepository
     * @codeCoverageIgnore
     */
    public function __construct(
        \Magento\Quote\Api\CartRepositoryInterface $cartRepository
    ){
        $this->cartRepository = $cartRepository;
    }

    public function beforeCalculate(
        \Magento\Checkout\Model\TotalsInformationManagement $subject,
        $cartId,
        \Magento\Checkout\Api\Data\TotalsInformationInterface $addressInformation
    )
    {
        $quote = $this->cartRepository->get($cartId);
        $quote->setShippingAddress($addressInformation->getAddress());
        $address = $quote->getShippingAddress();
        if( $address->getShippingMethod()=='' ) {

            $address->setPostcode('s2')
                ->setFirstname('T')
                ->setLastname('T')
                ->setCity('sheffield')
                ->setCountryId('GB')
                ->setStreet('-')
                ->setTelephone('07575757575');

            $address->setCollectShippingRates(true)->setShippingMethod(
                $addressInformation->getShippingCarrierCode() . '_' .
                $addressInformation->getShippingMethodCode()
            );

            try {
                $this->cartRepository->save($quote);
            } catch (\Exception $e) {
            }
        }
    }
}