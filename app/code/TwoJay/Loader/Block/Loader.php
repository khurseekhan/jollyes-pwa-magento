<?php
/**
 * TwoJay_Loader
 *
 * @category  XML
 * @package   TwoJay_Loader
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/
 **/

namespace TwoJay\Loader\Block;

/**
 * Class Loader
 * @package TwoJay\Loader\Block
 */
class Loader extends \Magento\Framework\View\Element\Template
{
    /**
     * Loader constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
    }
}
