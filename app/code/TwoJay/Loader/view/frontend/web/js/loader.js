/**
 * TwoJay_Loader
 *
 * @category  XML
 * @package   TwoJay_Loader
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/
 **/

require(['jquery'], function ($) {
    $(document).ready(function () {
        /*********start of success page loader**********/
        if ($('#success-guest-register')) {
            $('#success-guest-register').on('click submit', function (e) {
                var password = $('#password').val();
                var password_confirmation = $('#password-confirmation').val();
                if ((password != '') && (password_confirmation != '') && (password == password_confirmation)) {
                    $('body').loader('show');
                } else {
                    $('body').loader('hide');
                }
            });
        }
        /*********end of success page loader**********/

        /******start of customer account pages loader*******/
        var pathname = window.location.pathname;
        if (pathname.indexOf('/customer/account/edit/') != -1 ||
            pathname.indexOf('/customer/account/create/') != -1 ||
            pathname.indexOf('/customer/address/new/') != -1) {
            $('.save').add('.submit').on('click submit', function (e) {
                if ($('#form-validate').valid()) {
                    $('body').loader('show');
                } else {
                    $('body').loader('hide');
                }
            });
            $('.already-login').on('click', function (e) {
                $('body').loader('show');
            });
        }
        if (pathname.indexOf('/customer/address/edit/') != -1) {
            $('.submit').on('click submit', function (e) {
                $('body').loader('show');
            });
        }
        /******end of customer account pages loader*******/
    });
});