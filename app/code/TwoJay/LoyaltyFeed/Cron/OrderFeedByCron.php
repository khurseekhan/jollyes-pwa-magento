<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */
namespace TwoJay\LoyaltyFeed\Cron;

use Psr\Log\LoggerInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;
use TwoJay\LoyaltyFeed\Model\OrderFeedFactory;
use TwoJay\LoyaltyFeed\Helper\Data;
use Magento\Framework\Filesystem\Io\Sftp;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\ResourceConnection;

class OrderFeedByCron
{

    protected $_path = 'media/loyalty';

    /**
     * @var ResourceConnection
     */
    protected $_resource;

    /**
     * @var
     */
    protected $connection;

    protected $_helper;

    /**
     * @param DirectoryList $directoryList
     * @param Filesystem $filesystem
     * @param ScopeConfigInterface $scopeConfig
     * @param OrderFeedFactory $loyaltyOrderFeed
     * @param Sftp $sftp
     * @param ResourceConnection $resource
     * @param Data $helper
     */
    public function __construct(
        DirectoryList $directoryList,
        Filesystem $filesystem,
        ScopeConfigInterface $scopeConfig,
        OrderFeedFactory $loyaltyOrderFeed,
        Sftp $sftp,
        ResourceConnection $resource,
        Data $helper
    )

    {
        $this->filesystem = $filesystem;
        $this->directoryList = $directoryList;
        $this->scopeConfig = $scopeConfig;
        $this->sftp = $sftp;
        $this->loyaltyOrderFeed = $loyaltyOrderFeed;
        $this->_resource = $resource;
        $this->_helper = $helper;
    }

    public function execute() {
        $this->getCsvFile();
    }

    public function getCsvFile() {

        try {
            $connection = $this->getConnection();
            $host = $this->_helper->getHost();
            $port = $this->_helper->getPort();
            $username = $this->_helper->getUser();
            $password = $this->_helper->getPassword();
            $fileDirectoryPath = $this->directoryList->getPath(\Magento\Framework\App\Filesystem\DirectoryList::PUB);
            $fileDirectoryPath = $fileDirectoryPath . '/' . $this->_path;
            $conn = ssh2_connect($host, $port);

            if (!ssh2_auth_password($conn, $username, $password)) {
                $this->_helper->log('SSH authentication failed');
                throw new \Exception('SSH authentication failed');
            }else {
                $orderFeedFiles = $this->getOrderFeeds();
                if($orderFeedFiles) {
                    foreach($orderFeedFiles as $orderFeed){

                        $sourcePath = $fileDirectoryPath.'/'.$orderFeed['file_name'];
                        $destinationPath = '/'.$orderFeed['file_name'];

                        if(ssh2_scp_send($conn, $sourcePath, $destinationPath, 0644)){
                            $where = ['file_name = (?)' => $orderFeed['file_name']];
                            $connection->update('loyalty_feed_order_history', ['file_status' => 1], $where);
                        }else {
                            $this->_helper->log("File transfer is failed for ".$orderFeed['file_name']);
                        }
                    }
                }
            }
        } catch(\Exception $e) {
            $this->_helper->log($e->getMessage());
            echo $e->getMessage();

        }
    }

    /**
     * @return mixed
     */
    public function getConnection()
    {
        if (!$this->connection) {
            $this->connection = $this->_resource->getConnection();
        }

        return $this->connection;
    }

    /**
     *  get Order Feed
     */
    public function getOrderFeeds()
    {
        $table = $this->_resource->getTableName('loyalty_feed_order_history');
        $sql = $this->getConnection()->select()
            ->from(['ce' => $table], array('order_id','file_name'))
            ->where('file_status = 0');

        return $this->getConnection()->fetchAll($sql);
    }
}
?>