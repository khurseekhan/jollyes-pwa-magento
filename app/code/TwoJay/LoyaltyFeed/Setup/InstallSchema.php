<?php
/**
 * TwoJay_LoyaltyFeed
 *
 * @category  TwoJay
 * @package   TwoJay_LoyaltyFeed
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */

namespace TwoJay\LoyaltyFeed\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * @codeCoverageIgnore
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        /**
         * Create table 'loyalty_api_history'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('loyalty_feed_order_history')
        )->addColumn(
            'id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'Id'
        )->addColumn(
            'order_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['nullable' => true],
            'Order Id'
        )->addColumn(
            'file_name',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            250,
            ['nullable' => false],
            'File Name'
        )->addColumn(
            'file_status',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['nullable' => true, 'default' => 0],
            'File Transfer Status'
        )->addColumn(
            'created_at',
            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
            null,
            ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
            'Created At'
        )->addIndex(
            $installer->getIdxName('loyalty_feed_order_history', ['order_id']),
            ['order_id']
        )->addIndex(
            $installer->getIdxName('loyalty_feed_order_history', ['created_at', 'file_name']),
            ['created_at', 'file_name']
        )->setComment(
            'Loyalty Feed Order History'
        );

        $installer->getConnection()->createTable($table);

        $installer->endSetup();

    }
}
