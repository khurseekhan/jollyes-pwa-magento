<?php
/**
 * TwoJay_LoyaltyFeed
 *
 * @category  TwoJay
 * @package   TwoJay_LoyaltyFeed
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */

namespace TwoJay\LoyaltyFeed\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;

class Data extends AbstractHelper
{
    /**
     * @param Context $context
     */
    public function __construct(
        Context $context
    )
    {
        parent::__construct($context);
    }


    public function getHost() {
        return $this->scopeConfig->getValue(
            'twojay_section/general/sftp_host',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getPort() {
        return $this->scopeConfig->getValue(
            'twojay_section/general/sftp_port',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getUser() {
        return $this->scopeConfig->getValue(
            'twojay_section/general/sftp_username',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getPassword() {
        return $this->scopeConfig->getValue(
            'twojay_section/general/sftp_password',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @param $message
     */
    public function log($message)
    {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/loyalty_order_feed.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info($message);
    }
}
