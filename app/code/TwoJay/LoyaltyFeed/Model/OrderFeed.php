<?php
/**
 * TwoJay_LoyaltyFeed
 *
 * @category  TwoJay
 * @package   TwoJay_LoyaltyFeed
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */
namespace TwoJay\LoyaltyFeed\Model;

use Magento\Framework\Model\AbstractModel;

class OrderFeed extends AbstractModel
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('TwoJay\LoyaltyFeed\Model\ResourceModel\OrderFeed');
    }
}