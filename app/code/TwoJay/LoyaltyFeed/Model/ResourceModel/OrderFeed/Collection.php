<?php
/**
 * TwoJay_LoyaltyFeed
 *
 * @category  TwoJay
 * @package   TwoJay_LoyaltyFeed
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */
namespace TwoJay\LoyaltyFeed\Model\ResourceModel\OrderFeed;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * Identifier field name for collection items
     *
     * Can be used by collections with items without defined
     *
     * @var string
     */
    protected $_idFieldName = 'id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('TwoJay\LoyaltyFeed\Model\OrderFeed', 'TwoJay\LoyaltyFeed\Model\ResourceModel\OrderFeed');

    }

    /**
     * Define resource model
     *
     * @return string
     */
    public function getIdFieldName()
    {
        return $this->_idFieldName;
    }
}
