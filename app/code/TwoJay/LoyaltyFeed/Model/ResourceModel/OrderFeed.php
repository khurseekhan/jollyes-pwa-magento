<?php
/**
 * TwoJay_LoyaltyFeed
 *
 * @category  TwoJay
 * @package   TwoJay_LoyaltyFeed
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */
namespace TwoJay\LoyaltyFeed\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class OrderFeed extends AbstractDb
{

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('loyalty_feed_order_history', 'id');
    }

}