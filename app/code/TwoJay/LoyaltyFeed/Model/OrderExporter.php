<?php

namespace TwoJay\LoyaltyFeed\Model;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\ResourceConnection;
use Magento\Sales\Model\Order;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Tax\Api\TaxCalculationInterface;

class OrderExporter
{
    const ITIM_SOURCE = 'website';
    const ITIM_STORE_ID = 60;
    const ITIM_TILL_NUM = 1;
    const ITIM_SHIIPPING_CODE = 30365;
    const ITIM_SHIPPING_QUANTITY = 1;
    const ITIM_SHIPPING_DISCOUNT = 0;
    const EXPORT_PATH = 'media/loyalty';
    protected $loyaltyNumber='';
    /** @var ResourceConnection\ */
    protected $_resource;
    protected $_connection;
    /** @var DirectoryList */
    private $directoryList;
    /** @var CustomerRepositoryInterface  */
    private $customerRepositoryInterface;
    /** @var $productRepository  */
    private $productRepository;
    /**
     * OrderExporter constructor.
     * @param DirectoryList $directoryList
     * @param ResourceConnection $resource
     * @param CustomerRepositoryInterface $CustomerRepositoryInterface
     * @param ProductRepositoryInterface $productRepository
     * @param TaxCalculationInterface $taxCalculation
     * @param OrderItemRepositoryInterface $orderItemRepository
     */

    public function __construct(
        DirectoryList $directoryList,
        ResourceConnection $resource,
        CustomerRepositoryInterface $CustomerRepositoryInterface,
        ProductRepositoryInterface $productRepository,
        TaxCalculationInterface $taxCalculation


    )
    {
        $this->directoryList = $directoryList;
        $this->_resource = $resource;
        $this->customerRepositoryInterface = $CustomerRepositoryInterface;
        $this->productRepository = $productRepository;
        $this->taxCalculation = $taxCalculation;

    }

    /**
     * @return \Magento\Framework\DB\Adapter\AdapterInterface
     */
    public function getConnection()
    {
        if (!$this->_connection) {
            $this->_connection = $this->_resource->getConnection();
        }

        return $this->_connection;
    }

    /**
     * @param Order $order
     * @return array
     */
    public function getTxnHeader(Order $order)
    {
        $orderId = $order->getIncrementId();
        $operatorName = $order->getCustomerName();
        $email = $order->getCustomerEmail();
        $subtotal = $order->getSubtotalInclTax();
        $mobilePhone = $order->getBillingAddress()->getTelephone();
        $txnStatus = $order->getStatus();
        $customerId = $order->getCustomerId();
        if(!$customerId) {
            $this->loyaltyNumber = '';
        }else {
            $customer = $this->customerRepositoryInterface->getById($customerId);
            if ($customer->getCustomAttribute('loyalty_number')) {
                $this->loyaltyNumber = $customer->getCustomAttribute('loyalty_number')->getValue();
            }
        }
        $transactionDate= $order->getCreatedAt();
        if(!$transactionDate) {
            $transactionDate = date("Y-m-d H:i:s");
        }
        if ($txnStatus !== 'canceled') {
            $txnType = 'SALE';
        } else {
            $txnType = strtoupper($txnStatus);
        }
        $data = [
            ['StoreNumber', 'TillNumber', 'TransactionNumber', 'OrderNumber', 'TransactionDate', 'LoyaltyNumber', 'Email',
                'MobilePhone', 'SubTotal', 'TxnType', 'Source', 'OperatorName'],
            [self::ITIM_STORE_ID, self::ITIM_TILL_NUM, $orderId, $orderId, $transactionDate, $this->loyaltyNumber, $email,
                $mobilePhone, $subtotal, $txnType, self::ITIM_SOURCE, $operatorName]
        ];

        return $data;
    }

    /**
     * @param Order $order
     * @return array
     */
    public function getTxnItems(Order $order)
    {
        $orderId = $order->getIncrementId();

        $dataItems = array();

        $shippingAmount = $order->getShippingAmount();

        $dataItems[] = ['StoreNumber', 'TillNumber', 'TransactionNumber', 'ProductCode', 'Quantity',
            'SellPrice', 'DiscountValue', 'SoldValue', 'Description'];

        /** @var \Magento\Sales\Model\Order\Item $item */


        foreach ($order->getAllItems() as $item) {
            if($item->getData('has_children')) {
                continue;
            } else {
                if ($item->getParentItem())
                {
                    $discountAmount = $item->getParentItem()->getDiscountAmount();
                } else {
                    $discountAmount = $item->getDiscountAmount();
                }
                $product = $this->productRepository->getById($item->getProductId());

                if ($taxAttribute = $product->getCustomAttribute('tax_class_id')) {
                    $productRateId = $taxAttribute->getValue();
                    $rate = $this->taxCalculation->getCalculatedRate($productRateId);
                    $priceExcludingTax = $product->getPrice();
                    $priceIncludingTax = $priceExcludingTax + ($priceExcludingTax * ($rate / 100));
                }
                $qty = $item->getQtyOrdered();
                $sku = $item->getSku();
                $rowtotal = number_format($priceIncludingTax*$qty  , 2, '.', '') ;
                $description = $item->getName();
                $total = $rowtotal - $discountAmount;
                $dataItems[] =
                    [self::ITIM_STORE_ID, self::ITIM_TILL_NUM, $orderId, $sku, $qty, $rowtotal, $discountAmount, $total, $description];
            }
        }

        if ($shippingAmount > 0) {
            $dataItems[] =
                [self::ITIM_STORE_ID, self::ITIM_TILL_NUM, $orderId, self::ITIM_SHIIPPING_CODE,
                    self::ITIM_SHIPPING_QUANTITY, $shippingAmount, self::ITIM_SHIPPING_DISCOUNT, $shippingAmount];
        }

        return $dataItems;
    }

    /**
     * @param $fileName
     * @param $data
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    private function writeCsv($fileName, $data)
    {
        $fileDirectoryPath = $this->directoryList->getPath(DirectoryList::PUB);
        $fileDirectoryPath = $fileDirectoryPath . '/' . self::EXPORT_PATH;
        if (!file_exists($fileDirectoryPath)) {
            mkdir($fileDirectoryPath, 0777, true);
        }

        $filePath = $fileDirectoryPath . '/' . $fileName;
        $fh = fopen($filePath, 'w');
        foreach ($data as $dataRow) {
            fputs($fh, implode($dataRow,',')."\n");
        }
        fclose($fh);
    }

    /**
     * @param Order $order
     * @param $fileName
     */
    private function logCsvExported(Order $order, $fileName)
    {
        $connection = $this->getConnection();
        $bind = ['order_id' => (int)$order->getIncrementId(),
            'file_name' => (string)$fileName];
        $connection->insert('loyalty_feed_order_history', $bind);
    }

    /**
     * @param Order $order
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function writeTxnHeaders(Order $order)
    {
        $fileName = 'Loyalty_TxnHeader_' . $order->getIncrementId() .'.csv';
        $data = $this->getTxnHeader($order);
        $this->writeCsv($fileName, $data);
        $this->logCsvExported($order, $fileName);
    }

    /**
     * @param Order $order
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function writeTxnItems(Order $order)
    {
        $fileName = 'Loyalty_ItemLine_' . $order->getIncrementId() .'.csv';
        $data = $this->getTxnItems($order);
        $this->writeCsv($fileName, $data);
        $this->logCsvExported($order, $fileName);
    }
}