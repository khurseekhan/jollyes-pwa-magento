<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */

namespace TwoJay\LoyaltyFeed\Observer;

use Magento\Framework\Event\ObserverInterface;
use Psr\Log\LoggerInterface;
use Magento\Framework\Event\Observer;
use TwoJay\LoyaltyFeed\Model\OrderExporter;

class OrderObserver implements ObserverInterface
{
    /** @var \TwoJay\LoyaltyFeed\Model\OrderExporter  */
    private $orderExporter;
    /** @var LoggerInterface */
    private $logger;

    public function __construct(
        LoggerInterface $logger,
        OrderExporter $orderExporter)
    {
        $this->logger = $logger;
        $this->orderExporter = $orderExporter;
    }

    public function execute(Observer $observer)
    {
        try {
            $order = $observer->getEvent()->getOrder();
            $this->orderExporter->writeTxnHeaders($order);
            $this->orderExporter->writeTxnItems($order);
        } catch (\Exception $e) {
            $this->logger->info($e->getMessage());
        }
    }
}
