<?php
/**
 * TwoJay_SerializedToJson
 *
 * @category  TwoJay
 * @package   TwoJay_SerializedToJson
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2018 Twojay (http://www.twojay.co/
 *
 */

use \Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(
    ComponentRegistrar::MODULE,
    'TwoJay_SerializedToJson',
    __DIR__
);