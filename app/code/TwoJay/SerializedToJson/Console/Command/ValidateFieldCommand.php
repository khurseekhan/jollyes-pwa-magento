<?php
/**
 * TwoJay_SerializedToJson
 *
 * @category  TwoJay
 * @package   TwoJay_SerializedToJson
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2018 Twojay (http://www.twojay.co/
 *
 */

namespace TwoJay\SerializedToJson\Console\Command;

use Exception;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Db\Select;
use Magento\Framework\Filesystem\Io\File as FileIO;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\Framework\Serialize\Serializer\Json as JsonSerializer;

/**
 * Class ValidateFieldCommand
 */
class ValidateFieldCommand extends Command
{
    /**
     * Argument key for table name
     */
    const ARGUMENT_TABLE = 'table';

    /**
     * Argument key for identifier field name
     */
    const ARGUMENT_ID_FIELD = 'id-field';

    /**
     * Argument key for field name to be processed
     */
    const ARGUMENT_FIELD = 'field';


    /**
     * Argument key for field name to be processed
     */
    const ARGUMENT_CONVERT_VALID_JSON = 'valid-format';


    /**
     * Page size
     */
    const ROWS_PER_PAGE = 1000;

    /**
     * Output file name
     */
    const OUTPUT_FILE_NAME = 'serialized_to_json_validation.log';

    /**
     * Error message pattern
     */
    const ERROR_MESSAGE_PATTERN = '%s: %s - %s';

    /**
     * Resource Connection
     *
     * @var ResourceConnection
     */
    protected $resource;

    /**
     * Filesystem IO
     *
     * @var FileIO
     */
    protected $ioFile;

    /**
     * Directory list
     *
     * @var DirectoryList
     */
    protected $directoryList;

    /**
     * Json Serializer
     *
     * @var JsonSerializer
     */
    protected $jsonSerializer;

    /**
     * ValidateFieldCommand constructor
     *
     * @param ResourceConnection $resource
     * @param FileIO $ioFile
     * @param DirectoryList $directoryList
     * @param JsonSerializer $jsonSerializer
     * @param null $name
     */
    public function __construct(
        ResourceConnection $resource,
        FileIO $ioFile,
        DirectoryList $directoryList,
        JsonSerializer $jsonSerializer,
        $name = null
    ) {
        parent::__construct($name);
        $this->resource = $resource;
        $this->ioFile = $ioFile;
        $this->directoryList = $directoryList;
        $this->jsonSerializer = $jsonSerializer;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('twojay:serialized-to-json:validate');
        $this->setDescription('Get list of invalid serialized data');
        $this->addArgument(self::ARGUMENT_TABLE, InputArgument::REQUIRED, 'Table name');
        $this->addArgument(self::ARGUMENT_ID_FIELD, InputArgument::REQUIRED, 'Identifier field name');
        $this->addArgument(self::ARGUMENT_FIELD, InputArgument::REQUIRED, 'Field to be validated');
        $this->addArgument(self::ARGUMENT_CONVERT_VALID_JSON, InputArgument::REQUIRED, 'Field to be convert to valid format');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $tableName = $input->getArgument(self::ARGUMENT_TABLE);
        $idFieldName = $input->getArgument(self::ARGUMENT_ID_FIELD);
        $fieldName = $input->getArgument(self::ARGUMENT_FIELD);
        $convertValidJson = $input->getArgument(self::ARGUMENT_CONVERT_VALID_JSON);

        $invalidRecordsCount = 0;
        $resultOutput = [];
        $resultOutput[] = sprintf('Validation result for field "%s" in table "%s":', $fieldName, $tableName);

        $connection = $this->resource->getConnection();

        /** @var Select $countQuery */
        $countQuery = $connection->select();
        $countQuery->from($tableName, ['rows_count' => 'COUNT(*)']);
        $count = (int)$connection->fetchOne($countQuery);
        $pagesCount = ceil($count / self::ROWS_PER_PAGE);

        /** @var Select $select */
        $select = $connection->select();
        $select->from($tableName, [$idFieldName, $fieldName]);

        $output->writeln(sprintf('Process has been started'));

        for ($currPage = 1; $currPage <= $pagesCount; $currPage++) {
            $select->limitPage($currPage, self::ROWS_PER_PAGE);
            $rows = $connection->fetchAll($select);
            $connection->beginTransaction();
            try{
                $records = 0 ;
                foreach ($rows as $row) {
                    $rowId = $row[$idFieldName];
                    $value = $row[$fieldName];
                    if ($value === false || $value === null || $value === '') {
                        // The field should not contain the empty values
                        $row[$fieldName];
                        $invalidRecordsCount++;
                        $resultOutput[] = sprintf(self::ERROR_MESSAGE_PATTERN, $idFieldName, $rowId, 'contains empty value');

                        continue;
                    }

                    //if (!$this->isValidJson($value)) {

                      if(!$this->isJSON($value)){
                          if ($convertValidJson) {
                              $unserializeData = @unserialize($value); //$output->writeln(print_r($unserializeData));
                              $unserializeData =  json_encode($unserializeData) == "false" ? Null :  json_encode($unserializeData);
                              $row[$fieldName] = $unserializeData;
                              $where = [$idFieldName . ' = ?' => $row[$idFieldName]];
                              $connection->update($tableName, $row, $where);
                              $records++;

                          }
                      //}

                    }else{
                        continue;
                    }
                }
                $connection->commit();
                $output->writeln(sprintf('Records (%s) are updated successfully', $records));
            }catch (Exception $e) {
                $connection->rollBack();
                $output->writeln("<error>{$e->getMessage()}</error>");

                return Cli::RETURN_FAILURE;
            }


        }
        $successMessage = sprintf('Records (%s) are updated successfully', $records);
        $summaryMessage = sprintf('Invalid records count: %s', $invalidRecordsCount);

        $resultOutput[] = $successMessage;
        $resultOutput[] = $summaryMessage;
        $filePath = $this->directoryList->getPath(DirectoryList::LOG) . DIRECTORY_SEPARATOR . self::OUTPUT_FILE_NAME;
        $this->ioFile->checkAndCreateFolder(dirname($filePath));
        $this->ioFile->open();
        $this->ioFile->write($filePath, implode($resultOutput, PHP_EOL));
        $this->ioFile->close();

        $output->writeln($summaryMessage);
        $output->writeln(sprintf('Result output: %s', $filePath));

        return 0;
    }

    /**
     * Check whether the value has been already converted to json
     *
     * @param string $value
     *
     * @return bool
     */
    protected function isValidJson($value)
    {
        try {
            $this->jsonSerializer->unserialize($value);
        } catch (Exception $e) {
            return false;
        }

        return true;
    }

    /**
     * @param $string
     * @return bool
     */
    protected function isJSON($string){
        return is_string($string) && is_array(json_decode($string, true)) && (json_last_error() == JSON_ERROR_NONE) ? true : false;
    }
}