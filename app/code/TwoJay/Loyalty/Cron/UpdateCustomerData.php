<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */

namespace TwoJay\Loyalty\Cron;

use TwoJay\Loyalty\Api\Loyalty\CustomSaveOrUpdateInterface;
use TwoJay\Loyalty\Api\Loyalty\RegisterInterface;
use TwoJay\Loyalty\Api\Loyalty\CustomSearchInterface;
use Magento\Framework\Json\Helper\Data as JsonHelper;
use TwoJay\Loyalty\Helper\Data;
use Magento\Customer\Model\CustomerFactory;
use Magento\Customer\Api\AddressRepositoryInterface;
use Magento\Customer\Model\ResourceModel\Customer;
use TwoJay\Loyalty\Api\Loyalty\TokenInterface;

/**
 * Class UpdateCustomerData
 * @package TwoJay\Loyalty\Cron
 */
class UpdateCustomerData
{
    /**
     * @var CustomSaveOrUpdateInterface
     */
    protected $customSaveOrUpdateInterface;

    /**
     * @var Data
     */
    protected $helper;

    /**
     * @var CustomSearchInterface
     */
    protected $customSearch;

    /**
     * @var RegisterInterface
     */
    protected $register;

    /**
     * @var JsonHelper
     */
    protected $jsonHelper;

    /**
     * @var CustomerFactory
     */
    protected $customerFactory;

    /**
     * @var \Magento\Customer\Api\AddressRepositoryInterface
     */
    protected $addressRepository;

    /**
     * @var Customer
     */
    protected $customerResource;

    /**
     * @var TokenInterface
     */
    protected $tokenInterface;

    /**
     * UpdateCustomerData constructor.
     * @param CustomSaveOrUpdateInterface $customSaveOrUpdateInterface
     * @param Data $helper
     * @param CustomSearchInterface $customSearch
     * @param RegisterInterface $register
     * @param JsonHelper $jsonHelper
     * @param CustomerFactory $customerFactory
     * @param AddressRepositoryInterface $addressRepository
     * @param Customer $customerResource
     * @param TokenInterface $tokenInterface
     */
    public function __construct(
        CustomSaveOrUpdateInterface $customSaveOrUpdateInterface,
        Data $helper,
        CustomSearchInterface $customSearch,
        RegisterInterface $register,
        JsonHelper $jsonHelper,
        CustomerFactory $customerFactory,
        AddressRepositoryInterface $addressRepository,
        Customer $customerResource,
        TokenInterface $tokenInterface
    )
    {
        $this->customSaveOrUpdateInterface = $customSaveOrUpdateInterface;
        $this->helper = $helper;
        $this->customSearch = $customSearch;
        $this->register = $register;
        $this->jsonHelper = $jsonHelper;
        $this->customerFactory = $customerFactory;
        $this->addressRepository = $addressRepository;
        $this->customerResource = $customerResource;
        $this->tokenInterface = $tokenInterface;
    }

    /**
     * @return $this
     */
    public function execute()
    {
        if (!$this->helper->isModuleEnabled()) {
            return;
        }

        // check for 503 service unavailable
        $tokenData = $this->tokenInterface->getToken();
        if (!$this->helper->isJson($tokenData) && strpos($tokenData, '503') !== false) {
            return;
        }

        $this->helper->insertToLoyaltyLogs('Customer', 'CustomerUpdate',
            '', 'Cron Execution Started', null, null, null);
        try {
            $customers = $this->getCustomersToUpdate();
            $this->helper->log("Customers to update in case of 503 : " . print_r($customers, 1));
            if (count($customers) > 0) {
                foreach ($customers as $customerData) {
                    $updateFlag = 0;
                    $customerId = $customerData['customer_id'];
                    // load a customer
                    $customer = $this->customerFactory->create()->load($customerId);
                    
                    $loyaltyNumber = $customer->getLoyaltyNumber();
                    $email = $customer->getEmail();
                    $passwordHash = $customer->getPasswordHash();
                    $contactByEmail = $customer->getContactByEmail();
                    $contactBySms = $customer->getContactBySms();

                    // get the customer default billing address
                    $billingAddressId = $customer->getDefaultBilling();
                    $billingAddress = $this->addressRepository->getById($billingAddressId);

                    $billingFirstName = $billingAddress->getFirstname();
                    $billingLastName = $billingAddress->getLastname();
                    $billingTelephone = $billingAddress->getTelephone();
                    $billingStreetAddress1 = (isset($billingAddress->getStreet()[0])) ? $billingAddress->getStreet()[0] : '';
                    $billingStreetAddress2 = (isset($billingAddress->getStreet()[1])) ? $billingAddress->getStreet()[1] : '';
                    $billingStreet = $billingStreetAddress1 . ' ' . $billingStreetAddress2;
                    $billingCity = $billingAddress->getCity();
                    $billingPostCode = $billingAddress->getPostcode();

                    if (!empty($loyaltyNumber) && isset($loyaltyNumber)) {
                        // customer exists in loyalty, so just update the customer details in loyalty
                        $params = [
                            'LoyaltyNumber' => $loyaltyNumber,
                            "FirstName" => $billingFirstName,
                            "LastName" => $billingLastName,
                            "Birthdate" => $customer->getDob(),
                            "Pwd" => $passwordHash,
                            "IsPasswordHashed" => true,
                            "Email" => $email,
                            "MobilePhone" => $billingTelephone,
                            "ContactbyEmail" => $contactByEmail,
                            "ContactbySMS" => $contactBySms,
                            "Address" => $billingStreet,
                            "PostCode" => $billingPostCode,
                            "City" => $billingCity
                        ];
                        $responseData = $this->customSaveOrUpdateInterface->customSaveOrUpdate($params);
                        if ($responseData) {
                            $responseData = $this->jsonHelper->jsonDecode($responseData);
                            if (isset($responseData['LoyaltyNumber']) && !empty($responseData['LoyaltyNumber'])) {
                                $updateFlag = 1;
                                $msg = 'Customer has been updated in loyalty for ' . $email;
                                $this->helper->insertToLoyaltyLogs('Customer', 'UpdateByCron',
                                    'CustomSaveOrUpdateAPI', $msg, null, null, null);
                            }
                        }
                    } else {
                        // customer does not exist in loyalty, so create the customer in loyalty
                        $params = [
                            "Title" => $customer->getPrefix(),
                            "FirstName" => $billingFirstName,
                            "LastName" => $billingLastName,
                            "Birthdate" => $customer->getDob(),
                            "Pwd" => $passwordHash,
                            "IsPasswordHashed" => true,
                            "FavouriteStoreID" => $customer->getStoreId(),
                            "Email" => $email,
                            "MobilePhone" => $billingTelephone,
                            "AcceptTandC" => true,
                            "AcceptPrivacy" => true,
                            "OptInInternal" => true,
                            "OptInExternal" => true,
                            "ContactbyEmail" => $contactByEmail,
                            "ContactbySMS" => $contactBySms,
                            "ActivateDate" => $customer->getCreatedAt(),
                            "Address" => $billingStreet,
                            "PostCode" => $billingPostCode,
                            "City" => $billingCity,
                            "SignupChannel" => "WEB"
                        ];
                        $responseData = $this->register->register($params);
                        if ($responseData) {
                            $registerData = $this->jsonHelper->jsonDecode($responseData);
                            if (isset($registerData['LoyaltyNumber']) && !empty($registerData['LoyaltyNumber'])) {
                                // Save loyalty number to customer in Magento
                                $customerModelData = $customer->getDataModel();
                                $customerModelData->setCustomAttribute('loyalty_number', $registerData['LoyaltyNumber']);
                                $customer->updateData($customerModelData);
                                $this->customerResource->saveAttribute($customer, 'loyalty_number');
                                $updateFlag = 1;
                                $msg = 'Customer has been created in loyalty for ' . $email;
                                $this->helper->insertToLoyaltyLogs('Customer', 'RegisterByCron',
                                    'RegisterAPI', $msg, null, null, null);

                            }
                        }
                    }

                    if ($updateFlag == 1) {
                        $connection = $this->helper->getConnection();
                        $bind = ['loyalty_flag' => (int)1];
                        $where = ['log_id = ?' => (int)$customerData['log_id']];
                        $connection->update('loyalty_api_history', $bind, $where);
                    }
                }
            }
        } catch (\Exception $e) {
            $this->helper->insertToLoyaltyLogs('Customer', 'CustomerUpdate',
                '', print_r($e->getMessage(), 1), null, null, null);
        }
        $this->helper->insertToLoyaltyLogs('Customer', 'CustomerUpdate',
            '', 'Cron Execution Ended', null, null, null);
    }

    /**
     * Get all the customers to update in Loyalty with loyalty_flag as 0
     * @return mixed
     */
    public function getCustomersToUpdate()
    {
        $sql = $this->helper->getConnection()->select()
            ->from(['lah' => 'loyalty_api_history'], array('customer_id', 'log_id'))
            ->where('type IN (?)', ['503_Service', 'Customer'])
            ->where('action IN (?)', ['Register', 'Success Page Register'])
            ->where('loyalty_flag = ?', (int)0)
            ->where('customer_id IS NOT NULL')
            ->where('customer_id != 0');

        return $this->helper->getConnection()->fetchAll($sql);
    }
}