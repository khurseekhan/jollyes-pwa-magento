<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */

namespace TwoJay\Loyalty\Cron;

use Magento\Framework\Json\Helper\Data as JsonHelper;
use TwoJay\Loyalty\Helper\Data;
use TwoJay\Loyalty\Api\Loyalty\TokenInterface;

/**
 * Class CheckServiceStatus
 * @package TwoJay\Loyalty\Cron
 */
class CheckServiceStatus
{

    const XML_PATH_EMAIL_TEMPLATE_FIELD = 'loyalty/loyalty_503_service/email_template';

    /**
     * @var Data
     */
    protected $helper;

    /**
     * @var JsonHelper
     */
    protected $jsonHelper;

    /**
     * @var TokenInterface
     */
    protected $token;

    /**
     * Store manager
     *
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var \Magento\Framework\Translate\Inline\StateInterface
     */
    protected $inlineTranslation;

    /**
     * @var \Magento\Framework\Mail\Template\TransportBuilder
     */
    protected $_transportBuilder;

    /**
     * CheckServiceStatus constructor.
     * @param Data $helper
     * @param JsonHelper $jsonHelper
     * @param TokenInterface $token
     */
    public function __construct(
        Data $helper,
        JsonHelper $jsonHelper,
        TokenInterface $token,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder
    )
    {
        $this->helper = $helper;
        $this->jsonHelper = $jsonHelper;
        $this->token = $token;
        $this->_storeManager = $storeManager;
        $this->inlineTranslation = $inlineTranslation;
        $this->_transportBuilder = $transportBuilder;
    }

    /**
     * @return $this
     */
    public function execute()
    {
        if (!$this->helper->isModuleEnabled()) {
            return;
        }

        $this->helper->insertToLoyaltyLogs('API_Service', 'Check503Service',
            '', 'Cron Execution Started', null, null, null);
        try {
            $serviceEmails = $this->helper->emailRecipient();
            if (!empty($serviceEmails)) {
                $tokenData = $this->token->getToken();
                $serviceStatus = $this->getServiceStatus();
                // check for 503 service status
                if (!$this->helper->isJson($tokenData) && strpos($tokenData, '503') !== false) {
                    // set the service_status=1 in db
                    if ($serviceStatus['service_status'] == 0) {
                        $this->setServiceStatus(1, $serviceStatus['id']);
                        // notify customer about 503 service status
                        $emailMsg = 'Loyalty service is unavailable.';
                        $this->sendEmail($emailMsg);
                    }
                } else {
                    // set the service_status=0 in db
                    if ($serviceStatus['service_status'] == 1) {
                        $this->setServiceStatus(0, $serviceStatus['id']);
                        // notify customer about 503 service status
                        $emailMsg = 'Loyalty service is available now.';
                        $this->sendEmail($emailMsg);
                    }
                }
            }
        } catch (\Exception $e) {
            $this->helper->insertToLoyaltyLogs('API_Service', 'Check503Service',
                '', print_r($e->getMessage(), 1), null, null, null);
        }
        $this->helper->insertToLoyaltyLogs('API_Service', 'Check503Service',
            '', 'Cron Execution Ended', null, null, null);
    }

    /**
     * Get Service Status
     *
     * @return mixed
     */
    public function getServiceStatus()
    {
        $sql = $this->helper->getConnection()->select()
            ->from(['lss' => 'loyalty_service_status'], array('service_status', 'id'));
        return $this->helper->getConnection()->fetchRow($sql);
    }

    /**
     * Set Service Status
     *
     * @return mixed
     */
    public function setServiceStatus($serviceStatus, $id)
    {
        $connection = $this->helper->getConnection();
        $bind = ['service_status' => (int)$serviceStatus];
        $where = ['id = ?' => (int)$id];
        $connection->update('loyalty_service_status', $bind, $where);
    }

    /**
     * Send email for 503 service status update
     *
     * @return mixed
     */
    public function sendEmail($serviceMsg)
    {
        $emailTemplateVariables = [];
        $emailTempVariables['serviceMsg'] = $serviceMsg;
        $this->inlineTranslation->suspend();
        try {
            $transport = $this->_transportBuilder
                ->setTemplateIdentifier($this->helper->emailTemplate())
                ->setTemplateOptions(
                    [
                        'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                        'store' => $this->_storeManager->getStore()->getId()
                    ]
                )
                ->setTemplateVars($emailTempVariables)
                ->setFrom($this->helper->emailSender())
                ->addTo($this->helper->emailRecipient())
                ->getTransport();

            $transport->sendMessage();
        } catch (\Exception $e) {
            echo $e->getMessage();
        } finally {
            $this->inlineTranslation->resume();
        }
    }
}