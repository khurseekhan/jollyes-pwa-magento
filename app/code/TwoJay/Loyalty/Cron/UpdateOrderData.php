<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */

namespace TwoJay\Loyalty\Cron;

use Magento\Framework\Json\Helper\Data as JsonHelper;
use TwoJay\Loyalty\Helper\Data;
use Magento\Sales\Model\OrderRepository;
use TwoJay\Loyalty\Model\Loyalty\RedeemVoucher;
use TwoJay\Loyalty\Api\Loyalty\TokenInterface;

/**
 * Class UpdateOrderData
 * @package TwoJay\Loyalty\Cron
 */
class UpdateOrderData
{
    /**
     * @var Data
     */
    protected $helper;

    /**
     * @var JsonHelper
     */
    protected $jsonHelper;

    /**
     * @var OrderRepository
     */
    protected $orderRepository;

    /**
     * @var RedeemVoucher
     */
    protected $redeemVoucher;

    /**
     * @var TokenInterface
     */
    protected $tokenInterface;

    /**
     * UpdateOrderData constructor.
     * @param Data $helper
     * @param JsonHelper $jsonHelper
     * @param OrderRepository $orderRepository
     * @param RedeemVoucher $redeemVoucher
     * @param TokenInterface $tokenInterface
     */
    public function __construct(
        Data $helper,
        JsonHelper $jsonHelper,
        OrderRepository $orderRepository,
        RedeemVoucher $redeemVoucher,
        TokenInterface $tokenInterface
    )
    {
        $this->helper = $helper;
        $this->jsonHelper = $jsonHelper;
        $this->orderRepository = $orderRepository;
        $this->redeemVoucher = $redeemVoucher;
        $this->tokenInterface = $tokenInterface;
    }

    /**
     * @return $this
     */
    public function execute()
    {
        if (!$this->helper->isModuleEnabled()) {
            return;
        }

        // check for 503 service unavailable
        $tokenData = $this->tokenInterface->getToken();
        if (!$this->helper->isJson($tokenData) && strpos($tokenData, '503') !== false) {
            return;
        }

        $this->helper->insertToLoyaltyLogs('Order', 'OrderUpdate',
            '', 'Cron Execution Started', null, null, null);
        try {
            $orders = $this->getOrdersToUpdate();
            $this->helper->log("Orders to update in case of 503 : " . print_r($orders, 1));
            if (count($orders) > 0) {
                foreach ($orders as $orderData) {
                    $updateFlag = 0;
                    $orderId = $orderData['order_id'];
                    $order = $this->orderRepository->get($orderId);
                    // if order has loyalty vouchers, redeem the vouchers
                    $loyaltyCoupons = $order->getLoyaltyCoupons();
                    if (!empty($loyaltyCoupons)) {
                        // redeeem vouchers
                        $orderIncrementId = $order->getIncrementId();
                        $loyaltyCoupons = $this->jsonHelper->jsonDecode($loyaltyCoupons);
                        foreach ($loyaltyCoupons as $loyaltyCoupon) {
                            $params = [
                                'VoucherNumber' => $loyaltyCoupon['c'],
                                'RedeemValue' => $loyaltyCoupon['a'],
                                'RedemptionDate' => date('Y-m-d\Th:i:s'),
                                'TXNNumberRef' => $orderIncrementId,
                                'RedeemSource' => 'WEB',
                            ];
                            $this->helper->log(__CLASS__ . ' : Order ID :' . $orderIncrementId);
                            $this->helper->log(print_r($params, 1));
                            $responseData = $this->redeemVoucher->redeemVoucher($params);
                            // check for 503 service unavailable
                            if (!$this->helper->isJson($responseData) && strpos($responseData, '503') !== false) {
                                $msg = 'Redeem voucher cron failed due to 503 service API error';
                                $this->helper->insertToLoyaltyLogs(
                                    'Order', 'RedeemByCron', 'RedeemVoucherAPI', $msg, null,
                                    $orderId, null);
                            } else {
                                $updateFlag = 1;
                                $msg = 'Redeem voucher successful : '.$loyaltyCoupon['c'];
                                $this->helper->insertToLoyaltyLogs('Order', 'RedeemByCron',
                                    'RedeemVoucherAPI', $msg, null, $orderId, null);
                            }
                        }
                    }
                    if ($updateFlag == 1) {
                        $connection = $this->helper->getConnection();
                        $bind = ['loyalty_flag' => (int)1];
                        $where = ['log_id = ?' => (int)$orderData['log_id']];
                        $connection->update('loyalty_api_history', $bind, $where);
                    }
                }
            }
        } catch (\Exception $e) {
            $this->helper->insertToLoyaltyLogs('Order', 'OrderUpdate',
                '', print_r($e->getMessage(), 1), null, null, null);
        }
        $this->helper->insertToLoyaltyLogs('Order', 'OrderUpdate',
            '', 'Cron Execution Ended', null, null, null);
    }

    /**
     * Get all the orders to update in Loyalty with loyalty_flag as 0
     *
     * @return mixed
     */
    public function getOrdersToUpdate()
    {
        $sql = $this->helper->getConnection()->select()
            ->from(['lah' => 'loyalty_api_history'], array('order_id', 'log_id'))
            ->where('type = ?', (string)'503_Service')
            ->where('loyalty_flag = ?', (int)0)
            ->where('order_id IS NOT NULL')
            ->where('order_id != 0');
        return $this->helper->getConnection()->fetchAll($sql);
    }
}