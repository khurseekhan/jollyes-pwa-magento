<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */

namespace TwoJay\Loyalty\Model;

use Magento\Framework\Api\AttributeValueFactory;
use TwoJay\Loyalty\Api\Data\LoyaltyCouponsInterface;
use Magento\Framework\Json\Helper\Data as JsonHelper;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;
use Magento\Framework\Api\ExtensionAttributesFactory;
use TwoJay\Loyalty\Helper\Data;
use TwoJay\Loyalty\Api\Loyalty\ValidateVoucherInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Data\Collection\AbstractDb;

/**
 * Class LoyaltyCoupons
 * @package TwoJay\Loyalty\Model
 */
class LoyaltyCoupons extends \Magento\Framework\Model\AbstractExtensibleModel implements LoyaltyCouponsInterface
{
    const STATUS_DISABLED = 0;

    const STATUS_ENABLED = 1;

    const STATE_AVAILABLE = 0;

    const STATE_USED = 1;

    const STATE_REDEEMED = 2;

    const STATE_EXPIRED = 3;

    const REDEEMABLE = 1;

    const NOT_REDEEMABLE = 0;

    /**#@+
     * Constants defined for keys of array
     */
    const ENTITY_ID = 'entity_id';

    const LOYALTY_COUPONS = 'loyalty_coupons';

    const LOYALTY_COUPONS_AMOUNT = 'loyalty_coupons_amount';

    const BASE_LOYALTY_COUPONS_AMOUNT = 'base_loyalty_coupons_amount';

    const LOYALTY_COUPONS_AMOUNT_USED = 'loyalty_coupons_amount_used';

    const BASE_LOYALTY_COUPONS_AMOUNT_USED = 'base_loyalty_coupons_amount_used';

    /**
     * loyalty coupon id cart key
     *
     * @var string
     */
    const ID = 'i';

    /**
     * loyalty coupon code cart key
     *
     * @var string
     */
    const CODE = 'c';

    /**
     * loyalty coupon amount cart key
     *
     * @var string
     */
    const AMOUNT = 'a';

    /**
     * loyalty coupon base amount cart key
     *
     * @var string
     */
    const BASE_AMOUNT = 'ba';

    /**
     *
     */
    const INITIAL_AMOUNT = 'initial_amount';

    /**
     * loyalty coupon authorized cart key
     */
    const AUTHORIZED = 'authorized';

    /**
     * @var string
     */
    protected $_eventPrefix = 'loyaltycoupons';

    /**
     * @var string
     */
    protected $_eventObject = 'loyaltycoupons';

    /**
     * @var null|Data
     */
    protected $_loyaltyCouponsData = null;

    /**
     * @var ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var StoreManagerInterface|null
     */
    protected $_storeManager = null;

    /**
     * @var CheckoutSession|null
     */
    protected $_checkoutSession = null;

    /**
     * @var CustomerSession|null
     */
    protected $_customerSession = null;

    /**
     * @var TimezoneInterface
     */
    protected $_localeDate;

    /**
     * @var CartRepositoryInterface
     */
    protected $quoteRepository;

    /**
     * @var ValidateVoucherInterface
     */
    protected $_validateVoucher;

    /**
     * @var JsonHelper
     */
    protected $_jsonHelper;

    /**
     * LoyaltyCoupons constructor.
     * @param Context $context
     * @param Registry $registry
     * @param ExtensionAttributesFactory $extensionFactory
     * @param AttributeValueFactory $customAttributeFactory
     * @param Data $loyaltyCouponsData
     * @param ValidateVoucherInterface $validateVoucher
     * @param JsonHelper $jsonHelper
     * @param ScopeConfigInterface $scopeConfig
     * @param StoreManagerInterface $storeManager
     * @param CheckoutSession $checkoutSession
     * @param CustomerSession $customerSession
     * @param TimezoneInterface $localeDate
     * @param CartRepositoryInterface $quoteRepository
     * @param AbstractResource|null $resource
     * @param AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        ExtensionAttributesFactory $extensionFactory,
        AttributeValueFactory $customAttributeFactory,
        Data $loyaltyCouponsData,
        ValidateVoucherInterface $validateVoucher,
        JsonHelper $jsonHelper,
        ScopeConfigInterface $scopeConfig,
        StoreManagerInterface $storeManager,
        CheckoutSession $checkoutSession,
        CustomerSession $customerSession,
        TimezoneInterface $localeDate,
        CartRepositoryInterface $quoteRepository,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    )
    {
        $this->_loyaltyCouponsData = $loyaltyCouponsData;
        $this->_scopeConfig = $scopeConfig;
        $this->_storeManager = $storeManager;
        $this->_checkoutSession = $checkoutSession;
        $this->_localeDate = $localeDate;
        $this->quoteRepository = $quoteRepository;
        $this->_validateVoucher = $validateVoucher;
        $this->_jsonHelper = $jsonHelper;
        $this->_customerSession = $customerSession;
        parent::__construct(
            $context,
            $registry,
            $extensionFactory,
            $customAttributeFactory,
            $resource,
            $resourceCollection,
            $data
        );
    }

    /**
     * @codeCoverageIgnoreStart
     * {@inheritdoc}
     */
    public function getLoyaltyCoupons()
    {
        return $this->getData(self::LOYALTY_COUPONS);
    }

    /**
     * {@inheritdoc}
     */
    public function getEntityId()
    {
        return $this->getData(self::ENTITY_ID);
    }

    /**
     * {@inheritdoc}
     */
    public function setEntityId($id)
    {
        return $this->getData(self::ENTITY_ID, $id);
    }

    /**
     * {@inheritdoc}
     */
    public function setLoyaltyCoupons(array $cards)
    {
        return $this->setData(self::LOYALTY_COUPONS, $cards);
    }

    /**
     * {@inheritdoc}
     */
    public function getLoyaltyCouponsAmount()
    {
        return $this->_getData(self::LOYALTY_COUPONS_AMOUNT);
    }

    /**
     * {@inheritdoc}
     */
    public function setLoyaltyCouponsAmount($amount)
    {
        return $this->setData(self::LOYALTY_COUPONS_AMOUNT, $amount);
    }

    /**
     * {@inheritdoc}
     */
    public function getBaseLoyaltyCouponsAmount()
    {
        return $this->_getData(self::BASE_LOYALTY_COUPONS_AMOUNT);
    }

    /**
     * {@inheritdoc}
     */
    public function setBaseLoyaltyCouponsAmount($amount)
    {
        return $this->setData(self::BASE_LOYALTY_COUPONS_AMOUNT, $amount);
    }

    /**
     * {@inheritdoc}
     */
    public function getLoyaltyCouponsAmountUsed()
    {
        return $this->_getData(self::LOYALTY_COUPONS_AMOUNT_USED);
    }

    /**
     * {@inheritdoc}
     */
    public function setLoyaltyCouponsAmountUsed($amount)
    {
        return $this->setData(self::LOYALTY_COUPONS_AMOUNT_USED, $amount);
    }

    /**
     * {@inheritdoc}
     */
    public function getBaseLoyaltyCouponsAmountUsed()
    {
        return $this->_getData(self::BASE_LOYALTY_COUPONS_AMOUNT_USED);
    }

    /**
     * {@inheritdoc}
     */
    public function setBaseLoyaltyCouponsAmountUsed($amount)
    {
        return $this->setData(self::BASE_LOYALTY_COUPONS_AMOUNT_USED, $amount);
    }

    /**
     * Add loyalty coupon to quote loyalty coupon storage
     * @param string $code
     * @param bool $saveQuote
     * @param null $quote
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function addToCart($code = '', $saveQuote = true, $quote = null)
    {
        if ($this->_checkoutSession->getQuote()->getLoyaltyCoupons()) {
            $quoteCards = $this->_jsonHelper->jsonDecode($this->_checkoutSession->getQuote()->getLoyaltyCoupons());
            $countLoyaltyCoupons = count($quoteCards);
            if ($countLoyaltyCoupons == 5) {
                throw new \Magento\Framework\Exception\LocalizedException(
                    __('You can not add more than 5 PetCLUB Vouchers.')
                );
            }
        }

        if (is_array($code)) {
            $code = isset($code[0]) ? $code[0] : '';
        }

        if (is_null($quote)) {
            $quote = $this->_checkoutSession->getQuote();
        }

        $responseData = $this->isValid($code);
        if (isset($responseData['is_valid']) && $responseData['is_valid'] == true) {
            $validateVoucherResponse = $responseData['voucher_details'];
            if (isset($validateVoucherResponse['VoucherStatus'])) {
                if ($validateVoucherResponse['VoucherStatus'] == 'ACTIVE') {
                    $amount = isset($validateVoucherResponse['Balance']) ? $validateVoucherResponse['Balance'] : 0;
                    $grandTotal = $this->_checkoutSession->getQuote()->getGrandTotal();
                    if ($grandTotal >= $amount) {
                        $finalAmount = $amount;
                    } else {
                        $remainingAmount = $amount - $grandTotal;
                        $finalAmount = $amount - $remainingAmount;
                    }

                    if ($finalAmount == 0.00) {
                        throw new \Magento\Framework\Exception\LocalizedException(
                            __('No need to apply the coupon since the grand total is Zero')
                        );
                    }

                    $loyaltyCoupons = $this->_loyaltyCouponsData->getLoyaltyCoupons($quote);

                    if (!$loyaltyCoupons) {
                        $loyaltyCoupons = [];
                    } else {
                        foreach ($loyaltyCoupons as $one) {
                            if ($one['c'] == $code) {
                                throw new \Magento\Framework\Exception\LocalizedException(
                                    __('This PetCLUB Voucher is already in the cart.')
                                );
                            }
                        }
                    }
                    $loyaltyCoupons[] = [
                        self::CODE => $code,
                        self::AMOUNT => $finalAmount,
                        self::BASE_AMOUNT => $finalAmount,
                        self::INITIAL_AMOUNT => $amount
                    ];

                    $this->_loyaltyCouponsData->setLoyaltyCoupons($quote, $loyaltyCoupons);
                    if ($saveQuote) {
                        if ($quote->getShippingAddress()->getCountryId() === null) {
                            $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
                            $countryId = $this->_scopeConfig->getValue('shipping/origin/country_id', $storeScope);
                            $quote->getShippingAddress()->setCountryId($countryId);
                        }
                        $this->quoteRepository->save($quote);
                        $quote->collectTotals();
                    }
                }
            }
        }

        return $this;
    }

    /**
     * Remove loyalty coupon from quote loyalty coupon storage
     * @param null $param
     * @param bool $saveQuote
     * @param null $quote
     * @return $this
     */
    public function removeFromCart($param = null, $saveQuote = true, $quote = null)
    {
        if (is_null($quote)) {
            $quote = $this->_checkoutSession->getQuote();
        }

        $loyaltyCoupons = $quote->getLoyaltyCoupons();
        if ($loyaltyCoupons) {
            $loyaltyCoupons = $this->_jsonHelper->jsonDecode($loyaltyCoupons);
        }

        if ($quote->getShippingAddress()->getCountryId() === null) {
            $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
            $countryId = $this->_scopeConfig->getValue('shipping/origin/country_id', $storeScope);
            $quote->getShippingAddress()->setCountryId($countryId)->save();
        }

        $shippingAddress = $quote->getShippingAddress();
        $grandTotal = $shippingAddress->getSubtotal() + $shippingAddress->getShippingAmount() + $shippingAddress->getTaxAmount();
        $remainingAmount = $grandTotal;

        if (is_array($loyaltyCoupons)) {
            foreach ($loyaltyCoupons as $k => $one) {
                if ($one['c'] == $param) {
                    unset($loyaltyCoupons[$k]);
                }
            }

            if (count($loyaltyCoupons) > 0) {
                foreach ($loyaltyCoupons as $k => $one) {
                    if ($remainingAmount >= $one['initial_amount']) {
                        $remainingAmount = $remainingAmount - $one['initial_amount'];
                        $loyaltyCoupons[$k]['a'] = $loyaltyCoupons[$k]['ba'] = $one['initial_amount'];
                    } else {
                        $remainingAmount = 0.00;
                        $loyaltyCoupons[$k]['a'] = $loyaltyCoupons[$k]['ba'] = $remainingAmount;
                    }
                }
            }

            $this->_loyaltyCouponsData->setLoyaltyCoupons($quote, $loyaltyCoupons);
            if ($saveQuote) {
                $this->quoteRepository->save($quote);
                $quote->collectTotals();
            }
        }

        return $this;
    }

    /**
     * Check if this loyalty coupon is expired at the moment
     *
     * @return bool
     */
    public function isExpired()
    {
        if (!$this->getDateExpires()) {
            return false;
        }
        $timezone = $this->_localeDate->getConfigTimezone(
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->_storeManager->getWebsite($this->getWebsiteId())->getDefaultStore()->getId()
        );
        $expirationDate = (new \DateTime($this->getDateExpires(), new \DateTimeZone($timezone)))->setTime(0, 0, 0);
        $currentDate = (new \DateTime('now', new \DateTimeZone($timezone)))->setTime(0, 0, 0);
        if ($expirationDate < $currentDate) {
            return true;
        }

        return false;
    }

    /**
     * Check all the Loyalty voucher validity
     * @param $code
     * @return bool
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function isValid($code)
    {
        $validateVoucherResponse = $this->_validateVoucher->validateVoucher($code);
        // check for 503 service unavailable
        if (!$this->_loyaltyCouponsData->isJson($validateVoucherResponse) && strpos($validateVoucherResponse, '503') !== false) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __("%1", $this->_loyaltyCouponsData->getErrorMessage())
            );
        }

        if ($validateVoucherResponse) {
            $validateVoucherResponse = $this->_jsonHelper->jsonDecode($validateVoucherResponse);
        } else {
            throw new \Magento\Framework\Exception\LocalizedException(__(sprintf('PetCLUB Voucher response data is empty.')));
        }

        if (isset($validateVoucherResponse['ErrorCode'])) {
            $errorResponseCodes = [401, 404];
            if (in_array($validateVoucherResponse['ErrorCode'], $errorResponseCodes)) {
                throw new \Magento\Framework\Exception\LocalizedException(__(sprintf('%s is not valid PetCLUB Voucher.', $code)));
            }
        }

        if (isset($validateVoucherResponse['VoucherStatus']) && $validateVoucherResponse['VoucherStatus'] == 'EXPIRED') {
            throw new \Magento\Framework\Exception\LocalizedException(__('Your PetCLUB voucher %1 has expired.', $code));
        }

        if (isset($validateVoucherResponse['VoucherStatus']) && $validateVoucherResponse['VoucherStatus'] == 'REDEEMED') {
            throw new \Magento\Framework\Exception\LocalizedException(__('Your PetCLUB voucher %1 has already been redeemed.', $code));
        }

        if (isset($validateVoucherResponse['Balance']) && $validateVoucherResponse['Balance'] <= 0) {
            throw new \Magento\Framework\Exception\LocalizedException(__('Your PetCLUB voucher %1 has a zero balance.', $code));
        }

        return ['is_valid' => true, 'voucher_details' => $validateVoucherResponse];
    }

    /**
     * Revert amount to loyalty coupon balance if order was not placed
     *
     * @param   float $amount
     * @return  $this
     */
    public function revert($amount)
    {
        $amount = (double)$amount;

        if ($amount > 0) {
            $this->setBalanceDelta(
                $amount
            )->setBalance(
                $this->getBalance() + $amount
            )->setHistoryAction(5);
        }

        return $this;
    }


    /**
     * Return loyalty coupon Account state options
     *
     * @return array
     */
    public function getStatesAsOptionList()
    {
        $result = [];

        $result[self::STATE_AVAILABLE] = __('Available');
        $result[self::STATE_USED] = __('Used');
        $result[self::STATE_REDEEMED] = __('Redeemed');
        $result[self::STATE_EXPIRED] = __('Expired');

        return $result;
    }


    /**
     * Redeem loyalty coupon (-gca balance, +cb balance)
     *
     * @param int $customerId
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     */
    public function redeem($customerId = null)
    {
        if ($this->isValid(true, true, true, true)) {
            if ($this->getIsRedeemable() != self::REDEEMABLE) {
                $this->_throwException(sprintf('PetCLUB Voucher %s is not redeemable.', $this->getId()));
            }
            if (is_null($customerId)) {
                $customerId = $this->_customerSession->getCustomerId();
            }
            if (!$customerId) {
                throw new \Magento\Framework\Exception\LocalizedException(__('Please enter a valid customer ID.'));
            }

            //$additionalInfo = __('PetCLUB Voucher Redeemed: %1. For customer #%2.', $this->getCode(), $customerId);
            //$balance = '';

            $this->setBalanceDelta(
                -$this->getBalance()
            )->setHistoryAction(
                \TwoJay\Loyalty\Model\History::ACTION_REDEEMED
            )->setBalance(
                0
            )->setCustomerId(
                $customerId
            )->save();
        }

        return $this;
    }

    /**
     * Obscure real exception message to prevent brute force attacks
     *
     * @param string $realMessage
     * @param string $fakeMessage
     * @return void
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _throwException($realMessage, $fakeMessage = '')
    {
        $this->_logger->critical(new \Magento\Framework\Exception\LocalizedException(__($realMessage)));
        if (!$fakeMessage) {
            $fakeMessage = 'Please correct the PetCLUB Voucher.';
        }
        throw new \Magento\Framework\Exception\LocalizedException(__($fakeMessage));
    }

    /**
     * {@inheritdoc}
     *
     * @return \TwoJay\Loyalty\Api\Data\LoyaltyCouponsExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * @param \TwoJay\Loyalty\Api\Data\LoyaltyCouponsExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \TwoJay\Loyalty\Api\Data\LoyaltyCouponsExtensionInterface $extensionAttributes
    )
    {
        return $this->_setExtensionAttributes($extensionAttributes);
    }
}