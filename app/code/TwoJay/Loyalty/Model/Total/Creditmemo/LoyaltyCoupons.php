<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */
namespace TwoJay\Loyalty\Model\Total\Creditmemo;

/**
 * Class LoyaltyCoupons
 * @package TwoJay\Loyalty\Model\Total\Creditmemo
 */
class LoyaltyCoupons extends \Magento\Sales\Model\Order\Creditmemo\Total\AbstractTotal
{
    /**
     * @var \TwoJay\Loyalty\Helper\Data
     */
    protected $helper;

    /**
     * CreditmemoDataImport constructor.
     * @param \TwoJay\Loyalty\Helper\Data $helper
     */
    public function __construct(
        \TwoJay\Loyalty\Helper\Data $helper,
        array $data = []
    )
    {
        $this->helper = $helper;
        parent::__construct($data);
    }

    /**
     * Collect Loyalty Coupons totals for credit memo
     *
     * @param \Magento\Sales\Model\Order\Creditmemo $creditmemo
     * @return $this
     */
    public function collect(\Magento\Sales\Model\Order\Creditmemo $creditmemo)
    {
        if (!$this->helper->isModuleEnabled()) {
            return $this;
        }

        $order = $creditmemo->getOrder();
        if ($order->getBaseLoyaltyCouponsAmount() && $order->getBaseLoyaltyCouponsInvoiced() != 0) {
            $gcaLeft = $order->getBaseLoyaltyCouponsInvoiced() - $order->getBaseLoyaltyCouponsRefunded();

            $used = 0;
            $baseUsed = 0;

            if ($gcaLeft >= $creditmemo->getBaseGrandTotal()) {
                $baseUsed = $creditmemo->getBaseGrandTotal();
                $used = $creditmemo->getGrandTotal();

                $creditmemo->setBaseGrandTotal(0);
                $creditmemo->setGrandTotal(0);

                $creditmemo->setAllowZeroGrandTotal(true);
            } else {
                $baseUsed = $order->getBaseLoyaltyCouponsInvoiced() - $order->getBaseLoyaltyCouponsRefunded();
                $used = $order->getLoyaltyCouponsInvoiced() - $order->getLoyaltyCouponsRefunded();

                $creditmemo->setBaseGrandTotal($creditmemo->getBaseGrandTotal() + $baseUsed);
                $creditmemo->setGrandTotal($creditmemo->getGrandTotal() + $used);
            }

            $creditmemo->setBaseLoyaltyCouponsAmount($baseUsed);
            $creditmemo->setLoyaltyCouponsAmount($used);
        }

        return $this;
    }
}
