<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */

namespace TwoJay\Loyalty\Model\Total\Quote;

use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Quote\Model\Quote;
use Magento\Quote\Api\Data\ShippingAssignmentInterface;
use Magento\Quote\Model\Quote\Address;
use Magento\Framework\Json\Helper\Data as JsonHelper;

class LoyaltyCoupons extends \Magento\Quote\Model\Quote\Address\Total\AbstractTotal
{
    /**
     * @var null|\TwoJay\Loyalty\Helper\Data
     */
    protected $_loyaltyCouponsData = null;

    /**
     * @var \TwoJay\Loyalty\Model\LoyaltyCouponsFactory
     */
    protected $_loyaltyCouponsFactory;

    /**
     * @var PriceCurrencyInterface
     */
    protected $priceCurrency;

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $request;

    /**
     * @var \Magento\Framework\App\Response\RedirectInterface
     */
    protected $redirect;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $_registry;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;

    /**
     * @var \Magento\Quote\Model\QuoteFactory
     */
    protected $quoteFactory;

    /**
     * Core store config
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var JsonHelper
     */
    protected $_jsonHelper;

    /**
     * LoyaltyCoupons constructor.
     * @param \TwoJay\Loyalty\Helper\Data $loyaltyCouponsData
     * @param \TwoJay\Loyalty\Model\LoyaltyCouponsFactory $loyaltyCouponsFactory
     * @param PriceCurrencyInterface $priceCurrency
     * @param \Magento\Framework\App\RequestInterface $request
     * @param \Magento\Framework\App\Response\RedirectInterface $redirect
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Quote\Model\QuoteFactory $quoteFactory
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param JsonHelper $jsonHelper
     */
    public function __construct(
        \TwoJay\Loyalty\Helper\Data $loyaltyCouponsData,
        \TwoJay\Loyalty\Model\LoyaltyCouponsFactory $loyaltyCouponsFactory,
        PriceCurrencyInterface $priceCurrency,
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Framework\App\Response\RedirectInterface $redirect,
        \Magento\Framework\Registry $registry,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Quote\Model\QuoteFactory $quoteFactory,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        JsonHelper $jsonHelper
    )
    {
        $this->_loyaltyCouponsFactory = $loyaltyCouponsFactory;
        $this->_loyaltyCouponsData = $loyaltyCouponsData;
        $this->priceCurrency = $priceCurrency;
        $this->request = $request;
        $this->redirect = $redirect;
        $this->_registry = $registry;
        $this->_checkoutSession = $checkoutSession;
        $this->quoteFactory = $quoteFactory;
        $this->_scopeConfig = $scopeConfig;
        $this->_jsonHelper = $jsonHelper;
        $this->setCode('loyalty_coupons');
    }

    /**
     * Collect totals for loyalty coupon
     *
     * @param Quote $quote
     * @param ShippingAssignmentInterface $shippingAssignment
     * @param Quote\Address\Total $total
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function collect(
        \Magento\Quote\Model\Quote $quote,
        \Magento\Quote\Api\Data\ShippingAssignmentInterface $shippingAssignment,
        \Magento\Quote\Model\Quote\Address\Total $total
    )
    {
        if (!$this->_loyaltyCouponsData->isModuleEnabled()) {
            return;
        }
        
        $quoteFactoryObj = $this->_checkoutSession->getQuote();
        
        //$quoteCards = json_decode($quoteFactoryObj->getLoyaltyCoupons(), true);
        $countLoyaltyCoupons = 0;
        if ($quoteFactoryObj->getLoyaltyCoupons()) {
            $quoteCards = $this->_jsonHelper->jsonDecode($quoteFactoryObj->getLoyaltyCoupons());
            $countLoyaltyCoupons = count($quoteCards);
        }
        
        if ($countLoyaltyCoupons > 0) {
            if ($quoteFactoryObj->getShippingAddress()->getCountryId() === null) {
                $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
                $countryId = $this->_scopeConfig->getValue('shipping/origin/country_id', $storeScope);
                $quoteFactoryObj->getShippingAddress()->setCountryId($countryId)->save();
            }
            $shipAddress = $quoteFactoryObj->getShippingAddress();
            $grandTotal = $shipAddress->getTaxAmount() + $shipAddress->getSubtotal() +
                $shipAddress->getShippingAmount() + $shipAddress->getDiscountAmount();

            $remainingAmount = $grandTotal;
            $loyaltyCouponAmount = 0.00;
            $legalCode_loyaltyCoupon = [];

            foreach ($quoteCards as $eachCoupon) {

                if ($remainingAmount == 0.00) {
                    // remove coupon
                    continue;
                }
                if ($remainingAmount >= $eachCoupon['initial_amount']) {
                    $remainingAmount = $remainingAmount - $eachCoupon['initial_amount'];
                    $loyaltyCouponAmount += $eachCoupon['initial_amount'];
                    $eachCoupon['a'] = $eachCoupon['ba'] = $eachCoupon['initial_amount'];
                } else {
                    $eachCoupon['a'] = $eachCoupon['ba'] = $remainingAmount;
                    $loyaltyCouponAmount += $remainingAmount;
                    $remainingAmount = 0.00;
                }
                $legalCode_loyaltyCoupon[] = $eachCoupon;
            }


            if (!$quote->getLoyaltyFlag()) {

                $quote->setLoyaltyFlag(1);
                $loyaltyDiscount = ($loyaltyCouponAmount * -1);

                $total->addTotalAmount('loyalty_coupons_amount', $loyaltyDiscount);
                $total->addBaseTotalAmount('loyalty_coupons_amount', $loyaltyDiscount);

                $total->setBaseLoyaltyCouponsAmount($loyaltyDiscount);
                $total->setLoyaltyCouponsAmount($loyaltyDiscount);

            }
            $this->_loyaltyCouponsData->setLoyaltyCoupons($quote, $legalCode_loyaltyCoupon);
            //$quote->setLoyaltyCoupons(json_encode($legalCode_loyaltyCoupon));
                $quote->setBaseLoyaltyCouponsAmount((float)($loyaltyCouponAmount * -1))
                ->setLoyaltyCouponsAmount((float)($loyaltyCouponAmount * -1));
        }

        return $this;
    }

    /**
     * @param Address\Total $total
     */
    protected function clearValues(Address\Total $total)
    {
        if (!$this->_loyaltyCouponsData->isModuleEnabled()) {
            return;
        }

        $total->setTotalAmount('subtotal', 0);
        $total->setBaseTotalAmount('subtotal', 0);
        $total->setTotalAmount('tax', 0);
        $total->setBaseTotalAmount('tax', 0);
        $total->setTotalAmount('discount_tax_compensation', 0);
        $total->setBaseTotalAmount('discount_tax_compensation', 0);
        $total->setTotalAmount('shipping_discount_tax_compensation', 0);
        $total->setBaseTotalAmount('shipping_discount_tax_compensation', 0);
        $total->setSubtotalInclTax(0);
        $total->setBaseSubtotalInclTax(0);
    }

    /**
     * Return shopping cart total row items
     *
     * @param Quote $quote
     * @param Quote\Address\Total $total
     * @return array|null
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function fetch(\Magento\Quote\Model\Quote $quote, \Magento\Quote\Model\Quote\Address\Total $total)
    {
        if (!$this->_loyaltyCouponsData->isModuleEnabled()) {
            return;
        }

        $quoteId = $this->_checkoutSession->getQuote()->getEntityId();
        $quoteFactoryObj = $this->quoteFactory->create()->load($quoteId);

        //$loyaltyCoupons = json_decode($quoteFactoryObj->getLoyaltyCoupons());
        $loyaltyCoupons = $quoteFactoryObj->getLoyaltyCoupons();
        if (!empty($loyaltyCoupons)) {
            $loyaltyCoupons = $this->_jsonHelper->jsonDecode($loyaltyCoupons);
            return [
                'code' => $this->getCode(),
                'title' => __('PetCLUB Voucher'),
                'value' => -$total->getLoyaltyCouponsAmount(),
                'loyalty_coupons' => $loyaltyCoupons
            ];
        }
        return null;
    }

}
