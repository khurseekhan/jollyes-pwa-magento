<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */

namespace TwoJay\Loyalty\Model\Total\Invoice;

use Magento\Quote\Model\QuoteFactory;
use TwoJay\Loyalty\Helper\Data;

/**
 * Class LoyaltyCoupons
 * @package TwoJay\Loyalty\Model\Total\Invoice
 */
class LoyaltyCoupons extends \Magento\Sales\Model\Order\Invoice\Total\AbstractTotal
{
    /**
     * @var QuoteFactory
     */
    protected $quoteFactory;

    /**
     * @var Data
     */
    protected $helper;

    /**
     * LoyaltyCoupons constructor.
     * @param QuoteFactory $quoteFactory
     * @param array $data
     */
    public function __construct(
        QuoteFactory $quoteFactory,
        Data $helper,
        array $data = []
    )
    {
        $this->quoteFactory = $quoteFactory;
        $this->helper = $helper;
        parent::__construct($data);
    }

    /**
     * Collect Loyalty Coupons totals for invoice
     *
     * @param \Magento\Sales\Model\Order\Invoice $invoice
     * @return $this
     */
    public function collect(\Magento\Sales\Model\Order\Invoice $invoice)
    {
        if (!$this->helper->isModuleEnabled()) {
            return $this;
        }

        $order = $invoice->getOrder();
        $quoteId = $order->getQuoteId();
        $quote = $this->quoteFactory->create()->load($quoteId);
        if (!empty($quote->getLoyaltyCoupons()) && empty($order->getLoyaltyCoupons())) {
            $baseLoyaltyCouponsAmount = $quote->getBaseLoyaltyCouponsAmount();
            $loyaltyCouponsAmount = $quote->getLoyaltyCouponsAmount();
            $order->setBaseLoyaltyCouponsAmount($baseLoyaltyCouponsAmount);
            $order->setLoyaltyCouponsAmount($loyaltyCouponsAmount);
            $order->setLoyaltyCoupons($quote->getLoyaltyCoupons());
            $order->save();
        }

        if ($order->getBaseLoyaltyCouponsAmount() && $order->getBaseLoyaltyCouponsInvoiced() != $order->getBaseLoyaltyCouponsAmount()
        ) {
            $gcaLeft = $order->getBaseLoyaltyCouponsAmount() - $order->getBaseLoyaltyCouponsInvoiced();
            $used = 0;
            $baseUsed = 0;
            if ($gcaLeft >= $invoice->getBaseGrandTotal()) {
                $baseUsed = $invoice->getBaseGrandTotal();
                $used = $invoice->getGrandTotal();

                $invoice->setBaseGrandTotal(0);
                $invoice->setGrandTotal(0);
            } else {
                $baseUsed = $order->getBaseLoyaltyCouponsAmount() - $order->getBaseLoyaltyCouponsInvoiced();
                $used = $order->getLoyaltyCouponsAmount() - $order->getLoyaltyCouponsInvoiced();

                $invoice->setBaseGrandTotal($invoice->getBaseGrandTotal() + $baseUsed);
                $invoice->setGrandTotal($invoice->getGrandTotal() + $used);
            }
            $invoice->setBaseLoyaltyCouponsAmount($baseUsed);
            $invoice->setLoyaltyCouponsAmount($used);
        }
        return $this;
    }
}
