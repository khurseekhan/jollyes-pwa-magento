<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */

namespace TwoJay\Loyalty\Model\ResourceModel\Order\Grid;

use Magento\Framework\Data\Collection\Db\FetchStrategyInterface as FetchStrategy;
use Magento\Framework\Data\Collection\EntityFactoryInterface as EntityFactory;
use Magento\Framework\Event\ManagerInterface as EventManager;
use Psr\Log\LoggerInterface as Logger;
use Magento\Sales\Model\ResourceModel\Order\Grid\Collection as OriginalCollection;
use TwoJay\Loyalty\Helper\Data;

/**
 * Order grid collection
 */
class Collection extends OriginalCollection
{
    /**
     * @var \Magento\Backend\Model\Auth\Session
     */
    protected $authSession;

    /**
     * @var Data
     */
    protected $helper;

    /**
     * Collection constructor.
     * @param EntityFactory $entityFactory
     * @param Logger $logger
     * @param FetchStrategy $fetchStrategy
     * @param EventManager $eventManager
     * @param Data $helper
     * @param \Magento\Backend\Model\Auth\Session $authSession
     * @param string $mainTable
     * @param string $resourceModel
     */
    public function __construct(
        EntityFactory $entityFactory,
        Logger $logger,
        FetchStrategy $fetchStrategy,
        EventManager $eventManager,
        Data $helper,
        \Magento\Backend\Model\Auth\Session $authSession,
        $mainTable = 'sales_order_grid',
        $resourceModel = \Magento\Sales\Model\ResourceModel\Order::class
    )
    {

        $this->authSession = $authSession;
        parent::__construct($entityFactory, $logger, $fetchStrategy, $eventManager, $mainTable, $resourceModel);
        $this->helper = $helper;
    }

    /**
     * Use the normal order grid collection with sample flag
     */

    public function _renderFiltersBefore()
    {
        $joinTable = $this->getTable('sales_order');
        $this->getSelect()->join($joinTable,
            'main_table.entity_id = sales_order.entity_id',
            ['loyalty_coupons' => 'sales_order.loyalty_coupons',
                'loyalty_coupons_amount' => 'sales_order.loyalty_coupons_amount'
            ]);

        $this->getSelect()->where('sales_order.loyalty_coupons is not null');
        $this->helper->log((string)$this->getSelect());
        parent::_renderFiltersBefore();
    }

    public function getCurrentUser()
    {
        return $this->authSession->getUser();
    }
}
