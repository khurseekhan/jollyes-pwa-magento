<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */

namespace TwoJay\Loyalty\Model;

use TwoJay\Loyalty\Helper\Data as LoyaltyHelper;

class DefaultConfigProvider implements ConfigProviderInterface
{
    /**
     * @var LoyaltyHelper
     */
    protected $loyaltyHelper;

    /**
     * DefaultConfigProvider constructor.
     * @param LoyaltyHelper $loyaltyHelper
     */
    public function __construct(
        LoyaltyHelper $loyaltyHelper
    )
    {
        $this->loyaltyHelper = $loyaltyHelper;
    }

    /**
     * {@inheritdoc}
     */
    public function getConfig()
    {
        // check if module is enabled or not
        if ($this->loyaltyHelper->isModuleEnabled()) {
            $config = ['loyalty_module_status' => true];
        } else {
            $config = ['loyalty_module_status' => false];
        }


        return $config;
    }
}