<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */

namespace TwoJay\Loyalty\Model;

use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Checkout\Model\ConfigProviderInterface;
use TwoJay\Loyalty\Helper\Data as LoyaltyHelper;

class LoyaltyConfigProvider implements ConfigProviderInterface
{
    /**
     * @var CheckoutSession
     */
    protected $checkoutSession;

    /**
     * @var LoyaltyHelper
     */
    protected $loyaltyHelper;

    /**
     * LoyaltyConfigProvider constructor.
     * @param CheckoutSession $checkoutSession
     * @param LoyaltyHelper $loyaltyHelper
     */
    public function __construct(
        CheckoutSession $checkoutSession,
        LoyaltyHelper $loyaltyHelper
    )
    {
        $this->checkoutSession = $checkoutSession;
        $this->loyaltyHelper = $loyaltyHelper;
    }

    /**
     * Returns custom checkout configuration
     *
     * @return mixed
     */
    public function getConfig()
    {
        $quote = $this->checkoutSession->getQuote();
        $output['loyalty_config_provider'] = $quote->getLoyaltyCoupons();
        // check if module is enabled or not
        if ($this->loyaltyHelper->isModuleEnabled()) {
            $output['loyalty_module_status'] = true;
        } else {
            $output['loyalty_module_status'] = false;
        }
        return $output;
    }
}