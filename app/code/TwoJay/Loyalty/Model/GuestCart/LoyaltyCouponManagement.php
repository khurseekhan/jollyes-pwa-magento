<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace TwoJay\Loyalty\Model\GuestCart;

use TwoJay\Loyalty\Api\GuestLoyaltyCouponManagementInterface;
use TwoJay\Loyalty\Api\LoyaltyCouponManagementInterface;
use Magento\Quote\Model\QuoteIdMaskFactory;

/**
 * Class LoyaltyCouponManagement
 */
class LoyaltyCouponManagement implements GuestLoyaltyCouponManagementInterface
{
    /**
     * @var LoyaltyCouponManagementInterface
     */
    protected $loyaltyCouponManagement;

    /**
     * @var QuoteIdMaskFactory
     */
    protected $quoteIdMaskFactory;

    /**
     * LoyaltyCouponManagement constructor.
     * @param LoyaltyCouponManagementInterface $loyaltyCouponManagement
     * @param QuoteIdMaskFactory $quoteIdMaskFactory
     */
    public function __construct(
        LoyaltyCouponManagementInterface $loyaltyCouponManagement,
        QuoteIdMaskFactory $quoteIdMaskFactory
    ) {
        $this->loyaltyCouponManagement = $loyaltyCouponManagement;
        $this->quoteIdMaskFactory = $quoteIdMaskFactory;
    }

    /**
     * {@inheritDoc}
     */
    public function addLoyaltyCoupon(
        $cartId,
        \TwoJay\Loyalty\Api\Data\LoyaltyCouponsInterface $loyaltyCouponsData
    ) {
        $quoteIdMask = $this->quoteIdMaskFactory->create()->load($cartId, 'masked_id');
        return $this->loyaltyCouponManagement->saveByQuoteId($quoteIdMask->getQuoteId(), $loyaltyCouponsData);
    }

    /**
     * {@inheritDoc}
     */
    public function checkLoyaltyCoupon($cartId, $loyaltyCouponCode)
    {
        $quoteIdMask = $this->quoteIdMaskFactory->create()->load($cartId, 'masked_id');
        return $this->loyaltyCouponManagement->checkLoyaltyCoupon($quoteIdMask->getQuoteId(), $loyaltyCouponCode);
    }

    /**
     * {@inheritDoc}
     */
    public function deleteByQuoteId($cartId, $loyaltyCouponCode)
    {
        $quoteIdMask = $this->quoteIdMaskFactory->create()->load($cartId, 'masked_id');
        return $this->loyaltyCouponManagement->deleteByQuoteId($quoteIdMask->getQuoteId(), $loyaltyCouponCode);
    }
}
