<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */

namespace TwoJay\Loyalty\Model;

use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use TwoJay\Loyalty\Api\Data\LoyaltyCouponsInterface;
use TwoJay\Loyalty\Api\Data\LoyaltyCouponsInterfaceFactory;
use TwoJay\Loyalty\Api\Data\LoyaltyCouponsSearchResultInterfaceFactory;
use TwoJay\Loyalty\Api\LoyaltyCouponsRepositoryInterface;

class LoyaltyCouponsRepository implements LoyaltyCouponsRepositoryInterface
{
    /**
     * @var LoyaltyCouponsInterfaceFactory
     */
    private $loyaltyCouponsFactory;

    /**
     * @var LoyaltyCouponsSearchResultInterfaceFactory
     */
    private $searchResultFactory;

    /**
     * @var CollectionProcessorInterface
     */
    private $collectionProcessor;

    /**
     * LoyaltyCouponsRepository constructor.
     * @param LoyaltyCouponsInterfaceFactory $loyaltyCouponsFactory
     * @param LoyaltyCouponsSearchResultInterfaceFactory $searchResultFactory
     * @param CollectionProcessorInterface $collectionProcessor
     */
    public function __construct(
        LoyaltyCouponsInterfaceFactory $loyaltyCouponsFactory,
        LoyaltyCouponsSearchResultInterfaceFactory $searchResultFactory,
        CollectionProcessorInterface $collectionProcessor
    ) {
        $this->loyaltyCouponsFactory = $loyaltyCouponsFactory;
        $this->searchResultFactory = $searchResultFactory;
        $this->collectionProcessor = $collectionProcessor;
    }

    /**
     * @inheritdoc
     */
    public function get($id)
    {
        $entity = $this->loyaltyCouponsFactory->create();
        return $entity;
    }

    /**
     * @inheritdoc
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        $searchResult = $this->searchResultFactory->create();
        return $searchResult;
    }

    /**
     * @inheritdoc
     */
    public function save(LoyaltyCouponsInterface $loyaltyDataObject)
    {
        return $loyaltyDataObject;
    }

    /**
     * @inheritdoc
     */
    public function delete(LoyaltyCouponsInterface $loyaltyDataObject)
    {
        return true;
    }
}
