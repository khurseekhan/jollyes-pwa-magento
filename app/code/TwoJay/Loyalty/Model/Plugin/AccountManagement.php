<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */

namespace TwoJay\Loyalty\Model\Plugin;

use TwoJay\Loyalty\Api\Loyalty\IsLoyaltyMemberInterface;
use Magento\Customer\Model\AccountManagement as CustomerAccountManagement;
use Magento\Framework\Json\Helper\Data as JsonHelper;
use TwoJay\Loyalty\Helper\Data as LoyaltyHelper;
use Magento\Customer\Api\CustomerRepositoryInterface;

/**
 * Class AccountManagement
 *
 * @package TwoJay\Loyalty\Model\Plugin
 */
class AccountManagement
{
    /**
     * Password flag to set true
     */
    const SHOW_PASSWORD_FIELD_RESPONSE = false;

    /**
     * Password flag to set false
     */
    const HIDE_PASSWORD_FIELD_RESPONSE = true;

    /**
     * @var IsLoyaltyMemberInterface
     */
    private $isLoyaltyMemberInterface;

    /**
     * @var JsonHelper
     */
    private $jsonHelper;

    /**
     * @var LoyaltyHelper
     */
    protected $loyaltyHelper;

    /**
     * @var CustomerRepositoryInterface
     */
    protected $customerRepositoryInterface;

    /**
     * AccountManagement constructor.
     * @param IsLoyaltyMemberInterface $isLoyaltyMemberInterface
     * @param JsonHelper $jsonHelper
     * @param LoyaltyHelper $loyaltyHelper
     * @param CustomerRepositoryInterface $customerRepositoryInterface
     */
    public function __construct(
        IsLoyaltyMemberInterface $isLoyaltyMemberInterface,
        JsonHelper $jsonHelper,
        LoyaltyHelper $loyaltyHelper,
        CustomerRepositoryInterface $customerRepositoryInterface
    )
    {
        $this->isLoyaltyMemberInterface = $isLoyaltyMemberInterface;
        $this->jsonHelper = $jsonHelper;
        $this->loyaltyHelper = $loyaltyHelper;
        $this->customerRepositoryInterface = $customerRepositoryInterface;
    }

    /**
     * @param CustomerAccountManagement $subject
     * @param $result
     * @param $customerEmail
     * @param null $websiteId
     * @return bool
     */
    public function afterIsEmailAvailable(CustomerAccountManagement $subject, $result, $customerEmail, $websiteId = null)
    {
        if (!$this->loyaltyHelper->isModuleEnabled()) {
            return $result;
        }

        $customerDetails = $this->loyaltyHelper->getCustomerDetails($customerEmail);
        $loyaltyNumber = $this->_getLoyaltyNumber($customerDetails);

        if (isset($loyaltyNumber) && !empty($loyaltyNumber)) {
            $result = self::SHOW_PASSWORD_FIELD_RESPONSE;
        } elseif ($loyaltyMemberData = $this->isLoyaltyMemberInterface->getLoyaltyNumber($customerEmail)) {
            if ($this->_is503Response($loyaltyMemberData)) {
                $result = $this->_isCustomerPersisted($customerDetails) ?
                    self::SHOW_PASSWORD_FIELD_RESPONSE :
                    self::HIDE_PASSWORD_FIELD_RESPONSE;
            } elseif ($loyaltyMemberData = $this->jsonHelper->jsonDecode($loyaltyMemberData)) {
                $result = $this->_hasLoyaltyNumber($loyaltyMemberData) || $this->_isCustomerPersisted($customerDetails) ?
                    self::SHOW_PASSWORD_FIELD_RESPONSE :
                    self::HIDE_PASSWORD_FIELD_RESPONSE;
            }
        }

        return $result;
    }

    /**
     * Get customer loyalty number
     *
     * @param $customerDetails
     * @return mixed|null
     * @throws \Magento\Framework\Exception\NoSuchEntityException If customer with the specified ID does not exist.
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function _getLoyaltyNumber($customerDetails)
    {
        $loyaltyNumber = null;
        $customerId = isset($customerDetails['entity_id']) ? $customerDetails['entity_id'] : '';
        if (empty($customerId)) {
            return $loyaltyNumber;
        }

        $customer = $this->customerRepositoryInterface->getById($customerId);
        if (!empty($customer->getCustomAttribute('loyalty_number'))) {
            $loyaltyNumber = $customer->getCustomAttribute('loyalty_number')->getValue();
        }
        return $loyaltyNumber;
    }

    /**
     * Check for 503 response from Loyalty system
     *
     * @param $loyaltyMemberData
     * @return bool
     */
    private function _is503Response($loyaltyMemberData)
    {
        return !$this->loyaltyHelper->isJson($loyaltyMemberData) && strpos($loyaltyMemberData, '503') !== false;
    }

    /**
     * Check for customer which exists in Magento
     *
     * @param array $customerDetails
     * @return bool
     */
    private function _isCustomerPersisted($customerDetails)
    {
        return isset($customerDetails['entity_id']) && !empty($customerDetails['entity_id']);
    }

    /**
     * Check if customer response data has a loyalty number
     *
     * @param array $loyaltyMemberData
     * @return bool
     */
    private function _hasLoyaltyNumber(array $loyaltyMemberData)
    {
        return isset($loyaltyMemberData['LoyaltyNumber']) && !empty($loyaltyMemberData['LoyaltyNumber']);
    }
}
