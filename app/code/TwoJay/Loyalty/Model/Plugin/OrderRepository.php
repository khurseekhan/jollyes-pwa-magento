<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */

namespace TwoJay\Loyalty\Model\Plugin;

use TwoJay\Loyalty\Model\BaseCouponFactory;
use TwoJay\Loyalty\Model\LoyaltyCoupons;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\ObjectManagerInterface;

/**
 * Plugin for Order repository
 *
 * Class OrderRepository
 * @package TwoJay\Loyalty\Model\Plugin
 */
class OrderRepository
{
    /**
     * @var \Magento\Sales\Api\Data\OrderExtensionFactory
     */
    private $extensionFactory;

    /**
     * @var BaseCouponFactory
     */
    private $baseCouponFactory;

    /**
     * Instance of serializer.
     *
     * @var Json
     */
    private $serializer;

    /**
     * @var \TwoJay\Loyalty\Helper\Data
     */
    protected $helper;

    /**
     * @var ObjectManagerInterface
     */
    protected $objectManagerInterface;

    /**
     * OrderRepository constructor.
     * @param \Magento\Sales\Api\Data\OrderExtensionFactory $extensionFactory
     * @param BaseCouponFactory $baseCouponFactory
     * @param Json|null $serializer
     * @param \TwoJay\Loyalty\Helper\Data $helper
     * @param ObjectManagerInterface $objectManagerInterface
     */
    public function __construct(
        \Magento\Sales\Api\Data\OrderExtensionFactory $extensionFactory,
        BaseCouponFactory $baseCouponFactory,
        Json $serializer = null,
        \TwoJay\Loyalty\Helper\Data $helper,
        ObjectManagerInterface $objectManagerInterface
    )
    {
        $this->extensionFactory = $extensionFactory;
        $this->baseCouponFactory = $baseCouponFactory;
        $this->objectManagerInterface = $objectManagerInterface;
        $this->serializer = $serializer ?: $this->objectManagerInterface->get(Json::class);
        $this->helper = $helper;
    }

    /**
     * @param \Magento\Sales\Api\OrderRepositoryInterface $subject
     * @param \Magento\Sales\Api\Data\OrderInterface $entity
     *
     * @return \Magento\Sales\Api\Data\OrderInterface
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterGet(
        \Magento\Sales\Api\OrderRepositoryInterface $subject,
        \Magento\Sales\Api\Data\OrderInterface $entity
    )
    {
        if (!$this->helper->isModuleEnabled()) {
            return $entity;
        }

        if (!$entity->getLoyaltyCoupons()) {
            return $entity;
        }
        /** @var \Magento\Sales\Api\Data\OrderExtension $extensionAttributes */
        $extensionAttributes = $entity->getExtensionAttributes();

        if ($extensionAttributes === null) {
            $extensionAttributes = $this->extensionFactory->create();
        }

        $loyaltyCoupons = $this->createLoyaltyCoupons($this->serializer->unserialize($entity->getLoyaltyCoupons()));

        $extensionAttributes->setLoyaltyCoupons($loyaltyCoupons);
        $extensionAttributes->setBaseLoyaltyCouponsAmount($entity->getBaseLoyaltyCouponsAmount());
        $extensionAttributes->setLoyaltyCouponsAmount($entity->getLoyaltyCouponsAmount());
        $extensionAttributes->setBaseLoyaltyCouponsInvoiced($entity->getBaseLoyaltyCouponsInvoiced());
        $extensionAttributes->setLoyaltyCouponsInvoiced($entity->getLoyaltyCouponsInvoiced());
        $extensionAttributes->setBaseLoyaltyCouponsRefunded($entity->getBaseLoyaltyCouponsRefunded());
        $extensionAttributes->setLoyaltyCouponsRefunded($entity->getLoyaltyCouponsRefunded());

        $entity->setExtensionAttributes($extensionAttributes);

        return $entity;
    }

    /**
     * @param \Magento\Sales\Api\OrderRepositoryInterface $subject
     * @param \Magento\Sales\Api\Data\OrderSearchResultInterface $entities
     *
     * @return \Magento\Sales\Api\Data\OrderSearchResultInterface
     */
    public function afterGetList(
        \Magento\Sales\Api\OrderRepositoryInterface $subject,
        \Magento\Sales\Api\Data\OrderSearchResultInterface $entities
    )
    {
        if (!$this->helper->isModuleEnabled()) {
            return $entities;
        }

        /** @var \Magento\Sales\Api\Data\OrderInterface $entity */
        foreach ($entities->getItems() as $entity) {
            $this->afterGet($subject, $entity);
        }

        return $entities;
    }

    /**
     * Create Loyalty Coupons Data Objects
     *
     * @param array $items
     * @return array
     */
    private function createLoyaltyCoupons(array $items)
    {
        $loyaltyCoupons = [];

        foreach ($items as $item) {
            /** @var BaseCoupon $loyaltyCoupon */
            $loyaltyCoupon = $this->baseCouponFactory->create();
            $loyaltyCoupon->setCode($item[LoyaltyCoupons::CODE]);
            $loyaltyCoupon->setAmount($item[LoyaltyCoupons::AMOUNT]);
            $loyaltyCoupon->setBaseAmount($item[LoyaltyCoupons::BASE_AMOUNT]);
            $loyaltyCoupons[] = $loyaltyCoupon;
        }
        return $loyaltyCoupons;
    }
}
