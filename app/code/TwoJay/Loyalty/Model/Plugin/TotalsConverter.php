<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace TwoJay\Loyalty\Model\Plugin;

use Magento\Quote\Api\Data\TotalSegmentExtensionFactory;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Quote\Model\Cart\TotalsConverter as CartTotalsConverter;
use Magento\Quote\Api\Data\TotalSegmentInterface;
use Magento\Quote\Model\Quote\Address\Total as AddressTotal;
use Magento\Quote\Api\Data\TotalSegmentExtensionInterface;

/**
 * Class TotalsConverter
 * @package TwoJay\Loyalty\Model\Plugin
 */
class TotalsConverter
{
    /**
     * @var TotalSegmentExtensionFactory
     */
    private $totalSegmentExtensionFactory;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @var string
     */
    private $code;

    /**
     * @var \TwoJay\Loyalty\Helper\Data
     */
    private $helper;

    /**
     * @param TotalSegmentExtensionFactory $totalSegmentExtensionFactory
     * @param SerializerInterface $serializer
     */
    public function __construct(
        TotalSegmentExtensionFactory $totalSegmentExtensionFactory,
        SerializerInterface $serializer,
        \TwoJay\Loyalty\Helper\Data $helper
    ) {
        $this->totalSegmentExtensionFactory = $totalSegmentExtensionFactory;
        $this->serializer = $serializer;
        $this->code = 'loyalty_coupons';
        $this->helper = $helper;
    }

    /**
     * @param CartTotalsConverter $subject
     * @param $result
     * @param $addressTotals
     * @return mixed
     */
    public function afterProcess(CartTotalsConverter $subject, $result, $addressTotals)
    {
        if (!$this->helper->isModuleEnabled()) {
            return $result;
        }

        if (!isset($addressTotals[$this->code])) {
            return $result;
        }

        /** @var TotalSegmentExtensionInterface $totalSegmentExtension */
        $totalSegmentExtension = $this->totalSegmentExtensionFactory->create();
        $totalSegmentExtension->setData('loyalty_coupons',$this->serializer->serialize($addressTotals[$this->code]->getData('loyalty_coupons')));
        $result[$this->code]->setExtensionAttributes($totalSegmentExtension);

        return $result;
    }
}
