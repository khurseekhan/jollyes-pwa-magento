<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */

namespace TwoJay\Loyalty\Model\Plugin;

/**
 * Class CreditmemoRepository
 * @package TwoJay\Loyalty\Model\Plugin
 */
class CreditmemoRepository
{
    /**
     * @var \Magento\Sales\Api\Data\CreditmemoExtensionFactory
     */
    private $extensionFactory;

    /**
     * @var \TwoJay\Loyalty\Helper\Data
     */
    protected $helper;

    /**
     * @param \Magento\Sales\Api\Data\CreditmemoExtensionFactory $extensionFactory
     */
    public function __construct(
        \Magento\Sales\Api\Data\CreditmemoExtensionFactory $extensionFactory,
        \TwoJay\Loyalty\Helper\Data $helper
    )
    {
        $this->extensionFactory = $extensionFactory;
        $this->helper = $helper;
    }

    /**
     * @param \Magento\Sales\Api\CreditmemoRepositoryInterface $subject
     * @param \Magento\Sales\Api\Data\CreditmemoInterface $entity
     *
     * @return \Magento\Sales\Api\Data\CreditmemoInterface
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterGet(
        \Magento\Sales\Api\CreditmemoRepositoryInterface $subject,
        \Magento\Sales\Api\Data\CreditmemoInterface $entity
    )
    {
        if (!$this->helper->isModuleEnabled()) {
            return $entity;
        }

        /** @var \Magento\Sales\Api\Data\CreditmemoExtension $extensionAttributes */
        $extensionAttributes = $entity->getExtensionAttributes();
        if ($extensionAttributes === null) {
            $extensionAttributes = $this->extensionFactory->create();
        }

        $extensionAttributes->setBaseLoyaltyCouponsAmount($entity->getBaseLoyaltyCouponsAmount());
        $extensionAttributes->setLoyaltyCouponsAmount($entity->getLoyaltyCouponsAmount());
        $entity->setExtensionAttributes($extensionAttributes);
        return $entity;
    }

    /**
     * @param \Magento\Sales\Api\CreditmemoRepositoryInterface $subject
     * @param \Magento\Sales\Api\Data\CreditmemoSearchResultInterface $entities
     *
     * @return \Magento\Sales\Api\Data\CreditmemoSearchResultInterface
     */
    public function afterGetList(
        \Magento\Sales\Api\CreditmemoRepositoryInterface $subject,
        \Magento\Sales\Api\Data\CreditmemoSearchResultInterface $entities
    )
    {
        if (!$this->helper->isModuleEnabled()) {
            return $entities;
        }

        /** @var \Magento\Sales\Api\Data\CreditmemoInterface $entity */
        foreach ($entities->getItems() as $entity) {
            $this->afterGet($subject, $entity);
        }

        return $entities;
    }
}
