<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */

namespace TwoJay\Loyalty\Model\Plugin;

/**
 * Class InvoiceRepository
 * @package TwoJay\Loyalty\Model\Plugin
 */
class InvoiceRepository
{
    /**
     * @var \Magento\Sales\Api\Data\InvoiceExtensionFactory
     */
    private $extensionFactory;

    /**
     * @var \TwoJay\Loyalty\Helper\Data
     */
    protected $helper;

    /**
     * @param \Magento\Sales\Api\Data\InvoiceExtensionFactory $extensionFactory
     */
    public function __construct(
        \Magento\Sales\Api\Data\InvoiceExtensionFactory $extensionFactory,
        \TwoJay\Loyalty\Helper\Data $helper
    )
    {
        $this->extensionFactory = $extensionFactory;
        $this->helper = $helper;
    }

    /**
     * @param \Magento\Sales\Api\InvoiceRepositoryInterface $subject
     * @param \Magento\Sales\Api\Data\InvoiceInterface $entity
     *
     * @return \Magento\Sales\Api\Data\InvoiceInterface
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterGet(
        \Magento\Sales\Api\InvoiceRepositoryInterface $subject,
        \Magento\Sales\Api\Data\InvoiceInterface $entity
    )
    {
        if (!$this->helper->isModuleEnabled()) {
            return $entity;
        }

        /** @var \Magento\Sales\Api\Data\InvoiceExtension $extensionAttributes */
        $extensionAttributes = $entity->getExtensionAttributes();

        if ($extensionAttributes === null) {
            $extensionAttributes = $this->extensionFactory->create();
        }

        $extensionAttributes->setBaseLoyaltyCouponsAmount($entity->getBaseLoyaltyCouponsAmount());
        $extensionAttributes->setLoyaltyCouponsAmount($entity->getLoyaltyCouponsAmount());

        $entity->setExtensionAttributes($extensionAttributes);

        return $entity;
    }

    /**
     * @param \Magento\Sales\Api\InvoiceRepositoryInterface $subject
     * @param \Magento\Sales\Api\Data\InvoiceSearchResultInterface $entities
     *
     * @return \Magento\Sales\Api\Data\InvoiceSearchResultInterface
     */
    public function afterGetList(
        \Magento\Sales\Api\InvoiceRepositoryInterface $subject,
        \Magento\Sales\Api\Data\InvoiceSearchResultInterface $entities
    )
    {
        if (!$this->helper->isModuleEnabled()) {
            return $entities;
        }

        /** @var \Magento\Sales\Api\Data\InvoiceInterface $entity */
        foreach ($entities->getItems() as $entity) {
            $this->afterGet($subject, $entity);
        }

        return $entities;
    }

    /**
     * Sets loyalty coupon data from extension attributes
     * to Invoice model.
     *
     * @param \Magento\Sales\Api\InvoiceRepositoryInterface $subject
     * @param \Magento\Sales\Api\Data\InvoiceInterface $entity
     *
     * @return void
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function beforeSave(
        \Magento\Sales\Api\InvoiceRepositoryInterface $subject,
        \Magento\Sales\Api\Data\InvoiceInterface $entity
    )
    {
        if (!$this->helper->isModuleEnabled()) {
            return;
        }

        $extensionAttributes = $entity->getExtensionAttributes();

        if ($extensionAttributes) {
            $entity->setLoyaltyCouponsAmount($extensionAttributes->getLoyaltyCouponsAmount());
            $entity->setBaseLoyaltyCouponsAmount($extensionAttributes->getBaseLoyaltyCouponsAmount());
        }
    }
}
