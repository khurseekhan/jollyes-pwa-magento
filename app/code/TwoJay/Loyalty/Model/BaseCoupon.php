<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */

namespace TwoJay\Loyalty\Model;

use TwoJay\Loyalty\Api\Data\BaseCouponInterface;

/**
 * Class BaseCoupon
 * @package TwoJay\Loyalty\Model
 */
class BaseCoupon implements BaseCouponInterface
{
    /**
     * @var string
     */
    private $code;

    /**
     * @var float
     */
    private $amount;

    /**
     * @var float
     */
    private $baseAmount;


    /**
     * @inheritdoc
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @inheritdoc
     */
    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @inheritdoc
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getBaseAmount()
    {
        return $this->baseAmount;
    }

    /**
     * @inheritdoc
     */
    public function setBaseAmount($baseAmount)
    {
        $this->baseAmount = $baseAmount;
        return $this;
    }

    /**
     * @return string
     */
    public function getLoyaltyCoupons()
    {
        return $this->getCode();
    }

    /**
     * @return float
     */
    public function getLoyaltyCouponsAmount()
    {
        return $this->getAmount();
    }

    /**
     * @return float
     */
    public function getBaseLoyaltyCouponsAmount()
    {
        return $this->getBaseAmount();
    }

    /**
     * @return null
     */
    public function getLoyaltyCouponsAmountUsed()
    {
        return null;
    }

    /**
     * @return null
     */
    public function getBaseLoyaltyCouponsAmountUsed()
    {
        return null;
    }

    /**
     *
     */
    public function getExtensionAttributes()
    {
        return;
    }
}
