<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */

namespace TwoJay\Loyalty\Model\Loyalty;

use Magento\Framework\HTTP\Client\Curl;
use TwoJay\Loyalty\Api\Loyalty\LoginInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Psr\Log\LoggerInterface;
use TwoJay\Loyalty\Api\Loyalty\TokenInterface;
use Magento\Framework\Json\Helper\Data as JsonHelper;
use TwoJay\Loyalty\Helper\Data as LoyaltyHelper;

/**
 * Class Login
 */
class Login extends AbstractEntryPoint implements LoginInterface
{
    const API_Login = 'Login';

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var Curl
     */
    protected $curl;

    /**
     * @var TokenInterface
     */
    protected $token;

    /**
     * @var JsonHelper
     */
    private $jsonHelper;

    /**
     * @var LoyaltyHelper
     */
    protected $loyaltyHelper;

    /**
     * Login constructor.
     * @param ScopeConfigInterface $scopeConfig
     * @param Curl $curl
     * @param LoggerInterface $logger
     * @param TokenInterface $token
     * @param JsonHelper $jsonHelper
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        Curl $curl,
        LoggerInterface $logger,
        TokenInterface $token,
        JsonHelper $jsonHelper,
        LoyaltyHelper $loyaltyHelper
    )
    {
        parent::__construct($scopeConfig);
        $this->curl = $curl;
        $this->token = $token;
        $this->logger = $logger;
        $this->jsonHelper = $jsonHelper;
        $this->loyaltyHelper = $loyaltyHelper;
    }


    /**
     * @inheritdoc
     */
    public function login($params)
    {
        $cUrl = $this->getApiUrl('api/Customer/' . static::API_Login);
        try {
            $tokenData = $this->token->getToken();
            // check for 503 service unavailable
            if (!$this->loyaltyHelper->isJson($tokenData) && strpos($tokenData, '503') !== false) {
                return $tokenData;
            }
            $tokenData = $this->jsonHelper->jsonDecode($tokenData);
            $token = $tokenData['access_token'];
            $headers = array(
                'Authorization' => 'Bearer ' . $token,
                'Content-Type' => 'application/json'
            );

            // curl call started
            $this->curl->setHeaders($headers);
            $this->curl->post($cUrl, $params);
            $response = $this->curl->getBody();
            // curl call ended

            if (empty($response)) {
                $this->loyaltyHelper->insertToLoyaltyLogs('API', 'Login',
                    $cUrl, 'Could not receive response from Itim', null, null, null);
            }

            if ($this->loyaltyHelper->isLogEnabled()) {
                $this->loyaltyHelper->log('API Login Response: ' . $response);
            }

            return $response;
        } catch (\Exception $e) {
            $errorMsg = __class__ . ": Exception Error: " . $e->getMessage();
            $this->loyaltyHelper->insertToLoyaltyLogs('API', 'Login',
                $cUrl, $errorMsg, null, null, null);
        }
    }
}