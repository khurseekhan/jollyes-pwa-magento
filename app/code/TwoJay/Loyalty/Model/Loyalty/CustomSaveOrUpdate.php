<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */

namespace TwoJay\Loyalty\Model\Loyalty;

use TwoJay\Loyalty\Api\Loyalty\HttpClientInterface;
use TwoJay\Loyalty\Api\Loyalty\CustomSaveOrUpdateInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Psr\Log\LoggerInterface;
use TwoJay\Loyalty\Api\Loyalty\TokenInterface;
use Magento\Framework\Json\Helper\Data as JsonHelper;
use TwoJay\Loyalty\Helper\Data as LoyaltyHelper;

class CustomSaveOrUpdate extends AbstractEntryPoint implements CustomSaveOrUpdateInterface
{
    const API_CUSTOM_SAVE_OR_UPDATE = 'customSaveOrUpdate';

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var HttpClientInterface
     */
    protected $httpClient;

    /**
     * @var TokenInterface
     */
    protected $token;

    /**
     * @var JsonHelper
     */
    private $jsonHelper;

    /**
     * @var LoyaltyHelper
     */
    protected $loyaltyHelper;

    /**
     * CustomSaveOrUpdate constructor.
     * @param ScopeConfigInterface $scopeConfig
     * @param HttpClientInterface $httpClient
     * @param LoggerInterface $logger
     * @param TokenInterface $token
     * @param JsonHelper $jsonHelper
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        HttpClientInterface $httpClient,
        LoggerInterface $logger,
        TokenInterface $token,
        JsonHelper $jsonHelper,
        LoyaltyHelper $loyaltyHelper
    )
    {
        parent::__construct($scopeConfig);
        $this->httpClient = $httpClient;
        $this->token = $token;
        $this->logger = $logger;
        $this->jsonHelper = $jsonHelper;
        $this->loyaltyHelper = $loyaltyHelper;
    }

    /**
     * This function is used to update the customer data based on Json parameter
     * @param \string[] $params
     * @return string
     */
    public function customSaveOrUpdate($params)
    {
        if (!isset($params['LoyaltyNumber']) || empty($params['LoyaltyNumber'])) {
            if ($this->loyaltyHelper->isLogEnabled()) {
                $msg = 'Invalid request as Loyalty number is not present';
                $this->loyaltyHelper->log(__CLASS__ . ': Line ' . __line__ . 'Error: ' . $msg);
            }
            return false;
        }

        $curlUrl = $this->getApiUrl('api/Customer/' . static::API_CUSTOM_SAVE_OR_UPDATE);
        try {
            $tokenData = $this->token->getToken();
            // check for 503 service unavailable
            if (!$this->loyaltyHelper->isJson($tokenData) && strpos($tokenData, '503') !== false) {
                return $tokenData;
            }
            $tokenData = $this->jsonHelper->jsonDecode($tokenData);
            $token = $tokenData['access_token'];

            $headers = [
                'Authorization: Bearer ' . $token,
                'Content-Type: application/json'
            ];

            $jsonResponse = $this->httpClient->put($curlUrl, $this->jsonHelper->jsonEncode($params), '1.1', $headers);

            if (empty($jsonResponse)) {
                $this->loyaltyHelper->insertToLoyaltyLogs('API', 'customSaveOrUpdate',
                    $curlUrl, 'Could not receive response from Itim', null, null, null);
            }

            if ($this->loyaltyHelper->isLogEnabled()) {
                $this->loyaltyHelper->log('API CustomSaveOrUpdate Response::' . $jsonResponse);
            }

            return $jsonResponse;
        } catch (\Exception $e) {
            $errorMsg = __class__ . ": Exception Error: " . $e->getMessage();
            $this->loyaltyHelper->insertToLoyaltyLogs('API', 'CustomSaveOrUpdate',
                $curlUrl, $errorMsg, null, null, null);
        }
    }
}