<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */

namespace TwoJay\Loyalty\Model\Loyalty;

use TwoJay\Loyalty\Api\Loyalty\EntryPointInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;

/**
 * Class AbstractEntryPoint
 */
abstract class AbstractEntryPoint implements EntryPointInterface
{
    /** @var ScopeConfigInterface */
    protected $scopeConfig;

    /**
     * AbstractEntryPoint constructor.
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig
    )
    {
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * Get base Url of service
     *
     * @return string
     */
    public function getBaseUrl()
    {
        return $this->scopeConfig->getValue(static::XPATH_ENTRY_POINT_BASE_URL);
    }

    /**
     * Get URL of entry points by name
     *
     * @param string $key
     * @return string
     */
    public function getEntryPointUrl()
    {
        return $this->scopeConfig->getValue(static::XPATH_ENTRY_POINT_PREFIX);
    }

    /**
     * Get API user name
     *
     * @return string
     */
    public function getApiUserName()
    {
        return $this->scopeConfig->getValue(static::XPATH_ENTRY_POINT_USER_NAME);
    }

    /**
     * Get API user name
     *
     * @return string
     */
    public function getApiUserPassword()
    {
        return $this->scopeConfig->getValue(static::XPATH_ENTRY_POINT_PASSWORD);
    }

    /**
     * Get API user name
     *
     * @return string
     */
    public function getApiClientId()
    {
        return $this->scopeConfig->getValue(static::XPATH_ENTRY_POINT_CLIENT_ID);
    }

    /**
     * Get Grant Type
     *
     * @return string
     */
    public function getApiGrantType()
    {
        return $this->scopeConfig->getValue(static::XPATH_ENTRY_POINT_GRANT_TYPE);
    }


    /**
     * Retrieve URL of API from config
     *
     * @param $key
     * @return string
     */
    protected function getApiUrl($key)
    {
        $entryPointUrl = $this->getEntryPointUrl() . '/' . $key;

        return $entryPointUrl;
    }


    /**
     * Retrieve URL of API with parameters
     *
     * @param $key string
     * @param $params array
     * @return string
     */
    protected function getApiUrlWithParams($key, array $params)
    {
        $placeholders = array_keys($params);
        $values = array_values($params);

        $entryPointUrl = $this->getEntryPointUrl($key);
        $builtUrlTemplate = $this->getBaseUrl() . $entryPointUrl;

        return str_replace($placeholders, $values, $builtUrlTemplate);
    }
}