<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */

namespace TwoJay\Loyalty\Model\Loyalty;

use Magento\Framework\HTTP\Client\Curl;
use TwoJay\Loyalty\Api\Loyalty\TokenInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Psr\Log\LoggerInterface;
use TwoJay\Loyalty\Helper\Data as LoyaltyHelper;

/**
 * Class Token
 * @package TwoJay\Loyalty\Model\Loyalty
 */
class Token extends AbstractEntryPoint implements TokenInterface
{
    const API_TOKEN = 'Token';

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var Curl
     */
    protected $curl;

    /**
     * @var LoyaltyHelper
     */
    protected $loyaltyHelper;

    /**
     * Token constructor.
     * @param ScopeConfigInterface $scopeConfig
     * @param Curl $curl
     * @param LoggerInterface $logger
     * @param LoyaltyHelper $loyaltyHelper
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        Curl $curl,
        LoggerInterface $logger,
        LoyaltyHelper $loyaltyHelper
    )
    {
        parent::__construct($scopeConfig);
        $this->curl = $curl;
        $this->logger = $logger;
        $this->loyaltyHelper = $loyaltyHelper;
    }

    /**
     * @return string
     */
    public function getToken()
    {
        $tokenUrl = $this->getApiUrl(static::API_TOKEN);
        try {
            $requestParams = [
                'grant_type' => $this->getApiGrantType(),
                'username' => $this->getApiUserName(),
                'password' => $this->getApiUserPassword(),
                'client_id' => $this->getApiClientId()
            ];
            // curl call started
            $this->curl->post($tokenUrl, $requestParams);
            $response = $this->curl->getBody();
            // curl call ended

            if (empty($response)) {
                $this->loyaltyHelper->insertToLoyaltyLogs('API_Token', 'GetToken',
                    $tokenUrl, 'Could not receive response from Itim', null, null, null);
            }

            if ($this->loyaltyHelper->isLogEnabled()) {
                $this->loyaltyHelper->log('API Token Response::' . $response);
            }

            if (!$this->loyaltyHelper->isJson($response) && strpos($response, '503') !== false) {
                $this->loyaltyHelper->insertToLoyaltyLogs('API_Token', 'GetToken',
                    $tokenUrl, 'HTTP Error 503. The service is unavailable.', null, null, null);
                //throw new \Exception('Could not receive response from Itim');
            }

            return $response;
        } catch (\Exception $e) {
            $errorMsg = __class__ . ": Exception Error: " . $e->getMessage();
            $this->loyaltyHelper->insertToLoyaltyLogs('API_Token', 'GetToken',
                $tokenUrl, $errorMsg, null, null, null);
        }
    }
}