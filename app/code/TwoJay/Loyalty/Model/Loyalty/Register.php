<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */

namespace TwoJay\Loyalty\Model\Loyalty;

use TwoJay\Loyalty\Api\Loyalty\HttpClientInterface;
use TwoJay\Loyalty\Api\Loyalty\RegisterInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Psr\Log\LoggerInterface;
use TwoJay\Loyalty\Api\Loyalty\TokenInterface;
use TwoJay\Loyalty\Helper\Data as LoyaltyHelper;

/**
 * Class Register
 * @package TwoJay\Loyalty\Model\Loyalty
 */
class Register extends AbstractEntryPoint implements RegisterInterface
{
    const API_REGISTER_CUSTOMER = 'RegisterCustomer';

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var HttpClientInterface
     */
    protected $httpClient;

    /**
     * @var TokenInterface
     */
    protected $token;

    /**
     * @var LoyaltyHelper
     */
    protected $loyaltyHelper;

    /**
     * Register constructor.
     * @param ScopeConfigInterface $scopeConfig
     * @param HttpClientInterface $httpClient
     * @param LoggerInterface $logger
     * @param TokenInterface $token
     * @param LoyaltyHelper $loyaltyHelper
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        HttpClientInterface $httpClient,
        LoggerInterface $logger,
        TokenInterface $token,
        LoyaltyHelper $loyaltyHelper
    )
    {
        parent::__construct($scopeConfig);
        $this->httpClient = $httpClient;
        $this->token = $token;
        $this->logger = $logger;
        $this->loyaltyHelper = $loyaltyHelper;
    }

    /**
     * @param \string[] $params
     * @return string
     */
    public function register($params)
    {
        $curlUrl = $this->getApiUrl('api/Customer/'.static::API_REGISTER_CUSTOMER);
        try {
            $tokenData = $this->token->getToken();
            // check for 503 service unavailable
            if(!$this->loyaltyHelper->isJson($tokenData) && strpos($tokenData, '503') !== false){
                return $tokenData;
            }
            $tokenData = json_decode($tokenData,true);
            $token = $tokenData['access_token'];

            $headers = [
                'Authorization: Bearer '.$token,
                'Content-Type: application/json'
            ];

            $jsonResponse = $this->httpClient->put($curlUrl, json_encode($params),'1.1', $headers);

            if (empty($jsonResponse)) {
                $this->loyaltyHelper->insertToLoyaltyLogs('API', 'Register',
                    $curlUrl, 'Could not receive response from Itim', null, null, null);
            }

            if ($this->loyaltyHelper->isLogEnabled()) {
                $this->loyaltyHelper->log('API Register Response: ' . $jsonResponse);
            }

            return $jsonResponse;
        } catch (\Exception $e) {
            $errorMsg = __class__ . ": Exception Error: " . $e->getMessage();
            $this->loyaltyHelper->insertToLoyaltyLogs('API', 'Register',
                $curlUrl, $errorMsg, null, null, null);
        }
    }
}