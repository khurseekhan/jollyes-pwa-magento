<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */

namespace TwoJay\Loyalty\Model\Loyalty;

use TwoJay\Loyalty\Api\Loyalty\HttpClientInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Psr\Log\LoggerInterface;
use TwoJay\Loyalty\Api\Loyalty\TokenInterface;
use TwoJay\Loyalty\Api\Loyalty\RedeemVoucherInterface;
use Magento\Framework\Json\Helper\Data as JsonHelper;
use TwoJay\Loyalty\Helper\Data as LoyaltyHelper;

/**
 * Class RedeemVoucher
 * @package TwoJay\Loyalty\Model\Loyalty
 */
class RedeemVoucher extends AbstractEntryPoint implements RedeemVoucherInterface
{
    const API_REDEEM_VOUCHER = 'RedeemVoucher';

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var HttpClientInterface
     */
    protected $httpClient;

    /**
     * @var TokenInterface
     */
    protected $token;

    /**
     * @var JsonHelper
     */
    protected $jsonHelper;

    /**
     * @var LoyaltyHelper
     */
    protected $loyaltyHelper;

    /**
     * RedeemVoucher constructor.
     * @param ScopeConfigInterface $scopeConfig
     * @param HttpClientInterface $httpClient
     * @param LoggerInterface $logger
     * @param TokenInterface $token
     * @param JsonHelper $jsonHelper
     * @param LoyaltyHelper $loyaltyHelper
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        HttpClientInterface $httpClient,
        LoggerInterface $logger,
        TokenInterface $token,
        JsonHelper $jsonHelper,
        LoyaltyHelper $loyaltyHelper
    )
    {
        parent::__construct($scopeConfig);
        $this->httpClient = $httpClient;
        $this->token = $token;
        $this->logger = $logger;
        $this->jsonHelper = $jsonHelper;
        $this->loyaltyHelper = $loyaltyHelper;
    }

    /**
     * This function is used to Redeem voucher based on Loyalty Number
     * @param \string[] $params
     * @return string
     */
    public function redeemVoucher($params)
    {
        $curlUrl = $this->getApiUrl('api/Customer/' . static::API_REDEEM_VOUCHER);
        try {
            $tokenData = $this->token->getToken();
            // check for 503 service unavailable
            if (!$this->loyaltyHelper->isJson($tokenData) && strpos($tokenData, '503') !== false) {
                return $tokenData;
            }
            $tokenData = $this->jsonHelper->jsonDecode($tokenData, true);
            $token = $tokenData['access_token'];

            $headers = [
                'Authorization: Bearer ' . $token,
                'Content-Type: application/json'
            ];

            $jsonResponse = $this->httpClient->put($curlUrl, $this->jsonHelper->jsonEncode($params), '1.1', $headers);

            if (empty($jsonResponse)) {
                $this->loyaltyHelper->insertToLoyaltyLogs('API', 'RedeemVoucher',
                    $curlUrl, 'Could not receive response from Itim', null, null, null);
            }

            if ($this->loyaltyHelper->isLogEnabled()) {
                $this->loyaltyHelper->log('API RedeemVoucher Response: ' . $jsonResponse);
            }

            return $jsonResponse;
        } catch (\Exception $e) {
            $errorMsg = __class__ . ": Exception Error: " . $e->getMessage();
            $this->loyaltyHelper->insertToLoyaltyLogs('API', 'RedeemVoucher',
                $curlUrl, $errorMsg, null, null, null);
        }
    }
}
