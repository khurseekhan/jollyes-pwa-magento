<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */

namespace TwoJay\Loyalty\Model\Loyalty;

use Magento\Framework\HTTP\Client\Curl;
use TwoJay\Loyalty\Api\Loyalty\CustomSearchInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Psr\Log\LoggerInterface;
use TwoJay\Loyalty\Api\Loyalty\TokenInterface;
use Magento\Framework\Json\Helper\Data as JsonHelper;
use TwoJay\Loyalty\Helper\Data as LoyaltyHelper;

/**
 * Class CustomSearch
 */
class CustomSearch extends AbstractEntryPoint implements CustomSearchInterface
{
    const API_CustomSearch = 'CustomSearch';

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var Curl
     */
    protected $curl;

    /**
     * @var TokenInterface
     */
    protected $token;

    /**
     * @var JsonHelper
     */
    private $jsonHelper;

    /**
     * @var LoyaltyHelper
     */
    protected $loyaltyHelper;

    /**
     * CustomSearch constructor.
     * @param ScopeConfigInterface $scopeConfig
     * @param Curl $curl
     * @param LoggerInterface $logger
     * @param TokenInterface $token
     * @param JsonHelper $jsonHelper
     * @param LoyaltyHelper $loyaltyHelper
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        Curl $curl,
        LoggerInterface $logger,
        TokenInterface $token,
        JsonHelper $jsonHelper,
        LoyaltyHelper $loyaltyHelper
    )
    {
        parent::__construct($scopeConfig);
        $this->curl = $curl;
        $this->token = $token;
        $this->logger = $logger;
        $this->jsonHelper = $jsonHelper;
        $this->loyaltyHelper = $loyaltyHelper;
    }

    /**
     * This will accept FirstName ,Lastname, Email, Mobile Number, Home Number or Postcode
     * as a query parameter
     * @param string $param
     * @return string
     */
    public function customSearch($param = '')
    {
        $cUrl = $this->getApiUrl('api/Customer/' . static::API_CustomSearch);
        try {
            if (!empty($param)) {
                $param = '?lookupParam=' . trim($param);
                $cUrl .= $param;
                $tokenData = $this->token->getToken();
                // check for 503 service unavailable
                if (!$this->loyaltyHelper->isJson($tokenData) && strpos($tokenData, '503') !== false) {
                    return $tokenData;
                }
                $tokenData = $this->jsonHelper->jsonDecode($tokenData);
                $token = $tokenData['access_token'];
                $headers = array(
                    'Authorization' => 'Bearer ' . $token,
                    'Content-Type' => 'application/json',
                    'X-Translation-Depth' => 1
                );

                // curl call started
                $this->curl->setHeaders($headers);
                $this->curl->get($cUrl);
                $response = $this->curl->getBody();
                // curl call ended

                if (empty($response)) {
                    $this->loyaltyHelper->insertToLoyaltyLogs('API', 'customSearch',
                        $cUrl, 'Could not receive response from Itim', null, null, null);
                }

                if ($this->loyaltyHelper->isLogEnabled()) {
                    $this->loyaltyHelper->log('API customSearch Response: ' . $response);
                }
            } else {
                $response = 'Request query parameter is invalid';
                $this->loyaltyHelper->log("Error: " . $cUrl . '==>' . $response);
            }
            return $response;
        } catch (\Exception $e) {
            $errorMsg = __class__ . ": Exception Error: " . $e->getMessage();
            $this->loyaltyHelper->insertToLoyaltyLogs('API', 'customSearch',
                $cUrl, $errorMsg, null, null, null);
        }
    }
}