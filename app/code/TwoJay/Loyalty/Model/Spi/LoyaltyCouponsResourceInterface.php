<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */

namespace TwoJay\Loyalty\Model\Spi;

/**
 * Interface LoyaltyCouponsResourceInterface
 */
interface LoyaltyCouponsResourceInterface
{
    /**
     * Save object data
     *
     * @param \Magento\Framework\Model\AbstractModel $object
     * @return $this
     */
    public function save(\Magento\Framework\Model\AbstractModel $object);

    /**
     * Load an object
     *
     * @param mixed $value
     * @param \Magento\Framework\Model\AbstractModel $object
     * @param string|null $field field to load by (defaults to model id)
     * @return \TwoJay\Loyalty\Model\Spi\LoyaltyCouponsResourceInterface
     */
    public function load(\Magento\Framework\Model\AbstractModel $object, $value, $field = null);

    /**
     * Delete the object
     *
     * @param \Magento\Framework\Model\AbstractModel $object
     * @return bool
     */
    public function delete(\Magento\Framework\Model\AbstractModel $object);
}
