<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */

namespace TwoJay\Loyalty\Model\Service;

use TwoJay\Loyalty\Model\LoyaltyCoupons;
use Magento\Framework\Exception\NoSuchEntityException;
use TwoJay\Loyalty\Api\LoyaltyCouponManagementInterface;
use Magento\Framework\Json\Helper\Data as JsonHelper;
use Magento\Quote\Api\CartRepositoryInterface;
use TwoJay\Loyalty\Helper\Data;
use TwoJay\Loyalty\Model\LoyaltyCouponsFactory;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class LoyaltyCouponsManagement
 */
class LoyaltyCouponManagement implements LoyaltyCouponManagementInterface
{
    /**
     * @var CartRepositoryInterface
     */
    protected $quoteRepository;

    /**
     * @var Data
     */
    protected $loyaltyCouponHelper;

    /**
     * @var LoyaltyCouponsFactory
     */
    protected $loyaltyCouponFactory;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var JsonHelper
     */
    protected $jsonHelper;

    /**
     * LoyaltyCouponManagement constructor.
     * @param CartRepositoryInterface $quoteRepository
     * @param Data $loyaltyCouponsData
     * @param LoyaltyCouponsFactory $loyaltyCouponsFactory
     * @param StoreManagerInterface $storeManager
     * @param JsonHelper $jsonHelper
     */
    public function __construct(
        CartRepositoryInterface $quoteRepository,
        Data $loyaltyCouponsData,
        LoyaltyCouponsFactory $loyaltyCouponsFactory,
        StoreManagerInterface $storeManager,
        JsonHelper $jsonHelper
    )
    {
        $this->quoteRepository = $quoteRepository;
        $this->loyaltyCouponHelper = $loyaltyCouponsData;
        $this->loyaltyCouponFactory = $loyaltyCouponsFactory;
        $this->storeManager = $storeManager;
        $this->jsonHelper = $jsonHelper;
    }

    /**
     * {@inheritdoc}
     */
    public function saveByQuoteId(
        $cartId,
        \TwoJay\Loyalty\Api\Data\LoyaltyCouponsInterface $loyaltyCouponData
    )
    {
        $quote = $this->quoteRepository->getActive($cartId);
        if (!$quote->getItemsCount()) {
            throw new NoSuchEntityException(__('Cart %1 doesn\'t contain products', $cartId));
        }
        $loyaltyCode = $loyaltyCouponData->getLoyaltyCoupons();
        /** @var \TwoJay\Loyalty\Model\LoyaltyCoupons $loyaltyCoupon */
        $loyaltyCoupon = $this->loyaltyCouponFactory->create();
        try {
            $loyaltyCoupon->addToCart($loyaltyCode);
        } catch (\Exception $e) {
            return $result = $this->jsonHelper->jsonEncode(['status' => false, "message" => $e->getMessage()]);
        }
        return $result = $this->jsonHelper->jsonEncode(['status' => true, "message" => '']);
    }

    /**
     * {@inheritdoc}
     */
    public function deleteByQuoteId($cartId, $loyaltyCouponCode)
    {
        /** @var  \Magento\Quote\Model\Quote $quote */
        $quote = $this->quoteRepository->getActive($cartId);
        if (!$quote->getItemsCount()) {
            throw new NoSuchEntityException(__('Cart %1 doesn\'t contain products', $cartId));
        }

        $loyaltyCoupon = $this->loyaltyCouponFactory->create();

        try {
            $loyaltyCoupon->removeFromCart($loyaltyCouponCode, true, $quote);
            $quote->collectTotals();
        } catch (\Exception $e) {
            $errorMsg = 'Could not delete PetCLUB Voucher from quote' . $e->getMessage();
            return $result = $this->jsonHelper->jsonEncode(['status' => false, "message" => $errorMsg]);
        }
        return $result = $this->jsonHelper->jsonEncode(['status' => true, "message" => '']);
    }

    /**
     * {@inheritdoc}
     */
    public function getListByQuoteId($cartId)
    {
        /** @var  \Magento\Quote\Model\Quote $quote */
        $quote = $this->quoteRepository->getActive($cartId);
        $loyaltyCoupons = $this->loyaltyCouponHelper->getLoyaltyCoupons($quote);
        $cards = [];
        foreach ($loyaltyCoupons as $loyaltyCoupon) {
            $cards[] = $loyaltyCoupon;
        }
        $data = [
            LoyaltyCoupons::LOYALTY_COUPONS => $cards,
            LoyaltyCoupons::LOYALTY_COUPONS_AMOUNT => $quote->getLoyaltyCouponsAmount(),
            LoyaltyCoupons::BASE_LOYALTY_COUPONS_AMOUNT => $quote->getBaseLoyaltyCouponsAmount()
        ];

        $loyaltyCouponData = $this->loyaltyCouponFactory->create(['data' => $data]);
        return $loyaltyCouponData;
    }

    /**
     * {@inheritdoc}
     */
    public function checkLoyaltyCoupon($cartId, $loyaltyCouponCode)
    {

    }

}
