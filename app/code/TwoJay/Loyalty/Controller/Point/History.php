<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */
namespace TwoJay\Loyalty\Controller\Point;

use TwoJay\Loyalty\Model\ConfigInterface;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Customer\Model\Session;
class History extends \TwoJay\Loyalty\Controller\Index
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * History constructor.
     * @param Context $context
     * @param ConfigInterface $loyaltyConfig
     * @param PageFactory $resultPageFactory
     * @param Session $customerSession
     */
    public function __construct(
        Context $context,
        ConfigInterface $loyaltyConfig,
        PageFactory $resultPageFactory,
        Session $customerSession
    ) {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context, $loyaltyConfig, $customerSession);
    }

    /**
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        /** @var \Magento\Framework\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->set(__('PetCLUB Points History'));

        return $resultPage;
    }
}
