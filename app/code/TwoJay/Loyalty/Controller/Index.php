<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace TwoJay\Loyalty\Controller;

use TwoJay\Loyalty\Model\ConfigInterface;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Exception\NotFoundException;
use Magento\Customer\Model\Session;

/**
 * Contact module base controller
 */
abstract class Index extends \Magento\Framework\App\Action\Action
{

    /**
     * Enabled config path
     */
    const XML_PATH_ENABLED = ConfigInterface::XML_PATH_ENABLED;

    /**
     * @var ConfigInterface
     */
    private $loyaltyConfig;

    /**
     * @var Session
     */
    protected $_customerSession;

    /**
     * Index constructor.
     * @param Context $context
     * @param ConfigInterface $loyaltyConfig
     * @param Session $customerSession
     */
    public function __construct(
        Context $context,
        ConfigInterface $loyaltyConfig,
        Session $customerSession
    ) {
        parent::__construct($context);
        $this->loyaltyConfig = $loyaltyConfig;
        $this->_customerSession = $customerSession;
    }

    /**
     * Dispatch request
     *
     * @param RequestInterface $request
     * @return \Magento\Framework\App\ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function dispatch(RequestInterface $request)
    {
        $isLoggedIn = ($this->_customerSession->isLoggedIn())? 'true':'false';

        if (!$this->loyaltyConfig->isModuleEnabled() || ($isLoggedIn=='false')) {
            throw new NotFoundException(__('Page not found.'));
        }

        return parent::dispatch($request);
    }
}
