<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */

namespace TwoJay\Loyalty\Controller\Api;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Controller\Result\RawFactory;
use Magento\Framework\Json\Helper\Data as JsonHelper;
use TwoJay\Loyalty\Block\Checkout\Cart\Loyaltycoupon;

/**
 * Class Check
 * @package TwoJay\Loyalty\Controller\Api
 */
class Check extends Action
{
    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var \Magento\Framework\Controller\Result\RawFactory
     */
    protected $resultRawFactory;

    /**
     * @var JsonHelper
     */
    private $jsonHelper;

    /**
     * @var LoyaltyHelper
     */
    protected $loyaltyApiCheck;

    /**
     * @var \TwoJay\Loyalty\Helper\Data
     */
    protected $loyaltyHelper;

    /**
     * Check constructor.
     * @param Context $context
     * @param JsonFactory $resultJsonFactory
     * @param RawFactory $resultRawFactory
     * @param JsonHelper $jsonHelper
     * @param Loyaltycoupon $loyaltyApiCheck
     * @param \TwoJay\Loyalty\Helper\Data $loyaltyHelper
     */
    public function __construct(
        Context $context,
        JsonFactory $resultJsonFactory,
        RawFactory $resultRawFactory,
        JsonHelper $jsonHelper,
        Loyaltycoupon $loyaltyApiCheck,
        \TwoJay\Loyalty\Helper\Data $loyaltyHelper
    )
    {
        $this->resultJsonFactory = $resultJsonFactory;
        $this->resultRawFactory = $resultRawFactory;
        $this->jsonHelper = $jsonHelper;
        $this->loyaltyApiCheck = $loyaltyApiCheck;
        $this->loyaltyHelper = $loyaltyHelper;
        parent::__construct($context);
    }

    /**
     * Ajax call to check api availability
     *
     * @return $this
     */
    public function execute()
    {
        $httpBadRequestCode = 400;

        /** @var \Magento\Framework\Controller\Result\Raw $resultRaw */
        $resultRaw = $this->resultRawFactory->create();
        if ($this->getRequest()->getMethod() !== 'POST' || !$this->getRequest()->isXmlHttpRequest()) {
            return $resultRaw->setHttpResponseCode($httpBadRequestCode);
        }

        try {
            $response = [
                'flag' => false,
                'message' => __($this->loyaltyHelper->getErrorMessage())
            ];

            if ($this->loyaltyApiCheck->isApiAvailable()) {
                $response['flag'] = true;
            }
        } catch (\Exception $e) {
            return $resultRaw->setHttpResponseCode($httpBadRequestCode);
        }

        /** @var \Magento\Framework\Controller\Result\Json $resultJson */
        $resultJson = $this->resultJsonFactory->create();
        return $resultJson->setData($response);
    }
}