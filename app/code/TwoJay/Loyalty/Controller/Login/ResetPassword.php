<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */
namespace TwoJay\Loyalty\Controller\Login;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Controller\Result\RawFactory;
use Magento\Framework\Exception\InvalidEmailOrPasswordException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Json\Helper\Data as JsonHelper;
use Magento\Framework\Escaper;
use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Model\AccountManagement;

/**
 * Class ResetPassword
 * @package TwoJay\Loyalty\Controller\Login
 */
class ResetPassword extends Action
{
    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var \Magento\Framework\Controller\Result\RawFactory
     */
    protected $resultRawFactory;

    /**
     * @var JsonHelper
     */
    protected $jsonHelper;

    /**
     * @var Escaper
     */
    protected $escaper;

    /**
     * @var AccountManagementInterface
     */
    protected $accountManagementInterface;


    /**
     * ResetPassword constructor.
     * @param Context $context
     * @param JsonFactory $resultJsonFactory
     * @param RawFactory $resultRawFactory
     * @param JsonHelper $jsonHelper
     * @param Escaper $escaper
     * @param AccountManagementInterface $accountManagementInterface
     */
    public function __construct(
        Context $context,
        JsonFactory $resultJsonFactory,
        RawFactory $resultRawFactory,
        JsonHelper $jsonHelper,
        Escaper $escaper,
        AccountManagementInterface $accountManagementInterface
    ) {
        $this->resultJsonFactory = $resultJsonFactory;
        $this->resultRawFactory = $resultRawFactory;
        $this->jsonHelper = $jsonHelper;
        $this->escaper = $escaper;
        $this->accountManagementInterface = $accountManagementInterface;
        parent::__construct($context);
    }

    /**
     * Sends user new password for login and returns response
     *
     * Expects a GET. example for JSON {"username":"user@magento.com"}
     *
     * @return \Magento\Framework\Controller\ResultInterface
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function execute()
    {
        $credentials = null;
        $httpBadRequestCode = 400;

        /** @var \Magento\Framework\Controller\Result\Raw $resultRaw */
        $resultRaw = $this->resultRawFactory->create();
        $responseData = '';
        try {
            $email = $this->getRequest()->getParam('email');
            $this->accountManagementInterface->initiatePasswordReset($email, AccountManagement::EMAIL_RESET);
            /******End**********/
        } catch (\Exception $e) {
            return $resultRaw->setHttpResponseCode($httpBadRequestCode);
        }
        if ($this->getRequest()->getMethod() !== 'POST' || !$this->getRequest()->isXmlHttpRequest()) {
            return $resultRaw->setHttpResponseCode($httpBadRequestCode);
        }

        $response = [
            'errors' => false,
            'message' => __('New Password sent successfully')
        ];
        try {
            $response['data'] = $responseData;
        } catch (InvalidEmailOrPasswordException $e) {
            $response = [
                'errors' => true,
                'message' => $e->getMessage()
            ];
        } catch (LocalizedException $e) {
            $response = [
                'errors' => true,
                'message' => $e->getMessage()
            ];
        } catch (\Exception $e) {
            $response = [
                'errors' => true,
                'message' => __('Invalid Email')
            ];
        }

        /** @var \Magento\Framework\Controller\Result\Json $resultJson */
        $resultJson = $this->resultJsonFactory->create();
        return $resultJson->setData($response);
    }
}
