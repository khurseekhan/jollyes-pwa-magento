<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */

namespace TwoJay\Loyalty\Controller\Login;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Controller\Result\RawFactory;
use Magento\Framework\Exception\InvalidEmailOrPasswordException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Json\Helper\Data as JsonHelper;
use TwoJay\Loyalty\Api\Loyalty\IsLoyaltyMemberInterface;
use TwoJay\Loyalty\Helper\Data as LoyaltyHelper;
use Magento\Customer\Api\CustomerRepositoryInterface;

/**
 * Class Post
 * @package TwoJay\Loyalty\Controller\Login
 */
class Post extends Action
{
    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var \Magento\Framework\Controller\Result\RawFactory
     */
    protected $resultRawFactory;

    /**
     * @var JsonHelper
     */
    private $jsonHelper;

    /**
     * @var CustomSearchInterface
     */
    private $isLoyaltyMemberInterface;

    /**
     * @var LoyaltyHelper
     */
    protected $loyaltyHelper;

    /**
     * @var CustomerRepositoryInterface
     */
    protected $customerRepositoryInterface;

    /**
     * Post constructor.
     * @param Context $context
     * @param JsonFactory $resultJsonFactory
     * @param RawFactory $resultRawFactory
     * @param JsonHelper $jsonHelper
     * @param CustomSearchInterface $customSearchInterface
     * @param \Magento\Framework\UrlInterface $urlInterface
     * @param LoyaltyHelper $loyaltyHelper
     */
    public function __construct(
        Context $context,
        JsonFactory $resultJsonFactory,
        RawFactory $resultRawFactory,
        JsonHelper $jsonHelper,
        IsLoyaltyMemberInterface $isLoyaltyMemberInterface,
        \Magento\Framework\UrlInterface $urlInterface,
        LoyaltyHelper $loyaltyHelper,
        CustomerRepositoryInterface $customerRepositoryInterface
    )
    {
        $this->resultJsonFactory = $resultJsonFactory;
        $this->resultRawFactory = $resultRawFactory;
        $this->jsonHelper = $jsonHelper;
        $this->isLoyaltyMemberInterface = $isLoyaltyMemberInterface;
        $this->urlInterface = $urlInterface;
        $this->loyaltyHelper = $loyaltyHelper;
        $this->customerRepositoryInterface = $customerRepositoryInterface;
        parent::__construct($context);
    }

    /**
     * Check user email in loyalty system and returns response
     *
     * Expects a POST. ex for JSON {"username":"user@magento.com"}
     *
     * @return \Magento\Framework\Controller\ResultInterface
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function execute()
    {
        $httpBadRequestCode = 400;

        /** @var \Magento\Framework\Controller\Result\Raw $resultRaw */
        $resultRaw = $this->resultRawFactory->create();
        $loginFlag = 0;
        $errorMsg = 'Your email address was not found, please register a new account.';
        try {
            $email = $this->getRequest()->getParam('email');
            $customerDetails = $this->loyaltyHelper->getCustomerDetails($email);
            $customerId = isset($customerDetails['entity_id']) ? $customerDetails['entity_id'] : '';
            $loyaltyNumber = null;
            if ($customerId) {
                $customer = $this->customerRepositoryInterface->getById($customerId);
                if (!empty($customer->getCustomAttribute('loyalty_number'))) {
                    $loyaltyNumber = $customer->getCustomAttribute('loyalty_number')->getValue();
                }
            }
            if (isset($loyaltyNumber) && !empty($loyaltyNumber)) {
                $response = [
                    'errors' => false,
                    'customCheck' => false,
                    'message' => __('Loyalty Email Check successful'),
                    'status_code' => ''
                ];
                $loginFlag = 1;
            } else {
                $responseData = $this->isLoyaltyMemberInterface->getLoyaltyNumber($email);
                if (!$this->loyaltyHelper->isJson($responseData) && strpos($responseData, '503') !== false) {
                    $response = [
                        'errors' => true,
                        'customCheck' => false,
                        'status_code' => '503',
                        'message' => __($errorMsg)
                    ];
                    // check if customer exists in Magento then let the customer allow to enter password
                    // in case of 503 service
                    if (isset($customerId) && !empty($customerId)) {
                        $response['customCheck'] = true;
                    }
                    $resultJson = $this->resultJsonFactory->create();
                    return $resultJson->setData($response);
                } elseif ($responseData) {
                    $errorCheck = $this->jsonHelper->jsonDecode($responseData);
                    if (isset($errorCheck['ErrorCode'])) {
                        if (isset($customerId) && !empty($customerId)) {
                            $response = [
                                'errors' => false,
                                'customCheck' => true,
                                'status_code' => ''
                            ];
                        } else {
                            $response = [
                                'errors' => true,
                                'customCheck' => false,
                                'message' => $errorMsg,
                                'status_code' => ''
                            ];
                        }
                    } else {
                        $response = [
                            'errors' => false,
                            'customCheck' => false,
                            'message' => __('Loyalty Email Check successful'),
                            'status_code' => ''
                        ];
                        if (isset($errorCheck['LoyaltyNumber'])) {
                            $loginFlag = 1;
                        } else {
                            $loginFlag = 2;
                        }
                    }
                } else {
                    $response = [
                        'errors' => true,
                        'customCheck' => false,
                        'status_code' => '',
                        'message' => __('Loyalty response is empty.')
                    ];
                }
            }
        } catch (\Exception $e) {
            return $resultRaw->setHttpResponseCode($httpBadRequestCode);
        }

        if ($this->getRequest()->getMethod() !== 'POST' || !$this->getRequest()->isXmlHttpRequest()) {
            return $resultRaw->setHttpResponseCode($httpBadRequestCode);
        }

        try {
            $response['is_active'] = $loginFlag;
        } catch (InvalidEmailOrPasswordException $e) {
            $response = [
                'errors' => true,
                'customCheck' => false,
                'message' => $e->getMessage(),
                'status_code' => ''
            ];
        } catch (LocalizedException $e) {
            $response = [
                'errors' => true,
                'customCheck' => false,
                'message' => $e->getMessage(),
                'status_code' => ''
            ];
        } catch (\Exception $e) {
            $response = [
                'errors' => true,
                'customCheck' => false,
                'message' => __('Invalid Email'),
                'status_code' => ''
            ];
        }

        /** @var \Magento\Framework\Controller\Result\Json $resultJson */
        $resultJson = $this->resultJsonFactory->create();
        return $resultJson->setData($response);
    }
}