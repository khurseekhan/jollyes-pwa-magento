<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */

namespace TwoJay\Loyalty\Controller\Customer;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Controller\Result\RawFactory;
use Magento\Framework\Json\Helper\Data as JsonHelper;
use TwoJay\Loyalty\Api\Loyalty\CustomSearchInterface;
use TwoJay\Loyalty\Helper\Data as LoyaltyHelper;

/**
 * Class Post
 * @package TwoJay\Loyalty\Controller\Login
 */
class Validate extends Action
{
    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var \Magento\Framework\Controller\Result\RawFactory
     */
    protected $resultRawFactory;

    /**
     * @var JsonHelper
     */
    private $jsonHelper;

    /**
     * @var CustomSearchInterface
     */
    private $customSearchInterface;

    /**
     * Validate constructor.
     * @param Context $context
     * @param JsonFactory $resultJsonFactory
     * @param RawFactory $resultRawFactory
     * @param JsonHelper $jsonHelper
     * @param LoyaltyHelper $loyaltyHelper
     * @param CustomSearchInterface $customSearchInterface
     */
    public function __construct(
        Context $context,
        JsonFactory $resultJsonFactory,
        RawFactory $resultRawFactory,
        JsonHelper $jsonHelper,
        LoyaltyHelper $loyaltyHelper,
        CustomSearchInterface $customSearchInterface
    )
    {
        $this->resultJsonFactory = $resultJsonFactory;
        $this->resultRawFactory = $resultRawFactory;
        $this->jsonHelper = $jsonHelper;
        $this->loyaltyHelper = $loyaltyHelper;
        $this->customSearchInterface = $customSearchInterface;
        parent::__construct($context);
    }

    /**
     * Check telephone in loyalty system
     *
     * @return $this
     */
    public function execute()
    {
        $httpBadRequestCode = 400;
        /** @var \Magento\Framework\Controller\Result\Raw $resultRaw */
        $resultRaw = $this->resultRawFactory->create();

        if ($this->getRequest()->getMethod() !== 'POST' || !$this->getRequest()->isXmlHttpRequest()) {
            return $resultRaw->setHttpResponseCode($httpBadRequestCode);
        }

        $responseData = '';
        try {
            $telephone = $this->getRequest()->getParam('telephone');
            // check whether the telephone number exists in loyalty system
            $responseData = $this->customSearchInterface->customSearch($telephone);
            if ($responseData) {
                $errorCheck = $this->jsonHelper->jsonDecode($responseData);
                if (isset($errorCheck['ErrorCode'])) {
                    $errorMsg = 'Telephone is not available in loyalty system';
                    $errorMsg = isset($errorCheck->ErrorMessage) ? $errorCheck->ErrorMessage : $errorMsg;
                    $response = [
                        'errors' => false,
                        'message' => __($errorMsg)
                    ];
                } else {
                    $response = [
                        'errors' => true,
                        'message' => __('Telephone already exists in loyalty system.')
                    ];
                }
            }
        } catch (\Exception $e) {
            return $resultRaw->setHttpResponseCode($httpBadRequestCode);
        }

        try {
            $response['data'] = $responseData;
        } catch (\Exception $e) {
            $response = [
                'errors' => true,
                'message' => __($e->getMessage())
            ];
        }

        /** @var \Magento\Framework\Controller\Result\Json $resultJson */
        $resultJson = $this->resultJsonFactory->create();
        return $resultJson->setData($response);
    }
}