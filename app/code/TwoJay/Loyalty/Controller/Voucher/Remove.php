<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */

namespace TwoJay\Loyalty\Controller\Voucher;

use Magento\Checkout\Model\Cart as CustomerCart;
use TwoJay\Loyalty\Helper\Data as LoyaltyHelper;

class Remove extends \Magento\Checkout\Controller\Cart
{

    /**
     * @var \TwoJay\Loyalty\Model\LoyaltyCouponsFactory
     */
    protected $_loyaltyCouponsFactory;

    /**
     * @var
     */
    protected $_loyaltyHelper;

    /**
     * Remove constructor.
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Data\Form\FormKey\Validator $formKeyValidator
     * @param CustomerCart $cart
     * @param \TwoJay\Loyalty\Model\LoyaltyCouponsFactory $loyaltyCouponsFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Data\Form\FormKey\Validator $formKeyValidator,
        CustomerCart $cart,
        \TwoJay\Loyalty\Model\LoyaltyCouponsFactory $loyaltyCouponsFactory,
        LoyaltyHelper $loyaltyHelper
    )
    {
        $this->_formKeyValidator = $formKeyValidator;
        $this->_scopeConfig = $scopeConfig;
        $this->_checkoutSession = $checkoutSession;
        $this->_storeManager = $storeManager;
        $this->cart = $cart;
        $this->_loyaltyCouponsFactory = $loyaltyCouponsFactory;
        $this->_loyaltyHelper = $loyaltyHelper;
        parent::__construct($context, $scopeConfig, $checkoutSession, $storeManager, $formKeyValidator, $cart);
    }

    /**
     * @return \Magento\Framework\Controller\Result\Redirect|void
     */
    public function execute()
    {
        if ($this->_loyaltyHelper->isModuleEnabled()) {
            $code = $this->getRequest()->getParam('code');

            if ($code) {
                try {
                    $loyaltyCoupons = $this->_loyaltyCouponsFactory->create();
                    $loyaltyCoupons->removeFromCart(trim($code));
                    $this->messageManager->addSuccess(
                        __(
                            'PetCLUB Voucher "%1" was removed.',
                            $this->_objectManager->get(\Magento\Framework\Escaper::class)->escapeHtml($code)
                        )
                    );
                } catch (\Magento\Framework\Exception\LocalizedException $e) {
                    $this->messageManager->addError($e->getMessage());
                } catch (\Exception $e) {
                    $this->messageManager->addException($e, __('You can\'t remove this PetCLUB Voucher.'));
                }

            }
        }
        return $this->_goBack();
    }
}
