<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */

namespace TwoJay\Loyalty\Controller\Voucher;

use Magento\Checkout\Model\Cart as CustomerCart;
use TwoJay\Loyalty\Helper\Data as LoyaltyHelper;

/**
 * Class Add
 * @package TwoJay\Loyalty\Controller\Voucher
 */
class Add extends \Magento\Checkout\Controller\Cart
{

    /**
     * @var \TwoJay\Loyalty\Model\LoyaltyCouponsFactory
     */
    protected $_loyaltyCoupons;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;

    /**
     * @var \Magento\Framework\Json\Helper\Data
     */
    protected $_jsonHelper;

    /**
     * @var
     */
    protected $_loyaltyHelper;


    /**
     * Add constructor.
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Data\Form\FormKey\Validator $formKeyValidator
     * @param CustomerCart $cart
     * @param \TwoJay\Loyalty\Model\LoyaltyCouponsFactory $loyaltyCouponsFactory
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Data\Form\FormKey\Validator $formKeyValidator,
        CustomerCart $cart,
        \TwoJay\Loyalty\Model\LoyaltyCouponsFactory $loyaltyCouponsFactory,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        LoyaltyHelper $loyaltyHelper
    )
    {
        $this->_formKeyValidator = $formKeyValidator;
        $this->_scopeConfig = $scopeConfig;
        $this->_checkoutSession = $checkoutSession;
        $this->_storeManager = $storeManager;
        $this->cart = $cart;
        $this->_loyaltyCoupons = $loyaltyCouponsFactory;
        $this->_jsonHelper = $jsonHelper;
        $this->_loyaltyHelper = $loyaltyHelper;
        parent::__construct($context, $scopeConfig, $checkoutSession, $storeManager, $formKeyValidator, $cart);
    }

    /**
     * Add Loyalty Coupon to current quote
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        if ($this->_loyaltyHelper->isModuleEnabled()) {
            $data = $this->getRequest()->getPostValue();
            if ($this->_checkoutSession->getQuote()->getLoyaltyCoupons()) {
                $quoteCards = $this->_jsonHelper->jsonDecode($this->_checkoutSession->getQuote()->getLoyaltyCoupons());
                $countLoyaltyCoupons = count($quoteCards);
                if ($countLoyaltyCoupons == 5) {
                    $this->messageManager->addWarning('You can not add more than 5 PetCLUB Voucher');
                    return $this->_goBack();
                }
            }

            if (isset($data['loyaltycoupon_code'])) {
                $code = trim($data['loyaltycoupon_code']);

                try {
                    if (strlen($code) > \TwoJay\Loyalty\Helper\Data::LOYALTY_COUPON_CODE_MAX_LENGTH) {
                        throw new \Magento\Framework\Exception\LocalizedException(__('Please correct the PetCLUB Voucher.'));
                    }

                    $loyaltyCoupons = $this->_loyaltyCoupons->create();
                    $loyaltyCoupons->addToCart($code);
                    $this->messageManager->addSuccess(
                        __(
                            'PetCLUB Voucher "%1" was added.',
                            $this->_objectManager->get(\Magento\Framework\Escaper::class)->escapeHtml($code)
                        )
                    );
                } catch (\Magento\Framework\Exception\LocalizedException $e) {
                    $this->messageManager->addError($e->getMessage());
                } catch (\Exception $e) {
                    $this->messageManager->addException($e->getMessage());
                }
            }
        }

        return $this->_goBack();
    }
}
