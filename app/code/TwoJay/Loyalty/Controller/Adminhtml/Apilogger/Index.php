<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 Two Jay Limited (http://www.twojay.co/)
 *
 */
namespace TwoJay\Loyalty\Controller\Adminhtml\Apilogger;

class Index extends \Magento\Backend\App\Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'TwoJay_Loyalty::loyalty_api_logger';

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    private $resultPageFactory;

    /**
     * Index constructor.
     *
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {

        //Call page factory to render layout and page content
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        //Set the menu which will be active for this page
        $resultPage->setActiveMenu('TwoJay_Loyalty::loyalty_api_logger');
        //Set the header title of grid
        $resultPage->getConfig()->getTitle()->prepend(__('Loyalty API Logger'));

        $resultPage->addBreadcrumb(__('Report'), __('Report'));
        $resultPage->addBreadcrumb(__('Loyalty API Logger'), __('Loyalty API Logger'));

        return $resultPage;
    }
}
