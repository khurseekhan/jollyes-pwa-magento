<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 Two Jay Limited (http://www.twojay.co/)
 *
 */
namespace TwoJay\Loyalty\Controller\Adminhtml\Coupon;

class Index extends \TwoJay\Loyalty\Controller\Adminhtml\Loyalty
{
    /**
     * Orders grid
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        $resultPage = $this->_initAction();
        $resultPage->getConfig()->getTitle()->prepend(__('Orders PetCLUB Voucher'));
        return $resultPage;
    }
}
