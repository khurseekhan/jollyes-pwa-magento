<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */

namespace TwoJay\Loyalty\Block\Voucher;

use Magento\Framework\Json\Helper\Data as JsonHelper;
use TwoJay\Loyalty\Helper\Data as LoyaltyHelper;
use TwoJay\Loyalty\Api\Loyalty\GetCustomerVouchersInterface;
use TwoJay\Loyalty\Api\Loyalty\CustomSearchInterface;
use Magento\Framework\View\Element\Template\Context as TemplateContext;
use Magento\Framework\DataObjectFactory;
use Magento\Framework\Data\CollectionFactory;
use Magento\Framework\App\Request\Http;
use TwoJay\Loyalty\Api\Loyalty\IsLoyaltyMemberInterface;
use TwoJay\Loyalty\Api\Loyalty\TokenInterface;

/**
 * Class History
 * @package TwoJay\Loyalty\Block\Voucher
 */
class History extends \Magento\Framework\View\Element\Template
{

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * @var GetCustomerVouchersInterface
     */
    protected $_vouchers;

    /**
     * @var CustomSearchInterface
     */
    protected $_customSearch;

    /**
     * @var JsonHelper
     */
    protected $_jsonHelper;

    /**
     * @var LoyaltyHelper
     */
    protected $_loyaltyHelper;

    /**
     * @var DataObjectFactory
     */
    private $_objectFactory;

    /**
     * @var null
     */
    private $_voucherCollection = null;

    /**
     * @var null
     */
    private $_customerSearch = null;

    /**
     * @var null
     */
    private $_customerVouchers = null;

    /**
     * @var \Magento\Framework\Data\CollectionFactory
     */
    protected $_dataCollectionFactory;

    /**
     * Sort direction cookie name
     */
    const DIRECTION_PARAM_NAME = 'voucher_list_dir';

    /**
     * Default Sort Order
     */
    const DEFAULT_SORT_DIRECTION = 'desc';

    /**
     * Default direction
     *
     * @var string
     */
    protected $_direction = self::DEFAULT_SORT_DIRECTION;

    /**
     * @var bool $_paramsMemorizeAllowed
     */
    protected $_paramsMemorizeAllowed = true;

    /**
     * Request
     *
     * @var \Magento\Framework\App\Request\Http
     */
    protected $request;

    /**
     * @var IsLoyaltyMemberInterface
     */
    protected $isLoyaltyMemberInterface;

    /**
     * @var TokenInterface
     */
    protected $tokenInterface;

    /**
     * History constructor.
     * @param TemplateContext $context
     * @param \Magento\Customer\Model\Session $customerSession
     * @param GetCustomerVouchersInterface $vouchers
     * @param CustomSearchInterface $customSearch
     * @param JsonHelper $jsonHelper
     * @param LoyaltyHelper $loyaltyHelper
     * @param DataObjectFactory $objectFactory
     * @param CollectionFactory $dataCollectionFactory
     * @param Http $request
     * @param IsLoyaltyMemberInterface $isLoyaltyMemberInterface
     * @param TokenInterface $tokenInterface
     * @param array $data
     */
    public function __construct(
        TemplateContext $context,
        \Magento\Customer\Model\Session $customerSession,
        GetCustomerVouchersInterface $vouchers,
        CustomSearchInterface $customSearch,
        JsonHelper $jsonHelper,
        LoyaltyHelper $loyaltyHelper,
        DataObjectFactory $objectFactory,
        CollectionFactory $dataCollectionFactory,
        Http $request,
        IsLoyaltyMemberInterface $isLoyaltyMemberInterface,
        TokenInterface $tokenInterface,
        array $data = []
    )
    {
        $this->_isScopePrivate = true;
        $this->_customerSession = $customerSession;
        $this->_vouchers = $vouchers;
        $this->_customSearch = $customSearch;
        $this->_jsonHelper = $jsonHelper;
        $this->_loyaltyHelper = $loyaltyHelper;
        $this->_objectFactory = $objectFactory;
        $this->_dataCollectionFactory = $dataCollectionFactory;
        $this->request = $request;
        $this->isLoyaltyMemberInterface = $isLoyaltyMemberInterface;
        $this->tokenInterface = $tokenInterface;
        parent::__construct($context, $data);
    }

    /**
     * Get voucher collection
     *
     * @return null
     */
    public function getVoucherCollection()
    {
        if (!$this->_voucherCollection) {
            $sortOrder = ($this->getDirection()) ? strtolower($this->getDirection()) : 'desc';
            $voucherList = $this->getCustomerVouchers();
            if ($voucherList) {
                $voucherList = $this->sortPoints($voucherList, $sortOrder);
                $collection = null;
                if (count($voucherList) > 0) {
                    $this->_voucherCollection = $this->_dataCollectionFactory->create();
                    foreach ($voucherList as $voucher) {
                        $dataObject = $this->_objectFactory->create();
                        $dataObject->setData($voucher);
                        $this->_voucherCollection->addItem($dataObject);
                    }
                }
            }
        }

        return $this->_voucherCollection;
    }

    /**
     * Sort the collection based on sort order
     *
     * @param $transactionData
     * @param string $sortOrder
     * @return mixed
     */
    protected function sortPoints(& $transactionData, $sortOrder = "desc")
    {
        if ($sortOrder == "desc") {

            usort($transactionData, function ($row1, $row2) {
                if (isset($row1['UpdatedOn']) && isset($row2['UpdatedOn']) &&
                    (strtotime($row1['UpdatedOn']) < strtotime($row2['UpdatedOn']))
                )
                    return 1;
                else if (isset($row1['UpdatedOn']) && isset($row2['UpdatedOn']) &&
                    (strtotime($row1['UpdatedOn']) > strtotime($row2['UpdatedOn']))
                )
                    return -1;
                else
                    return 0;
            });
        } else {

            usort($transactionData, function ($row1, $row2) {
                if (isset($row1['UpdatedOn']) && isset($row2['UpdatedOn']) &&
                    (strtotime($row1['UpdatedOn']) < strtotime($row2['UpdatedOn']))
                )
                    return -1;
                else if (isset($row1['UpdatedOn']) && isset($row2['UpdatedOn']) &&
                    (strtotime($row1['UpdatedOn']) > strtotime($row2['UpdatedOn']))
                )
                    return 1;
                else
                    return 0;
            });
        }
        return $transactionData;
    }


    /**
     * Get Voucher history
     *
     * @return array
     */
    public function getCustomerVouchers()
    {
        $email = $this->_customerSession->getCustomer()->getEmail();

        // check for 503 service unavailable
        $tokenData = $this->tokenInterface->getToken();
        if (!$this->_loyaltyHelper->isJson($tokenData) && strpos($tokenData, '503') !== false) {
            return null;
        }

        if (isset($email)) {
            $loyaltyNumber = $this->_loyaltyHelper->getLoyaltyNumber($email);
            if (!isset($loyaltyNumber) || empty($loyaltyNumber)) {
                $responseData = $this->isLoyaltyMemberInterface->getLoyaltyNumber($email);
                if (!$this->_loyaltyHelper->isJson($responseData) && strpos($responseData, '503') !== false) {
                    return null;
                }
                if ($responseData) {
                    $responseData = $this->_jsonHelper->jsonDecode($responseData);
                    $loyaltyNumber = isset($responseData['LoyaltyNumber']) ? $responseData['LoyaltyNumber'] : '';
                }
            }
            if ($loyaltyNumber) {
                $vouchersData = $this->getApiCustomVoucher($loyaltyNumber);
                if ($vouchersData) {
                    $vouchersData = $this->_jsonHelper->jsonDecode($vouchersData);
                    if (isset($vouchersData['ErrorCode'])) {
                        if (in_array($vouchersData['ErrorCode'], [404])) {
                            return null;
                        }
                    }
                }

                return $vouchersData;
            }
        }
        return null;
    }

    /**
     * Get Customer Lookup Data
     * @return array
     */
    public function getCustomerSchemeLookUp()
    {
        $email = $this->_customerSession->getCustomer()->getEmail();
        $customerLookup = $this->getCustomSearch($email);

        if (!$this->_loyaltyHelper->isJson($customerLookup) && strpos($customerLookup, '503') !== false) {
            return false;
        }
        $result = [];
        if ($customerLookup) {
            $customerLookupData = $this->_jsonHelper->jsonDecode($customerLookup);

            if (isset($customerLookupData['ErrorCode'])) {
                if (in_array($customerLookupData['ErrorCode'], [404])) {
                    return 404;
                }
            }

            $rewardTrigger = isset($customerLookupData[0]['CustomerSchemeLookUP']['RewardTrigger'])
                ? $customerLookupData[0]['CustomerSchemeLookUP']['RewardTrigger'] : '';
            $totalPoints = isset($customerLookupData[0]['TotalPoints']) ?
                $customerLookupData[0]['TotalPoints'] : '';
            $finalPoints = $rewardTrigger - $totalPoints;

            $totalRemainingMonth = isset($customerLookupData[0]['CustomerSchemeLookUP']['ExpireVoucherInDays'])
                ? $customerLookupData[0]['CustomerSchemeLookUP']['ExpireVoucherInDays'] : '';
            $totalRemainingMonth = floor($totalRemainingMonth * 0.0328767);
            $result = ['months' => $totalRemainingMonth, 'total_amount' => $finalPoints];
        }

        return $result;
    }

    /**
     * Memorize parameter value for session
     *
     * @param string $param parameter name
     * @param mixed $value parameter value
     * @return $this
     */
    protected function _memorizeParam($param, $value)
    {
        if ($this->_paramsMemorizeAllowed && !$this->_customerSession->getParamsMemorizeDisabled()) {
            $this->_customerSession->setData($param, $value);
        }
        return $this;
    }

    /**
     * Get sort direction
     *
     * @return string|bool
     */
    public function getCurrentDirection()
    {
        $directions = ['asc', 'desc'];
        $dir = strtolower($this->getDirection());
        if (!$dir || !in_array($dir, $directions)) {
            $dir = $this->_direction;
        }

        if ($dir != $this->_direction) {
            $this->_memorizeParam('sort_direction', $dir);
        }

        $this->setData('_current_grid_direction', $dir);

        return $dir;

    }

    /**
     * Get sort direction
     *
     * @return string|bool
     */
    public function getDirection()
    {
        return $this->request->getParam(self::DIRECTION_PARAM_NAME);
    }

    /**
     * @param array $customOptions
     * @return string
     */
    public function getWidgetOptionsJson(array $customOptions = [])
    {
        $options = [

            'direction' => self::DIRECTION_PARAM_NAME,
            'directionDefault' => self::DEFAULT_SORT_DIRECTION,
            'url' => $this->getPagerUrl(),
        ];
        $options = array_replace_recursive($options, $customOptions);

        return json_encode(['loyaltyListToolbarForm' => $options]);
    }

    /**
     * Return current URL with rewrites and additional parameters
     *
     * @param array $params Query parameters
     * @return string
     */
    public function getPagerUrl($params = [])
    {
        $urlParams = [];
        $urlParams['_current'] = true;
        $urlParams['_escape'] = false;
        $urlParams['_use_rewrite'] = true;
        $urlParams['_query'] = $params;
        return $this->getUrl('*/*/*', $urlParams);
    }

    /**
     * Generate url by route and parameters
     *
     * @param   string $route
     * @param   array $params
     * @return  string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->_urlBuilder->getUrl($route, $params);
    }

    /**
     * @param $loyaltyNumber
     * @return null|\TwoJay\Loyalty\Api\Loyalty\vouchers
     */
    protected function getApiCustomVoucher($loyaltyNumber)
    {
        if (!$this->_customerVouchers) {
            $this->_customerVouchers = $this->_vouchers->getCustomerVouchers($loyaltyNumber);
        }
        return $this->_customerVouchers;
    }

    /**
     * @param $email
     * @return null|string
     */
    protected function getCustomSearch($email)
    {
        if (!$this->_customerSearch) {
            $this->_customerSearch = $this->_customSearch->customSearch($email);
        }
        return $this->_customerSearch;
    }

    /**
     * @return $this
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if ($this->getVoucherCollection()) {
            $pager = $this->getLayout()->createBlock(
                \Magento\Theme\Block\Html\Pager::class,
                'loyalty.coupon.history.pager'
            );
            $pager->setCollection($this->getVoucherCollection());
            $this->setChild('pager', $pager);
            $this->getVoucherCollection()->load();
        }
        return $this;
    }

    /**
     * @return string
     */
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }

    /**
     * @return string
     */
    public function getBackUrl()
    {
        return $this->getUrl('customer/account/');
    }

}
