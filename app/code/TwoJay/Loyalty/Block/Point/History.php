<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */

namespace TwoJay\Loyalty\Block\Point;

use Magento\Framework\View\Element\Template\Context as TemplateContext;
use TwoJay\Loyalty\Api\Loyalty\GetCustomerTransactionsInterface;
use Magento\Customer\Model\Session;
use Magento\Framework\Json\Helper\Data as JsonHelper;
use TwoJay\Loyalty\Api\Loyalty\CustomSearchInterface;
use Magento\Framework\DataObjectFactory;
use Magento\Framework\Data\CollectionFactory;
use TwoJay\Loyalty\Helper\Data as LoyaltyHelper;
use Magento\Framework\App\Request\Http;
use TwoJay\Loyalty\Api\Loyalty\IsLoyaltyMemberInterface;
use TwoJay\Loyalty\Api\Loyalty\TokenInterface;

/**
 * Class History
 * @package TwoJay\Loyalty\Block\Point
 */
class History extends \Magento\Framework\View\Element\Template
{
    /**
     * @var GetCustomerTransactionsInterface
     */
    protected $_customerTransactions;

    /**
     * @var Session
     */
    protected $_customerSession;

    /**
     * @var JsonHelper
     */
    protected $jsonHelper;

    /**
     * @var CustomSearchInterface
     */
    protected $_customSearch;

    /**
     * @var DataObjectFactory
     */
    private $_objectFactory;

    /**
     * @var \Magento\Framework\Data\CollectionFactory
     */
    protected $_dataCollectionFactory;

    /**
     * @var null
     */
    protected $_transactionCollection = null;

    /**
     * @var null
     */
    protected $_customerSearch = null;

    /**
     * @var LoyaltyHelper
     */
    protected $_loyaltyHelper;

    /**
     * Sort direction cookie name
     */
    const DIRECTION_PARAM_NAME = 'point_list_dir';

    /**
     *
     */
    const DEFAULT_SORT_DIRECTION = 'desc';

    /**
     * Default direction
     *
     * @var string
     */
    protected $_direction = self::DEFAULT_SORT_DIRECTION;

    /**
     * @var bool $_paramsMemorizeAllowed
     */
    protected $_paramsMemorizeAllowed = true;

    /**
     * Request
     *
     * @var \Magento\Framework\App\Request\Http
     */
    protected $request;

    /**
     * @var IsLoyaltyMemberInterface
     */
    protected $isLoyaltyMemberInterface;

    /**
     * @var TokenInterface
     */
    protected $tokenInterface;

    /**
     * History constructor.
     * @param TemplateContext $context
     * @param GetCustomerTransactionsInterface $customerTransactions
     * @param Session $customerSession
     * @param JsonHelper $jsonHelper
     * @param LoyaltyHelper $loyaltyHelper
     * @param CustomSearchInterface $customSearch
     * @param DataObjectFactory $objectFactory
     * @param CollectionFactory $dataCollectionFactory
     * @param Http $request
     * @param IsLoyaltyMemberInterface $isLoyaltyMemberInterface
     * @param TokenInterface $tokenInterface
     * @param array $data
     */
    public function __construct(
        TemplateContext $context,
        GetCustomerTransactionsInterface $customerTransactions,
        Session $customerSession,
        JsonHelper $jsonHelper,
        LoyaltyHelper $loyaltyHelper,
        CustomSearchInterface $customSearch,
        DataObjectFactory $objectFactory,
        CollectionFactory $dataCollectionFactory,
        Http $request,
        IsLoyaltyMemberInterface $isLoyaltyMemberInterface,
        TokenInterface $tokenInterface,
        array $data = []
    )
    {
        parent::__construct($context, $data);
        $this->_isScopePrivate = true;
        $this->_customerTransactions = $customerTransactions;
        $this->_customerSession = $customerSession;
        $this->jsonHelper = $jsonHelper;
        $this->_loyaltyHelper = $loyaltyHelper;
        $this->_customSearch = $customSearch;
        $this->_objectFactory = $objectFactory;
        $this->_dataCollectionFactory = $dataCollectionFactory;
        $this->request = $request;
        $this->isLoyaltyMemberInterface = $isLoyaltyMemberInterface;
        $this->tokenInterface = $tokenInterface;
    }


    /**
     * Get Point collection
     *
     * @return null
     */
    public function getLoyaltyCollection()
    {
        if (!$this->_loyaltyHelper->isModuleEnabled()) {
            return $this->_transactionCollection;
        }

        if (!$this->_transactionCollection) {
            $sortOrder = ($this->getDirection()) ? strtolower($this->getDirection()) : 'desc';
            $pointTransaction = $this->getPointTransactions();

            if ($pointTransaction) {
                $pointTransaction = $this->sortPoints($pointTransaction, $sortOrder);
                $collection = null;
                if (count($pointTransaction) > 0) {
                    $collection = $this->_dataCollectionFactory->create();
                    foreach ($pointTransaction as $point) {
                        $dataObject = $this->_objectFactory->create();
                        $dataObject->setData($point);
                        $collection->addItem($dataObject);
                    }
                }
                $this->_transactionCollection = $collection;
            }
        }

        return $this->_transactionCollection;
    }


    /**
     * Sort the collection based on sort order
     *
     * @param $transactionData
     * @param string $sortOrder
     * @return mixed
     */
    protected function sortPoints(& $transactionData, $sortOrder = "desc")
    {
        if ($sortOrder == "desc") {

            usort($transactionData, function ($row1, $row2) {
                if (isset($row1['TransactionDate']) && isset($row2['TransactionDate'])
                    && (strtotime($row1['TransactionDate']) < strtotime($row2['TransactionDate'])))
                    return 1;
                else if (isset($row1['TransactionDate']) && isset($row2['TransactionDate']) &&
                    (strtotime($row1['TransactionDate']) > strtotime($row2['TransactionDate'])))
                    return -1;
                else
                    return 0;
            });
        } else {

            usort($transactionData, function ($row1, $row2) {
                if (isset($row1['TransactionDate']) && isset($row2['TransactionDate']) &&
                    (strtotime($row1['TransactionDate']) < strtotime($row2['TransactionDate'])))
                    return -1;
                else if (isset($row1['TransactionDate']) && isset($row2['TransactionDate'])
                    && (strtotime($row1['TransactionDate']) > strtotime($row2['TransactionDate'])))
                    return 1;
                else
                    return 0;
            });

        }

        return $transactionData;
    }

    /**
     * Get Point Transaction Data
     *
     * @param string $loyaltyNumber
     * @return mixed|\TwoJay\Loyalty\Api\Loyalty\transactions
     */
    public function getPointTransactions($loyaltyNumber = '')
    {
        if (!$this->_loyaltyHelper->isModuleEnabled()) {
            return null;
        }

        // check for 503 service unavailable
        $tokenData = $this->tokenInterface->getToken();
        if (!$this->_loyaltyHelper->isJson($tokenData) && strpos($tokenData, '503') !== false) {
            return null;
        }

        $email = $this->_customerSession->getCustomer()->getEmail();
        if ($email) {
            $loyaltyNumber = $this->_loyaltyHelper->getLoyaltyNumber($email);
            if (!isset($loyaltyNumber) || empty($loyaltyNumber)) {
                $responseData = $this->isLoyaltyMemberInterface->getLoyaltyNumber($email);
                if (!$this->_loyaltyHelper->isJson($responseData) && strpos($responseData, '503') !== false) {
                    return null;
                }

                if ($responseData) {
                    $responseData = $this->jsonHelper->jsonDecode($responseData);
                    $loyaltyNumber = isset($responseData['LoyaltyNumber']) ? $responseData['LoyaltyNumber'] : '';
                }
            }

            if ($loyaltyNumber) {
                $customerTransactions = $this->_customerTransactions->getCustomerTransactions($loyaltyNumber);
                if ($customerTransactions) {
                    $customerTransactions = $this->jsonHelper->jsonDecode($customerTransactions);
                }

                return $customerTransactions;
            }
        }

        return null;
    }

    /**
     * Memorize parameter value for session
     *
     * @param string $param parameter name
     * @param mixed $value parameter value
     * @return $this
     */
    protected function _memorizeParam($param, $value)
    {
        if ($this->_paramsMemorizeAllowed && !$this->_customerSession->getParamsMemorizeDisabled()) {
            $this->_customerSession->setData($param, $value);
        }
        return $this;
    }

    /**
     * Get sort direction
     *
     * @return string|bool
     */
    public function getCurrentDirection()
    {
        $directions = ['asc', 'desc'];
        $dir = strtolower($this->getDirection());
        if (!$dir || !in_array($dir, $directions)) {
            $dir = $this->_direction;
        }

        if ($dir != $this->_direction) {
            $this->_memorizeParam('sort_direction', $dir);
        }

        $this->setData('_current_grid_direction', $dir);

        return $dir;

    }

    /**
     * Get sort direction
     *
     * @return string|bool
     */
    public function getDirection()
    {
        return $this->request->getParam(self::DIRECTION_PARAM_NAME);
    }

    /**
     * @param array $customOptions
     * @return string
     */
    public function getWidgetOptionsJson(array $customOptions = [])
    {
        $options = [
            'direction' => self::DIRECTION_PARAM_NAME,
            'directionDefault' => self::DEFAULT_SORT_DIRECTION,
            'url' => $this->getPagerUrl(),
        ];
        $options = array_replace_recursive($options, $customOptions);

        return json_encode(['loyaltyListToolbarForm' => $options]);
    }

    /**
     * Return current URL with rewrites and additional parameters
     *
     * @param array $params Query parameters
     * @return string
     */
    public function getPagerUrl($params = [])
    {
        $urlParams = [];
        $urlParams['_current'] = true;
        $urlParams['_escape'] = false;
        $urlParams['_use_rewrite'] = true;
        $urlParams['_query'] = $params;
        return $this->getUrl('*/*/*', $urlParams);
    }

    /**
     * Generate url by route and parameters
     *
     * @param   string $route
     * @param   array $params
     * @return  string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->_urlBuilder->getUrl($route, $params);
    }

    /**
     * @return mixed
     */
    public function getLoyaltyNumber()
    {
        return $this->_customerSession->getCustomer()->getData('loyalty_number');
    }

    /**
     * Get Customer Lookup Data
     * @return array
     */
    public function getCustomerLookUpData()
    {
        if (!$this->_loyaltyHelper->isModuleEnabled()) {
            return false;
        }

        $email = $this->_customerSession->getCustomer()->getEmail();
        $customerSearch = $this->getCustomSearch($email);
        if (!$this->_loyaltyHelper->isJson($customerSearch) && strpos($customerSearch, '503') !== false) {
            return false;
        }

        if ($customerSearch) {
            $customerSearch = $this->jsonHelper->jsonDecode($customerSearch);

            if (isset($customerSearch['ErrorCode'])) {
                if (in_array($customerSearch['ErrorCode'], [404])) {
                    return 404;
                }
            }

            $expirePointsInDays = isset($customerSearch[0]['CustomerSchemeLookUP']['ExpirePointsInDays']) ?
                $customerSearch[0]['CustomerSchemeLookUP']['ExpirePointsInDays'] : 0;
            $totalPoints = isset($customerSearch[0]['TotalPoints']) ? $customerSearch[0]['TotalPoints'] : 0;
            $rewardTrigger = isset($customerSearch[0]['CustomerSchemeLookUP']['RewardTrigger']) ?
                $customerSearch[0]['CustomerSchemeLookUP']['RewardTrigger'] : '';
            $finalPoints = $rewardTrigger - $totalPoints;
            $totalRemainingMonth = isset($customerSearch[0]['CustomerSchemeLookUP']['ExpireVoucherInDays']) ?
                $customerSearch[0]['CustomerSchemeLookUP']['ExpireVoucherInDays'] : 0;
            $totalRemainingMonth = floor($totalRemainingMonth * 0.0328767);
            $result = ['expirePointsInDays' => $expirePointsInDays, 'total_points' => $totalPoints, 'months' => $totalRemainingMonth, 'total_amount' => $finalPoints];

            return $result;
        }
    }

    protected function getCustomSearch($email)
    {
        if (!$this->_customerSearch) {
            $this->_customerSearch = $this->_customSearch->customSearch($email);
        }
        return $this->_customerSearch;
    }

    /**
     * @return $this
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if ($collection = $this->getLoyaltyCollection()) {
            $pager = $this->getLayout()->createBlock(
                \Magento\Theme\Block\Html\Pager::class,
                'loyalty.coupon.history.pager'
            );
            $pager->setCollection($collection);
            $this->setChild('pager', $pager);
        }
        return $this;
    }

    /**
     * @return string
     */
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }

}
