<?php

namespace TwoJay\Loyalty\Block\Customer\Attribute;

use Magento\Framework\View\Element\Template\Context as TemplateContext;

/**
 * Class AnimalOwn
 * @package TwoJay\Loyalty\Block\Customer\Attribute
 */
class AnimalOwn extends \Magento\Framework\View\Element\Template
{
     /**
     * @var GetCustomerSession
     */
    protected $customerSession;

    /**
     * @var GetCustomerAttribute
     */
    public $eavConfig;


    public function __construct(
        TemplateContext $context,
        \Magento\Eav\Model\Config $eavConfig,
        \Magento\Customer\Model\Session $customerSession,
        array $data = []
    ) {
        $this->eavConfig = $eavConfig;
        $this->customerSession = $customerSession;
        parent::__construct($context, $data);
    }

    /**
     * Entity Type customer
     * Attribute code animals_own
     * @return array
     */
    public function getAttribute()
    {   
        $attribute = $this->eavConfig->getAttribute('customer', 'animals_own');       
        
        return $attribute;
    }

    public function getAttributeOptions()
    {   
        $attribute = $this->getAttribute();
        $attributeOptions = $attribute->getSource()->getAllOptions();

        //$attributeOptions = $this->customerSession->getCustomer()->getId();

        $customer = $this->customerSession->getCustomer();

        if($customer){
            $animaSelected = $customer->getAnimalsOwn();
            $animaSelected = explode(",",$animaSelected);

            foreach($attributeOptions as $k=>$v){

                if(in_array($v['value'],$animaSelected)){
                    $attributeOptions[$k]['Isselected'] = 1;
                }
                else{
                    $attributeOptions[$k]['Isselected'] = 0;
                }   
            }
        }
        //echo "<pre>"; print_r($attributeOptions); exit;
        return $attributeOptions;
    }

}
