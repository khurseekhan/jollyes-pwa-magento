<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */

namespace TwoJay\Loyalty\Block\Account\Dashboard\Points;

use Magento\Framework\View\Element\Template\Context as TemplateContext;
use TwoJay\Loyalty\Api\Loyalty\GetCustomerTransactionsInterface;
use Magento\Customer\Model\Session;
use Magento\Framework\Json\Helper\Data as JsonHelper;
use TwoJay\Loyalty\Api\Loyalty\CustomSearchInterface;
use TwoJay\Loyalty\Api\Loyalty\GetCustomerVouchersInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Directory\Model\Currency;
use TwoJay\Loyalty\Helper\Data as LoyaltyHelper;
use TwoJay\Loyalty\Api\Loyalty\TokenInterface;
use TwoJay\Loyalty\Api\Loyalty\IsLoyaltyMemberInterface;

/**
 * Class History
 * @package TwoJay\Loyalty\Block\Point
 */
class Info extends \Magento\Framework\View\Element\Template
{

    /**
     * @var GetCustomerTransactionsInterface
     */
    protected $_customerTransactions;

    /**
     * @var Session
     */
    protected $_customerSession;

    /**
     * @var JsonHelper
     */
    protected $_jsonHelper;

    /**
     * @var CustomSearchInterface
     */
    protected $_customSearch;

    /**
     * @var GetCustomerVouchersInterface
     */
    protected $_vouchers;

    /**
     * @var StoreManagerInterface
     */
    protected $_storeManagerInterface;

    /**
     * @var Currency
     */
    protected $_currency;

    /**
     * @var LoyaltyHelper
     */
    protected $_loyaltyHelper;

    /**
     * @var TokenInterface
     */
    protected $tokenInterface;

    /**
     * @var IsLoyaltyMemberInterface
     */
    protected $isLoyaltyMemberInterface;

    /**
     * Info constructor.
     * @param TemplateContext $context
     * @param GetCustomerTransactionsInterface $customerTransactions
     * @param Session $customerSession
     * @param JsonHelper $jsonHelper
     * @param CustomSearchInterface $customSearch
     * @param GetCustomerVouchersInterface $vouchers
     * @param StoreManagerInterface $storeManagerInterface
     * @param Currency $currency
     * @param LoyaltyHelper $loyaltyHelper
     * @param TokenInterface $tokenInterface
     * @param IsLoyaltyMemberInterface $isLoyaltyMemberInterface
     * @param array $data
     */
    public function __construct(
        TemplateContext $context,
        GetCustomerTransactionsInterface $customerTransactions,
        Session $customerSession,
        JsonHelper $jsonHelper,
        CustomSearchInterface $customSearch,
        GetCustomerVouchersInterface $vouchers,
        StoreManagerInterface $storeManagerInterface,
        Currency $currency,
        LoyaltyHelper $loyaltyHelper,
        TokenInterface $tokenInterface,
        IsLoyaltyMemberInterface $isLoyaltyMemberInterface,
        array $data = []
    )
    {
        $this->_isScopePrivate = true;
        $this->_customerTransactions = $customerTransactions;
        $this->_customerSession = $customerSession;
        $this->_jsonHelper = $jsonHelper;
        $this->_customSearch = $customSearch;
        $this->_vouchers = $vouchers;
        $this->_storeManagerInterface = $storeManagerInterface;
        $this->_currency = $currency;
        $this->_loyaltyHelper = $loyaltyHelper;
        $this->tokenInterface = $tokenInterface;
        $this->isLoyaltyMemberInterface = $isLoyaltyMemberInterface;
        parent::__construct($context, $data);
    }

    /**
     * @return array
     */
    public function getCustomerLoyaltyPoints()
    {
        if (!$this->_loyaltyHelper->isModuleEnabled()) {
            return false;
        }

        $email = $this->_customerSession->getCustomer()->getEmail();
        $customerSearch = $this->_customSearch->customSearch($email);
        if (!$this->_loyaltyHelper->isJson($customerSearch) && strpos($customerSearch, '503') !== false) {
            return false;
        }

        $result = [];
        if ($customerSearch) {
            $customerSearch = $this->_jsonHelper->jsonDecode($customerSearch);
            if (isset($customerSearch['ErrorCode'])) {
                if (in_array($customerSearch['ErrorCode'], [404])) {
                    return 404;
                }
            }

            $loyaltyNumber = isset($customerSearch[0]['LoyaltyNumber']) ? $customerSearch[0]['LoyaltyNumber'] : '';
            $totalPoints = isset($customerSearch[0]['TotalPoints']) ? $customerSearch[0]['TotalPoints'] : 0;

            $result = ['loyalty_number' => $loyaltyNumber, 'total_points' => $totalPoints];
        }

        return $result;
    }

    /**
     * @return mixed|null|\TwoJay\Loyalty\Api\Loyalty\vouchers
     */
    public function getCustomerVouchers()
    {
        if (!$this->_loyaltyHelper->isModuleEnabled()) {
            return null;
        }

        // check for 503 service unavailable
        $tokenData = $this->tokenInterface->getToken();
        if (!$this->_loyaltyHelper->isJson($tokenData) && strpos($tokenData, '503') !== false) {
            return null;
        }

        $email = $this->_customerSession->getCustomer()->getEmail();
        if ($email) {
            $loyaltyNumber = $this->_loyaltyHelper->getLoyaltyNumber($email);
            if (!isset($loyaltyNumber) || empty($loyaltyNumber)) {
                $responseData = $this->isLoyaltyMemberInterface->getLoyaltyNumber($email);
                if (!$this->_loyaltyHelper->isJson($responseData) && strpos($responseData, '503') !== false) {
                    return null;
                }
                if ($responseData) {
                    $responseData = $this->_jsonHelper->jsonDecode($responseData);
                    $loyaltyNumber = isset($responseData['LoyaltyNumber']) ? $responseData['LoyaltyNumber'] : '';
                }
            }

            if ($loyaltyNumber) {
                $vouchersData = $this->_vouchers->getCustomerVouchers($loyaltyNumber);
                if ($vouchersData) {
                    $vouchersData = $this->_jsonHelper->jsonDecode($vouchersData);
                    if (isset($vouchersData['ErrorCode'])) {
                        if (in_array($vouchersData['ErrorCode'], [404])) {
                            return 404;
                        }
                    }
                }
                return $vouchersData;
            }
        }

        return null;
    }

    /**
     * get currency symbol
     *
     * @return mixed
     */
    public function getCurrencySymbol()
    {
        $currentCurrencyCode = $this->_storeManagerInterface->getStore()->getCurrentCurrencyCode();
        return $this->_currency->load($currentCurrencyCode)->getCurrencySymbol();
    }

}
