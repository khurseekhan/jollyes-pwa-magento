<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */

namespace TwoJay\Loyalty\Block\EarnPoints;

use Magento\Framework\View\Element\Template\Context as TemplateContext;

/**
 * Class History
 * @package TwoJay\Loyalty\Block\Point
 */
class Info extends \Magento\Framework\View\Element\Template
{
    /**
     * @var string
     */
   // protected $_template = 'order/info.phtml';

    public function __construct(
        TemplateContext $context,
        array $data = []
    ) {
        $this->_isScopePrivate = true;
        parent::__construct($context, $data);
    }

}
