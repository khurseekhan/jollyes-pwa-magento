<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */

namespace TwoJay\Loyalty\Block;

use TwoJay\Loyalty\Helper\Data;
use Magento\Framework\View\Element\Template\Context;

class Link extends \Magento\Framework\View\Element\Html\Link
{
    /**
     * Template name
     *
     * @var string
     */
    protected $_template = 'TwoJay_Loyalty::link.phtml';

    /**
     * @var \Magento\Wishlist\Helper\Data
     */
    protected $_dataHelper;

    /**
     * Link constructor.
     * @param Context $context
     * @param Data $dataHelper
     * @param array $data
     */
    public function __construct(
        Context $context,
        Data $dataHelper,
        array $data = []
    )
    {
        $this->_dataHelper = $dataHelper;
        parent::__construct($context, $data);
    }

    /**
     * @return string
     */
    protected function _toHtml()
    {
        if (!$this->_dataHelper->isModuleEnabled()) {
            return '';
        }
        return parent::_toHtml();
    }

    /**
     * @return string
     */
    public function getHref()
    {
        return $this->getUrl('loyalty/point/history/');
    }

    /**
     * @return \Magento\Framework\Phrase
     */
    public function getLabel()
    {
        return __('PetCLUB Points');
    }
}
