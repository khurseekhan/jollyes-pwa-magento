<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */

namespace TwoJay\Loyalty\Block\Sales\Order;

use TwoJay\Loyalty\Helper\Data as LoyaltyHelper;
use TwoJay\Loyalty\Model\LoyaltyCoupons as LoyaltyCouponsModel;

/**
 * Class LoyaltyCoupons
 * @package TwoJay\Loyalty\Block\Sales\Order
 */
class LoyaltyCoupons extends \Magento\Framework\View\Element\Template
{
    /**
     * @var Data
     */
    protected $_loyaltyHelper;

    /**
     * LoyaltyCoupons constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param LoyaltyHelper $loyaltyHelper
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        LoyaltyHelper $loyaltyHelper,
        array $data = []
    )
    {
        $this->_loyaltyHelper = $loyaltyHelper;
        parent::__construct($context, $data);
        $this->_isScopePrivate = true;
    }

    /**
     * Check if we need display full tax total info
     *
     * @return bool
     */
    /*public function displayFullSummary()
    {
        return true;
    }*/

    /**
     * Retrieve current order model instance
     *
     * @return \Magento\Sales\Model\Order
     */
    public function getOrder()
    {
        return $this->getParentBlock()->getOrder();
    }

    /**
     * @return mixed
     */
    public function getSource()
    {
        return $this->getParentBlock()->getSource();
    }

    /**
     * @return array
     */
    public function getLabelProperties()
    {
        return $this->getParentBlock()->getLabelProperties();
    }

    /**
     * @return array
     */
    public function getValueProperties()
    {
        return $this->getParentBlock()->getValueProperties();
    }

    /**
     * Retrieve loyalty coupons applied to current order
     *
     * @return array
     */
    public function getLoyaltyCoupons()
    {
        $result = [];

        if (!$this->_loyaltyHelper->isModuleEnabled()) {
            return $result;
        }

        $source = $this->getSource();
        if (!$source instanceof \Magento\Sales\Model\Order) {
            return $result;
        }
        $loyaltyCoupons = $this->_loyaltyHelper->getLoyaltyCoupons($this->getOrder());
        foreach ($loyaltyCoupons as $loyaltyCoupon) {
            $obj = new \Magento\Framework\DataObject();
            $obj->setBaseAmount($loyaltyCoupon[LoyaltyCouponsModel::BASE_AMOUNT])
                ->setAmount($loyaltyCoupon[LoyaltyCouponsModel::AMOUNT])
                ->setCode($loyaltyCoupon[LoyaltyCouponsModel::CODE]);

            $result[] = $obj;
        }

        return $result;
    }

    /**
     * Initialize Loyalty Coupons order total
     *
     * @return $this
     */
    public function initTotals()
    {
        if (!$this->_loyaltyHelper->isModuleEnabled()) {
            return $this;
        }

        $total = new \Magento\Framework\DataObject(
            [
                'code' => $this->getNameInLayout(),
                'block_name' => $this->getNameInLayout(),
                'area' => $this->getArea(),
            ]
        );
        $this->getParentBlock()->addTotalBefore($total, ['loyalty_coupons', 'grand_total']);
        return $this;
    }
}