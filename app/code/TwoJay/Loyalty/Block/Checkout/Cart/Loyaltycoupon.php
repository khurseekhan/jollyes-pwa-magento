<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */

namespace TwoJay\Loyalty\Block\Checkout\Cart;

class Loyaltycoupon extends \Magento\Checkout\Block\Cart\AbstractCart
{
    /**
     * @var \TwoJay\Loyalty\Model\Loyalty\Token
     */
    protected $token;

    /**
     * @var \TwoJay\Loyalty\Helper\Data
     */
    protected $helper;

    /**
     * Loyaltycoupon constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \TwoJay\Loyalty\Model\Loyalty\Token $token
     * @param \TwoJay\Loyalty\Helper\Data $helper
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Checkout\Model\Session $checkoutSession,
        \TwoJay\Loyalty\Model\Loyalty\Token $token,
        \TwoJay\Loyalty\Helper\Data $helper,
        array $data = []
    )
    {
        parent::__construct($context, $customerSession, $checkoutSession);
        $this->token = $token;
        $this->helper = $helper;
    }

    /**
     * URLs with secure/unsecure protocol switching
     *
     * @param string $route
     * @param array $params
     * @return string
     */
    public function getUrl($route = '', $params = [])
    {
        if (!array_key_exists('_secure', $params)) {
            $params['_secure'] = $this->getRequest()->isSecure();
        }
        return parent::getUrl($route, $params);
    }

    /**
     * Check if API service is available or not
     *
     * @return bool
     */
    public function isApiAvailable()
    {
        $tokenData = $this->token->getToken();
        if (!$this->helper->isJson($tokenData) && strpos($tokenData, '503') !== false) {
            return false;
        } else {
            return true;
        }
    }
}
