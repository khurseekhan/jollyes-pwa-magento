<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */

namespace TwoJay\Loyalty\Block\Checkout\Cart;

class Total extends \Magento\Checkout\Block\Total\DefaultTotal
{
    /**
     * @var string
     */
    protected $_template = 'TwoJay_Loyalty::cart/total.phtml';

    /**
     * @var null|\TwoJay\Loyalty\Helper\Data
     */
    protected $_loyaltyCouponData = null;

    /**
     * Total constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Sales\Model\Config $salesConfig
     * @param \TwoJay\Loyalty\Helper\Data $loyaltyCouponData
     * @param array $layoutProcessors
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Sales\Model\Config $salesConfig,
        \TwoJay\Loyalty\Helper\Data $loyaltyCouponData,
        array $layoutProcessors = [],
        array $data = []
    ) {
        $this->_loyaltyCouponData = $loyaltyCouponData;
        parent::__construct($context, $customerSession, $checkoutSession, $salesConfig, $layoutProcessors, $data);
        $this->_isScopePrivate = true;
    }

    /**
     * Get sales quote
     *
     * @return \Magento\Quote\Model\Quote
     */
    public function getQuote()
    {
        return $this->_checkoutSession->getQuote();
    }

    /**
     * @return mixed
     */
    public function getQuoteLoyaltyCoupons()
    {
        return $this->_loyaltyCouponData->getLoyaltyCoupons($this->getQuote());
    }
}