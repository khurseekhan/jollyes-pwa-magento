<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */

namespace TwoJay\Loyalty\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer as EventObserver;
use TwoJay\Loyalty\Helper\Data;

/**
 * Class AddLoyaltyAmountItem
 * @package TwoJay\Loyalty\Observer
 */
class AddLoyaltyAmountItem implements ObserverInterface
{
    /**
     * @var Data
     */
    protected $helper;

    /**
     * AddLoyaltyAmountItem constructor.
     * @param Data $helper
     */
    public function __construct(
        Data $helper
    )
    {
        $this->helper = $helper;
    }

    /**
     * If Loyalty coupon is added, its amount must added in paypal request
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(EventObserver $observer)
    {
        if (!$this->helper->isModuleEnabled()) {
            return;
        }
        /** @var \Magento\Payment\Model\Cart $cart */
        $cart = $observer->getEvent()->getCart();
        $salesEntity = $cart->getSalesModel();

        $amount = abs($salesEntity->getDataUsingMethod('loyalty_coupons_amount'));

        if ($amount > 0.0001) {
            $amount = ($amount * (-1));
            $cart->addCustomItem('PetCLUB Voucher ', 1, $amount, 'loyaltycoupon');
        }
        return $this;
    }

}