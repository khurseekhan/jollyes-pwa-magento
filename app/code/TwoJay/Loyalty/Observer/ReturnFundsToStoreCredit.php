<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */

namespace TwoJay\Loyalty\Observer;

use Magento\Framework\Event\ObserverInterface;
use TwoJay\Loyalty\Helper\Data;
use Jollyes\CustomerBalance\Model\Balance;
use Magento\Store\Model\StoreManagerInterface;

class ReturnFundsToStoreCredit implements ObserverInterface
{
    /**
     * @var null|Data
     */
    protected $loyaltyCouponHelper = null;

    /**
     * @var Balance|null
     */
    protected $customerBalance = null;

    /**
     * @var StoreManagerInterface|null
     */
    protected $storeManager = null;

    /**
     * ReturnFundsToStoreCredit constructor.
     * @param Data $loyaltyCouponHelper
     * @param Balance $customerBalance
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        Data $loyaltyCouponHelper,
        Balance $customerBalance,
        StoreManagerInterface $storeManager
    )
    {
        $this->loyaltyCouponHelper = $loyaltyCouponHelper;
        $this->customerBalance = $customerBalance;
        $this->storeManager = $storeManager;
    }

    /**
     * Return funds to store credit
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        if (!$this->loyaltyCouponHelper->isModuleEnabled()) {
            return;
        }

        /** @var \Magento\Sales\Model\Order $order */
        $order = $observer->getEvent()->getOrder();

        $loyaltyCoupons = $this->loyaltyCouponHelper->getLoyaltyCoupons($order);
        if (is_array($loyaltyCoupons)) {
            $balance = 0;
            foreach ($loyaltyCoupons as $loyaltyCoupon) {
                $balance -= $loyaltyCoupon[\TwoJay\Loyalty\Model\LoyaltyCoupons::BASE_AMOUNT];
            }

            if ($balance > 0) {
                $this->customerBalance->setCustomerId(
                    $order->getCustomerId()
                )->setWebsiteId(
                    $this->storeManager->getStore($order->getStoreId())->getWebsiteId()
                )->setAmountDelta(
                    $balance
                )->setHistoryAction(
                    \Jollyes\CustomerBalance\Model\Balance\History::ACTION_REVERTED
                )->setOrder(
                    $order
                )->save();
            }
        }

        return $this;
    }
}
