<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */

namespace TwoJay\Loyalty\Observer;

use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer as EventObserver;
use TwoJay\Loyalty\Model\Loyalty\AbstractEntryPoint;
use TwoJay\Loyalty\Api\Loyalty\RegisterInterface;
use TwoJay\Loyalty\Api\Loyalty\IsLoyaltyMemberInterface;
use Magento\Framework\Json\Helper\Data as JsonHelper;
use TwoJay\Loyalty\Api\Loyalty\LoginInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\App\ActionFlag;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\App\Response\RedirectInterface;
use TwoJay\Loyalty\Helper\Data;
use Magento\Framework\App\RequestInterface;
use Magento\Customer\Model\Url;
use Magento\Customer\Model\Session;
use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Model\AccountManagement;
use TwoJay\Loyalty\Api\Loyalty\TokenInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Stdlib\CookieManagerInterface;
use Magento\Framework\Stdlib\Cookie\CookieMetadataFactory;
use Magento\Framework\Controller\Result\RedirectFactory;
use Magento\Framework\Event\ManagerInterface as EventManager;
/**
 * Class CheckLoyaltyCustomer
 * @package TwoJay\Loyalty\Observer
 */
class CheckLoyaltyCustomer extends AbstractEntryPoint implements ObserverInterface
{

    /**
     * @var Data
     */
    protected $helper;

    /**
     * @var ActionFlag
     */
    protected $actionFlag;

    /**
     * @var ManagerInterface
     */
    protected $messageManager;

    /**
     * @var RedirectInterface
     */
    protected $redirect;

    /**
     * @var HttpClientInterface
     */
    protected $httpClient;

    /**
     * @var \RegisterInterface
     */
    protected $register;

    /**
     * @var IsLoyaltyMemberInterface
     */
    protected $isLoyaltyMember;

    /**
     * @var LoginInterface
     */
    protected $login;

    /**
     * @var JsonHelper
     */
    protected $jsonHelper;

    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * @var CustomerRepositoryInterface
     */
    protected $customerRepository;

    /**
     * @var Url
     */
    protected $url;

    /**
     * @var Session
     */
    protected $session;


    /**
     * @var CookieMetadataFactory
     */
    private $cookieMetadataFactory;

    /**
     * @var RedirectFactory
     */
    protected $resultRedirectFactory;

    /**
     * @var AccountManagementInterface
     */
    protected $accountManagementInterface;

    /**
     * @var TokenInterface
     */
    protected $tokenInterface;

    /**
     * @var CookieManagerInterface
     */
    private $cookieManager;

    /**
     * @var EventManager
     */
    protected $eventManager;

    /**
     * CheckLoyaltyCustomer constructor.
     * @param Data $helper
     * @param ActionFlag $actionFlag
     * @param ManagerInterface $messageManager
     * @param RedirectInterface $redirect
     * @param IsLoyaltyMemberInterface $isLoyaltyMember
     * @param RegisterInterface $register
     * @param JsonHelper $jsonHelper
     * @param LoginInterface $login
     * @param CustomerRepositoryInterface $customerRepository
     * @param RequestInterface $request
     * @param Url $url
     * @param Session $customerSession
     * @param RedirectFactory $resultRedirectFactory
     * @param AccountManagementInterface $accountManagementInterface
     * @param TokenInterface $tokenInterface
     * @param CookieManagerInterface $cookieManager
     * @param CookieMetadataFactory $cookieMetadataFactory
     * @param ScopeConfigInterface $scopeConfig
     * @param EventManager $eventManager
     */
    public function __construct(
        Data $helper,
        ActionFlag $actionFlag,
        ManagerInterface $messageManager,
        RedirectInterface $redirect,
        IsLoyaltyMemberInterface $isLoyaltyMember,
        RegisterInterface $register,
        JsonHelper $jsonHelper,
        LoginInterface $login,
        CustomerRepositoryInterface $customerRepository,
        RequestInterface $request,
        Url $url,
        Session $customerSession,
        RedirectFactory $resultRedirectFactory,
        AccountManagementInterface $accountManagementInterface,
        TokenInterface $tokenInterface,
        CookieManagerInterface $cookieManager,
        CookieMetadataFactory $cookieMetadataFactory,
        ScopeConfigInterface $scopeConfig,
        EventManager $eventManager
    )
    {
        $this->actionFlag = $actionFlag;
        $this->messageManager = $messageManager;
        $this->redirect = $redirect;
        $this->register = $register;
        $this->isLoyaltyMember = $isLoyaltyMember;
        $this->jsonHelper = $jsonHelper;
        $this->login = $login;
        $this->request = $request;
        $this->helper = $helper;
        $this->customerRepository = $customerRepository;
        $this->url = $url;
        $this->session = $customerSession;
        $this->resultRedirectFactory = $resultRedirectFactory;
        $this->accountManagementInterface = $accountManagementInterface;
        $this->tokenInterface = $tokenInterface;
        $this->cookieManager = $cookieManager;
        $this->cookieMetadataFactory = $cookieMetadataFactory;
        $this->eventManager = $eventManager;
        parent::__construct($scopeConfig);
    }

    /**
     * Check Loyalty Customer is exist in Loyalty API
     *
     * @param EventObserver $observer
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute(EventObserver $observer)
    {
        if (!$this->helper->isModuleEnabled()) {
            return $this;
        }

        $registerPostParams = $this->getPostParams();
        $postEmail = isset($registerPostParams['email']) ? $registerPostParams['email'] : '';
        $customer = $this->getCustomer($postEmail);

        if ($this->isLoyaltyUnavailable()) {
            $this->helper->insertToLoyaltyLogs('Customer', 'Register',
                $postEmail, 'Unable to register due to loyalty API HTTP Error 503.', null, null, $customer->getId());

            return $this;
        }

        try {
            $loyaltyNumber = $this->getLoyaltyNumber($postEmail);

            if (isset($loyaltyNumber) && !empty($loyaltyNumber)) {
                $this->authenticateWithLoyalty($postEmail, $customer);
            } else {
                $this->registerCustomerWithLoyalty($postEmail, $customer);
            }
        } catch (\Exception $e) {
            $errorMsg = __class__ . ' : ' . $e->getMessage();
            $this->helper->insertToLoyaltyLogs('Customer', 'Register',
                $postEmail, $errorMsg, null, null, $customer->getId());
        }

        return $this;
    }


    /**
     * @param $email
     * @return CustomerInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function getCustomer($email)
    {
        return $this->customerRepository->get($email);
    }

    /**
     * @return CustomerRepositoryInterface
     */
    protected function getCustomerRepository()
    {
        return $this->customerRepository;
    }


    /**
     * @param $param
     * @param $customer
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\State\InputMismatchException
     */
    protected function updateCustomAttributes($param, $customer)
    {
        $customerRep = $this->getCustomerRepository();
        $customerObj = $customerRep->getById($customer->getId());
        if (isset($param['contact_by_email'])) {
            $customerObj->setCustomAttribute('contact_by_email', $param['contact_by_email']);
        }
        if (isset($param['contact_by_sms'])) {
            $customerObj->setCustomAttribute('contact_by_sms', $param['contact_by_sms']);
        }
        if (isset($param['contact_in_post'])) {
            $customerObj->setCustomAttribute('contact_in_app', $param['contact_in_post']);
        }
        $customerRep->save($customerObj);

    }


    /**
     * @return CookieManagerInterface
     */
    private function getCookieManager()
    {
        return $this->cookieManager;
    }

    /**
     * Retrieve cookie metadata factory
     *
     * @return CookieMetadataFactory
     */
    private function getCookieMetadataFactory()
    {
        return $this->cookieMetadataFactory;
    }

    /**
     * @param $email
     * @return \Magento\Framework\Controller\Result\Redirect
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Stdlib\Cookie\FailureToSendException
     */
    protected function redirectToCustomerLogin($email)
    {
        $message = __(
            'We found a Loyalty account for your email address. Please use the link we have emailed to you to reset your password.');
        $this->messageManager->getMessages(true);
        $this->messageManager->addError($message);
        $this->session->setUsername($email);
        $lastCustomerId = $this->session->getId();
        $this->session->logout()->setBeforeAuthUrl($this->url->getRegisterUrl())
            ->setLastCustomerId($lastCustomerId);
        if ($this->getCookieManager()->getCookie('mage-cache-sessid')) {
            $metadata = $this->getCookieMetadataFactory()->createCookieMetadata();
            $metadata->setPath('/');
            $this->getCookieManager()->deleteCookie('mage-cache-sessid', $metadata);
        }

        /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath('customer/account/login');

        return $resultRedirect;
    }

    /**
     * @return bool
     */
    private function isLoyaltyUnavailable()
    {
        $tokenData = $this->tokenInterface->getToken();

        return !$this->helper->isJson($tokenData) && strpos($tokenData, '503') !== false;
    }

    /**
     * @param $email
     * @return string
     */
    private function getLoyaltyNumberFromMagento($email)
    {
        return $this->helper->getLoyaltyNumber($email);
    }

    /**
     * @param $email
     * @return string
     */
    private function getLoyaltyNumberFromAPI($email)
    {
        $loyaltyMemberData = $this->isLoyaltyMember->getLoyaltyNumber($email);
        if ($loyaltyMemberData) {
            $loyaltyMemberData = $this->jsonHelper->jsonDecode($loyaltyMemberData);
            $loyaltyNumber = isset($loyaltyMemberData['LoyaltyNumber']) ? $loyaltyMemberData['LoyaltyNumber'] : '';
        } else {
            $loyaltyNumber = '';
        }

        return $loyaltyNumber;
    }

    /**
     * @param $email
     * @return string
     */
    private function getLoyaltyNumber($email)
    {
        return ($loyaltyNumber = $this->getLoyaltyNumberFromMagento($email)) ?
            $loyaltyNumber :
            $this->getLoyaltyNumberFromAPI($email);
    }


    /**
     * @param $email
     * @param CustomerInterface $customer
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\State\InputMismatchException
     */
    public function authenticateWithLoyalty($email, CustomerInterface $customer)
    {
        $registerPostParams = $this->getPostParams();
        $loginParams = [
            'Email' => $email, 'Password' => $registerPostParams['password']
        ];
        $loginParams = $this->jsonHelper->jsonEncode($loginParams);

        // check if email and password hash authenticate via Loyalty API
        $jsonResponse = $this->login->login($loginParams);
        if ($jsonResponse) {
            $loginResponse = $this->jsonHelper->jsonDecode($jsonResponse);

            if (isset($loginResponse['LoyaltyNumber']) && !empty($loginResponse['LoyaltyNumber'])) {
                $customerRep = $this->getCustomerRepository();
                $customerObj = $customerRep->getById($customer->getId());
                $customerObj->setCustomAttribute('loyalty_number', $loginResponse['LoyaltyNumber']);
                $customerRep->save($customerObj);
            } else {
                $this->accountManagementInterface->initiatePasswordReset($email, AccountManagement::EMAIL_RESET);
                $this->messageManager->getMessages(true);
                $this->redirectToCustomerLogin($email);
            }
        }
    }

    /**
     * @return mixed
     */
    public function getPostParams()
    {
        $registerPostParams = $this->request->getPost();
        return $registerPostParams;
    }


    /**
     * @param $email
     * @param CustomerInterface $customer
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\State\InputMismatchException
     */
    public function registerCustomerWithLoyalty($email, CustomerInterface $customer)
    {
        $registerPostParams = $this->getPostParams();
        $this->updateCustomAttributes($registerPostParams, $customer);

        $streetAddress = $registerPostParams['street'];
        $address = implode(' ', $streetAddress);

        $params = [
            "Title" => $registerPostParams['title'],
            "FirstName" => trim($registerPostParams['firstname']),
            "LastName" => trim($registerPostParams['lastname']),
            "Birthdate" => "",
            "Pwd" => trim($registerPostParams['password']),
            "FavouriteStoreID" => $customer->getStoreId(),
            "Email" => trim($email),
            "MobilePhone" => trim($registerPostParams['telephone']),
            "AcceptTandC" => true,
            "AcceptPrivacy" => true,
            "OptInInternal" => true,
            "OptInExternal" => true,
            "ContactbyEmail" => $registerPostParams['contact_by_email'],
            "ContactbySMS" => $registerPostParams['contact_by_sms'],
            "ActivateDate" => $customer->getCreatedAt(),
            "Address" => $address,
            "PostCode" => trim($registerPostParams['postcode']),
            "City" => trim($registerPostParams['city']),
            "IsPasswordHashed" => false,
            "SignupChannel" => "WEB"
        ];

        // Register the customer in loyalty system
        $registerResponse = $this->register->register($params);
        if ($registerResponse) {
            $registerData = $this->jsonHelper->jsonDecode($registerResponse);
            if (isset($registerData['LoyaltyNumber']) && !empty($registerData['LoyaltyNumber'])) {
                // Save loyalty number to customer in Magento
                $customerRep = $this->getCustomerRepository();
                $customerObj = $customerRep->getById($customer->getId());
                $customerObj->setCustomAttribute('loyalty_number', $registerData['LoyaltyNumber']);
                $customerRep->save($customerObj);
                $this->eventManager->dispatch('ayko_customer_register_after_post_dispatch', ['customer' => $customerObj, 'register_data'=>$registerData]);
            }
        }
    }

}
