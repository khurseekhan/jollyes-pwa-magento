<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */

namespace TwoJay\Loyalty\Observer;

use Magento\Framework\Event\ObserverInterface;
use TwoJay\Loyalty\Helper\Data;

/**
 * Class Refund
 * @package TwoJay\Loyalty\Observer
 */
class Refund implements ObserverInterface
{
    /**
     * @var Data
     */
    protected $helper;

    /**
     * Refund constructor.
     * @param Data $helper
     */
    public function __construct(
        Data $helper
    )
    {
        $this->helper = $helper;
    }

    /**
     * Set refund amount to creditmemo
     * used for event: sales_order_creditmemo_refund
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        if (!$this->helper->isModuleEnabled()) {
            return;
        }

        $creditmemo = $observer->getEvent()->getCreditmemo();
        $order = $creditmemo->getOrder();
        if ($creditmemo->getBaseLoyaltyCouponsAmount()) {
            if ($creditmemo->getRefundLoyaltyCoupons()) {
                $baseAmount = $creditmemo->getBaseLoyaltyCouponsAmount();
                $amount = $creditmemo->getLoyaltyCouponsAmount();

                $creditmemo->setBsCustomerBalTotalRefunded($creditmemo->getBsCustomerBalTotalRefunded() + $baseAmount);
                $creditmemo->setCustomerBalTotalRefunded($creditmemo->getCustomerBalTotalRefunded() + $amount);
            }

            $order->setBaseLoyaltyCouponsRefunded(
                $order->getBaseLoyaltyCouponsRefunded() + $creditmemo->getBaseLoyaltyCouponsAmount()
            );
            $order->setLoyaltyCouponsRefunded($order->getLoyaltyCouponsRefunded() + $creditmemo->getLoyaltyCouponsAmount());

            // we need to update flag after credit memo was refunded and order's properties changed
            if ($order->getLoyaltyCouponsInvoiced() > 0 && $order->getLoyaltyCouponsInvoiced() == $order->getLoyaltyCouponsRefunded()
            ) {
                $order->setForcedCanCreditmemo(false);
            }
        }

        return $this;
    }
}
