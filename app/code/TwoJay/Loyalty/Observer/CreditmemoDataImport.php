<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */

namespace TwoJay\Loyalty\Observer;

use Magento\Framework\Event\ObserverInterface;
use TwoJay\Loyalty\Helper\Data;

/**
 * Class CreditmemoDataImport
 * @package TwoJay\Loyalty\Observer
 */
class CreditmemoDataImport implements ObserverInterface
{
    /**
     * @var Data
     */
    protected $helper;

    /**
     * CreditmemoDataImport constructor.
     * @param Data $helper
     */
    public function __construct(
        Data $helper
    )
    {
        $this->helper = $helper;
    }

    /**
     * Set refund flag to creditmemo based on user input
     * used for event: adminhtml_sales_order_creditmemo_register_before
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        if (!$this->helper->isModuleEnabled()) {
            return;
        }

        $creditmemo = $observer->getEvent()->getCreditmemo();

        $input = $observer->getEvent()->getInput();

        if (isset($input['refund_loyaltycoupons']) && $input['refund_loyaltycoupons']) {
            $creditmemo->setRefundLoyaltyCoupons(true);
        }

        return $this;
    }
}
