<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */

namespace TwoJay\Loyalty\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer as EventObserver;
use Magento\Customer\Model\Session;
use TwoJay\Loyalty\Model\Loyalty\AbstractEntryPoint;
use Magento\Framework\Json\Helper\Data as JsonHelper;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Customer\Model\CustomerFactory;
use Magento\Customer\Model\AddressFactory;
use Magento\Framework\Escaper;
use Magento\Framework\App\ResourceConnection;
use TwoJay\Loyalty\Api\Loyalty\IsLoyaltyMemberInterface;
use TwoJay\Loyalty\Api\Loyalty\LoginInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\InputException;
use Magento\Customer\Model\Account\Redirect as AccountRedirect;
use Magento\Customer\Model\Url;
use Magento\Framework\App\ActionFlag;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Model\CustomerRegistry;
use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Model\AccountManagement;
use TwoJay\Loyalty\Api\Loyalty\TokenInterface;

/**
 * Class CustomerAjaxLoginPreDispatch
 * @package TwoJay\Loyalty\Observer
 */
class CustomerAjaxLoginPreDispatch extends AbstractEntryPoint implements ObserverInterface
{
    /**
     * @var \TwoJay\Loyalty\Helper\Data
     */
    protected $_helper;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;

    /**
     * @var JsonHelper
     */
    private $jsonHelper;

    /**
     * @var AccountRedirect
     */
    protected $accountRedirect;

    /**
     * @var Session
     */
    protected $session;

    /**
     * @var ResourceConnection
     */
    protected $_resource;

    /**
     * @var
     */
    protected $connection;

    /**
     * @var IsLoyaltyMemberInterface
     */
    protected $_isLoyaltyMember;

    /**
     * @var LoginInterface
     */
    protected $_login;

    /**
     * @var Store
     */
    protected $_store;

    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    protected $customerFactory;

    /**
     * @var \Magento\Customer\Model\AddressFactory
     */
    protected $addressFactory;

    /**
     * @var \Magento\Framework\Escaper
     */
    protected $escaper;

    /**
     * @var \Magento\Framework\Escaper
     */
    protected $url;

    /**
     * @var ActionFlag
     */
    protected $actionFlag;

    /**
     * @var
     */
    protected $pageRediect;

    /**
     * @var CustomerRepositoryInterface
     */
    protected $customerRepositoryInterface;

    /**
     * @var CustomerRegistry
     */
    protected $customerRegistry;

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var \Magento\Framework\Controller\Result\RawFactory
     */
    protected $resultRawFactory;

    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $urlInterface;

    /**
     * @var AccountManagementInterface
     */
    protected $accountManagementInterface;

    /**
     * @var TokenInterface
     */
    protected $tokenInterface;


    /**
     * CustomerAjaxLoginPreDispatch constructor.
     * @param \TwoJay\Loyalty\Helper\Data $helper
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param JsonHelper $jsonHelper
     * @param Session $customerSession
     * @param AccountRedirect $accountRedirect
     * @param ResourceConnection $resource
     * @param IsLoyaltyMemberInterface $isLoyaltyMember
     * @param LoginInterface $login
     * @param StoreManagerInterface $store
     * @param CustomerFactory $customerFactory
     * @param AddressFactory $addressFactory
     * @param Escaper $escaper
     * @param Url $url
     * @param ActionFlag $actionFlag
     * @param CustomerRepositoryInterface $customerRepositoryInterface
     * @param CustomerRegistry $customerRegistry
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Magento\Framework\Controller\Result\RawFactory $resultRawFactory
     * @param \Magento\Framework\UrlInterface $urlInterface
     * @param AccountManagementInterface $accountManagementInterface
     * @param TokenInterface $tokenInterface
     */
    public function __construct(
        \TwoJay\Loyalty\Helper\Data $helper,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        JsonHelper $jsonHelper,
        Session $customerSession,
        AccountRedirect $accountRedirect,
        ResourceConnection $resource,
        IsLoyaltyMemberInterface $isLoyaltyMember,
        LoginInterface $login,
        StoreManagerInterface $store,
        CustomerFactory $customerFactory,
        AddressFactory $addressFactory,
        Escaper $escaper,
        Url $url,
        ActionFlag $actionFlag,
        CustomerRepositoryInterface $customerRepositoryInterface,
        CustomerRegistry $customerRegistry,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Framework\Controller\Result\RawFactory $resultRawFactory,
        \Magento\Framework\UrlInterface $urlInterface,
        AccountManagementInterface $accountManagementInterface,
        TokenInterface $tokenInterface
    )
    {
        $this->session = $customerSession;
        $this->url = $url;
        $this->actionFlag = $actionFlag;
        $this->accountRedirect = $accountRedirect;
        $this->_helper = $helper;
        $this->messageManager = $messageManager;
        $this->jsonHelper = $jsonHelper;
        $this->_isLoyaltyMember = $isLoyaltyMember;
        $this->_login = $login;
        $this->_store = $store;
        $this->customerFactory = $customerFactory;
        $this->addressFactory = $addressFactory;
        $this->escaper = $escaper;
        $this->_resource = $resource;
        $this->customerRepositoryInterface = $customerRepositoryInterface;
        $this->customerRegistry = $customerRegistry;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->resultRawFactory = $resultRawFactory;
        $this->urlInterface = $urlInterface;
        $this->accountManagementInterface = $accountManagementInterface;
        $this->tokenInterface = $tokenInterface;
    }

    /**
     * Check Loyalty Customer is exist in Loyalty API
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(EventObserver $observer)
    {
        if (!$this->_helper->isModuleEnabled()) {
            return;
        }

        // check for 503 service unavailable
        $tokenData = $this->tokenInterface->getToken();
        if (!$this->_helper->isJson($tokenData) && strpos($tokenData, '503') !== false) {
            return;
        }

        $credentials = null;
        $httpBadRequestCode = 400;
        $controller = $observer->getControllerAction();
        /** @var \Magento\Framework\Controller\Result\Raw $resultRaw */
        $resultRaw = $this->resultRawFactory->create();
        try {
            $credentials = $this->jsonHelper->jsonDecode($controller->getRequest()->getContent());
            $email = trim($credentials['username']);
            $textPassword = trim($credentials['password']);
        } catch (\Exception $e) {
            return $resultRaw->setHttpResponseCode($httpBadRequestCode);
        }
        if (!$credentials || $controller->getRequest()->getMethod() !== 'POST' || !$controller->getRequest()->isXmlHttpRequest()) {
            return $resultRaw->setHttpResponseCode($httpBadRequestCode);
        }

        if (!empty($email) && isset($textPassword) && !empty($textPassword)) {
            $customerEmails = $this->_helper->getCustomerDetails($email);
            if (isset($customerEmails['entity_id']) &&
                !empty($customerEmails['entity_id'])
            ) {
                $customerId = $customerEmails['entity_id'];
            } else {
                $customerId = '';
            }
            $loyaltyNumber = $this->_helper->getLoyaltyNumber($email);
            if (!isset($loyaltyNumber) || empty($loyaltyNumber)) {
                $loyaltyMemberData = $this->_isLoyaltyMember->getLoyaltyNumber($email);
                if ($loyaltyMemberData) {
                    $loyaltyMemberData = $this->jsonHelper->jsonDecode($loyaltyMemberData);
                    $loyaltyNumber = isset($loyaltyMemberData['LoyaltyNumber']) ? $loyaltyMemberData['LoyaltyNumber'] : '';
                    // save Loyalty number to Magento db
                    if (!empty($loyaltyNumber) && !empty($customerId)) {
                        $customerObj = $this->customerRepositoryInterface->getById($customerId);
                        $customerObj->setCustomAttribute('loyalty_number', $loyaltyNumber);
                        $this->customerRepositoryInterface->save($customerObj);
                    }
                }
            }

            $loginParams = [
                'Email' => $email, 'Password' => $textPassword
            ];
            $loginParams = $this->jsonHelper->jsonEncode($loginParams);

            // check if customer email exists in magento
            if (!empty($customerId)) {
                // check if customer has a loyalty number
                if (isset($loyaltyNumber) && !empty($loyaltyNumber)) {
                    $jsonResponse = $this->_login->login($loginParams);
                    if ($jsonResponse) {
                        $loginResponse = $this->jsonHelper->jsonDecode($jsonResponse);
                        // check if email and password hash authenticate via Loyalty API
                        if (isset($loginResponse['LoyaltyNumber']) && !empty($loginResponse['LoyaltyNumber'])) {
                            // create customer login session using $customerId
                            // after successful authentication with Loyalty
                            if ($this->session->loginById($customerId)) {
                                $this->session->regenerateId();
                                $this->session->setAutoLoginCustomerId(
                                    $customerId
                                );
                            }
                        } elseif (isset($loginResponse['ErrorCode'])) {
                            $errorResponseCodes = [401, 404];// 401 - Invalid password, 404 - Invalid username
                            if (in_array($loginResponse['ErrorCode'], $errorResponseCodes)) {
                                // invalid username or password
                                if ($this->_helper->isLogEnabled()) {
                                    $this->_helper->log(__CLASS__ . ': Login Response for : ' . $email . ' ErrorMessage: ' . $loginResponse['ErrorMessage']);
                                }
                            }
                        } elseif (empty($loginResponse['success'])) {
                            if (isset($loginResponse['Message'])) {
                                if ($this->_helper->isLogEnabled()) {
                                    $this->_helper->log(__CLASS__ . ': Login Response for : ' . $email . ' ErrorMessage: ' . $loginResponse['Message']);
                                }
                            }
                        }
                    }
                } else {
                    // this should never happen - an unacceptable case
                    $errorMsg = __CLASS__ . ': This customer has a Magento account but don\'t have a loyalty account';
                    $this->_helper->insertToLoyaltyLogs('Customer', 'CheckoutLogin',
                        $email, $errorMsg, null, null, $customerId);
                }

            } else {
                // If customer email doesn't exist in magento, get details from loyalty system
                if (isset($loyaltyNumber) && !empty($loyaltyNumber)) {
                    // if username and email matches to loyalty system,
                    // update details from loyalty system to Magento
                    $jsonResponse = $this->_login->login($loginParams);
                    if ($jsonResponse) {
                        $loginResponse = $this->jsonHelper->jsonDecode($jsonResponse);
                        if (isset($loginResponse['LoyaltyNumber']) && !empty($loginResponse['LoyaltyNumber'])) {
                            // create new customer in Magento from Loyalty response
                            $this->createNewCustomer($loginResponse, $controller, $textPassword);
                        } else {
                            // trigger reset customer password email
                            try {
                                if ($this->_helper->isLogEnabled()) {
                                    $this->_helper->log(__CLASS__ . ' : Line ' . __LINE__ . ' : Reset Password Request : ' . $email);
                                }
                                $this->accountManagementInterface->initiatePasswordReset($email, AccountManagement::EMAIL_RESET);
                                $message = 'We found a Loyalty account for your email address. Please use the link we have emailed to you to reset your password';
                                $this->messageManager->addError($message);
                                $this->session->setUsername($email);
                            } catch (\Exception $e) {
                                $errorMsg = __class__ . ' : ' . $e->getMessage();
                                $this->_helper->insertToLoyaltyLogs('Checkout', 'Login',
                                    $email, $errorMsg, null, null, null);
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Create new customer in Magento
     *
     * @param $loginResponse
     * @param $controller
     * @return $this
     */
    protected function createNewCustomer($loginResponse, $controller, $textPassword)
    {
        // log customer details to create customers
        if ($this->_helper->isLogEnabled()) {
            $this->_helper->log(__class__ . ' : Line ' . __line__ . ' : ' . print_r($loginResponse, 1));
        }

        $websiteId = $this->_store->getWebsite()->getWebsiteId();

        $firstName = isset($loginResponse['FirstName']) ? $loginResponse['FirstName'] : '';
        $lastName = isset($loginResponse['LastName']) ? $loginResponse['LastName'] : '';
        $email = isset($loginResponse['Email']) ? $loginResponse['Email'] : "";
        $mobilePhone = isset($loginResponse['MobilePhone']) ? $loginResponse['MobilePhone'] : '';
        $loyaltyNumber = isset($loginResponse['LoyaltyNumber']) ? $loginResponse['LoyaltyNumber'] : '';
        $address = isset($loginResponse['Address']) ? $loginResponse['Address'] : '';
        $postCode = isset($loginResponse['PostCode']) ? $loginResponse['PostCode'] : '';
        $city = isset($loginResponse['City']) ? $loginResponse['City'] : '';
        $contactByEmail = isset($loginResponse['ContactbyEmail']) ? $loginResponse['ContactbyEmail'] : '';
        $contactBySms = isset($loginResponse['ContactbySMS']) ? $loginResponse['ContactbySMS'] : '';
        $contactInApp = isset($loginResponse['ContactInApp']) ? $loginResponse['ContactInApp'] : '';

        // instantiate customer object
        $customer = $this->customerFactory->create();
        $customer->setWebsiteId($websiteId);
        try {
            // prepare customer data
            $customer->setEmail($email);
            $customer->setFirstname($firstName);
            $customer->setLastname($lastName);

            // set $textPassword to generate password
            $customer->setPassword($textPassword);

            // set the customer as confirmed
            $customer->setForceConfirmed(true);

            // set the customer loyalty number
            $customer->setLoyaltyNumber($loyaltyNumber);

            // set the customer attributes
            $customer->setData('contact_by_email', $contactByEmail);
            $customer->setData('contact_by_sms', $contactBySms);
            $customer->setData('contact_in_app', $contactInApp);
            // save data
            $customer->save();

            // save customer address
            $customerAddress = $this->addressFactory->create();
            $customerAddress->setCustomerId($customer->getId())
                ->setFirstname($firstName)
                ->setLastname($lastName)
                ->setCountryId('GB')// need to make it dynamic
                ->setPostcode($postCode)
                ->setCity($city)
                ->setTelephone($mobilePhone)
                ->setStreet(array(
                    '0' => $address // compulsory
                    //'1' => 'Your Customer Address 2' // optional
                ))
                ->setIsDefaultBilling('1')
                ->setIsDefaultShipping('1')
                ->setSaveInAddressBook('1');

            try {
                // save customer address
                $customerAddress->save();
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('We can\'t save the customer address.'));
            }

            $this->messageManager->addSuccess(
                __(
                    'Customer account with email %1 created successfully.',
                    $email
                )
            );
            if ($this->session->loginById($customer->getEntityId())) {
                $this->session->regenerateId();
                $this->session->setAutoLoginCustomerId(
                    $customer->getEntityId()
                );
            }
        } catch (InputException $e) {
            $this->messageManager->addError($this->escaper->escapeHtml($e->getMessage()));
            foreach ($e->getErrors() as $error) {
                $this->messageManager->addError($this->escaper->escapeHtml($error->getMessage()));
            }
        } catch (LocalizedException $e) {
            $this->messageManager->addError($this->escaper->escapeHtml($e->getMessage()));
        } catch (\Exception $e) {
            $this->messageManager->addException($e, __('We can\'t save the customer.'));
        }
    }

    /**
     * Stop controller dispatch
     *
     * @param $controller
     * @param string $email
     */
    protected function stopDispatch($controller, $email = '')
    {
        if (empty($email)) {
            $credentials = $this->jsonHelper->jsonDecode($controller->getRequest()->getContent());
            $email = trim($credentials['username']);
        }

        $value = $this->url->getForgotPasswordUrl();
        $message = __(
            'Incorrect password, please try again. <a href="%1">Forgotten password?</a>',
            $value
        );
        $this->messageManager->addError($message);
        $this->session->setUsername($email);

        $this->actionFlag->set('', \Magento\Framework\App\Action\Action::FLAG_NO_DISPATCH, true);
        $this->actionFlag->set('', \Magento\Framework\App\Action\Action::FLAG_NO_POST_DISPATCH, true);
    }
}
