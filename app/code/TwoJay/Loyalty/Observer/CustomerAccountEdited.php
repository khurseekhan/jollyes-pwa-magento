<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */

namespace TwoJay\Loyalty\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer as EventObserver;
use TwoJay\Loyalty\Api\Loyalty\CustomSaveOrUpdateInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Customer\Model\Session;
use TwoJay\Loyalty\Api\Loyalty\IsLoyaltyMemberInterface;
use Magento\Framework\Json\Helper\Data as JsonHelper;
use Magento\Customer\Model\ResourceModel\Customer;
use TwoJay\Loyalty\Helper\Data as LoyaltyHelper;
use Magento\Customer\Api\CustomerRepositoryInterface;

/**
 * Class CustomerAccountEdited
 * @package TwoJay\Loyalty\Observer
 */
class CustomerAccountEdited implements ObserverInterface
{
    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * @var CustomSaveOrUpdateInterface
     */
    protected $customSaveOrUpdateInterface;

    /**
     * @var Session
     */
    protected $session;

    /**
     * @var LoyaltyHelper
     */
    protected $helper;

    /**
     * @var CustomerRepositoryInterface
     */
    protected $customerRepositoryInterface;

    /**
     * @var IsLoyaltyMemberInterface
     */
    protected $isLoyaltyMemberInterface;

    /**
     * @var JsonHelper
     */
    private $jsonHelper;

    /**
     * @var Customer
     */
    protected $customerResource;

    /**
     * CustomerAccountEdited constructor.
     * @param RequestInterface $request
     * @param CustomSaveOrUpdateInterface $customSaveOrUpdateInterface
     * @param Session $customerSession
     * @param LoyaltyHelper $helper
     * @param CustomerRepositoryInterface $customerRepositoryInterface
     * @param IsLoyaltyMemberInterface $isLoyaltyMemberInterface
     * @param JsonHelper $jsonHelper
     * @param Customer $customerResource
     */
    public function __construct(
        RequestInterface $request,
        CustomSaveOrUpdateInterface $customSaveOrUpdateInterface,
        Session $customerSession,
        LoyaltyHelper $helper,
        CustomerRepositoryInterface $customerRepositoryInterface,
        IsLoyaltyMemberInterface $isLoyaltyMemberInterface,
        JsonHelper $jsonHelper,
        Customer $customerResource
    )
    {
        $this->request = $request;
        $this->customSaveOrUpdateInterface = $customSaveOrUpdateInterface;
        $this->session = $customerSession;
        $this->helper = $helper;
        $this->customerRepositoryInterface = $customerRepositoryInterface;
        $this->isLoyaltyMemberInterface = $isLoyaltyMemberInterface;
        $this->jsonHelper = $jsonHelper;
        $this->customerResource = $customerResource;
    }

    /**
     * @param EventObserver $observer
     * @return mixed
     */
    public function execute(EventObserver $observer)
    {
        if (!$this->helper->isModuleEnabled()) {
            return $this;
        }
        $email = $observer->getEvent()->getEmail();
        try {
            $customerDetails = $this->helper->getCustomerDetails($email);
            $customerId = isset($customerDetails['entity_id']) ? $customerDetails['entity_id'] : '';
            if ($customerId) {
                $this->session->loginById($customerId);
                $this->session->regenerateId();
                $this->session->setAutoLoginCustomerId($customerId);

                $customer = $this->customerRepositoryInterface->getById($customerId);
                $loyaltyNumber = null;

                if (!empty($customer->getCustomAttribute('loyalty_number'))) {
                    $loyaltyNumber = $customer->getCustomAttribute('loyalty_number')->getValue();
                }

                if (empty($loyaltyNumber)) {
                    $responseData = $this->isLoyaltyMemberInterface->getLoyaltyNumber($email);

                    // check for 503 service unavailable
                    if (!$this->helper->isJson($responseData) && strpos($responseData, '503') !== false) {
                        $msg = 'Update password failed in Loyalty system due to API 503 service error in loyalty for ' . $email;
                        $this->helper->insertToLoyaltyLogs('503_Service', 'MyAccountChangePassword',
                            '', $msg, null, null, $customerId);
                        return $this;
                    }

                    if ($responseData) {
                        $loyaltyMemberData = $this->jsonHelper->jsonDecode($responseData);
                        if ($this->helper->isValidApiResponse($loyaltyMemberData, $email, $line = __LINE__)) {
                            $loyaltyNumber = isset($loyaltyMemberData['LoyaltyNumber']) ? $loyaltyMemberData['LoyaltyNumber'] : '';
                            // save loyalty number in Magento db
                            if (!empty($loyaltyNumber)) {
                                $customerObj = $this->session->getCustomer();
                                $customerModelData = $customerObj->getDataModel();
                                $customerModelData->setCustomAttribute('loyalty_number', $loyaltyNumber);
                                $customerObj->updateData($customerModelData);
                                $this->customerResource->saveAttribute($customerObj, 'loyalty_number');
                            }
                        }
                    }
                }

                // update password in loyalty system
                if (!empty($loyaltyNumber) && $email && $this->request->getParam('change_password')) {
                    $newPass = $this->request->getPost('password');
                    $confPass = $this->request->getPost('password_confirmation');
                    if ($newPass == $confPass) {
                        // trigger update password in loyalty
                        $params = [
                            'LoyaltyNumber' => $loyaltyNumber,
                            'Email' => $email,
                            'Pwd' => $newPass,
                            'IsPasswordHashed' => false
                        ];
                        $this->customSaveOrUpdateInterface->customSaveOrUpdate($params);
                        $msg = 'New password has been set in loyalty for ' . $email;
                        $this->helper->insertToLoyaltyLogs('Customer', 'ChangePassword',
                            '', $msg, null, null, $customerId);
                    }

                    return $this;
                }
            }
        } catch (\Exception $e) {
            $errorMsg = 'Error : '.$email.' ==> '.$e->getMessage();
            $this->helper->insertToLoyaltyLogs('Customer', 'ChangePassword',
                '', $errorMsg, null, null, null);
        }
    }
}
