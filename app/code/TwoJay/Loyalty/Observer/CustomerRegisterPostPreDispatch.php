<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */

namespace TwoJay\Loyalty\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer as EventObserver;
use TwoJay\Loyalty\Model\Loyalty\AbstractEntryPoint;
use Magento\Framework\Json\Helper\Data as JsonHelper;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\App\ActionFlag;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\App\Response\RedirectInterface;
use TwoJay\Loyalty\Helper\Data;
use Magento\Framework\App\RequestInterface;
use TwoJay\Loyalty\Api\Loyalty\CustomSearchInterface;
use TwoJay\Loyalty\Api\Loyalty\TokenInterface;

/**
 * Class CustomerRegisterPostPreDispatch
 * @package TwoJay\Loyalty\Observer
 */
class CustomerRegisterPostPreDispatch extends AbstractEntryPoint implements ObserverInterface
{
    /**
     * @var Data
     */
    protected $_helper;

    /**
     * @var ActionFlag
     */
    protected $_actionFlag;

    /**
     * @var ManagerInterface
     */
    protected $messageManager;

    /**
     * @var RedirectInterface
     */
    protected $redirect;

    /**
     * @var HttpClientInterface
     */
    protected $httpClient;

    /**
     * @var JsonHelper
     */
    protected $_jsonHelper;

    /**
     * @var RequestInterface
     */
    protected $_request;

    /**
     * @var CustomerRepositoryInterface
     */
    protected $_customerRepository;

    /**
     * @var CustomSearchInterface
     */
    private $customSearchInterface;

    /**
     * @var TokenInterface
     */
    protected $tokenInterface;


    /**
     * CustomerRegisterPostPreDispatch constructor.
     * @param Data $helper
     * @param ActionFlag $actionFlag
     * @param ManagerInterface $messageManager
     * @param RedirectInterface $redirect
     * @param JsonHelper $jsonHelper
     * @param CustomerRepositoryInterface $customerRepository
     * @param RequestInterface $request
     * @param CustomSearchInterface $customSearchInterface
     * @param TokenInterface $tokenInterface
     */
    public function __construct(
        Data $helper,
        ActionFlag $actionFlag,
        ManagerInterface $messageManager,
        RedirectInterface $redirect,
        JsonHelper $jsonHelper,
        CustomerRepositoryInterface $customerRepository,
        RequestInterface $request,
        CustomSearchInterface $customSearchInterface,
        TokenInterface $tokenInterface
    )
    {
        $this->_helper = $helper;
        $this->_actionFlag = $actionFlag;
        $this->messageManager = $messageManager;
        $this->redirect = $redirect;
        $this->_jsonHelper = $jsonHelper;
        $this->_request = $request;
        $this->_helper = $helper;
        $this->_customerRepository = $customerRepository;
        $this->customSearchInterface = $customSearchInterface;
        $this->tokenInterface = $tokenInterface;
    }

    /**
     * Check Loyalty Customer is exist in Loyalty API
     * @param EventObserver $observer
     * @return $this
     * @throws \Exception
     */
    public function execute(EventObserver $observer)
    {
        if (!$this->_helper->isModuleEnabled()) {
            return;
        }

        $registerPostParams = $this->_request->getPost();
        $controller = $observer->getControllerAction();

        // check for 503 service unavailable
        $tokenData = $this->tokenInterface->getToken();
        if (!$this->_helper->isJson($tokenData) && strpos($tokenData, '503') !== false) {
            return;
        }

        try {
            $responseData = $this->customSearchInterface->customSearch(trim($registerPostParams['telephone']));
            if ($responseData) {
                $customSearchResponse = $this->_jsonHelper->jsonDecode($responseData);
                if (isset($customSearchResponse['ErrorCode'])) {
                    $errorResponseCodes = [401, 404];
                    if (in_array($customSearchResponse['ErrorCode'], $errorResponseCodes)) {
                        return $this;
                    }
                } else {
                    $this->messageManager->getMessages(true);
                    $this->messageManager->addNoticeMessage(__('This mobile number is already registered with other users.'));
                    $this->_actionFlag->set('', \Magento\Framework\App\Action\Action::FLAG_NO_DISPATCH, true);
                    $this->_actionFlag->set('', \Magento\Framework\App\Action\Action::FLAG_NO_POST_DISPATCH, true);
                    $this->redirect->redirect($controller->getResponse(), 'customer/account/create');
                }
            }
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
        return $this;
    }
}
