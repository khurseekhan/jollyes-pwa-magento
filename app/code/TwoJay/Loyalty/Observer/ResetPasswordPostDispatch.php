<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */

namespace TwoJay\Loyalty\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer as EventObserver;
use TwoJay\Loyalty\Api\Loyalty\CustomSaveOrUpdateInterface;
use TwoJay\Loyalty\Block\Checkout\Cart\Loyaltycoupon;
use TwoJay\Loyalty\Helper\Data as LoyaltyHelper;
use Magento\Customer\Api\CustomerRepositoryInterface;
use TwoJay\Loyalty\Api\Loyalty\CustomSearchInterface;
use Magento\Framework\Json\Helper\Data as JsonHelper;
use TwoJay\Loyalty\Api\Loyalty\IsLoyaltyMemberInterface;

/**
 * Class ResetPasswordPostDispatch
 * @package TwoJay\Loyalty\Observer
 */
class ResetPasswordPostDispatch implements ObserverInterface
{
    /**
     * @var LoyaltyHelper
     */
    protected $loyaltyHelper;

    /**
     * @var Loyaltycoupon
     */
    protected $loyaltycoupon;

    /**
     * @var CustomSearchInterface
     */
    protected $customSaveOrUpdateInterface;

    /**
     * @var CustomerRepositoryInterface
     */
    protected $customerRepository;

    /**
     * @var CustomSearchInterface
     */
    protected $customSearch;

    /**
     * @var JsonHelper
     */
    private $jsonHelper;

    /**
     * @var IsLoyaltyMemberInterface
     */
    protected $isLoyaltyMember;

    /**
     * ResetPasswordPostDispatch constructor.
     * @param LoyaltyHelper $loyaltyHelper
     * @param Loyaltycoupon $loyaltycoupon
     * @param CustomSaveOrUpdateInterface $customSaveOrUpdateInterface
     * @param CustomerRepositoryInterface $customerRepositoryInterface
     * @param CustomSearchInterface $customSearch
     * @param JsonHelper $jsonHelper
     * @param IsLoyaltyMemberInterface $isLoyaltyMember
     */
    public function __construct(
        LoyaltyHelper $loyaltyHelper,
        Loyaltycoupon $loyaltycoupon,
        CustomSaveOrUpdateInterface $customSaveOrUpdateInterface,
        CustomerRepositoryInterface $customerRepositoryInterface,
        CustomSearchInterface $customSearch,
        JsonHelper $jsonHelper,
        IsLoyaltyMemberInterface $isLoyaltyMember
    )
    {
        $this->loyaltyHelper = $loyaltyHelper;
        $this->loyaltycoupon = $loyaltycoupon;
        $this->customSaveOrUpdateInterface = $customSaveOrUpdateInterface;
        $this->customerRepository = $customerRepositoryInterface;
        $this->customSearch = $customSearch;
        $this->jsonHelper = $jsonHelper;
        $this->isLoyaltyMember = $isLoyaltyMember;
    }

    /**
     * Check Loyalty Customer is exist in Loyalty API
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(EventObserver $observer)
    {
        if (!$this->loyaltyHelper->isModuleEnabled()) {
            return;
        }

        try {
            $controller = $observer->getControllerAction();
            $customerId = (int)$controller->getRequest()->getQuery('id');
            $password = (string)$controller->getRequest()->getPost('password');
            $passwordConfirmation = (string)$controller->getRequest()->getPost('password_confirmation');

            if (!$this->loyaltycoupon->isApiAvailable() || $password !== $passwordConfirmation || iconv_strlen($password) <= 0) {
                return;
            }

            if ($password == $passwordConfirmation) {
                $customerEmail = $this->customerRepository->getById($customerId)->getEmail();
                $loyaltyNumber = $this->loyaltyHelper->getLoyaltyNumber($customerEmail);
                if (!isset($loyaltyNumber) || empty($loyaltyNumber)) {
                    $loyaltyMemberData = $this->isLoyaltyMember->getLoyaltyNumber($customerEmail);
                    // check for 503 service available
                    if (!$this->loyaltyHelper->isJson($loyaltyMemberData) && strpos($loyaltyMemberData, '503') !== false) {
                        return;
                    }

                    if ($loyaltyMemberData) {
                        $loyaltyMemberData = $this->jsonHelper->jsonDecode($loyaltyMemberData);
                        $loyaltyNumber = isset($loyaltyMemberData['LoyaltyNumber']) ? $loyaltyMemberData['LoyaltyNumber'] : '';
                    }
                }

                if (!empty($loyaltyNumber)) {
                    // trigger update password in loyalty
                    $params = [
                        'LoyaltyNumber' => $loyaltyNumber,
                        'Email' => $customerEmail,
                        'Pwd' => $password,
                        'IsPasswordHashed' => false
                    ];
                    $this->customSaveOrUpdateInterface->customSaveOrUpdate($params);
                    $this->loyaltyHelper->log(__CLASS__ . ' : ' . __line__ . ' : New password has been set in loyalty for ' . $customerEmail);
                }
            }
        } catch (\Exception $e) {
            $this->loyaltyHelper->log($e->getMessage());
        }
    }
}
