<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */

namespace TwoJay\Loyalty\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer as EventObserver;
use TwoJay\Loyalty\Api\Loyalty\CustomSaveOrUpdateInterface;
use TwoJay\Loyalty\Api\Loyalty\CustomSearchInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Customer\Model\Session;
use TwoJay\Loyalty\Helper\Data;
use Magento\Framework\Json\Helper\Data as JsonHelper;
use Magento\Framework\Message\ManagerInterface;
use Magento\Customer\Api\AddressRepositoryInterface;

class UpdateCustomerDetails implements ObserverInterface
{
    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * @var CustomSaveOrUpdateInterface
     */
    protected $customSaveOrUpdateInterface;

    /**
     * @var Session
     */
    protected $session;

    /**
     * @var Data
     */
    protected $helper;

    /**
     * @var CustomSearchInterface
     */
    protected $customSearch;

    /**
     * @var JsonHelper
     */
    protected $jsonHelper;

    /**
     * @var ManagerInterface
     */
    protected $messageManager;

    /**
     * @var AddressRepositoryInterface
     */
    protected $addressRepository;

    /**
     * UpdateCustomerDetails constructor.
     * @param RequestInterface $request
     * @param CustomSaveOrUpdateInterface $customSaveOrUpdateInterface
     * @param CustomSearchInterface $customSearch
     * @param Session $customerSession
     * @param Data $helper
     * @param JsonHelper $jsonHelper
     * @param ManagerInterface $messageManager
     * @param AddressRepositoryInterface $addressRepository
     */
    public function __construct(
        RequestInterface $request,
        CustomSaveOrUpdateInterface $customSaveOrUpdateInterface,
        CustomSearchInterface $customSearch,
        Session $customerSession,
        Data $helper,
        JsonHelper $jsonHelper,
        ManagerInterface $messageManager,
        AddressRepositoryInterface $addressRepository
    )
    {
        $this->request = $request;
        $this->customSaveOrUpdateInterface = $customSaveOrUpdateInterface;
        $this->session = $customerSession;
        $this->helper = $helper;
        $this->customSearch = $customSearch;
        $this->jsonHelper = $jsonHelper;
        $this->messageManager = $messageManager;
        $this->addressRepository = $addressRepository;
    }

    /**
     * @param EventObserver $observer
     * @return mixed
     */
    public function execute(EventObserver $observer)
    {
        if (!$this->helper->isModuleEnabled()) {
            return;
        }
        try {
            /** @var $customerAddress Address */
            $customerAddress = $observer->getCustomerAddress();
            $params = $this->request->getParams();
            $customer = $customerAddress->getCustomer();
            $loyaltyNumber = $customer->getData('loyalty_number');

            if (!$customer->getDefaultBillingAddress()) {
                return $this;
            }

            if (empty($loyaltyNumber)) {
                return $this;
            }

            $customerLookup = $this->customSearch->customSearch($loyaltyNumber);
            $email = (isset($params['email'])) ? $params['email'] : $customer->getEmail();
            // check for 503 service unavailable
            if (!$this->helper->isJson($customerLookup) && strpos($customerLookup, '503') !== false) {
                $errorMsg = __CLASS__ . ': Service is unavailable of Loyalty system for : ' . $email . " when user trying to update the address";
                $this->helper->insertToLoyaltyLogs('503_Service', 'UpdateCustomerAddress', 'MyAccount', $errorMsg, null,
                    null, $customer->getEntityId());
            }

            $billingAddressId = $customer->getDefaultBillingAddress()->getId();
            $billingAddress = $this->addressRepository->getById($billingAddressId);
            $billingFirstName = $billingAddress->getFirstname();
            $billingLastName = $billingAddress->getLastname();
            $billingStreetAddress1 = (isset($billingAddress->getStreet()[0])) ? $billingAddress->getStreet()[0] : '';
            $billingStreetAddress2 = (isset($billingAddress->getStreet()[1])) ? $billingAddress->getStreet()[1] : '';
            $billingStreet = $billingStreetAddress1 . ' ' . $billingStreetAddress2;
            $billingCity = $billingAddress->getCity();
            $billingPostCode = $billingAddress->getPostcode();
            $billingTelephone = $billingAddress->getTelephone();

            if ($customerLookup && $this->helper->isJson($customerLookup)) {
                $customerLookupData = $this->jsonHelper->jsonDecode($customerLookup);
                if ($this->helper->isValidApiResponse($customerLookupData, $email, $line = __LINE__)) {
                    $loyaltyNumber = (isset($customerLookupData['0']['LoyaltyNumber'])) ? $customerLookupData['0']['LoyaltyNumber'] : '';
                    $firstName = (isset($customerLookupData['0']['FirstName'])) ? $customerLookupData['0']['FirstName'] : '';
                    $lastName = (isset($customerLookupData['0']['LastName'])) ? $customerLookupData['0']['LastName'] : '';
                    $address = (isset($customerLookupData['0']['Address'])) ? $customerLookupData['0']['Address'] : '';
                    $postCode = (isset($customerLookupData['0']['PostCode'])) ? $customerLookupData['0']['PostCode'] : '';
                    $city = (isset($customerLookupData['0']['City'])) ? $customerLookupData['0']['City'] : '';
                    $telephone = (isset($customerLookupData['0']['MobilePhone'])) ? $customerLookupData['0']['MobilePhone'] : '';

                    $newFirstName = (isset($billingFirstName)) ? $billingFirstName : $firstName;
                    $newLastName = (isset($billingLastName)) ? $billingLastName : $lastName;
                    $newAddress = (isset($billingStreet)) ? $billingStreet : $address;
                    $newPostCode = (isset($billingPostCode)) ? $billingPostCode : $postCode;
                    $newCity = (isset($billingCity)) ? $billingCity : $city;
                    $newTelephone = (isset($billingTelephone)) ? $billingTelephone : $telephone;

                    $requestParams = [
                        'LoyaltyNumber' => $loyaltyNumber,
                        'Email' => $email,
                        'FirstName' => $newFirstName,
                        'LastName' => $newLastName,
                        'Address' => $newAddress,
                        'PostCode' => $newPostCode,
                        'City' => $newCity,
                        'MobilePhone' => $newTelephone
                    ];

                    $this->customSaveOrUpdateInterface->customSaveOrUpdate($requestParams);
                    if ($this->helper->isLogEnabled()) {
                        $this->helper->log(__CLASS__ . 'Customer details have been saved in loyalty for ' . $email);
                    }
                }
            }
            return $this;
        } catch (\Exception $e) {
            $errorMsg = __class__ . ": Exception Error: " . $e->getMessage();
            $this->loyaltyHelper->insertToLoyaltyLogs('Customer', 'AccountUpdateAddress',
                '', $errorMsg, null, null, null);
        }
    }
}