<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */

namespace TwoJay\Loyalty\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer as EventObserver;
use TwoJay\Loyalty\Helper\Data;
use Magento\Framework\Json\Helper\Data as JsonHelper;

/**
 * Class PaypalOrderSuccess
 * @package TwoJay\Loyalty\Observer
 */
class PaypalOrderSuccess implements ObserverInterface
{

    /**
     * @var Data
     */
    protected $helper;

    /**
     * @var JsonHelper
     */
    protected $jsonHelper;

    /**
     * PaypalOrderSuccess constructor.
     * @param Data $helper
     * @param JsonHelper $jsonHelper
     */
    public function __construct(
        Data $helper,
        JsonHelper $jsonHelper
    )
    {
        $this->helper = $helper;
        $this->jsonHelper = $jsonHelper;
    }

    /**
     * @param EventObserver $observer
     * @return mixed
     */
    public function execute(EventObserver $observer)
    {
        if (!$this->helper->isModuleEnabled()) {
            return $this;
        }

        $quote = $observer->getEvent()->getQuote();
        $order = $observer->getEvent()->getOrder();
        if (!empty($quote->getLoyaltyCoupons()) && empty($order->getLoyaltyCoupons())) {
            $baseLoyaltyCouponsAmount = $quote->getBaseLoyaltyCouponsAmount();
            $loyaltyCouponsAmount = $quote->getLoyaltyCouponsAmount();
            $order->setBaseLoyaltyCouponsAmount($baseLoyaltyCouponsAmount);
            $order->setLoyaltyCouponsAmount($loyaltyCouponsAmount);
            $order->setLoyaltyCoupons($quote->getLoyaltyCoupons());
            $order->save();
        }
        return $this;
    }
}