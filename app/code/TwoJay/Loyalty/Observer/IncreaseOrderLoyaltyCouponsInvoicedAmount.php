<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */

namespace TwoJay\Loyalty\Observer;

use Magento\Framework\Event\ObserverInterface;
use TwoJay\Loyalty\Helper\Data;

/**
 * Class IncreaseOrderLoyaltyCouponsInvoicedAmount
 * @package TwoJay\Loyalty\Observer
 */
class IncreaseOrderLoyaltyCouponsInvoicedAmount implements ObserverInterface
{
    /**
     * @var Data
     */
    protected $helper;

    /**
     * IncreaseOrderLoyaltyCouponsInvoicedAmount constructor.
     * @param Data $helper
     */
    public function __construct(
        Data $helper
    )
    {
        $this->helper = $helper;
    }

    /**
     * Increase order loyalty_coupons_amount_invoiced attribute based on created invoice
     * used for event: sales_order_invoice_register
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        if (!$this->helper->isModuleEnabled()) {
            return $this;
        }

        $invoice = $observer->getEvent()->getInvoice();
        $order = $invoice->getOrder();
        if ($invoice->getBaseLoyaltyCouponsAmount()) {
            $order->setBaseLoyaltyCouponsInvoiced($order->getBaseLoyaltyCouponsInvoiced() + $invoice->getBaseLoyaltyCouponsAmount());
            $order->setLoyaltyCouponsInvoiced($order->getLoyaltyCouponsInvoiced() + $invoice->getLoyaltyCouponsAmount());
        }
        return $this;
    }
}
