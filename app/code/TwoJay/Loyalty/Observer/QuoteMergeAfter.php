<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */

namespace TwoJay\Loyalty\Observer;

use Magento\Framework\Event\ObserverInterface;
use TwoJay\Loyalty\Helper\Data;

/**
 * Class QuoteMergeAfter
 * @package TwoJay\Loyalty\Observer
 */
class QuoteMergeAfter implements ObserverInterface
{
    /**
     * @var Data
     */
    protected $helper;

    /**
     * QuoteMergeAfter constructor.
     * @param Data $helper
     */
    public function __construct(
        Data $helper
    )
    {
        $this->helper = $helper;
    }

    /**
     * Set the source Loyalty Coupons into new quote
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        if (!$this->helper->isModuleEnabled()) {
            return;
        }

        $quote = $observer->getEvent()->getQuote();
        $source = $observer->getEvent()->getSource();

        if ($source->getLoyaltyCoupons()) {
            $quote->setLoyaltyCoupons($source->getLoyaltyCoupons());
        }
    }
}
