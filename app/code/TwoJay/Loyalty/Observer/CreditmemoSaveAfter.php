<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */

namespace TwoJay\Loyalty\Observer;

use Magento\Framework\Event\ObserverInterface;
use TwoJay\Loyalty\Helper\Data as LoyaltyCouponDataHelper;
use TwoJay\Loyalty\Model\LoyaltyCouponsFactory;
use TwoJay\Loyalty\Api\LoyaltyCouponsRepositoryInterface;
use TwoJay\Loyalty\Model\LoyaltyCoupons;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\Event\Observer;
use Magento\Sales\Model\Order\Creditmemo;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Status\HistoryFactory;
use Magento\Sales\Api\OrderStatusHistoryRepositoryInterface;
use Magento\Framework\Phrase;

/**
 * LoyaltyCoupons Module Observer.
 * used for event: sales_order_creditmemo_save_after
 */
class CreditmemoSaveAfter implements ObserverInterface
{
    /**
     * @var string
     */
    private static $messageRefundToLoyaltyCoupon = "We refunded %1 to PetCLUB Voucher (%2)";

    /**
     * @var string
     */
    private static $messageRefundToStoryCredit = "We refunded %1 to Store Credit from PetCLUB Voucher (%2)";

    /**
     * @var LoyaltyCouponDataHelper
     */
    private $loyaltyCouponDataHelper;

    /**
     * @var LoyaltyCouponsFactory
     */
    private $loyaltyCouponsFactory;

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var LoyaltyCouponsRepositoryInterface
     */
    private $loyaltyCouponsRepository;

    /**
     * @var OrderStatusHistoryRepositoryInterface
     */
    private $orderStatusHistoryRepository;

    /**
     * @var HistoryFactory
     */
    private $historyFactory;

    /**
     * CreditmemoSaveAfter constructor.
     * @param LoyaltyCouponDataHelper $loyaltyCouponDataHelper
     * @param LoyaltyCouponsFactory $loyaltyCouponsFactory
     * @param ScopeConfigInterface $scopeConfig
     * @param LoyaltyCouponsRepositoryInterface $loyaltyCouponsRepository
     * @param HistoryFactory $historyFactory
     * @param OrderStatusHistoryRepositoryInterface $orderStatusHistoryRepository
     */
    public function __construct(
        LoyaltyCouponDataHelper $loyaltyCouponDataHelper,
        LoyaltyCouponsFactory $loyaltyCouponsFactory,
        ScopeConfigInterface $scopeConfig,
        LoyaltyCouponsRepositoryInterface $loyaltyCouponsRepository,
        HistoryFactory $historyFactory,
        OrderStatusHistoryRepositoryInterface $orderStatusHistoryRepository
    )
    {
        $this->loyaltyCouponDataHelper = $loyaltyCouponDataHelper;
        $this->loyaltyCouponsFactory = $loyaltyCouponsFactory;
        $this->scopeConfig = $scopeConfig;
        $this->loyaltyCouponsRepository = $loyaltyCouponsRepository;
        $this->historyFactory = $historyFactory;
        $this->orderStatusHistoryRepository = $orderStatusHistoryRepository;
    }

    /**
     * Refunds process.
     *
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        if (!$this->loyaltyCouponDataHelper->isModuleEnabled()) {
            return;
        }
        /** @var Creditmemo $creditmemo */
        $creditmemo = $observer->getEvent()->getCreditmemo();

        if (!$this->refundToCustomerBalanceExpected($creditmemo)) {
            $this->refundToLoyaltyCoupons($creditmemo);

            return;
        }

        $this->addRefundToStoryCreditComments($creditmemo);
    }

    /**
     * Refunds Loyalty Coupon amount to Store Credit comments.
     * Customer is not guest and Credit Store enabled.
     *
     * @param Creditmemo $creditmemo
     * @return void
     */
    private function addRefundToStoryCreditComments(Creditmemo $creditmemo)
    {
        /** @var Order $order */
        $order = $creditmemo->getOrder();
        $loyaltyCoupons = $this->loyaltyCouponDataHelper->getLoyaltyCoupons($order);
        if (is_array($loyaltyCoupons)) {
            foreach ($loyaltyCoupons as $loyaltyCoupon) {
                /** @var LoyaltyCoupons $account */
                $loyaltyCouponCode = $loyaltyCoupon[LoyaltyCoupons::CODE];
                $loyaltyCouponAmount = $loyaltyCoupon[LoyaltyCoupons::AMOUNT];
                $comment = __(
                    self::$messageRefundToStoryCredit,
                    $order->getBaseCurrency()->formatTxt($loyaltyCouponAmount),
                    $loyaltyCouponCode
                );
                $this->addCommentToHistory($order->getId(), $comment);
            }
        }
    }

    /**
     * Checks conditions for refund of Loyalty Coupon.
     *
     * @param Creditmemo $creditmemo
     * @return bool
     */
    private function refundToCustomerBalanceExpected(Creditmemo $creditmemo)
    {
        /** @var Order $order */
        $order = $creditmemo->getOrder();
        if (!$creditmemo->getCustomerBalanceRefundFlag() || $order->getCustomerIsGuest()) {
            return false;
        }

        // Gets 'Enable Store Credit Functionality' flag from the Scope Config.
        $customerBalanceIsEnabled = $this->scopeConfig->isSetFlag(
            'customer/jollyes_customerbalance/is_enabled',
            ScopeInterface::SCOPE_STORE
        );

        return $customerBalanceIsEnabled;
    }

    /**
     * Refunds to LoyaltyCoupons if customer is not guest or Credit Store disabled.
     *
     * @param Creditmemo $creditmemo
     * @return void
     */
    private function refundToLoyaltyCoupons(Creditmemo $creditmemo)
    {
        /** @var Order $order */
        $order = $creditmemo->getOrder();
        $loyaltyCoupons = $this->loyaltyCouponDataHelper->getLoyaltyCoupons($order);
        if (is_array($loyaltyCoupons)) {
            foreach ($loyaltyCoupons as $loyaltyCoupon) {
                $loyaltyCouponModel = $this->loyaltyCouponsFactory->create();
                $loyaltyCouponCode = $loyaltyCoupon[LoyaltyCoupons::CODE];
                $loyaltyCouponAmount = $loyaltyCoupon[LoyaltyCoupons::AMOUNT];
                //$loyaltyCouponModel->loadByCode($loyaltyCouponCode);
                // The Loyalty Coupon was removed manually or by cron job
                /*if (!$loyaltyCouponModel->getId()) {
                    $loyaltyCouponModel = $this->createLoyaltyCoupon($order, $loyaltyCoupon);
                }*/

                $loyaltyCouponModel->revert($loyaltyCouponAmount);
                $comment = __(
                    self::$messageRefundToLoyaltyCoupon,
                    $order->getBaseCurrency()->formatTxt($loyaltyCouponAmount),
                    $loyaltyCouponCode
                );
                $this->addCommentToHistory($order->getId(), $comment);
                $this->loyaltyCouponsRepository->save($loyaltyCouponModel);
            }
        }
    }

    /**
     * Adds new comment in the order's history.
     *
     * @param string $orderId
     * @param Phrase $comment
     * @return void
     */
    private function addCommentToHistory($orderId, Phrase $comment)
    {
        $history = $this->historyFactory->create();
        $history->setParentId($orderId)
            ->setComment($comment)
            ->setEntityName('order')
            ->setStatus(Order::STATE_CLOSED)
            ->save();

        $this->orderStatusHistoryRepository->save($history);
    }
}
