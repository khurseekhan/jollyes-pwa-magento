<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */

namespace TwoJay\Loyalty\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer as EventObserver;
use Magento\Checkout\Model\Session;
use TwoJay\Loyalty\Helper\Data;

/**
 * Class OrderPlaceBefore
 * @package TwoJay\Loyalty\Observer
 */
class OrderPlaceBefore implements ObserverInterface
{
    /**
     * @var Session
     */
    protected $session;

    /**
     * @var Data
     */
    protected $helper;

    /**
     * OrderPlaceBefore constructor.
     * @param Session $session
     * @param Data $helper
     */
    public function __construct(
        Session $session,
        Data $helper
    )
    {
        $this->session = $session;
        $this->helper = $helper;
    }

    /**
     * Set loyalty coupons amount
     *
     * @param EventObserver $observer
     * @return $this
     */
    public function execute(EventObserver $observer)
    {
        if (!$this->helper->isModuleEnabled()) {
            return;
        }

        $quote = $this->session->getQuote();

        if (!empty($quote->getLoyaltyCoupons())) {
            $order = $observer->getEvent()->getOrder();
            $baseLoyaltyCouponsAmount = $quote->getBaseLoyaltyCouponsAmount();
            $loyaltyCouponsAmount = $quote->getLoyaltyCouponsAmount();
            $order->setBaseLoyaltyCouponsAmount($baseLoyaltyCouponsAmount);
            $order->setLoyaltyCouponsAmount($loyaltyCouponsAmount);
            $order->setLoyaltyCoupons($quote->getLoyaltyCoupons());
        }

        return $this;
    }
}
