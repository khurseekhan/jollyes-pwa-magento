<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */

namespace TwoJay\Loyalty\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Json\Helper\Data as JsonHelper;
use Magento\Setup\Exception;
use TwoJay\Loyalty\Helper\Data as LoyaltyHelper;
use Magento\Quote\Model\QuoteFactory;
use Magento\Checkout\Model\Session;
use Magento\Sales\Model\Order\Email\Sender\OrderSender;
use TwoJay\Loyalty\Model\Loyalty\RedeemVoucher;
use Magento\Sales\Model\Order;

/**
 * Class OrderSuccessAction
 * @package TwoJay\Loyalty\Observer
 */
class OrderSuccessAction implements ObserverInterface
{
    /**
     * @var Order
     */
    protected $order;

    /**
     * @var RedeemVoucher
     */
    protected $redeemVoucher;

    /**
     * @var JsonHelper
     */
    protected $jsonHelper;

    /**
     * @var LoyaltyHelper
     */
    protected $loyaltyHelper;

    /**
     * @var QuoteFactory
     */
    protected $quoteFactory;

    /**
     * @var Session
     */
    protected $checkoutSession;

    /**
     * @var OrderSender
     */
    protected $orderSender;

    /**
     * OrderSuccessAction constructor.
     * @param Order $order
     * @param RedeemVoucher $redeemVoucher
     * @param JsonHelper $jsonHelper
     * @param LoyaltyHelper $loyaltyHelper
     * @param QuoteFactory $quoteFactory
     * @param Session $checkoutSession
     * @param OrderSender $orderSender
     */
    public function __construct(
        Order $order,
        RedeemVoucher $redeemVoucher,
        JsonHelper $jsonHelper,
        LoyaltyHelper $loyaltyHelper,
        QuoteFactory $quoteFactory,
        Session $checkoutSession,
        OrderSender $orderSender
    )
    {
        $this->order = $order;
        $this->redeemVoucher = $redeemVoucher;
        $this->jsonHelper = $jsonHelper;
        $this->loyaltyHelper = $loyaltyHelper;
        $this->quoteFactory = $quoteFactory;
        $this->checkoutSession = $checkoutSession;
        $this->orderSender = $orderSender;
    }

    public function execute(EventObserver $observer)
    {
        $orderIds = $observer->getEvent()->getOrderIds();
        $orderId = is_array($orderIds) ? isset($orderIds[0]) ? $orderIds[0] : '' : $orderIds;
        if (!empty($orderId)) {
            // load order details
            $order = $this->order->load($orderId);

            if (!$this->loyaltyHelper->isModuleEnabled()) {
                $this->checkoutSession->setForceOrderMailSentOnSuccess(true);
                $this->orderSender->send($order, true);
                return $this;
            }

            $loyaltyCoupons = $order->getLoyaltyCoupons();
            if (!empty($loyaltyCoupons)) {
                // redeeem vouchers
                $orderIncrementId = $order->getIncrementId();
                $loyaltyCoupons = $this->jsonHelper->jsonDecode($loyaltyCoupons);
                foreach ($loyaltyCoupons as $loyaltyCoupon) {
                    $params = [
                        'VoucherNumber' => $loyaltyCoupon['c'],
                        'RedeemValue' => $loyaltyCoupon['a'],
                        'RedemptionDate' => date('Y-m-d\Th:i:s'),
                        'TXNNumberRef' => $orderIncrementId,
                        'RedeemSource' => 'WEB',
                    ];
                    if ($this->loyaltyHelper->isLogEnabled()) {
                        $this->loyaltyHelper->log(__CLASS__ . ' : Order ID :' . $orderIncrementId);
                        $this->loyaltyHelper->log(print_r($params, 1));
                    }
                    try {
                        $responseData = $this->redeemVoucher->redeemVoucher($params);
                        // check for 503 service unavailable
                        if (!$this->loyaltyHelper->isJson($responseData) && strpos($responseData, '503') !== false) {
                            $this->loyaltyHelper->insertToLoyaltyLogs(
                                '503_Service', 'PlaceOrder', 'RedeemVoucherAPI', 'Redeem voucher failed', null,
                                $orderId, null);
                        }
                    } catch (Exception $e) {
                        $errorMsg = __class__ . ": Exception Error: " . $e->getMessage();
                        $this->loyaltyHelper->insertToLoyaltyLogs('Order', 'Success',
                            '', $errorMsg, null, $orderId, null);
                    }
                }
            }
            $this->checkoutSession->setForceOrderMailSentOnSuccess(true);
            $this->orderSender->send($order, true);
        }
        return $this;
    }
}
