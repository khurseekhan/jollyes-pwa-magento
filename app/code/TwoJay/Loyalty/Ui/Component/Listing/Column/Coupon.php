<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 Two Jay Limited (http://www.twojay.co/)
 *
 */

namespace TwoJay\Loyalty\Ui\Component\Listing\Column;

use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use TwoJay\Loyalty\Helper\Data;

/**
 * Class Coupon
 * @package TwoJay\Loyalty\Ui\Component\Listing\Column
 */
class Coupon extends Column
{

    /**
     * @var null|Data
     */
    protected $_loyaltyCouponsData = null;

    /**
     * Coupon constructor.
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param Data $loyaltyCouponsData
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        Data $loyaltyCouponsData,
        array $components = [],
        array $data = []
    )
    {
        $this->_loyaltyCouponsData = $loyaltyCouponsData;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }


    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {

            foreach ($dataSource['data']['items'] as & $item) {

                $coupons = '';
                if ($item["loyalty_coupons"]) {
                    $allCoupons = $this->_loyaltyCouponsData->unserializeData($item["loyalty_coupons"]);

                    foreach ($allCoupons as $eachCoupon) {
                        $cTitle = (is_array($eachCoupon['c'])) ? $eachCoupon['c'][0] : $eachCoupon['c'];
                        $coupons .= $cTitle . ' : ' . $eachCoupon['a'] . ',  ';
                    }
                }
                $item['loyalty_coupons'] = trim($coupons, ',  ');
            }
        }
        return $dataSource;
    }
}
