<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */

namespace TwoJay\Loyalty\Setup;

use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Eav\Model\Config;
use Magento\Customer\Model\Customer;
use Magento\Sales\Setup\SalesSetupFactory;
use Magento\Quote\Setup\QuoteSetupFactory;

class InstallData implements InstallDataInterface
{
    /**
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * @var SalesSetupFactory
     */
    protected $salesSetupFactory;

    /**
     * @var QuoteSetupFactory
     */
    protected $quoteSetupFactory;

    /**
     * InstallData constructor.
     * @param EavSetupFactory $eavSetupFactory
     * @param Config $eavConfig
     * @param SalesSetupFactory $salesSetupFactory
     * @param QuoteSetupFactory $quoteSetupFactory
     */
    public function __construct(
        EavSetupFactory $eavSetupFactory,
        Config $eavConfig,
        SalesSetupFactory $salesSetupFactory,
        QuoteSetupFactory $quoteSetupFactory
    )
    {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->eavConfig       = $eavConfig;
        $this->salesSetupFactory = $salesSetupFactory;
        $this->quoteSetupFactory = $quoteSetupFactory;
    }

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        /**************Add loyalty table data******************/
        $installer = $setup;
        $quoteInstaller = $this->quoteSetupFactory->create(['resourceName' => 'quote_setup', 'setup' => $setup]);
        $salesInstaller = $this->salesSetupFactory->create(['resourceName' => 'sales_setup', 'setup' => $setup]);
        $installer->startSetup();
        // 0.0.1 => 0.0.2
        $quoteInstaller->addAttribute('quote', 'loyalty_coupons', ['type' => 'text']);        // 0.0.2 => 0.0.3
        $quoteInstaller->addAttribute('quote', 'loyalty_coupons_amount', ['type' => 'decimal']);
        $quoteInstaller->addAttribute('quote', 'base_loyalty_coupons_amount', ['type' => 'decimal']);

        // 0.0.3 => 0.0.4
        $quoteInstaller->addAttribute('quote_address', 'loyalty_coupons', ['type' => 'text']);
        $quoteInstaller->addAttribute('quote_address', 'loyalty_coupons_amount', ['type' => 'decimal']);
        $quoteInstaller->addAttribute('quote_address', 'base_loyalty_coupons_amount', ['type' => 'decimal']);

        // 0.0.4 => 0.0.5
        $salesInstaller->addAttribute('order', 'loyalty_coupons', ['type' => 'text']);
        $salesInstaller->addAttribute('order', 'loyalty_coupons_amount', ['type' => 'decimal']);
        $salesInstaller->addAttribute('order', 'base_loyalty_coupons_amount', ['type' => 'decimal']);

        // 0.0.9 => 0.0.9
        $salesInstaller->addAttribute('order', 'base_loyalty_coupons_invoiced', ['type' => 'decimal']);
        $salesInstaller->addAttribute('order', 'loyalty_coupons_invoiced', ['type' => 'decimal']);

        $salesInstaller->addAttribute('invoice', 'base_loyalty_coupons_amount', ['type' => 'decimal']);
        $salesInstaller->addAttribute('invoice', 'loyalty_coupons_amount', ['type' => 'decimal']);

        // 0.0.11 => 0.0.12
        $salesInstaller->addAttribute('order', 'base_loyalty_coupons_refunded', ['type' => 'decimal']);
        $salesInstaller->addAttribute('order', 'loyalty_coupons_refunded', ['type' => 'decimal']);

        $salesInstaller->addAttribute('creditmemo', 'base_loyalty_coupons_amount', ['type' => 'decimal']);
        $salesInstaller->addAttribute('creditmemo', 'loyalty_coupons_amount', ['type' => 'decimal']);

        $installer->endSetup();
        /**************End loyalty table schema******************/
    }

}