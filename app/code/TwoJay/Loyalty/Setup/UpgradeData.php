<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */

namespace TwoJay\Loyalty\Setup;


use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Customer\Model\Customer;
use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Model\Entity\Attribute\Set as AttributeSet;
use Magento\Eav\Model\Entity\Attribute\SetFactory as AttributeSetFactory;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Quote\Setup\QuoteSetupFactory;

class UpgradeData implements UpgradeDataInterface
{

    /**
     * @var CustomerSetupFactory
     */
    protected $customerSetupFactory;

    /**
     * @var AttributeSetFactory
     */
    private $attributeSetFactory;

    /**
     * @var EavSetup
     */
    protected $eavSetup;

    /**
     * @var QuoteSetupFactory
     */
    protected $quoteSetupFactory;

    /**
     * UpgradeData constructor.
     * @param CustomerSetupFactory $customerSetupFactory
     * @param AttributeSetFactory $attributeSetFactory
     * @param EavSetup $eavSetup
     * @param QuoteSetupFactory $quoteSetupFactory
     */
    public function __construct(
        CustomerSetupFactory $customerSetupFactory,
        AttributeSetFactory $attributeSetFactory,
        EavSetup $eavSetup,
        QuoteSetupFactory $quoteSetupFactory
    )
    {
        $this->customerSetupFactory = $customerSetupFactory;
        $this->attributeSetFactory = $attributeSetFactory;
        $this->eavSetup = $eavSetup;
        $this->quoteSetupFactory = $quoteSetupFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.0.4', '<')) {
            /** @var CustomerSetup $customerSetup */
            $customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);

            $customerEntity = $customerSetup->getEavConfig()->getEntityType('customer');
            $attributeSetId = $customerEntity->getDefaultAttributeSetId();

            /** @var $attributeSet AttributeSet */
            $attributeSet = $this->attributeSetFactory->create();
            $attributeGroupId = $attributeSet->getDefaultGroupId($attributeSetId);

            $isAttributeExist = $this->eavSetup->getAttribute('1', 'loyalty_number');

            if (!$isAttributeExist) {

                $customerSetup->addAttribute(Customer::ENTITY, 'loyalty_number', [
                    'type' => 'varchar',
                    'label' => 'Loyalty Number',
                    'input' => 'text',
                    'required' => false,
                    'visible' => true,
                    'user_defined' => true,
                    'position' => 980,
                    'system' => 0,
                ]);

                $attribute = $customerSetup->getEavConfig()->getAttribute(Customer::ENTITY, 'loyalty_number')
                    ->addData([
                        'attribute_set_id' => $attributeSetId,
                        'attribute_group_id' => $attributeGroupId,
                        'used_in_forms' => ['adminhtml_customer', 'customer_account_edit'],//you can use other forms also ['adminhtml_customer_address', 'customer_address_edit', 'customer_register_address']
                    ]);

                $attribute->save();
            }
        }
        if (version_compare($context->getVersion(), '1.0.5', '<')) {
            $this->createAcceptTermsAndConditionsAttribute($setup);
            $this->createContactByEmailAttribute($setup);
            $this->createContactBySMSAttribute($setup);
            $this->createContactInAppAttribute($setup);
        }

        if (version_compare($context->getVersion(), '1.0.6', '<')) {
            $this->createPasswordCheckAttribute($setup);
        }

        if (version_compare($context->getVersion(), '1.0.9', '<')) {
            $setup->getConnection()->insert('loyalty_service_status', ['service_status' => 0]);
        }

        $setup->endSetup();
    }

    protected function createAcceptTermsAndConditionsAttribute($setup)
    {
        /** @var CustomerSetup $customerSetup */
        $customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);

        $customerEntity = $customerSetup->getEavConfig()->getEntityType('customer');
        $attributeSetId = $customerEntity->getDefaultAttributeSetId();

        /** @var $attributeSet AttributeSet */
        $attributeSet = $this->attributeSetFactory->create();
        $attributeGroupId = $attributeSet->getDefaultGroupId($attributeSetId);

        $isAttributeExist = $this->eavSetup->getAttribute('1', 'accept_tnc_privacy');

        if (!$isAttributeExist) {

            $customerSetup->addAttribute(Customer::ENTITY, 'accept_tnc_privacy', [
                'type' => 'int',
                'label' => 'Accept terms & conditions',
                'input' => 'boolean',
                'required' => false,
                'visible' => true,
                'user_defined' => true,
                'position' => 981,
                'system' => 0,
            ]);

            $attribute = $customerSetup->getEavConfig()->getAttribute(Customer::ENTITY, 'accept_tnc_privacy')
                ->addData([
                    'attribute_set_id' => $attributeSetId,
                    'attribute_group_id' => $attributeGroupId,
                    'used_in_forms' => ['adminhtml_customer', 'customer_account_edit', 'customer_account_create'],//you can use other forms also ['adminhtml_customer_address', 'customer_address_edit', 'customer_register_address']
                ]);

            $attribute->save();
        }
    }

    protected function createContactByEmailAttribute($setup)
    {
        /** @var CustomerSetup $customerSetup */
        $customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);

        $customerEntity = $customerSetup->getEavConfig()->getEntityType('customer');
        $attributeSetId = $customerEntity->getDefaultAttributeSetId();

        /** @var $attributeSet AttributeSet */
        $attributeSet = $this->attributeSetFactory->create();
        $attributeGroupId = $attributeSet->getDefaultGroupId($attributeSetId);

        $isAttributeExist = $this->eavSetup->getAttribute('1', 'contact_by_email');

        if (!$isAttributeExist) {

            $customerSetup->addAttribute(Customer::ENTITY, 'contact_by_email', [
                'type' => 'int',
                'label' => 'Contact By Email',
                'input' => 'boolean',
                'required' => false,
                'visible' => true,
                'user_defined' => true,
                'position' => 982,
                'system' => 0,
            ]);

            $attribute = $customerSetup->getEavConfig()->getAttribute(Customer::ENTITY, 'contact_by_email')
                ->addData([
                    'attribute_set_id' => $attributeSetId,
                    'attribute_group_id' => $attributeGroupId,
                    'used_in_forms' => ['adminhtml_customer', 'customer_account_edit', 'customer_account_create'],//you can use other forms also ['adminhtml_customer_address', 'customer_address_edit', 'customer_register_address']
                ]);

            $attribute->save();
        }
    }

    protected function createContactBySMSAttribute($setup)
    {
        /** @var CustomerSetup $customerSetup */
        $customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);

        $customerEntity = $customerSetup->getEavConfig()->getEntityType('customer');
        $attributeSetId = $customerEntity->getDefaultAttributeSetId();

        /** @var $attributeSet AttributeSet */
        $attributeSet = $this->attributeSetFactory->create();
        $attributeGroupId = $attributeSet->getDefaultGroupId($attributeSetId);

        $isAttributeExist = $this->eavSetup->getAttribute('1', 'contact_by_sms');

        if (!$isAttributeExist) {

            $customerSetup->addAttribute(Customer::ENTITY, 'contact_by_sms', [
                'type' => 'int',
                'label' => 'Contact By SMS',
                'input' => 'boolean',
                'required' => false,
                'visible' => true,
                'user_defined' => true,
                'position' => 983,
                'system' => 0,
            ]);

            $attribute = $customerSetup->getEavConfig()->getAttribute(Customer::ENTITY, 'contact_by_sms')
                ->addData([
                    'attribute_set_id' => $attributeSetId,
                    'attribute_group_id' => $attributeGroupId,
                    'used_in_forms' => ['adminhtml_customer', 'customer_account_edit', 'customer_account_create'],//you can use other forms also ['adminhtml_customer_address', 'customer_address_edit', 'customer_register_address']
                ]);

            $attribute->save();
        }
    }

    protected function createContactInAppAttribute($setup)
    {
        /** @var CustomerSetup $customerSetup */
        $customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);

        $customerEntity = $customerSetup->getEavConfig()->getEntityType('customer');
        $attributeSetId = $customerEntity->getDefaultAttributeSetId();

        /** @var $attributeSet AttributeSet */
        $attributeSet = $this->attributeSetFactory->create();
        $attributeGroupId = $attributeSet->getDefaultGroupId($attributeSetId);

        $isAttributeExist = $this->eavSetup->getAttribute('1', 'contact_in_app');

        if (!$isAttributeExist) {

            $customerSetup->addAttribute(Customer::ENTITY, 'contact_in_app', [
                'type' => 'int',
                'label' => 'Contact In App',
                'input' => 'boolean',
                'required' => false,
                'visible' => true,
                'user_defined' => true,
                'position' => 984,
                'system' => 0,
            ]);

            $attribute = $customerSetup->getEavConfig()->getAttribute(Customer::ENTITY, 'contact_in_app')
                ->addData([
                    'attribute_set_id' => $attributeSetId,
                    'attribute_group_id' => $attributeGroupId,
                    'used_in_forms' => ['adminhtml_customer', 'customer_account_edit', 'customer_account_create'],//you can use other forms also ['adminhtml_customer_address', 'customer_address_edit', 'customer_register_address']
                ]);

            $attribute->save();
        }
    }

    protected function createPasswordCheckAttribute($setup)
    {
        /** @var CustomerSetup $customerSetup */
        $customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);

        $customerEntity = $customerSetup->getEavConfig()->getEntityType('customer');
        $attributeSetId = $customerEntity->getDefaultAttributeSetId();

        /** @var $attributeSet AttributeSet */
        $attributeSet = $this->attributeSetFactory->create();
        $attributeGroupId = $attributeSet->getDefaultGroupId($attributeSetId);

        $isAttributeExist = $this->eavSetup->getAttribute('1', 'contact_in_app');

        if (!$isAttributeExist) {

            $customerSetup->addAttribute(Customer::ENTITY, 'is_reset_password_sent', [
                'type' => 'int',
                'label' => 'Is Reset Password Sent',
                'input' => 'boolean',
                'required' => false,
                'visible' => false,
                'user_defined' => true,
                'type' => 'static',
                'position' => 985,
                'system' => 0,
            ]);

            $attribute = $customerSetup->getEavConfig()->getAttribute(Customer::ENTITY, 'is_reset_password_sent')
                ->addData([
                    'attribute_set_id' => $attributeSetId,
                    'attribute_group_id' => $attributeGroupId,
                    'used_in_forms' => ['adminhtml_customer', 'customer_account_edit', 'customer_account_create'],//you can use other forms also ['adminhtml_customer_address', 'customer_address_edit', 'customer_register_address']
                ]);

            $attribute->save();
        }
    }

}
