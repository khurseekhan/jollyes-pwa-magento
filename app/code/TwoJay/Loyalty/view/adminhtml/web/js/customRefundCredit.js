require([
    'jquery'
], function ($) {
    'use strict';

    $(document).ready(function(){
        var finalTotal = $('.order-subtotal-table > tfoot > price').val();
        $('#creditmemo_customerbalance_return').val(finalTotal.slice(1));
    });

});