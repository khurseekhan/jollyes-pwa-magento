/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'ko',
    'Magento_Checkout/js/model/url-builder',
    'mage/storage',
    '../model/payment/loyalty-coupon-messages',
    'TwoJay_Loyalty/js/model/loyalty-coupon',
    'Magento_Customer/js/model/customer',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/model/error-processor'
], function (ko, urlBuilder, storage, messageList, loyaltyCoupon, customer, quote, errorProcessor) {
    'use strict';

    return {
        isLoading: ko.observable(false),

        /**
         * @param {*} loyaltyCouponCode
         */
        check: function (loyaltyCouponCode) {
            var self = this,
                serviceUrl;

            this.isLoading(true);

            if (!customer.isLoggedIn()) {
                serviceUrl = urlBuilder.createUrl('/carts/guest-carts/:cartId/checkLoyaltyCoupon/:loyaltyCouponCode', {
                    cartId: quote.getQuoteId(),
                    loyaltyCouponCode: loyaltyCouponCode
                });
            } else {
                serviceUrl = urlBuilder.createUrl('/carts/mine/checkLoyaltyCoupon/:loyaltyCouponCode', {
                    loyaltyCouponCode: loyaltyCouponCode
                });

            }
            messageList.clear();
            storage.get(
                serviceUrl, false
            ).done(function (response) {
                loyaltyCoupon.isChecked(true);
                loyaltyCoupon.code(loyaltyCouponCode);
                loyaltyCoupon.amount(response);
                loyaltyCoupon.isValid(true);
            }).fail(function (response) {
                loyaltyCoupon.isValid(false);
                console.log('Get Loyalty Coupon response : '+response);
                console.log(messageList);
                errorProcessor.process(response, messageList);
            }).always(function () {
                self.isLoading(false);
            });
        }
    };
});
