/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */

define([
    'jquery',
    'ko',
    'uiComponent',
    'Magento_Customer/js/model/customer',
    'TwoJay_Loyalty/js/action/set-loyalty-coupon-information',
    'TwoJay_Loyalty/js/action/get-loyalty-coupon-information',
    'Magento_Checkout/js/model/totals',
    'TwoJay_Loyalty/js/model/loyalty-coupon',
    'Magento_Checkout/js/model/quote',
    'Magento_Catalog/js/price-utils',
    'mage/validation'
], function ($, ko, Component, customer, setLoyaltyCouponAction, getLoyaltyCouponAction, totals, loyaltyCoupon, quote, priceUtils) {
    'use strict';

    return Component.extend({
        defaults: {
            template: 'TwoJay_Loyalty/payment/loyalty-coupon',
            loyaltyCartCode: ''
        },
        isLoading: getLoyaltyCouponAction.isLoading,
        loyaltyCoupon: loyaltyCoupon,

        /** @inheritdoc */
        initObservable: function () {
            this._super()
                .observe('loyaltyCartCode');

            return this;
        },

        /**
         * Set loyalty coupon.
         */
        setLoyaltyCoupon: function () {
            if (this.validate()) {
                setLoyaltyCouponAction([this.loyaltyCartCode()]);
            }
        },

        /**
         * Check balance.
         */
        checkBalance: function () {
            if (this.validate()) {
                getLoyaltyCouponAction.check(this.loyaltyCartCode());
            }
        },

        /**
         * @param {*} price
         * @return {String|*}
         */
        getAmount: function (price) {
            return priceUtils.formatPrice(price, quote.getPriceFormat());
        },

        /**
         * @return {jQuery}
         */
        validate: function () {
            var form = '#loyaltycoupon-form';

            return $(form).validation() && $(form).validation('isValid');
        },

        /**
         * @return {Object|Boolean}
         */
        isCustomerLoggedIn: function () {
            if (customer.isLoggedIn()) {
                return true;
            } else {
                return false;
            }
        },

        /**
         * check if loyalty module is enabled or not
         *
         * @return {Object|Boolean}
         */
        isLoyaltyModuleEnabled: function () {
            if (window.checkoutConfig.loyalty_module_status) {
                return true;
            } else {
                return false;
            }
        }
    });
});
