/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */
define(
    [
        'jquery',
        'Magento_Checkout/js/model/url-builder',
        'mage/storage',
        'Magento_Customer/js/model/customer',
        'Magento_Checkout/js/model/quote',
        'Magento_Checkout/js/action/get-payment-information',
        'Magento_Checkout/js/model/full-screen-loader',
        'Magento_Checkout/js/model/error-processor',
        'TwoJay_Loyalty/js/model/payment/loyalty-coupon-messages'
    ],
    function ($,
              urlBuilder,
              storage,
              customer,
              quote,
              getPaymentInformationAction,
              fullScreenLoader,
              errorProcessor,
              messageList) {
        'use strict';

        return function (loyaltyCoupon) {
            var serviceUrl;
            var message;
            message = 'PetCLUB Voucher ' + loyaltyCoupon + ' was removed.';

            if (!customer.isLoggedIn()) {
                serviceUrl = urlBuilder.createUrl('/carts/guest-carts/:cartId/loyaltyCoupons/:loyaltyCoupon', {
                    cartId: quote.getQuoteId(),
                    loyaltyCoupon: loyaltyCoupon
                });
            } else {
                serviceUrl = urlBuilder.createUrl('/carts/mine/loyaltyCoupons/:loyaltyCoupon', {
                    loyaltyCoupon: loyaltyCoupon
                });
            }

            fullScreenLoader.startLoader();

            return storage.delete(
                serviceUrl
            ).done(
                function (response) {
                    response = JSON.parse(response);
                    if (response.status == true) {
                        $.when(getPaymentInformationAction()).always(function () {
                            messageList.addSuccessMessage({
                                'message': message
                            });
                            fullScreenLoader.stopLoader();
                        });
                    } else {
                        messageList.addErrorMessage({
                            'message': response.message
                        });
                    }
                }
            ).fail(
                function (response) {
                    response = JSON.parse(response);
                    errorProcessor.process(response, messageList);
                    fullScreenLoader.stopLoader();
                }
            );
        };
    }
);
