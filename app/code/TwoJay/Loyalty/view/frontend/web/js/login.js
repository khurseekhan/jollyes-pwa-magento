/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */

require(['jquery'], function ($) {
    $(document).ready(function () {
        if (window.customConfig.loyalty_module_status) {
            $(".form-login > .login > .reset-password").hide();
            $(".form-login > .login > .actions-toolbar").hide();
            $(".form-login > .login > .password").hide();

            var username = $("#email").val();
            var emailFlag;
            var isLoyaltyCustomer;
            var timer, delay = 1000;

            //validate email with regex
            var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if (!regex.test(username)) {
                emailFlag = false;
            } else {
                emailFlag = true;
            }

            if (username.length != 0 && emailFlag && username.trim()) {
                $(".form-login > .login > .actions-toolbar").show();
                $(".form-login > .login > .password").show();
            }

            // email check ajax call
            $('#email').on('change input', function (e) {
                username = $("#email").val();

                clearTimeout(timer);
                timer = setTimeout(function() {

                    //validate email with regex
                    if (!regex.test(username)) {
                        emailFlag = false;
                    } else {
                        emailFlag = true;
                    }

                    if (username.length != 0 && emailFlag && username.trim()) {

                            // check local browser storage
                            if (localStorage.getItem(username)) {
                                isLoyaltyCustomer = localStorage.getItem(username);
                                // if loyalty number exists that is equal to response as 1, show password field
                                if (isLoyaltyCustomer == 1) {
                                    $("#loyalty-email-error").hide();
                                    $(".form-login > .login > .password").show();
                                    $(".form-login > .login > .actions-toolbar").show();
                                } else if(isLoyaltyCustomer == 2) {
                                    // if loyalty number is equal to response as 2, show resetpassword UI and hide password field
                                    $(".form-login > .login > .password").hide();
                                    $(".form-login > .login > .actions-toolbar").hide();
                                    $(".reset-password").show();
                                    $("#reset-password").attr("data-email", username);
                                } else {
                                    $(".reset-password").hide();
                                    $(".form-login > .login > .password").hide();
                                    $(".form-login > .login > .actions-toolbar").hide();
                                }

                                return;
                            }
                            $('body').loader('show');

                            var loyaltyCheckUrl = document.location.origin + "/loyalty/login/post";
                            jQuery.ajax({
                                type: "POST",
                                url: loyaltyCheckUrl,
                                dataType: 'json',
                                data: {
                                    email: username
                                },
                                complete: function (response) {
                                    //console.log(response.responseJSON);
                                    if (response.responseJSON.status_code && response.responseJSON.status_code == 503) {
                                        if (response.responseJSON.customCheck && response.responseJSON.customCheck == true) {
                                            $("#loyalty-email-error").hide();
                                            $(".form-login > .login > .password").show();
                                            $(".form-login > .login > .actions-toolbar").show();
                                        } else {
                                            $("#loyalty-email-error").text(response.responseJSON.message).css("color", "red");
                                            $("#loyalty-email-error").show(0).delay(3000).hide(0);
                                            $(".form-login > .login > .password").hide();
                                            $(".form-login > .login > .actions-toolbar").hide();
                                        }
                                    } else if (response.responseJSON.errors && response.responseJSON.errors == true) {
                                        //console.log(response.responseJSON.message);
                                        $("#loyalty-email-error").text(response.responseJSON.message).css("color", "red");
                                        $("#loyalty-email-error").show(0).delay(3000).hide(0);
                                        $(".form-login > .login > .password").hide();
                                        $(".form-login > .login > .actions-toolbar").hide();
                                    } else if (response.responseJSON.customCheck && response.responseJSON.customCheck == true) {
                                        $(".form-login > .login > .password").hide();
                                        $(".form-login > .login > .actions-toolbar").hide();
                                        return true;
                                    } else {
                                        isLoyaltyCustomer = response.responseJSON.is_active;
                                        localStorage.setItem(username, isLoyaltyCustomer);

                                        // if loyalty number exists that is equal to response as 1, show password field
                                        if (isLoyaltyCustomer == 1) {
                                            $("#loyalty-email-error").hide();
                                            $(".form-login > .login > .password").show();
                                            $(".form-login > .login > .actions-toolbar").show();
                                        } else if(isLoyaltyCustomer == 2) {
                                            // if loyalty number is equal to response as 2, show resetpassword UI and hide password field
                                            $(".form-login > .login > .password").hide();
                                            $(".form-login > .login > .actions-toolbar").hide();
                                            $(".reset-password").show();
                                            $("#reset-password").attr("data-email", username);
                                        } else {
                                            $(".reset-password").hide();
                                            $(".form-login > .login > .password").hide();
                                            $(".form-login > .login > .actions-toolbar").hide();
                                        }
                                    }
                                },
                                error: function (xhr, status, errorThrown) {
                                    if (typeof console != "undefined") {
                                        console.log('Loyalty post check error Occured. Try again.');
                                    }
                                }
                            });
                    } else {
                        $(".reset-password").hide();
                        $(".form-login > .login > .password").hide();
                        $(".form-login > .login > .actions-toolbar").hide();
                    }
                    $('body').loader('hide');
                }, delay);
            });
            // reset password ajax call
            $("#reset-password").click(function () {
                var dataEmail = $('#reset-password').data('email');
                if (dataEmail) {
                    var resetPwdUrl = document.location.origin + "/loyalty/login/resetPassword";
                    jQuery.ajax({
                        type: "POST",
                        url: resetPwdUrl,
                        dataType: 'json',
                        data: {
                            email: dataEmail
                        },
                        complete: function (response) {
                            var resetPassErrorsFlag = response.responseJSON.errors;
                            if (resetPassErrorsFlag == false) {
                                // show password field
                                $(".reset-password").hide();
                                $("#loyalty-email-error").hide();
                                $(".form-login > .login > .password").show();
                                $(".form-login > .login > .actions-toolbar").show();
                            }
                        },
                        error: function (xhr, status, errorThrown) {
                            if (typeof console != "undefined") {
                                console.log('Resetpassword Error Occured. Try again.');
                            }
                        }
                    });
                }
            });
        } else {
            $(".reset-password").hide();
        }
    });
});