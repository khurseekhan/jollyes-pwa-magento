/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
define(
    [
        'ko',
        'uiComponent',
        'Magento_Checkout/js/model/totals',
        'Magento_Checkout/js/model/quote',
        'Magento_Catalog/js/price-utils'
    ],
    function (
        ko,
        Component,
        totals,
        quote,
        priceUtils
    ) {
        'use strict';

        return Component.extend({
            defaults: {
                template: 'TwoJay_Loyalty/payment/loyalty-coupon-information'
            },
            isVisible: ko.observable(false),
            amountApplied: ko.observable(null),

            /**
             * Init component
             */
            initialize: function () {
                this._super();

                totals.totals.subscribe(function () {
                    var loyaltyCouponSegment = totals.getSegment('loyalty_coupons');
                    this.isVisible(loyaltyCouponSegment !== null);
                    if (loyaltyCouponSegment) {
                        this.amountApplied(this.getFormattedPrice(loyaltyCouponSegment.value * -1));
                    }

                }, this);
            },

            /**
             * @param {Number} price
             * @return {String}
             */
            getFormattedPrice: function (price) {
                return priceUtils.formatPrice(price, quote.getPriceFormat());
            }
        });
    }
);
