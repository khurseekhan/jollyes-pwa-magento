/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'jquery',
    'ko',
    'Magento_Checkout/js/view/summary/abstract-total',
    'mage/url',
    'Magento_Checkout/js/model/totals',
    'TwoJay_Loyalty/js/action/remove-loyalty-coupon-from-quote'
], function ($, ko, generic, url, totals, removeAction) {
    'use strict';

    return generic.extend({
        defaults: {
            template: 'TwoJay_Loyalty/summary/loyalty_coupon'
        },

        /**
         * Get information about applied loyalty coupons and their amounts
         *
         * @returns {Array}.
         */
        getAppliedLoyaltyCoupons: function () {
            var segmentCoupons = totals.getSegment('loyalty_coupons');
            if (segmentCoupons) {
                return JSON.parse(segmentCoupons['extension_attributes']['loyalty_coupons']);
            }

            return [];
        },

        /**
         * @return {Object|Boolean}
         */
        isAvailable: function () {
            if (totals.getSegment('loyalty_coupons')) {
                return true;
            } else {
                return false;
            }
        },

        /**
         * @param {Number} usedBalance
         * @return {*|String}
         */
        getAmount: function (usedBalance) {
            return this.getFormattedPrice(usedBalance);
        },

        /**
         * @param {String} loyaltyCouponCode
         * @param {Object} event
         */
        removeLoyaltyCoupon: function (loyaltyCouponCode, event) {
            event.preventDefault();

            if (loyaltyCouponCode) {
                removeAction(loyaltyCouponCode);
            }
        },

        /**
         * check if loyalty module is enabled or not
         *
         * @return {Object|Boolean}
         */
        isLoyaltyModuleEnabled: function () {
            if (window.checkoutConfig.loyalty_module_status) {
                return true;
            } else {
                return false;
            }
        }
    });
});
