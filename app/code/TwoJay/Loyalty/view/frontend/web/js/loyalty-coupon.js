/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'jquery',
    'jquery/ui',
    'mage/validation'
], function ($) {
    'use strict';

    $.widget('mage.loyaltyCoupon', {
        /**
         * @private
         */
        _create: function () {
            $(this.options.checkStatus).on('click', $.proxy(function () {
                var loyaltyCouponStatusId, loyaltyCouponSpinnerId, messages;

                if (this.element.validation().valid()) {
                    loyaltyCouponStatusId = this.options.loyaltyCouponStatusId;
                    loyaltyCouponSpinnerId = $(this.options.loyaltyCouponSpinnerId);
                    messages = this.options.messages;
                    $.ajax({
                        url: this.options.loyaltyCouponStatusUrl,
                        type: 'post',
                        cache: false,
                        data: {
                            'loyaltycoupon_code': $(this.options.loyaltyCouponCodeSelector).val()
                        },

                        /**
                         * Before send.
                         */
                        beforeSend: function () {
                            loyaltyCouponSpinnerId.show();
                        },

                        /**
                         * @param {*} response
                         */
                        success: function (response) {
                            $(messages).hide();
                            $(loyaltyCouponStatusId).html(response);
                        },

                        /**
                         * Complete.
                         */
                        complete: function () {
                            loyaltyCouponSpinnerId.hide();
                        }
                    });
                }
            }, this));
        }
    });

    return $.mage.loyaltyCoupon;
});
