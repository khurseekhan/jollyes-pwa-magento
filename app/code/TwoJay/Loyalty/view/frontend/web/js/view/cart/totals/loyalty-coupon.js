/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'TwoJay_Loyalty/js/view/summary/loyalty-coupon',
    'mage/url',
    'Magento_Checkout/js/model/totals'
], function (Component, url, totals) {
    'use strict';

    return Component.extend({
        defaults: {
            template: 'TwoJay_Loyalty/cart/totals/loyalty-coupon'
        },

        /**
         * @param {String} loyaltyCouponCode
         * @return {*|Boolean|Object|jQuery}
         */
        getRemoveUrl: function (loyaltyCouponCode) {
            return url.build('loyalty/voucher/remove/code/' + loyaltyCouponCode);
        },

        /**
         * @override
         *
         * @returns {bool}
         */
        isAvailable: function () {
            if((totals.getSegment('loyalty_coupons'))){
                return true;
            } else {
                return false;
            }
        }
    });
});
