/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */

define([
    'jquery',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/model/url-builder',
    'mage/storage',
    '../model/payment/loyalty-coupon-messages',
    'Magento_Checkout/js/model/error-processor',
    'Magento_Customer/js/model/customer',
    'Magento_Checkout/js/model/full-screen-loader',
    'Magento_Checkout/js/action/get-payment-information',
    'Magento_Checkout/js/model/totals'
], function ($,
             quote,
             urlBuilder,
             storage,
             messageList,
             errorProcessor,
             customer,
             fullScreenLoader,
             getPaymentInformationAction,
             totals) {
    'use strict';

    return function (loyaltyCouponCode) {
        var serviceUrl,
            payload,
            message = 'PetCLUB Voucher ' + loyaltyCouponCode + ' was added.';

        /**
         * Checkout for guest and registered customer.
         */
        if (!customer.isLoggedIn()) {
            serviceUrl = urlBuilder.createUrl('/carts/guest-carts/:cartId/loyaltyCoupons', {
                cartId: quote.getQuoteId()
            });
            payload = {
                cartId: quote.getQuoteId(),
                //loyaltyCouponCode: loyaltyCouponCode
                loyaltyCouponData: {
                    'loyalty_coupons': loyaltyCouponCode
                }
            };
        } else {
            serviceUrl = urlBuilder.createUrl('/carts/mine/loyaltyCoupons', {});
            payload = {
                //loyaltyCouponCode: loyaltyCouponCode
                loyaltyCouponData: {
                    'loyalty_coupons': loyaltyCouponCode
                }
            };
        }
        messageList.clear();
        fullScreenLoader.startLoader();
        storage.post(
            serviceUrl, JSON.stringify(payload)
        ).done(function (response) {
            var deferred = $.Deferred();
            response = JSON.parse(response);
            if (response.status == true) {

                totals.isLoading(true);
                getPaymentInformationAction(deferred);
                $.when(deferred).done(function () {
                    totals.isLoading(false);
                });
                messageList.addSuccessMessage({
                    'message': message
                });
            } else {
                messageList.addErrorMessage({
                    'message': response.message
                });
            }
        }).fail(function (response) {
            response = JSON.parse(response);
            totals.isLoading(false);
            errorProcessor.process(response, messageList);
        }).always(function () {
            fullScreenLoader.stopLoader();
        });
    };
});
