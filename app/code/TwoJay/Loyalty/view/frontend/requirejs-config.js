/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

var config = {
    map: {
        '*': {
            loyaltyCoupon: 'TwoJay_Loyalty/js/loyalty-coupon',
            paymentMethod: 'TwoJay_Loyalty/js/payment-method',
            loyaltyListToolbarForm: 'TwoJay_Loyalty/js/list/toolbar',
        }
    }
};
