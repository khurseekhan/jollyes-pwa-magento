<?php

namespace TwoJay\Loyalty\Api\Loyalty;

/**
 * Interface GetCustomerTransactionsInterface
 *
 * Service Contract that gets customer transactions
 */
interface GetCustomerTransactionsInterface
{
    /**
     * Request API to get the customer transactions
     *
     * @param $loyaltyNumber
     *
     * @return transactions array
     */
    public function getCustomerTransactions($loyaltyNumber);
}