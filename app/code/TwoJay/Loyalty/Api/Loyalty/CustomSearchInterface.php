<?php

namespace TwoJay\Loyalty\Api\Loyalty;

/**
 * Interface CustomSearchInterface
 *
 * Service Contract to search customer details in loyalty system
 */
interface CustomSearchInterface
{
    /**
     * Request API to search customer details in loyalty system
     *
     * @param string $param
     *
     * @return string
     */
    public function customSearch($param);
}