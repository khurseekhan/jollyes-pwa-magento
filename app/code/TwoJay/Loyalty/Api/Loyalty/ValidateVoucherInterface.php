<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */

namespace TwoJay\Loyalty\Api\Loyalty;

/**
 * Interface ValidateVoucherInterface
 *
 * Service Contract to validate voucher in loyalty system
 */
interface ValidateVoucherInterface
{
    /**
     * Request API to validate voucher in loyalty system
     *
     * @param string $param
     *
     * @return string
     */
    public function validateVoucher($param);
}