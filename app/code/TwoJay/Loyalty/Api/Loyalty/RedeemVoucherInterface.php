<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */

namespace TwoJay\Loyalty\Api\Loyalty;

/**
 * Interface RedeemVoucherInterface
 *
 * Service Contract to redeem voucher in loyalty system
 */
interface RedeemVoucherInterface
{
    /**
     * Request API to redeem voucher in loyalty system
     *
     * @param string $param
     *
     * @return string
     */
    public function redeemVoucher($param);
}