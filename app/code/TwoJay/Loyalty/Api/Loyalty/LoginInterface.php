<?php

namespace TwoJay\Loyalty\Api\Loyalty;

/**
 * Interface LoginInterface
 *
 * Service Contract to check login for customer
 */
interface LoginInterface
{
    /**
     * Request API to check login for customer
     *
     * @param string $params
     * @return string
     */
    public function login($params);
}