<?php

namespace TwoJay\Loyalty\Api\Loyalty;

/**
 * Interface GetCustomerVouchersInterface
 *
 * Service Contract that gets customer transactions
 */
interface GetCustomerVouchersInterface
{

    /**
     * Request API to get the customer vouchers
     *
     * @param  $params string
     * @return vouchers array
     */
    public function getCustomerVouchers($params);
}