<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */
namespace TwoJay\Loyalty\Api\Loyalty;

/**
 * Interface RegisterInterface
 *
 * Service Contract to check registration for customer
 */
interface RegisterInterface
{
    /**
     * API Request For Register a customer
     *
     * @param string[] $params
     *
     * @return boolean
     */
    public function register($params);
}