<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */

namespace TwoJay\Loyalty\Api\Loyalty;

/**
 * Interface EntryPointInterface
 */
interface EntryPointInterface
{
    const XPATH_ENTRY_POINT_BASE_URL    = 'loyalty/entry_points/base_url';

    const XPATH_ENTRY_POINT_PREFIX      = 'loyalty/entry_points/base_url';

    const XPATH_ENTRY_POINT_USER_NAME   = 'loyalty/entry_points/user';

    const XPATH_ENTRY_POINT_PASSWORD   = 'loyalty/entry_points/password';

    const XPATH_ENTRY_POINT_CLIENT_ID   = 'loyalty/entry_points/client_id';

    const XPATH_ENTRY_POINT_GRANT_TYPE   = 'loyalty/entry_points/grant_type';

    /**
     * Get base Url of service
     *
     * @return string
     */
    public function getBaseUrl();

    /**
     * Get URL of entry points by name
     *
     * @param string $key
     * @return string
     */
    public function getEntryPointUrl();
}