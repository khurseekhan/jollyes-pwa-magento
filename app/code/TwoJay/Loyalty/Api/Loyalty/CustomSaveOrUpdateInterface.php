<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */

namespace TwoJay\Loyalty\Api\Loyalty;

/**
 * Interface CustomSaveOrUpdateInterface
 *
 * Service Contract to update customer data in loyalty system
 */
interface CustomSaveOrUpdateInterface
{
    /**
     * Request API to update customer data in loyalty system
     *
     * @param string $param
     *
     * @return string
     */
    public function customSaveOrUpdate($param);

}