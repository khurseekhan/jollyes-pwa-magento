<?php

namespace TwoJay\Loyalty\Api\Loyalty;

/**
 * Interface ResetPasswordInterface
 *
 * Service Contract to send new resetted password to customer email
 */
interface ResetPasswordInterface
{
    /**
     * Request API to send new password to customer email
     *
     * @param string[] $param
     *
     * @return boolean
     */
    public function resetPassword($param);
}