<?php

namespace TwoJay\Loyalty\Api\Loyalty;

/**
 * Interface IsLoyaltyMemberInterface
 *
 * Service Contract that gets customer loyalty number from Itim
 */
interface IsLoyaltyMemberInterface
{
    /**
     * Request API to get the loyalty number
     *
     * @param string $emailOrPhone
     *
     * @return loyaltyNumber
     */
    public function getLoyaltyNumber($emailOrPhone);
}