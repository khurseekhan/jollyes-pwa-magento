<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */

namespace TwoJay\Loyalty\Api\Data;

/**
 * Interface BaseCouponInterface
 * @package TwoJay\Loyalty\Api\Data
 */
interface BaseCouponInterface
{
    /**
     * Get Code
     *
     * @return string
     * @since 100.2.0
     */
    public function getCode();

    /**
     * Set Code
     *
     * @param string $code
     * @return $this
     * @since 100.2.0
     */
    public function setCode($code);

    /**
     * Get Amount
     *
     * @return float
     * @since 100.2.0
     */
    public function getAmount();

    /**
     * Set Amount
     *
     * @param float $amount
     * @return $this
     * @since 100.2.0
     */
    public function setAmount($amount);

    /**
     * Get Base Amount
     *
     * @return float
     * @since 100.2.0
     */
    public function getBaseAmount();

    /**
     * Set Base Amount
     *
     * @param float $baseAmount
     * @return $this
     * @since 100.2.0
     */
    public function setBaseAmount($baseAmount);
}
