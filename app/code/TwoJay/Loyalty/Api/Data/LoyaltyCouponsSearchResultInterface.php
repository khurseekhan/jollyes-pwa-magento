<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */

namespace TwoJay\Loyalty\Api\Data;

/**
 * Interface LoyaltyCouponsSearchResultInterface
 * @api
 * @since 100.0.2
 */
interface LoyaltyCouponsSearchResultInterface extends \Magento\Framework\Api\SearchResultsInterface
{
    /**
     * Get Loyalty Coupons list
     *
     * @return \TwoJay\Loyalty\Api\Data\LoyaltyCouponsInterface[]
     */
    public function getItems();
}
