<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */

namespace TwoJay\Loyalty\Api\Data;

/**
 * Loyalty Coupons data
 *
 * @codeCoverageIgnore
 * @api
 * @since 100.0.2
 */
interface LoyaltyCouponsInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{
    /**
     * Loyalty Coupons codes
     *
     * @return string[]
     */
    public function getLoyaltyCoupons();

    /**
     * Set Loyalty Coupons codes
     *
     * @param array $cards
     * @return \TwoJay\Loyalty\Api\Data\LoyaltyCouponsInterface
     */
    public function setLoyaltyCoupons(array $cards);

    /**
     * Loyalty Coupons amount in quote currency
     *
     * @return float
     */
    public function getLoyaltyCouponsAmount();

    /**
     * Set Loyalty Coupons amount in quote currency
     *
     * @param float $amount
     * @return \TwoJay\Loyalty\Api\Data\LoyaltyCouponsInterface
     */
    public function setLoyaltyCouponsAmount($amount);

    /**
     * Loyalty Coupons amount in base currency
     *
     * @return float
     */
    public function getBaseLoyaltyCouponsAmount();

    /**
     * Set Loyalty Coupons amount in base currency
     *
     * @param float $amount
     * @return \TwoJay\Loyalty\Api\Data\LoyaltyCouponsInterface
     */
    public function setBaseLoyaltyCouponsAmount($amount);

    /**
     * Loyalty Coupons amount used in quote currency
     *
     * @return float
     */
    public function getLoyaltyCouponsAmountUsed();

    /**
     * Set Loyalty Coupons amount used in quote currency
     *
     * @param float $amount
     * @return \TwoJay\Loyalty\Api\Data\LoyaltyCouponsInterface
     */
    public function setLoyaltyCouponsAmountUsed($amount);

    /**
     * Loyalty Coupons amount used in base currency
     *
     * @return float
     */
    public function getBaseLoyaltyCouponsAmountUsed();

    /**
     * Set Loyalty Coupons amount used in base currency
     *
     * @param float $amount
     * @return \TwoJay\Loyalty\Api\Data\LoyaltyCouponsInterface
     */
    public function setBaseLoyaltyCouponsAmountUsed($amount);

    /**
     * Retrieve existing extension attributes object or create a new one.
     *
     * @return \TwoJay\Loyalty\Api\Data\LoyaltyCouponsExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     *
     * @param LoyaltyCouponsExtensionInterface $extensionAttributes
     * @return mixed
     */
    public function setExtensionAttributes(
        \TwoJay\Loyalty\Api\Data\LoyaltyCouponsExtensionInterface $extensionAttributes
    );
}
