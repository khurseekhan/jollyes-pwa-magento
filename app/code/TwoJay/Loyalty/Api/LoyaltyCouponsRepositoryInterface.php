<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */

namespace TwoJay\Loyalty\Api;

/**
 * Interface LoyaltyCouponsRepositoryInterface
 * @package TwoJay\Loyalty\Api
 */
interface LoyaltyCouponsRepositoryInterface
{
    /**
     * Return data object for specified Loyalty Coupons id
     *
     * @param int $id
     * @return \TwoJay\Loyalty\Api\Data\LoyaltyCouponsInterface
     */
    public function get($id);

    /**
     * Return list of Loyalty Coupons data objects based on search criteria
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \TwoJay\Loyalty\Api\Data\LoyaltyCouponsSearchResultInterface
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * Save Loyalty Coupons
     *
     * @param \TwoJay\Loyalty\Api\Data\LoyaltyCouponsInterface $loyaltyCouponsDataObject
     * @return \TwoJay\Loyalty\Api\Data\LoyaltyCouponsInterface
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function save(\TwoJay\Loyalty\Api\Data\LoyaltyCouponsInterface $loyaltyCouponsDataObject);

    /**
     * Delete Loyalty Coupons Account
     *
     * @param \TwoJay\Loyalty\Api\Data\LoyaltyCouponsInterface $loyaltyCouponsDataObject
     * @return bool
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function delete(\TwoJay\Loyalty\Api\Data\LoyaltyCouponsInterface $loyaltyCouponsDataObject);
}
