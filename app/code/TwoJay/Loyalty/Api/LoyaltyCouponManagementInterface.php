<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */

namespace TwoJay\Loyalty\Api;

/**
 * Interface LoyaltyCouponManagementInterface
 * @package TwoJay\Loyalty\Api
 */
interface LoyaltyCouponManagementInterface
{

    /**
     * Remove LoyaltyCoupon Account entity
     *
     * @param int $cartId
     * @param string $loyaltyCouponCode
     * @return bool
     */
    public function deleteByQuoteId($cartId, $loyaltyCouponCode);

    /**
     * Return LoyaltyCoupon Account cards
     *
     * @param int $quoteId
     * @return \TwoJay\Loyalty\Api\Data\LoyaltyCouponsInterface
     */
    public function getListByQuoteId($quoteId);

    /**
     * @param int $cartId
     * @param \TwoJay\Loyalty\Api\Data\LoyaltyCouponsInterface $loyaltyCouponData
     * @return bool
     */
    public function saveByQuoteId(
        $cartId,
        \TwoJay\Loyalty\Api\Data\LoyaltyCouponsInterface $loyaltyCouponData
    );

    /**
     * @param int $cartId
     * @param string $loyaltyCouponCode
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @return float
     */
    public function checkLoyaltyCoupon($cartId, $loyaltyCouponCode);

}
