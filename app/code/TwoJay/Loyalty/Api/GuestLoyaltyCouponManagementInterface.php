<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */

namespace TwoJay\Loyalty\Api;

/**
 * Interface GuestLoyaltyCouponManagementInterface
 * @package TwoJay\Loyalty\Api
 */
interface GuestLoyaltyCouponManagementInterface
{
    /**
     * @param string $cartId
     * @param \TwoJay\Loyalty\Api\Data\LoyaltyCouponsInterface $loyaltyCouponData
     * @return bool
     */
    public function addLoyaltyCoupon(
        $cartId,
        \TwoJay\Loyalty\Api\Data\LoyaltyCouponsInterface $loyaltyCouponData
    );

    /**
     * @param string $cartId
     * @param string $loyaltyCouponCode
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @return float
     */
    public function checkLoyaltyCoupon($cartId, $loyaltyCouponCode);

    /**
     * Remove LoyaltyCoupon Account entity
     *
     * @param string $cartId
     * @param string $loyaltyCouponCode
     * @return bool
     * @since 100.1.0
     */
    public function deleteByQuoteId($cartId, $loyaltyCouponCode);
}
