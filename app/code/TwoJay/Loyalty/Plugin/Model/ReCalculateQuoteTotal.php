<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */

namespace TwoJay\Loyalty\Plugin\Model;

use Magento\Framework\Exception\InputException;
use TwoJay\Loyalty\Model\LoyaltyCoupons;
use Magento\Framework\Json\Helper\Data as JsonHelper;
use Magento\Framework\ObjectManagerInterface;
use Magento\Quote\Api\CartRepositoryInterface;
use TwoJay\Loyalty\Helper\Data;
use Magento\Checkout\Model\Session;

/**
 * Class ReCalculateQuoteTotal
 * @package TwoJay\Loyalty\Plugin\Model
 */
class ReCalculateQuoteTotal
{
    /**
     * @var CartRepositoryInterface
     */
    protected $quoteRepository;

    /**
     * @var null|Data
     */
    protected $_loyaltyCouponsData = null;

    /**
     * @var Session|null
     */
    protected $_checkoutSession = null;

    /**
     * @var JsonHelper
     */
    protected $_jsonHelper;

    /**
     * @var ObjectManagerInterface
     */
    protected $objectManagerInterface;

    /**
     * ReCalculateQuoteTotal constructor.
     * @param CartRepositoryInterface $quoteRepository
     * @param Data $loyaltyCouponsData
     * @param Session $checkoutSession
     * @param JsonHelper $jsonHelper
     * @param ObjectManagerInterface $objectManagerInterface
     */
    public function __construct(
        CartRepositoryInterface $quoteRepository,
        Data $loyaltyCouponsData,
        Session $checkoutSession,
        JsonHelper $jsonHelper,
        ObjectManagerInterface $objectManagerInterface
    )
    {
        $this->quoteRepository = $quoteRepository;
        $this->_loyaltyCouponsData = $loyaltyCouponsData;
        $this->_checkoutSession = $checkoutSession;
        $this->_jsonHelper = $jsonHelper;
        $this->objectManagerInterface = $objectManagerInterface;
    }

    /**
     * @param \Magento\Checkout\Model\ShippingInformationManagement $subject
     * @param $result
     * @param $cartId
     * @param \Magento\Checkout\Api\Data\ShippingInformationInterface $addressInformation
     * @return mixed
     * @throws InputException
     */
    public function afterSaveAddressInformation(
        \Magento\Checkout\Model\ShippingInformationManagement $subject,
        $result,
        $cartId,
        \Magento\Checkout\Api\Data\ShippingInformationInterface $addressInformation
    )
    {
        if (!$this->_loyaltyCouponsData->isModuleEnabled()) {
            return $result;
        }

        $saveQuote = true;
        try {
            $quote = $this->quoteRepository->getActive($cartId);

            if ($quote->getLoyaltyCoupons()) {
                $loyaltyCoupons = $this->_jsonHelper->jsonDecode($quote->getLoyaltyCoupons());

                $grandTotal = $quote->getGrandTotal();
                $newLoyaltyCoupons = [];
                if (!empty($loyaltyCoupons)) {
                    foreach ($loyaltyCoupons as $one) {
                        $amount = $one['initial_amount'];
                        if ($amount == $one['a']) {
                            $newLoyaltyCoupons[] = [
                                LoyaltyCoupons::CODE => $one['c'],
                                LoyaltyCoupons::AMOUNT => $one['a'],
                                LoyaltyCoupons::BASE_AMOUNT => $one['a'],
                                LoyaltyCoupons::INITIAL_AMOUNT => $one['initial_amount']
                            ];
                        } else {
                            if ($grandTotal < $amount && $one['a'] < $amount) {
                                $remainingAmount = $amount - $grandTotal;
                                $finalAmount = ($amount + $one['a']) - $remainingAmount;

                            } else  $finalAmount = $amount;

                            $newLoyaltyCoupons[] = [
                                LoyaltyCoupons::CODE => $one['c'],
                                LoyaltyCoupons::AMOUNT => $finalAmount,
                                LoyaltyCoupons::BASE_AMOUNT => $finalAmount,
                                LoyaltyCoupons::INITIAL_AMOUNT => $one['initial_amount']
                            ];
                        }

                    }
                }

                $this->_loyaltyCouponsData->setLoyaltyCoupons($quote, $newLoyaltyCoupons);

                if ($saveQuote) {
                    $this->quoteRepository->save($quote);
                    $quote = $this->_checkoutSession->getQuote();
                    $quote->collectTotals();

                }
            }
        } catch (\Exception $e) {
            throw new InputException(__('Unable to save shipping information. Please check input data.' . $e->getMessage()));
        }

        $totals = $result->getTotals();
        $r_loyaltyCoupon = [];
        $grandTotal = 0.0000;
        $loyaltyData = [];

        foreach ($totals->getTotalSegments() as $code => $eachsegment) {
            $data = $eachsegment->getData();
            if ($code == 'subtotal' || $code == 'shipping' || $code == 'discount') {
                $grandTotal += $data['value'];
            }

            if ($code == 'grand_total') {
                $gtSegment = $eachsegment;
            }

            if ($code == 'loyalty_coupons' && !empty($data['extension_attributes']->getLoyaltyCoupons())) {
                $r_loyaltyCoupon = $this->_jsonHelper->jsonDecode($data['extension_attributes']->getLoyaltyCoupons());
                $loyaltyData = $eachsegment;
            }

        }

        $remainingAmount = $grandTotal;
        $newLoyaltyCoupon = [];
        if (!empty($r_loyaltyCoupon)) {
            foreach ($r_loyaltyCoupon as $coupon) {
                if ($remainingAmount == 0.00) {
                    continue;
                }
                if ($remainingAmount >= $coupon['initial_amount']) {
                    $remainingAmount = $remainingAmount - $coupon['initial_amount'];
                    $coupon['a'] = $coupon['ba'] = $coupon['initial_amount'];
                } else {
                    $coupon['a'] = $coupon['ba'] = $remainingAmount;
                    $remainingAmount = 0.00;
                }
                $newLoyaltyCoupon[] = $coupon;
            }

            if ($remainingAmount == 0.00) {
                $methods = [];
                $isFreeExist = false;
                foreach ($result->getPaymentMethods() as $methodInstance) {
                    $methods[] = $methodInstance;
                    if ($methodInstance->getCode() == 'free') {
                        $isFreeExist = true;
                    }
                }

                if (!$isFreeExist) {
                    $paymentMethodInstanceFactory = $this->objectManagerInterface->create(
                        \Magento\Payment\Model\Method\InstanceFactory::class
                    );

                    $paymentMethodList = $this->objectManagerInterface->get(
                        \Magento\Payment\Api\PaymentMethodListInterface::class
                    );
                    $store = $quote ? $quote->getStoreId() : null;
                    foreach ($paymentMethodList->getActiveList($store) as $method) {
                        if ($method->getCode() == 'free') {
                            $methodInstance = $paymentMethodInstanceFactory->create($method);
                            $methods[] = $methodInstance;
                        }
                    }

                    $result->setPaymentMethods($methods);
                }
            }

            $data = $gtSegment->getData();
            $data['value'] = $remainingAmount;
            $gtSegment->setData($data);

            $jsonLoyaltyCoupon = $this->_loyaltyCouponsData->serializeData($newLoyaltyCoupon);

            $data = $loyaltyData->getData();
            $data['extension_attributes']->setLoyaltyCoupons($jsonLoyaltyCoupon);

            $loyaltyData->setData($data);
            $totalSegment = $totals->getTotalSegments();

            $totalSegment['loyalty_coupons'] = $loyaltyData;
            $totalSegment['grand_total'] = $gtSegment;

            $totals->setTotalSegments($totalSegment);
            $result->setTotals($totals);
        }

        return $result;
    }
}
