<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */

namespace TwoJay\Loyalty\Plugin\Model\Checkout;

use Magento\Framework\Json\Helper\Data as JsonHelper;
use Magento\Checkout\Model\Session;
use TwoJay\Loyalty\Helper\Data;

/**
 * Class ResetConfigProvider
 * @package TwoJay\Loyalty\Plugin\Model\Checkout
 */
class ResetConfigProvider
{
    /**
     * @var Session|null
     */
    protected $_checkoutSession = null;

    /**
     * @var JsonHelper
     */
    protected $_jsonHelper;

    /**
     * @var null|Data
     */
    protected $_loyaltyCouponsData = null;

    /**
     * ResetConfigProvider constructor.
     * @param Session $checkoutSession
     * @param Data $loyaltyCouponsData
     * @param JsonHelper $jsonHelper
     */
    public function __construct(
        Session $checkoutSession,
        Data $loyaltyCouponsData,
        JsonHelper $jsonHelper
    )
    {
        $this->_checkoutSession = $checkoutSession;
        $this->_jsonHelper = $jsonHelper;
        $this->_loyaltyCouponsData = $loyaltyCouponsData;
    }

    /**
     * @param \Magento\Checkout\Model\DefaultConfigProvider $subject
     * @param $result
     * @return mixed
     */
    public function afterGetConfig(
        \Magento\Checkout\Model\DefaultConfigProvider $subject,
        $result
    )
    {
        if (!$this->_loyaltyCouponsData->isModuleEnabled()) {
            return $result;
        }

        $quote = $this->_checkoutSession->getQuote();
        $segments = $result['totalsData']['total_segments'];
        $grandTotal = 0.00;
        $codeLoyaltyCoupon = null;
        foreach ($segments as $ndx => $segment) {
            $code = $segment['code'];
            if ($code == 'shipping' || $code == 'subtotal' || $code == 'giftwrapping' || $code == 'discount') {
                $grandTotal += $segment['value'];
            }

            if ($code == 'loyalty_coupons' && !empty($segments[$ndx]['extension_attributes']['loyalty_coupons'])) {
                $codeLoyaltyCoupon = $this->_jsonHelper->jsonDecode($segments[$ndx]['extension_attributes']['loyalty_coupons']);
            }
        }
        $remainingAmount = $grandTotal;
        $loyaltyCouponAmount = 0.00;
        $needToUpdate = false;
        $legalCodeLoyaltyCoupon = [];

        if (!empty($codeLoyaltyCoupon) && $grandTotal > 0.00) {
            foreach ($codeLoyaltyCoupon as $eachCoupon) {
                if ($remainingAmount == 0.00) {
                    // remove coupon
                    $needToUpdate = true;
                    continue;
                }
                if ($remainingAmount >= $eachCoupon['initial_amount']) {
                    $remainingAmount = $remainingAmount - $eachCoupon['initial_amount'];
                    $loyaltyCouponAmount += $eachCoupon['a'];
                    $eachCoupon['a'] = $eachCoupon['ba'] = $eachCoupon['initial_amount'];
                } else {
                    $eachCoupon['a'] = $eachCoupon['ba'] = $remainingAmount;
                    $loyaltyCouponAmount += $remainingAmount;
                    $remainingAmount = 0.00;
                }
                $legalCodeLoyaltyCoupon[] = $eachCoupon;
            }

            $result['totalsData']['total_segments'][$ndx]['extension_attributes']['loyalty_coupons'] = $this->_jsonHelper->jsonEncode($legalCodeLoyaltyCoupon);
            if ($needToUpdate) {
                $this->_loyaltyCouponsData->setLoyaltyCoupons($quote, $legalCodeLoyaltyCoupon);
                $quote->setLoyaltyCouponsAmount($loyaltyCouponAmount);
            }
        }

        return $result;
    }
}