<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */


namespace TwoJay\Loyalty\Plugin\Sales;

use Magento\Quote\Model\QuoteFactory;
use TwoJay\Loyalty\Helper\Data as LoyaltyHelper;

/**
 * class set loyalty coupon before place order
 *
 * @category TwoJay
 * @package  TwoJay_Loyalty
 * @module   Loyalty
 * @author   TwoJay Developer
 */
class OrderEmailAttachment
{
    /**
     * @var QuoteFactory
     */
    protected $quoteFactory;

    /**
     * @var LoyaltyHelper
     */
    protected $loyaltyHelper;

    /**
     * OrderEmailAttachment constructor.
     * @param QuoteFactory $quoteFactory
     * @param LoyaltyHelper $loyaltyHelper
     */
    public function __construct(
        QuoteFactory $quoteFactory,
        LoyaltyHelper $loyaltyHelper
    )
    {
        $this->quoteFactory = $quoteFactory;
        $this->loyaltyHelper = $loyaltyHelper;
    }

    /**
     * @param \Magento\Sales\Model\Service\OrderService $subject
     * @param \Magento\Sales\Api\Data\OrderInterface $result
     *
     * @return \Magento\Sales\Api\Data\OrderInterface|Order
     */
    public function afterPlace(
        \Magento\Sales\Model\Service\OrderService $subject,
        \Magento\Sales\Api\Data\OrderInterface $result
    )
    {
        try {
            $quote = $this->quoteFactory->create()->load($result->getQuoteId());

            if (!empty($quote->getLoyaltyCoupons()) && empty($result->getLoyaltyCoupons())) {
                $baseLoyaltyCouponsAmount = $quote->getBaseLoyaltyCouponsAmount();
                $loyaltyCouponsAmount = $quote->getLoyaltyCouponsAmount();
                $result->setBaseLoyaltyCouponsAmount($baseLoyaltyCouponsAmount);
                $result->setLoyaltyCouponsAmount($loyaltyCouponsAmount);
                $result->setLoyaltyCoupons($quote->getLoyaltyCoupons());
            }
        } catch (\Exception $e) {
            $errorMsg = __class__ . ' : ' . $e->getMessage();
            $this->loyaltyHelper->insertToLoyaltyLogs(
                'Order', 'OrderAfterPlace', '', $errorMsg, $result->getQuoteId(),
                null, null
            );
        }
        return $result;
    }
}