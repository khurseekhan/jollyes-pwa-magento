<?php

namespace TwoJay\Loyalty\Plugin\Sales\Order\Email\Container;

use Magento\Checkout\Model\Session;
use TwoJay\Loyalty\Helper\Data;

/**
 * Class OrderIdentityPlugin
 * @package TwoJay\Loyalty\Plugin\Sales\Order\Email\Container
 */
class OrderIdentityPlugin
{
    /**
     * @var Session
     */
    protected $checkoutSession;

    /**
     * @var Data
     */
    protected $helper;

    /**
     * OrderIdentityPlugin constructor.
     * @param Session $checkoutSession
     * @param Data $helper
     */
    public function __construct(
        Session $checkoutSession,
        Data $helper
    )
    {
        $this->checkoutSession = $checkoutSession;
        $this->helper = $helper;
    }

    /**
     * @param \Magento\Sales\Model\Order\Email\Container\OrderIdentity $subject
     * @param callable $proceed
     * @return bool
     */
    public function aroundIsEnabled(\Magento\Sales\Model\Order\Email\Container\OrderIdentity $subject, callable $proceed)
    {
        if (!$this->helper->isModuleEnabled()) {
            return $proceed();
        }
        $returnValue = $proceed();

        $forceOrderMailSentOnSuccess = $this->checkoutSession->getForceOrderMailSentOnSuccess();
        if (isset($forceOrderMailSentOnSuccess) && $forceOrderMailSentOnSuccess) {
            if ($returnValue)
                $returnValue = false;
            else
                $returnValue = true;

            $this->checkoutSession->unsForceOrderMailSentOnSuccess();
        }

        return $returnValue;
    }
}