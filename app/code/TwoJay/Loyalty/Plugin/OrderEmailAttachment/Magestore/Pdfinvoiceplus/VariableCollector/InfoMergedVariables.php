<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */

namespace TwoJay\Loyalty\Plugin\OrderEmailAttachment\Magestore\Pdfinvoiceplus\VariableCollector;

/**
 * Class InfoMergedVariables
 * @package TwoJay\Loyalty\Plugin\OrderEmailAttachment\Magestore\Pdfinvoiceplus
 */
class InfoMergedVariables
{
    const COUPON_LABEL = 'PETClub Vouchers Amount[ %1 ]';
    const LEBEL_COLUMN = 'order_loyalty_coupons';

    /**
     * @var \TwoJay\Loyalty\Helper\Data
     */
    protected $helper;

    /**
     * CreditmemoDataImport constructor.
     * @param \TwoJay\Loyalty\Helper\Data $helper
     */
    public function __construct(
        \TwoJay\Loyalty\Helper\Data $helper
    )
    {
        $this->helper = $helper;
    }

    /**
     * @param \Magestore\Pdfinvoiceplus\Model\PdfTemplateRender\VariableCollector $subject
     * @param $result
     * @return mixed
     */
    public function afterGetInfoMergedVariables(
        \Magestore\Pdfinvoiceplus\Model\PdfTemplateRender\VariableCollector $subject,
        $result
    )
    {
        if (!$this->helper->isModuleEnabled()) {
            return $result;
        }

        foreach ($result as $ndx => $var) {
            if (isset($var[self::LEBEL_COLUMN]['value'])) {
                $loyaltyCouponLabel = '';
                if (!is_null($var[self::LEBEL_COLUMN]['value'])) {
                    $loyaltyCoupons = json_decode($var[self::LEBEL_COLUMN]['value'], true);
                    foreach ($loyaltyCoupons as $cndx => $couponData) {
                        $loyaltyCouponLabel .= $couponData['c'] . ', ';
                    }
                    $loyaltyCouponLabel = trim($loyaltyCouponLabel, ', ');
                }
                $result[$ndx][self::LEBEL_COLUMN]['value'] = __(self::COUPON_LABEL, $loyaltyCouponLabel);
            }
        }

        return $result;
    }
}