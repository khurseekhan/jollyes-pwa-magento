<?php
/**
 * TwoJay_Loyalty
 *
 * @category  TwoJay
 * @package   TwoJay_Loyalty
 * @author    TwoJay Development Team <support@twojay.co>
 * @copyright Copyright (c) 2019 TwoJay (http://www.twojay.co/)
 *
 */

namespace TwoJay\Loyalty\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\ResourceConnection;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Json\Helper\Data as JsonHelper;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\Module\Manager;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\Serialize\JsonValidator;

/**
 * Class Data
 * @package TwoJay\Loyalty\Helper
 */
class Data extends AbstractHelper
{
    /**
     * Maximal loyalty coupon code length according to database table definitions (longer codes are truncated)
     */
    const LOYALTY_COUPON_CODE_MAX_LENGTH = 255;

    /**
     * Instance of serializer.
     *
     * @var Json
     */
    private $serializer;

    /**
     * @var ResourceConnection
     */
    protected $_resource;

    /**
     * @var
     */
    protected $connection;

    /**
     * @var Session
     */
    protected $customerSession;

    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * Module name
     */
    const XPATH_FULL_MODULENAME_LOYALTY = 'TwoJay_Loyalty';

    /**
     * Enabled config path
     */
    const XPATH_LOYALTY_IS_ENABLED = 'loyalty/general/enabled';

    /**
     * Recipient email config path
     */
    const XML_PATH_EMAIL_RECIPIENT = 'loyalty/email/recipient_email';

    /**
     * Sender email config path
     */
    const XML_PATH_EMAIL_SENDER = 'loyalty/email/sender_email_identity';

    /**
     * Email template config path
     */
    const XML_PATH_EMAIL_TEMPLATE = 'loyalty/email/email_template';

    /**
     *  Custom Error Message config path
     */
    const XPATH_LOYALTY_API_MESSAGE = "loyalty/loyalty_messages/custom_message";

    /**
     *  Enable loyalty log debugging
     */
    const XPATH_LOYALTY_LOG_DEBUG = "loyalty/log_debug/enabled";


    /**
     * @var JsonHelper
     */
    protected $_jsonHelper;

    /**
     * @var Manager
     */
    protected $_moduleManager;

    /**
     * @var CustomerRepositoryInterface
     */
    protected $customerRepositoryInterface;

    /**
     * @var ObjectManagerInterface
     */
    protected $objectManagerInterface;

    /**
     * @var JsonValidator
     */
    private $jsonValidator;

    /**
     * Data constructor.
     * @param Context $context
     * @param Json|null $serializer
     * @param ResourceConnection $resource
     * @param Session $customerSession
     * @param ScopeConfigInterface $scopeConfig
     * @param JsonHelper $jsonHelper
     * @param Manager $moduleManager
     * @param CustomerRepositoryInterface $customerRepositoryInterface
     * @param ObjectManagerInterface $objectManagerInterface
     * @param JsonValidator|null $jsonValidator
     */
    public function __construct(
        Context $context,
        Json $serializer = null,
        ResourceConnection $resource,
        Session $customerSession,
        ScopeConfigInterface $scopeConfig,
        JsonHelper $jsonHelper,
        Manager $moduleManager,
        CustomerRepositoryInterface $customerRepositoryInterface,
        ObjectManagerInterface $objectManagerInterface,
        JsonValidator $jsonValidator
    )
    {
        parent::__construct($context);
        $this->objectManagerInterface = $objectManagerInterface;
        $this->serializer = $serializer ?: $this->objectManagerInterface->get(Json::class);
        $this->_resource = $resource;
        $this->customerSession = $customerSession;
        $this->scopeConfig = $scopeConfig;
        $this->_jsonHelper = $jsonHelper;
        $this->_moduleManager = $moduleManager;
        $this->customerRepositoryInterface = $customerRepositoryInterface;
        $this->jsonValidator = $jsonValidator;
    }

    /**
     * check if module if enabled or not
     *
     * @return bool
     */
    public function isModuleEnabled()
    {
        return $this->_moduleManager->isOutputEnabled(self::XPATH_FULL_MODULENAME_LOYALTY)
            && $this->scopeConfig->getValue(
                self::XPATH_LOYALTY_IS_ENABLED,
                ScopeInterface::SCOPE_STORE
            );
    }

    /**
     * Get Error message from configuration
     * @return string
     */
    public function getErrorMessage()
    {
        return $this->scopeConfig->getValue(
            self::XPATH_LOYALTY_API_MESSAGE,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * check if module log mode is enabled or not from configuration
     *
     * @return string
     */
    public function isLogEnabled()
    {
        return $this->scopeConfig->getValue(
            self::XPATH_LOYALTY_LOG_DEBUG,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * {@inheritdoc}
     */
    public function emailTemplate()
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_EMAIL_TEMPLATE,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * {@inheritdoc}
     */
    public function emailSender()
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_EMAIL_SENDER,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * {@inheritdoc}
     */
    public function emailRecipient()
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_EMAIL_RECIPIENT,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Unserialize and return Loyalty Coupons list from specified object
     *
     * @param \Magento\Framework\DataObject $from
     * @return mixed
     */
    public function getLoyaltyCoupons(\Magento\Framework\DataObject $from)
    {
        $value = $from->getLoyaltyCoupons();
        if (!$value) {
            return [];
        }

        return $this->serializer->unserialize($value);
    }

    /**
     * Serialize and set Loyalty Coupons list to specified object
     *
     * @param \Magento\Framework\DataObject $to
     * @param mixed $value
     * @return void
     */
    public function setLoyaltyCoupons(\Magento\Framework\DataObject $to, $value)
    {
        $serializedValue = $this->serializer->serialize($value);
        $to->setLoyaltyCoupons($serializedValue);
    }

    /**
     * Serialize specified object
     *
     * @param mixed $value
     * @return void
     */
    public function serializeData($value)
    {
        return $this->serializer->serialize($value);
    }

    /**
     * Serialize specified object
     *
     * @param mixed $value
     * @return void
     */
    public function unserializeData($value)
    {
        return $this->serializer->unserialize($value);
    }

    /**
     * @return mixed
     */
    public function getConnection()
    {
        if (!$this->connection) {
            $this->connection = $this->_resource->getConnection();
        }

        return $this->connection;
    }

    /**
     * Get customer ID and password_hash by email id
     *
     * @param $customerEmail
     * @return mixed
     */
    public function getCustomerDetails($customerEmail)
    {
        $table = $this->_resource->getTableName('customer_entity');
        $sql = $this->getConnection()->select()
            ->from(['ce' => $table], array('entity_id', 'password_hash', 'is_reset_password_sent'))
            ->where('email = ?', $customerEmail);
        return $this->getConnection()->fetchRow($sql);
    }

    /**
     * check whether customer is logged in or not
     *
     * @return bool
     */
    public function isCustomerLoggedIn()
    {
        if ($this->customerSession->isLoggedIn()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Checks if the given value is json encoded
     *
     * @param  $sValue
     * @return bool
     */
    public function isJson($sValue)
    {
        return $this->jsonValidator->isValid($sValue);
    }

    /**
     * Validate the API Response
     * @param $request
     * @param $email
     * @param $line
     * @return bool
     */
    public function isValidApiResponse($request, $email, $line)
    {

        if (isset($request['ErrorCode'])) {
            $errorResponseCodes = [401, 404, 403];// 404 - Customer Not Found
            if (in_array($request['ErrorCode'], $errorResponseCodes)) {
                if ($this->isLogEnabled()) {
                    $this->log(__class__ . "Line : " . $line . ': Response for : ' . $email . ' ErrorMessage: ' . $request['ErrorMessage']);
                }
                return false;
            }
        }

        return true;
    }

    /**
     * @param $message
     */
    public function log($message)
    {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/api_loyalty.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info($message);
    }

    /**
     * Inserts a new entry of log error to the loyalty error table 'loyalty_api_history'
     *
     * @param string $errorType
     * @param string $action
     * @param string $tokenUrl
     * @param $description
     * @param null $quoteId
     * @param null $orderId
     * @param null $customerId
     */
    public function insertToLoyaltyLogs(
        $errorType = '', $action = '', $tokenUrl = '', $description, $quoteId = null,
        $orderId = null, $customerId = null
    )
    {
        $connection = $this->getConnection();
        $bind = [
            'type' => (string)$errorType,
            'action' => (string)$action,
            'request_api' => (string)$tokenUrl,
            'description' => (string)$description,
            'quote_id' => (int)$quoteId,
            'order_id' => (int)$orderId,
            'customer_id' => (int)$customerId,
            'loyalty_flag' => 0,
            'created_at' => (string)date('Y-m-d H:i:s')
        ];
        $connection->insert('loyalty_api_history', $bind);
    }

    /**
     * Get loyalty number of customer
     *
     * @return string
     */
    public function getLoyaltyNumber($email = '')
    {
        try {
            $customerDetails = $this->getCustomerDetails($email);
            $customerId = isset($customerDetails['entity_id']) ? $customerDetails['entity_id'] : '';
            $loyaltyNumber = '';
            if ($customerId) {
                $customer = $this->customerRepositoryInterface->getById($customerId);
                if (!empty($customer->getCustomAttribute('loyalty_number'))) {
                    $loyaltyNumber = $customer->getCustomAttribute('loyalty_number')->getValue();
                }
            }
            return $loyaltyNumber;
        } catch (\Exception $e) {
            $this->log($e->getMessage());
        }
    }
}
