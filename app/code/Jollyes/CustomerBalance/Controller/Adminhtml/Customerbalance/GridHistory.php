<?php
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Jollyes\CustomerBalance\Controller\Adminhtml\Customerbalance;

class GridHistory extends \Jollyes\CustomerBalance\Controller\Adminhtml\Customerbalance
{
    /**
     * Customer balance grid
     *
     * @return void
     */
    public function execute()
    {
        $this->initCurrentCustomer();
        $this->_view->loadLayout();
        $this->getResponse()->setBody(
            $this->_view->getLayout()->createBlock(
                \Jollyes\CustomerBalance\Block\Adminhtml\Customer\Edit\Tab\Customerbalance\Balance\History\Grid::class
            )->toHtml()
        );
    }
}
