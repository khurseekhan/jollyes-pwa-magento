<?php
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Jollyes\CustomerBalance\Controller\Adminhtml\Customerbalance;

class Form extends \Jollyes\CustomerBalance\Controller\Adminhtml\Customerbalance
{
    /**
     * Customer balance form
     *
     * @return void
     */
    public function execute()
    {
        $this->initCurrentCustomer();
        $this->_view->loadLayout();
        $this->_view->renderLayout();
    }
}
