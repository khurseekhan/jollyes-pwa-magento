<?php
	namespace Jollyes\Webservices\Model;
		
	class InventoryStore extends \Magento\Framework\Model\AbstractModel
	{
		/**
			* Initialize resource model
			*
			* @return void
		*/
		
		public function _construct() {
			$this->_init('Jollyes\Webservices\Model\ResourceModel\InventoryStore');
		}
		
	}
?>
