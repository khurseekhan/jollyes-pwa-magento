<?php
namespace Jollyes\Webservices\Model;

class RequestQueue extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'request_queue';
    protected $_cacheTag = 'request_queue';

    protected $_eventPrefix = 'request_queue';
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }
    /**
    * Initialize resource model
    *
    * @return void
    */
    protected function _construct()
    {
        $this->_init('Jollyes\Webservices\Model\ResourceModel\RequestQueue');
    }
    
    /**
    * @param info of request queue  $QueueInfo
    * 
    * @return boolean
    */
    public function saveRequestQueue($QueueInfo)
    {
        if(!$QueueInfo) return false;
        
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $model = $objectManager->create('Jollyes\Webservices\Model\RequestQueue');
        $model->setData($QueueInfo);
		
        try {
            $model->save();
            return true;
		}
		catch (\Exception $e) {
		echo $e->getMessage();
            return false;
        }
    }
    
     /**
     * @param update process parameter  $updateProcess
     * 
     * @return boolean
     */
    public function updateProcessQueue($updateProcess = null)
    {
        if(!$updateProcess) return false;
        
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $model = $objectManager->create('Jollyes\Webservices\Model\RequestQueue');
        if($id = $updateProcess['id'])
        {
            $model->load($id);
            $model->setData($updateProcess);
        }
        try {
		    $model->save();
		    return true;
		}
		catch (\Exception $e) {
            return false;
        }         
    }
}
?>