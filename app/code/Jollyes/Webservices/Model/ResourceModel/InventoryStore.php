<?php
namespace Jollyes\Webservices\Model\ResourceModel;

class InventoryStore extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('jj_store_stock', 'id');
    }
}
?>