<?php

namespace Jollyes\Webservices\Model\ResourceModel\InventoryStore;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Jollyes\Webservices\Model\InventoryStore', 'Jollyes\Webservices\Model\ResourceModel\InventoryStore');
        $this->_map['fields']['page_id'] = 'main_table.page_id';
    }

}
?>