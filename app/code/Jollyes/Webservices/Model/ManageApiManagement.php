<?php
	
	namespace Jollyes\Webservices\Model;
	
	use Jollyes\Webservices\Api\ManageApiManagementInterface;
	
	class ManageApiManagement implements ManageApiManagementInterface
	{
		
		public $_availableService = [];
		
		/**
			* @var \Magento\Framework\Webapi\Rest\Request
		*/
		protected $request;
		
		/**
			* @var \Magento\Framework\Stdlib\DateTime\DateTime
		*/
		protected $date;
		
		/**
			* @var InventoryStoreFactory
		*/
		protected $inventoryStoreFactory;
		
		/**
			* @var ResourceModel\InventoryStore\CollectionFactory
		*/
		protected $inventoryStoreColFactory;
		
		/**
			* @var ResourceModel\InventoryStore
		*/
		protected $inventoryStoreResource;
		
		/**
			* @var \Jjcommerce\StoreLocator\Model\ResourceModel\Storelocator\CollectionFactory
		*/
		protected $storeLocatorColFactory;
		
		/**
			* @var \Magento\Catalog\Model\ProductFactory
		*/
		protected $productFactory;
		
		/**
			* @var \Magento\Catalog\Api\ProductRepositoryInterface
		*/
		protected $productRepository;
		
		/**
			* @var RequestQueueFactory
		*/
		protected $requestQueueFactory;
		
		/**
			* @var \Magento\CatalogInventory\Api\StockRegistryInterface
		*/
		public $stockRegistry;
		
		/**
			* ManageApiManagement constructor.
			* @param \Magento\Framework\Webapi\Rest\Request $request
			* @param \Magento\Framework\Stdlib\DateTime\DateTime $date
			* @param InventoryStoreFactory $inventoryStoreFactory
			* @param ResourceModel\InventoryStore\CollectionFactory $inventoryStoreColFactory
			* @param ResourceModel\InventoryStore $inventoryStoreResource
			* @param \Jjcommerce\StoreLocator\Model\ResourceModel\Storelocator\CollectionFactory $storeLocatorColFactory
			* @param \Magento\Catalog\Model\ProductFactory $productFactory
			* @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
			* @param RequestQueueFactory $requestQueueFactory
			* @param \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry
		*/
		public function __construct(
        \Magento\Framework\Webapi\Rest\Request $request,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Jollyes\Webservices\Model\InventoryStoreFactory $inventoryStoreFactory,
        \Jollyes\Webservices\Model\ResourceModel\InventoryStore\CollectionFactory $inventoryStoreColFactory,
        \Jollyes\Webservices\Model\ResourceModel\InventoryStore $inventoryStoreResource,
        \Jjcommerce\StoreLocator\Model\ResourceModel\Storelocator\CollectionFactory $storeLocatorColFactory,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Jollyes\Webservices\Model\RequestQueueFactory $requestQueueFactory,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry
		) {
			$this->request = $request;
			$this->date = $date;
			$this->_availableService = ['inventoryUpdates'];
			$this->inventoryStoreFactory = $inventoryStoreFactory;
			$this->inventoryStoreColFactory = $inventoryStoreColFactory;
			$this->inventoryStoreResource = $inventoryStoreResource;
			$this->storeLocatorColFactory = $storeLocatorColFactory;
			$this->productFactory = $productFactory;
			$this->productRepository = $productRepository;
			$this->requestQueueFactory = $requestQueueFactory;
			$this->stockRegistry= $stockRegistry;
		}
		
		/**
			* Save API data in request_queue table
			* @return string|void
		*/
		public function saveapidata()
		{
			try {
				error_reporting(0);
				libxml_use_internal_errors(true);
				
				$responseData = [];
				$postData = '';
				$xmlInventories = [];
				$requestData = $this->request->getBodyParams();
				$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
				if (!empty($requestData)) {
					$postData = json_encode($requestData);
				}
				
				if (!empty($requestData)) {
					
					if (!in_array($requestData['serviceName'], $this->_availableService)) {
						throw new \Exception('Servicename with ' . $requestData['serviceName'] . ' does not exist.');
					}
					
					if (!isset($requestData['requestID'])) {
						throw new \Exception('Request id is not found');
					}
					
					$requestId = $requestData['requestID'];
								
					$responseData['requestID'] = $requestId;
					$responseData['status'] = 'Ok';
					
					if ($requestData['serviceName'] == 'inventoryUpdates') {
						
						$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
						$logger = $objectManager->get('Jollyes\Webservices\Logger\Logger');
						
						$logger->info("Received- ".$requestId);
						
						/**
							* Inventory Update Starts
						*/
						
						$finalErrorList = [];
						$finalProcessList = [];
						$errorList = [];
					
					
						if(array_key_exists(0,$requestData['products']))
						{
							$xmlInventories = $requestData['products'];
						}
						else
						{
							$xmlInventories[] = $requestData['products'];
							
						}	 
					
						//looping all the inventories data
						foreach($xmlInventories as $key=>$_inventoryData){
						
							$sku = $_inventoryData['SKU'];
						
							$productId = $this->productFactory->create()->getIdBySku($sku);							
							if (empty($productId)) {
								$responseData['status'] = 'Error';
								$responseData['message'] = "Requested product doesn't exist";
								
								$errorList['requestId'] = $requestId;
								$errorList['SKU'] = $sku;
								$errorList['message'] = $sku . " Requested Sku Doesn't exist";
								$errorList['storeID'] = $requestDataArray['storeID'];
								$finalErrorList[] = $errorList;
							}
							$storeObj = $this->storeLocatorColFactory->create()
							->addFieldToFilter('store_code', $_inventoryData['storeID'])->getFirstItem();
							
							if ($storeObj->getShopId() == '') {
								$responseData['status'] = 'Error';
								$responseData['message'] = "Requested store doesn't exist";
							}
							
							if (!empty($productId) && $storeObj->getShopId()) {
								$result = $this->updateInventories($_inventoryData, $productId,$objectManager);
								$finalProcessList[] = $sku;
							}
						}
											
						//store encoded data in magento table start
						if (is_array($finalErrorList)) {
							$process['error_list'] = json_encode($finalErrorList);
						}
						if (is_array($finalProcessList)) {
							$process['processed_list'] = json_encode($finalProcessList);
						}
						
						//store encoded data in magento table end
						
						$process['request_id'] = $requestData['requestID'];
						$process['request_type'] = $requestData['serviceName'];
						$process['request_xml'] = $postData;
						$process['request_datetime'] = date('Y-m-d H:i:s');
						$process['created_at'] = date('Y-m-d H:i:s');
						$process['updated_at'] = date('Y-m-d H:i:s');
						
						$process['processed'] = 1;
						$process['processed_at'] = date('Y-m-d H:i:s');
						$process['acknowledgment'] = 1;
						$process['ack_datetime'] = date('Y-m-d H:i:s');
						
						$model = $this->requestQueueFactory->create();
						$model->saveRequestQueue($process);
						
						} else {
						
						if (isset($requestData['serviceName'])) {
							$apiRequestInfo['request_id'] = $requestData['requestID'];
							$apiRequestInfo['request_type'] = $requestData['serviceName'];
							$apiRequestInfo['request_xml'] = $postData;
							$apiRequestInfo['request_datetime'] = date('Y-m-d H:i:s');
							$apiRequestInfo['created_at'] = date('Y-m-d H:i:s');
							$apiRequestInfo['updated_at'] = date('Y-m-d H:i:s');
							
							$model = $this->requestQueueFactory->create();
							$requestSave = $model->saveRequestQueue($apiRequestInfo);
							
							if (!$requestSave) {
								$responseData['status'] = 'Error';
								$responseData['message'] = 'Sync Failed';
							}
						}
					}
				}
				} catch (\Exception $e) {
				if (!isset($requestData['serviceName'])) {
					$requestData['serviceName'] = 'Error';
				}
				$responseData['status'] = 'Error';
				$responseData['message'] = $e->getMessage();
			}
			
			/*$xmldata = '<' . $requestData['serviceName'] . '>';
				foreach ($responseData as $key => $value) {
				$xmldata .= '<' . $key . '>' . $value . '</' . $key . '>';
				}
			$xmldata .= '</' . $requestData['serviceName'] . '>';*/
			
			echo json_encode($responseData);
			die();
		}
		
		/**
			* @param $data
			* @param string $formatType
			* @return mixed|\SimpleXMLElement
			* @throws \Exception
		*/
		public function convertFormatToArray($data, $formatType = 'json')
		{
			
			if ($formatType == 'xml') {
				$xmlData = simplexml_load_string($data);
				
				if ($xmlData === false) {
					throw new \Exception('Invalid request data');
					return $xmlData;
				}
				$requestData = json_decode(json_encode((array)simplexml_load_string($data)), 1);
			}
			
			if ($formatType == 'json') {
				$requestData = json_decode($data, true);
				//$requestData = $jsondata['params'];
			}
			return $requestData;
		}
		
		/**
			* @param $inventoryData
			* @param $productId
			* @return bool
			* @throws \Magento\Framework\Exception\AlreadyExistsException
			* @throws \Magento\Framework\Exception\NoSuchEntityException
		*/
		public function updateInventories($inventoryData, $productId,$objectManager)
		{
			$storeObj = $this->storeLocatorColFactory->create()
			->addFieldToFilter('store_code', $inventoryData['storeID'])->getFirstItem();
			
			$resultFactory = $this->inventoryStoreColFactory->create()
			->addFieldToFilter('store_id', $storeObj->getShopId())
			->addFieldToFilter('product_id', $productId)
			->setOrder('id', 'DESC')->getLastItem();
			
			$storeRecID = $resultFactory->getId();
			
			$modelData = $this->inventoryStoreFactory->create();
			
			if (isset($storeRecID)) {
				$data = ['id' => $storeRecID, 'store_id' => $storeObj->getShopId(), 'product_id' => $productId, 'stock' => $inventoryData['qty']];
				} else {
				$data = ['id' => null, 'store_id' => $storeObj->getShopId(), 'product_id' => $productId, 'stock' => $inventoryData['qty']];
			}
			
			$modelData->setData($data);
			$this->inventoryStoreResource->save($modelData);
			
			$globalQty = $this->getStockQty($productId);
			$sku = $inventoryData['SKU'];
			
			$stockRegistry = $objectManager->create('Magento\CatalogInventory\Api\StockRegistryInterface');
			$stockItem = $stockRegistry->getStockItemBySku($sku);
			
			if ($globalQty != '') {
			$stockItem->setData('qty', $globalQty);
			
			if ($globalQty > 0) {
			$stockItem->setData('is_in_stock', 1);
			} else {
			$stockItem->setData('is_in_stock', 0);
			}
			
			$stockRegistry->updateStockItemBySku($sku, $stockItem);
			}
			
			return true;
			}
			
			/**
			* Retrieve stock qty whether product
			*
			* @param $productId
			* @return int|mixed
			* @throws \Magento\Framework\Exception\NoSuchEntityException
			*/
			public function getStockQty($productId)
			{
			$product = $this->productRepository->getById($productId);
			$data = $product->getStorelocator();
			
			$qty = 0;
			if (is_array($data) && !empty($data)):
			foreach ($data as $store => $stock):
			$qty = $stock + $qty;
			endforeach;
			endif;
			
			return $qty;
			}
			}
						