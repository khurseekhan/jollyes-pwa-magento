<?php
	namespace Jollyes\Webservices\Model;	
	use Magento\Catalog\Api\CategoryRepositoryInterface;
	
	class Service
	{
		protected $_storeManager;
				
		const MANAGE_PRODUCTS = 'productUpdates';
		
		const INVENTORY_UPDATES = 'inventoryUpdates';

			/**
			* Initialize resource model
			*
			* @return void
		*/		
		protected function _construct(\Magento\Store\Model\StoreManagerInterface $storeManager)
		{
			$this->_storeManager = $storeManager;					
		}
		
	}
?>