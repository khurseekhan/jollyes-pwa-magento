<?php
namespace Jollyes\Webservices\Plugin\Checkout\Model;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\OrderManagementInterface;
use Magento\Framework\Exception\LocalizedException;
use Jjcommerce\ProductSticker\Helper\Data as StickerHelper;

class OrderManagement
{
    /**
     * @var \Magento\Quote\Model\QuoteFactory
     */
    protected $quoteFactory;

    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var \Magento\Framework\App\ResponseFactory
     */
    private $resultRedirectFactory;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $_messageManager;

    protected $stickerHelper;

    /**
     * OrderManagement constructor.
     * @param \Magento\Quote\Model\QuoteFactory $quoteFactory
     * @param ProductRepositoryInterface $productRepository
     * @param \Magento\Framework\Controller\Result\RedirectFactory $resultRedirectFactory
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     */
    public function __construct(
        \Magento\Quote\Model\QuoteFactory $quoteFactory,
        ProductRepositoryInterface $productRepository,
        \Magento\Framework\Controller\Result\RedirectFactory $resultRedirectFactory,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        StickerHelper $stickerHelper
    )
    {
        $this->quoteFactory = $quoteFactory;
        $this->productRepository = $productRepository;
        $this->resultRedirectFactory = $resultRedirectFactory;
        $this->_messageManager = $messageManager;
        $this->stickerHelper = $stickerHelper;
    }

    /**
     * @param OrderManagementInterface $subject
     * @param OrderInterface $order
     * @throws LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function beforePlace(
        OrderManagementInterface $subject,
        OrderInterface $order
    )
    {
        $quote = $this->quoteFactory->create()->load($order->getQuoteId());
        $items = $quote->getAllVisibleItems();
        $productErrors = [];
        if($quote->getDeliveryType() == "collectByStore") {
            $store = json_decode($quote->getStorelocatorDescription(), true);
            /** @var \Magento\Quote\Model\Quote\Item $item */
            foreach ($items as $item) {
                $storeQty = $this->getStockQty($item->getSku(), $store['name']);
                if($storeQty < $item->getQty()) {
                    $productErrors[] = $item->getName();
                }
            }

        } else {
            /** @var \Magento\Quote\Model\Quote\Item $item */
            foreach ($items as $item) {
                $storeQty = $this->getStockQty($item->getSku());
                if($storeQty < $item->getQty()) {
                    $productErrors[] = $item->getName();
                }
            }
        }

        if ($productErrors) {
            $this->_messageManager->addError(__('Sorry the following products are not available in
                    the requested quantity so it cannot be added to your cart.<br>' . join('<br>', $productErrors)));
            throw new LocalizedException(__('Some products are not available in 
                    the requested quantity so it cannot be added to your cart.'));
        }
    }

    /**
     * Retrieve stock qty whether product
     *
     * @param $productSKU
     * @return int|mixed
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getStockQty($productSKU, $storeName = null)
    {
        $product = $this->productRepository->get($productSKU);

        $data = $product->getStorelocator();
        $qty = 0;
        if( is_array($data) && !empty( $data ) ):
            foreach( $data as $store => $stock ):
                if($storeName) {
                    $name = strtolower(str_replace( " ", "_", $storeName));
                    if($name == $store) {
                        $qty = $stock;
                    }
                } else {
                    if($store == $this->stickerHelper->getWarehouseName()) {
                        $qty = $stock;
                    }
                }
            endforeach;
        endif;

        return $qty;
    }
}