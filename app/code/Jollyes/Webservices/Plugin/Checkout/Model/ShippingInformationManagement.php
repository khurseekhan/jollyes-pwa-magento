<?php
namespace Jollyes\Webservices\Plugin\Checkout\Model;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Exception\LocalizedException;
use Jjcommerce\ProductSticker\Helper\Data as StickerHelper;

class ShippingInformationManagement
{
    /**
     * @var \Magento\Quote\Model\QuoteFactory
     */
    protected $quoteFactory;

    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var \Magento\Framework\App\ResponseFactory
     */
    private $resultRedirectFactory;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $_messageManager;

    /**
     * @var StickerHelper
     */
    protected $stickerHelper;

    /**
     * ShippingInformationManagement constructor.
     * @param \Magento\Quote\Model\QuoteFactory $quoteFactory
     * @param ProductRepositoryInterface $productRepository
     * @param \Magento\Framework\Controller\Result\RedirectFactory $resultRedirectFactory
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     */
    public function __construct(
        \Magento\Quote\Model\QuoteFactory $quoteFactory,
        ProductRepositoryInterface $productRepository,
        \Magento\Framework\Controller\Result\RedirectFactory $resultRedirectFactory,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        StickerHelper $stickerHelper
    )
    {
        $this->quoteFactory = $quoteFactory;
        $this->productRepository = $productRepository;
        $this->resultRedirectFactory = $resultRedirectFactory;
        $this->stickerHelper = $stickerHelper;
        $this->_messageManager = $messageManager;
    }

    /**
     * @param \Magento\Checkout\Model\ShippingInformationManagement $subject
     * @param $cartId
     * @param \Magento\Checkout\Api\Data\ShippingInformationInterface $addressInformation
     * @throws LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function beforeSaveAddressInformation(
        \Magento\Checkout\Model\ShippingInformationManagement $subject,
        $cartId,
        \Magento\Checkout\Api\Data\ShippingInformationInterface $addressInformation
    )
    {
        $quote = $this->quoteFactory->create()->load($cartId);
        $items = $quote->getAllVisibleItems();
        $productErrors = [];

        if($addressInformation->getShippingCarrierCode() == "clickandcollect") {
            $store = json_decode($quote->getStorelocatorDescription(), true);
            /** @var \Magento\Quote\Model\Quote\Item $item */
            foreach ($items as $item) {
                $storeQty = $this->getStockQty($item->getSku(), $store['name']);
                if($storeQty < $item->getQty()) {
                    $productErrors[] = $item->getName();
                }
            }

        } else {
            /** @var \Magento\Quote\Model\Quote\Item $item */
            foreach ($items as $item) {
                $storeQty = $this->getStockQty($item->getSku());
                if($storeQty < $item->getQty()) {
                    $productErrors[] = $item->getName();
                }
            }
        }
    
        if($addressInformation->getShippingCarrierCode() == "clickandcollect" && $quote->getStorelocatorId() <= 0){
            $this->_messageManager->addError(__('Please select shipping method.'));
            throw new LocalizedException(__('Please select shipping method.'));
        }
        
        if ($productErrors) {
            $this->_messageManager->addError(__('Sorry the following products are not available in
                    the requested quantity so it cannot be added to your cart.<br>' . join('<br>', $productErrors)));
            throw new LocalizedException(__('Some products are not available in 
                    the requested quantity so it cannot be added to your cart.'));
        }
    }

    /**
     * Retrieve stock qty whether product
     *
     * @param $productSKU
     * @return int|mixed
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getStockQty($productSKU, $storeName = null)
    {
        $product = $this->productRepository->get($productSKU);
        
        $data = $product->getStorelocator();
        $qty = 0;
        if( is_array($data) && !empty( $data ) ):
            foreach( $data as $store => $stock ):
                if($storeName) {
                    $name = strtolower(str_replace( " ", "_", $storeName));
                    if($name == $store) {
                        $qty = $stock;
                    }
                } else {
                    if($store == $this->stickerHelper->getWarehouseName()) {
                        $qty = $stock;
                    }
                }
            endforeach;
        endif;

        return $qty;
    }
}