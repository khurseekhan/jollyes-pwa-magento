<?php
namespace Jollyes\Webservices\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\ProductFactory;
use Jollyes\Webservices\Model\InventoryStoreFactory;
use Jjcommerce\StoreLocator\Model\StorelocatorFactory;

class Order implements ObserverInterface
{

    /**
     * @var \Magento\Sales\Api\OrderRepositoryInterface
     */
    protected $orderRepository;

    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var ProductFactory
     */
    protected $productFactory;

    /**
     * @var InventoryStoreFactory
     */
    protected $inventoryStoreFactory;

    /**
     * @var StorelocatorFactory
     */
    protected $storelocatorFactory;

    /**
     * @var string
     */
    protected $storeCode = "33960";

    /**
     * Order constructor.
     * @param \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
     * @param ProductRepositoryInterface $productRepository
     * @param ProductFactory $productFactory
     * @param InventoryStoreFactory $inventoryStoreFactory
     */
    public function __construct(
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        ProductRepositoryInterface $productRepository,
        ProductFactory $productFactory,
        InventoryStoreFactory $inventoryStoreFactory,
        StorelocatorFactory $storelocatorFactory
    ){
        $this->orderRepository = $orderRepository;
        $this->productRepository = $productRepository;
        $this->productFactory = $productFactory;
        $this->inventoryStoreFactory = $inventoryStoreFactory;
        $this->storelocatorFactory = $storelocatorFactory;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $orderData = $observer->getEvent()->getOrder();
        $orderId = $orderData->getId();
        $order = $this->orderRepository->get($orderId);
        $model = $this->inventoryStoreFactory->create();
        $storeLocatorModel = $this->storelocatorFactory->create();
        $storeLocatorId = null;
        if($order->getShippingMethod() == "clickandcollect_clickandcollect") {
            $storeLocatorId = $order->getStorelocatorId();
        } else {
            $resultFactory = $storeLocatorModel->getCollection()
                ->addFieldToFilter('store_code', $this->storeCode)
                ->getFirstItem();
            $storeLocatorId = $resultFactory->getShopId();
        }

        foreach ($order->getAllItems() as $item)
        {
            if($item->getProductType() != "configurable"){  // for configurable product e not need to decrease qty

                $resultFactory = $model->getCollection()
                ->addFieldToFilter('store_id', $storeLocatorId)
                ->addFieldToFilter('product_id', $item->getProductId())
                ->setOrder('id', 'DESC')
                ->getLastItem();

                $storeRecID = $resultFactory->getId();
                if(isset($storeRecID)) {
                    $data = array(
                        'id'=> $storeRecID,
                        'store_id' => $storeLocatorId,
                        'product_id' => $item->getProductId(),
                        'stock'=> $resultFactory->getStock() - $item->getQtyOrdered()
                    );
                    $model->setData($data);
                    $model->save();
                }

                $globalQty = $this->getStockQty($item->getProductId(), $item->getQtyOrdered());
                $product = $this->productFactory->create()->load($item->getProductId());
                $product->setStockData(['qty' => $globalQty, 'is_in_stock' => 1, 'manage_stock' => 1]);
                $product->save();

            }
        }
    }

    /**
     * Retrieve stock qty whether product
     *
     * @param $productId
     * @return int|mixed
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getStockQty($productId, $qtyOrder)
    {
        $product = $this->productRepository->getById($productId);
        $data = $product->getStorelocator();
        $qty = 0;
        if( is_array($data) && !empty( $data ) ):
            foreach( $data as $store => $stock ):
                $qty = $stock + $qty;
            endforeach;
        endif;

        return $qty - $qtyOrder;
    }

}