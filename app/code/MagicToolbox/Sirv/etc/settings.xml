<?xml version="1.0"?>
<!--
/**
 * Module settings
 *
 * @author    Sirv Limited <support@sirv.com>
 * @copyright Copyright (c) 2018-2020 Sirv Limited <support@sirv.com>. All rights reserved
 * @license   https://sirv.com/
 * @link      https://sirv.com/integration/magento/
 */
-->
<settings>
    <notice><![CDATA[Accelerate your website with faster loading images. <a href="https://sirv.com" target="_blank">About Sirv CDN</a> | <a href="https://my.sirv.com/#/contact/" target="_blank">Get support</a>]]></notice>
    <group id="user">
        <label>Sirv account</label>
        <fields>
            <field>
                <name>account_exists</name>
                <label>I already have a Sirv account</label>
                <type>radios</type>
                <type_class>\MagicToolbox\Sirv\Block\Adminhtml\Settings\Edit\Form\Element\Radios\NewAccount</type_class>
                <options>
                    <option>
                        <label>Yes</label>
                        <value>yes</value>
                    </option>
                    <option>
                        <label>No</label>
                        <value>no</value>
                    </option>
                </options>
                <value>yes</value>
            </field>
            <field>
                <name>email</name>
                <label>Email</label>
                <type>email</type>
                <type_class>\MagicToolbox\Sirv\Block\Adminhtml\Settings\Edit\Form\Element\Email</type_class>
                <placeholder>Sirv account email</placeholder>
                <autocomplete>off</autocomplete>
                <required>true</required>
                <autofocus/>
            </field>
            <field>
                <name>password</name>
                <label>Password</label>
                <type>password</type>
                <type_class>\MagicToolbox\Sirv\Block\Adminhtml\Settings\Edit\Form\Element\Password</type_class>
                <placeholder>Sirv account password</placeholder>
                <autocomplete>new-password</autocomplete>
                <required>true</required>
            </field>
            <field>
                <name>first_and_last_name</name>
                <label>First and last name</label>
                <type>text</type>
                <type_class>\MagicToolbox\Sirv\Block\Adminhtml\Settings\Edit\Form\Element\Text</type_class>
                <placeholder>Your first and last name</placeholder>
                <value></value>
                <required>true</required>
            </field>
            <field>
                <name>alias</name>
                <label>Account name</label>
                <notice><![CDATA[Choose a name that suits your business. At least 6 characters, may include letters, numbers and hyphens.]]></notice>
                <type>text</type>
                <type_class>\MagicToolbox\Sirv\Block\Adminhtml\Settings\Edit\Form\Element\Text</type_class>
                <placeholder>Choose a name</placeholder>
                <value></value>
                <required>true</required>
            </field>
            <field>
                <name>account</name>
                <label>Account</label>
                <type>select</type>
                <type_class>\MagicToolbox\Sirv\Block\Adminhtml\Settings\Edit\Form\Element\Select</type_class>
                <options></options>
                <value></value>
                <placeholder>Select Sirv account</placeholder>
                <required>true</required>
            </field>
            <field>
                <name>connect</name>
                <label></label>
                <type>button</type>
                <type_class>\MagicToolbox\Sirv\Block\Adminhtml\Settings\Edit\Form\Element\Button</type_class>
                <value>Connect account</value>
                <title>Connect account</title>
            </field>
            <field>
                <name>register</name>
                <label></label>
                <notice><![CDATA[No credit card needed.<br/>Enjoy free 5GB storage & 20GB transfer for 30 days. Then choose a <a href="https://sirv.com/pricing/" target="_blank">free or paid plan</a>.<br/>By signing up, you agree to our <a href="https://sirv.com/terms/" target="_blank">Terms of Service</a>.<br/>]]></notice>
                <type>button</type>
                <type_class>\MagicToolbox\Sirv\Block\Adminhtml\Settings\Edit\Form\Element\Button</type_class>
                <value>Get started</value>
                <title>Get started</title>
            </field>
        </fields>
    </group>
    <group id="general">
        <label>General settings</label>
        <fields>
            <field>
                <name>enabled</name>
                <label>Enable Sirv</label>
                <tooltip><![CDATA[Enable Sirv to automatically copy your media gallery to Sirv. Files will stay in sync and be optimised and served by Sirv CDN.]]></tooltip>
                <type>radios</type>
                <type_class>\MagicToolbox\Sirv\Block\Adminhtml\Settings\Edit\Form\Element\Radios</type_class>
                <options>
                    <option>
                        <label>Yes</label>
                        <value>true</value>
                    </option>
                    <option>
                        <label>No</label>
                        <value>false</value>
                    </option>
                </options>
                <value>false</value>
                <install/>
            </field>
            <field>
                <name>network</name>
                <label>Network</label>
                <tooltip><![CDATA[Sirv CDN will rapidly serve files from Sirv's global network. Sirv Direct is better while testing - it will serve all files from Sirv's primary datacentre in Germany.]]></tooltip>
                <type>radios</type>
                <type_class>\MagicToolbox\Sirv\Block\Adminhtml\Settings\Edit\Form\Element\Radios</type_class>
                <options>
                    <option>
                        <label>Sirv CDN</label>
                        <value>cdn</value>
                    </option>
                    <option>
                        <label>Sirv Direct</label>
                        <value>direct</value>
                    </option>
                </options>
                <value>cdn</value>
                <install/>
            </field>
            <field>
                <name>image_folder</name>
                <label>Folder name on Sirv</label>
                <tooltip><![CDATA[Images will be copied here in <a target="_blank" href="https://my.sirv.com/#/browse/">your Sirv account</a>. The folder will be automatically created by Sirv.]]></tooltip>
                <type>text</type>
                <type_class>\MagicToolbox\Sirv\Block\Adminhtml\Settings\Edit\Form\Element\Folder</type_class>
                <value>magento</value>
                <install/>
            </field>
            <field>
                <name>profile</name>
                <label>Image profile</label>
                <tooltip><![CDATA[Choose one of your <a target="_blank" href="https://my.sirv.com/#/profiles/">profiles</a> if you wish to apply some settings to all images (e.g. watermarks, text overlays, effects). <a target="_blank" href="https://sirv.com/help/articles/dynamic-imaging/profiles/">Learn more</a>.]]></tooltip>
                <type>select</type>
                <type_class>\MagicToolbox\Sirv\Block\Adminhtml\Settings\Edit\Form\Element\Select</type_class>
                <options>
                    <option>
                        <label>Default</label>
                        <value>Default</value>
                    </option>
                </options>
                <value>Default</value>
                <install/>
            </field>
            <field>
                <name>image_quality</name>
                <label>Image quality</label>
                <tooltip><![CDATA[JPEG image quality. The default JPEG image quality is set on your <a target="_blank" href="{{URL}}">Magento Stores Configuration page</a>.]]></tooltip>
                <type>select</type>
                <type_class>\MagicToolbox\Sirv\Block\Adminhtml\Settings\Edit\Form\Element\Select</type_class>
                <options>
                    <option>
                        <label>Default (Magento default)</label>
                        <value>0</value>
                    </option>
                </options>
                <value>0</value>
            </field>
            <field>
                <name>magento_watermark</name>
                <label>Use Magento watermark</label>
                <type>radios</type>
                <type_class>\MagicToolbox\Sirv\Block\Adminhtml\Settings\Edit\Form\Element\Radios</type_class>
                <options>
                    <option>
                        <label>Yes</label>
                        <value>true</value>
                    </option>
                    <option>
                        <label>No</label>
                        <value>false</value>
                    </option>
                </options>
                <value>true</value>
                <install/>
            </field>
            <field>
                <name>auto_fetch</name>
                <label>Serve static files</label>
                <notice><![CDATA[Sirv will fetch files from this domain:]]></notice>
                <tooltip><![CDATA[Sirv CDN can serve your JS, CSS and static theme files too. It minifies JS & CSS and uses multiplexing to accelerate file loading. <a target="_blank" href="https://sirv.com/help/articles/magento-cdn-sirv-extension/#how-to-set-up-a-cdn-in-magento-2">Learn more</a>.]]></tooltip>
                <type>radios</type>
                <type_class>\MagicToolbox\Sirv\Block\Adminhtml\Settings\Edit\Form\Element\Radios</type_class>
                <options>
                    <option>
                        <label>JS, CSS &amp; associated files (recommended)</label>
                        <value>custom</value>
                    </option>
                    <option>
                        <label>JS, CSS &amp; all static files</label>
                        <value>all</value>
                    </option>
                    <option>
                        <label>None</label>
                        <value>none</value>
                    </option>
                </options>
                <value>none</value>
            </field>
            <field>
                <name>url_prefix</name>
                <label></label>
                <notice></notice>
                <type>select</type>
                <type_class>\MagicToolbox\Sirv\Block\Adminhtml\Settings\Edit\Form\Element\Select</type_class>
                <options></options>
                <placeholder></placeholder>
                <can_hide_select/>
            </field>
        </fields>
    </group>
    <group id="sirv_media_viewer">
        <label>Product page media gallery</label>
        <fields>
            <field>
                <name>product_gallery_view</name>
                <label>Product media gallery</label>
                <tooltip><![CDATA[Replace your product media gallery viewer with <a target="_blank" href="https://sirv.com/help/articles/sirv-media-viewer/">Sirv Media Viewer</a>.]]></tooltip>
                <type>radios</type>
                <type_class>\MagicToolbox\Sirv\Block\Adminhtml\Settings\Edit\Form\Element\Radios\ProductGalleryView</type_class>
                <options>
                    <option>
                        <label>Original</label>
                        <value>original</value>
                    </option>
                    <option>
                        <label>Sirv Media Viewer</label>
                        <value>smv</value>
                    </option>
                </options>
                <value>original</value>
                <install/>
            </field>
            <field>
                <name>viewer_contents</name>
                <label>Order of content</label>
                <type>radios</type>
                <type_class>\MagicToolbox\Sirv\Block\Adminhtml\Settings\Edit\Form\Element\Radios</type_class>
                <options>
                    <option>
                        <label>Magento images &amp; videos</label>
                        <value>1</value>
                    </option>
                    <option>
                        <label>Magento images &amp; videos, then Sirv assets</label>
                        <value>2</value>
                    </option>
                    <option>
                        <label>Sirv assets, then Magento images &amp; videos</label>
                        <value>3</value>
                    </option>
                    <option>
                        <label>Sirv assets only</label>
                        <value>4</value>
                    </option>
                </options>
                <value>1</value>
                <install/>
            </field>
            <field>
                <name>product_assets_folder</name>
                <label>Product folder names on Sirv</label>
                <tooltip><![CDATA[Location of product assets for <b>Sirv Media Viewer</b>.<br/>Placeholders <b>{product-sku}</b> or <b>{product-id}</b> can be used here.<br/>e.g. products/{product-sku} <a target="_blank" href="https://sirv.com/help/articles/magento-cdn-sirv-extension/#gallery/">Learn more</a>.]]></tooltip>
                <notice><![CDATA[Folder structure on Sirv e.g. products/{product-sku}]]></notice>
                <type>text</type>
                <type_class>\MagicToolbox\Sirv\Block\Adminhtml\Settings\Edit\Form\Element\Folder</type_class>
                <value></value>
                <install/>
            </field>
            <field>
                <name>smv_js_options</name>
                <label>Sirv Media Viewer options</label>
                <notice><![CDATA[<a target="_blank" href="https://sirv.com/help/viewer/">Choose your options here</a>, then copy and paste the Script tab content.]]></notice>
                <type>textarea</type>
                <type_class>\MagicToolbox\Sirv\Block\Adminhtml\Settings\Edit\Form\Element\Textarea</type_class>
                <value>SirvOptions = {&#xa;  viewer: {&#xa;    zoom: {&#xa;      mode: 'inner',&#xa;    }&#xa;  }&#xa;}</value>
            </field>
            <field>
                <name>smv_max_height</name>
                <label>Max height</label>
                <type>text</type>
                <type_class>\MagicToolbox\Sirv\Block\Adminhtml\Settings\Edit\Form\Element\Text</type_class>
                <tooltip><![CDATA[Maximum height of thumbnail (in pixels). Empty to disable.]]></tooltip>
                <value></value>
            </field>
            <field>
                <name>assets-cache</name>
                <label>Flush Sirv content cache</label>
                <type>button</type>
                <type_class>\MagicToolbox\Sirv\Block\Adminhtml\Settings\Edit\Form\Element\MultiSwitchButton\FlushAssets</type_class>
            </field>
        </fields>
    </group>
    <group id="synchronization">
        <label>Synchronization</label>
        <fields>
            <field>
                <label>Status</label>
                <name>synchronizer</name>
                <type>synchronizer</type>
                <type_class>\MagicToolbox\Sirv\Block\Adminhtml\Settings\Edit\Form\Element\Synchronizer</type_class>
            </field>
            <field>
                <name>urls-cache</name>
                <label>Flush product image cache</label>
                <type>button</type>
                <type_class>\MagicToolbox\Sirv\Block\Adminhtml\Settings\Edit\Form\Element\MultiSwitchButton\FlushUrls</type_class>
            </field>
        </fields>
    </group>
    <group id="usage">
        <label></label>
        <fields>
            <field>
                <name>usage</name>
                <type>usage</type>
                <type_class>\MagicToolbox\Sirv\Block\Adminhtml\Settings\Edit\Form\Element\Usage</type_class>
            </field>
        </fields>
    </group>
    <group id="support">
        <label>Support</label>
        <fields>
            <field>
                <name>join_with_mysql</name>
                <label>Join data with MySQL</label>
                <type>radios</type>
                <type_class>\MagicToolbox\Sirv\Block\Adminhtml\Settings\Edit\Form\Element\Radios</type_class>
                <options>
                    <option>
                        <label>Yes</label>
                        <value>true</value>
                    </option>
                    <option>
                        <label>No</label>
                        <value>false</value>
                    </option>
                </options>
                <value>false</value>
            </field>
        </fields>
    </group>
</settings>
