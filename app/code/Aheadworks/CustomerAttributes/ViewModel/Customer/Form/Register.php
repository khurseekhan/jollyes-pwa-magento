<?php
/**
 * Aheadworks Inc.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://ecommerce.aheadworks.com/end-user-license-agreement/
 *
 * @package    CustomerAttributes
 * @version    1.0.3
 * @copyright  Copyright (c) 2020 Aheadworks Inc. (http://www.aheadworks.com)
 * @license    https://ecommerce.aheadworks.com/end-user-license-agreement/
 */

namespace Aheadworks\CustomerAttributes\ViewModel\Customer\Form;

use Aheadworks\CustomerAttributes\Model\Source\Attribute\UsedInForms;
use Magento\Framework\View\Element\Block\ArgumentInterface;

/**
 * Class Register
 * @package Aheadworks\CustomerAttributes\ViewModel\Customer\Form
 */
class Register extends AbstractView implements ArgumentInterface
{
    /**
     * @var string
     */
    protected $formCode = UsedInForms::CUSTOMER_ACCOUNT_CREATE;

    /**
     * {@inheritDoc}
     */
    protected function getValue($attribute)
    {
        return $attribute->getDefaultValue();
    }
}
