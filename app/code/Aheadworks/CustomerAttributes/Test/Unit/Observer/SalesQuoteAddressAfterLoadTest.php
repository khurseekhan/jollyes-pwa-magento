<?php
/**
 * Aheadworks Inc.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://ecommerce.aheadworks.com/end-user-license-agreement/
 *
 * @package    CustomerAttributes
 * @version    1.0.3
 * @copyright  Copyright (c) 2020 Aheadworks Inc. (http://www.aheadworks.com)
 * @license    https://ecommerce.aheadworks.com/end-user-license-agreement/
 */

namespace Aheadworks\CustomerAttributes\Test\Unit\Observer;

use Aheadworks\CustomerAttributes\Observer\SalesQuoteAddressAfterLoad;
use Magento\Framework\Event;
use Magento\Framework\Event\Observer;
use Magento\Framework\Model\AbstractModel;
use PHPUnit\Framework\TestCase;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use Aheadworks\CustomerAttributes\Model\Sales\AttributesData\QuoteAddressPersistor;

/**
 * Class SalesQuoteAddressAfterLoadTest
 * @package Aheadworks\CustomerAttributes\Test\Unit\Observer
 */
class SalesQuoteAddressAfterLoadTest extends TestCase
{
    /**
     * @var QuoteAddressPersistor|\PHPUnit_Framework_MockObject_MockObject
     */
    private $persistorMock;

    /**
     * @var SalesQuoteAddressAfterLoad
     */
    private $observer;

    /**
     * Init mocks for tests
     *
     * @return void
     */
    public function setUp()
    {
        $objectManager = new ObjectManager($this);
        $this->persistorMock = $this->createMock(QuoteAddressPersistor::class);
        $this->observer = $objectManager->getObject(
            SalesQuoteAddressAfterLoad::class,
            [
                'quoteAddressPersistor' => $this->persistorMock
            ]
        );
    }

    /**
     * Test execute method
     */
    public function testExecute()
    {
        $eventMock = $this->createMock(Event::class);
        $observerMock = $this->createConfiguredMock(Observer::class, ['getEvent' => $eventMock]);
        $addressMock = $this->createMock(AbstractModel::class);

        $eventMock->expects($this->once())
            ->method('__call')
            ->with('getQuoteAddress')
            ->willReturn($addressMock);
        $this->persistorMock->expects($this->once())
            ->method('load')
            ->with($addressMock);

        $this->observer->execute($observerMock);
    }
}
