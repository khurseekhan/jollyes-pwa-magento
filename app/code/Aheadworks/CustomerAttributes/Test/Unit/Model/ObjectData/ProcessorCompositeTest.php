<?php
/**
 * Aheadworks Inc.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://ecommerce.aheadworks.com/end-user-license-agreement/
 *
 * @package    CustomerAttributes
 * @version    1.0.3
 * @copyright  Copyright (c) 2020 Aheadworks Inc. (http://www.aheadworks.com)
 * @license    https://ecommerce.aheadworks.com/end-user-license-agreement/
 */

namespace Aheadworks\CustomerAttributes\Test\Unit\Model\ObjectData;

use Aheadworks\CustomerAttributes\Model\ObjectData\ProcessorComposite;
use Aheadworks\CustomerAttributes\Model\ObjectData\ProcessorInterface;
use Magento\Framework\DataObject;
use PHPUnit\Framework\TestCase;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;

/**
 * Class ProcessorCompositeTest
 * @package Aheadworks\CustomerAttributes\Test\Unit\Model\ObjectData
 */
class ProcessorCompositeTest extends TestCase
{
    /**
     * @var ProcessorInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $realProcessorMock;

    /**
     * @var ProcessorComposite
     */
    private $processor;

    /**
     * Init mocks for tests
     *
     * @return void
     */
    public function setUp()
    {
        $objectManager = new ObjectManager($this);
        $this->realProcessorMock = $this->createMock(ProcessorInterface::class);
        $processors = [
            $this->realProcessorMock,
            new DataObject()
        ];
        $this->processor = $objectManager->getObject(
            ProcessorComposite::class,
            [
                'processors' => $processors
            ]
        );
    }

    /**
     * Test prepareDataBeforeSave method
     */
    public function testPrepareDataBeforeSave()
    {
        $objectMock = $this->createMock(DataObject::class);

        $this->realProcessorMock->expects($this->once())
            ->method('beforeSave')
            ->with($objectMock);

        $this->assertSame($objectMock, $this->processor->prepareDataBeforeSave($objectMock));
    }

    /**
     * Test prepareDataAfterLoad method
     */
    public function testPrepareDataAfterLoad()
    {
        $objectMock = $this->createMock(DataObject::class);

        $this->realProcessorMock->expects($this->once())
            ->method('afterLoad')
            ->with($objectMock);

        $this->assertSame($objectMock, $this->processor->prepareDataAfterLoad($objectMock));
    }
}
