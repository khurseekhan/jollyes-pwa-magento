define(
    ['underscore'],
    function (_) {
        'use strict';

        return function (Component) {
            return Component.extend({

                /**
                 * @inheritDoc
                 */
                createRendererComponent: function (address) {
                    this._super(_.omit(address, 'customAttributes'));
                }
            });
        }
    }
);
