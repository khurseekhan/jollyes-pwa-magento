define([
    'underscore',
    'ko',
    'Magento_Checkout/js/model/quote'
    ], function (_, ko, quote) {
        'use strict';

        return function (Component) {
            return Component.extend({
                currentBillingAddress: ko.observable(quote.billingAddress()),

                /**
                 * @inheritDoc
                 */
                initialize: function () {
                    var self = this;

                    this._super();
                    quote.billingAddress.subscribe(function (address) {
                        self.currentBillingAddress(_.omit(address, 'customAttributes'));
                    });
                }
            });
        }
    }
);
