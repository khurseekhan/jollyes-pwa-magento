var config = {
    map: {
        '*': {
            awCustAttrRelationManager: 'Aheadworks_CustomerAttributes/js/relation-manager',
            awUiCustAttrRelationManager: 'Aheadworks_CustomerAttributes/js/ui-relation-manager'
        }
    }
};
