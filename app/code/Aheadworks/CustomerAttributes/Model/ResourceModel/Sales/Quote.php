<?php
/**
 * Aheadworks Inc.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://ecommerce.aheadworks.com/end-user-license-agreement/
 *
 * @package    CustomerAttributes
 * @version    1.0.3
 * @copyright  Copyright (c) 2020 Aheadworks Inc. (http://www.aheadworks.com)
 * @license    https://ecommerce.aheadworks.com/end-user-license-agreement/
 */

namespace Aheadworks\CustomerAttributes\Model\ResourceModel\Sales;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Aheadworks\CustomerAttributes\Model\ResourceModel\Attribute;

/**
 * Class Quote
 * @package Aheadworks\CustomerAttributes\Model\ResourceModel\Sales
 */
class Quote extends AbstractDb
{
    /**
     * {@inheritDoc}
     */
    protected $_isPkAutoIncrement = false;

    /**
     * {@inheritDoc}
     */
    protected function _construct()
    {
        $this->_init(Attribute::QUOTE_ATTRIBUTE_TABLE_NAME, Attribute::QUOTE_ID);
    }
}
