<?php
/**
 * Aheadworks Inc.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://ecommerce.aheadworks.com/end-user-license-agreement/
 *
 * @package    CustomerAttributes
 * @version    1.0.3
 * @copyright  Copyright (c) 2020 Aheadworks Inc. (http://www.aheadworks.com)
 * @license    https://ecommerce.aheadworks.com/end-user-license-agreement/
 */

namespace Aheadworks\CustomerAttributes\Block\Attribute\Renderer;

use Magento\Framework\Data\Form\Element\Textarea as FrameworkTextarea;

/**
 * Class Textarea
 * @package Aheadworks\CustomerAttributes\Block\Attribute\Renderer
 */
class Textarea extends FrameworkTextarea
{
    /**
     * {@inheritDoc}
     */
    public function getDefaultHtml()
    {
        $html = '<div class="' . $this->getClass() . '">' . "\n";
        $html .= $this->getLabelHtml();
        $html .= $this->getElementHtml();
        $html .= '</div>' . "\n";

        return $html;
    }
}
